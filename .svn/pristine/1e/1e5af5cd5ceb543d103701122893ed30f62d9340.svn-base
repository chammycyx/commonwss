package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

@Entity
@Table(name = "cid_hospital_application")
public class HospitalApplication {
	@EmbeddedId
	private HospitalApplicationPk hospitalApplicationPk;

	@Column(name = "application_enabled", nullable = false)
	private Character applicationEnabled;
	
	@ManyToOne(optional = false)
	@JoinColumns({
		@JoinColumn (name = "application_id", referencedColumnName = "application_id"),
		@JoinColumn (name = "application_version", referencedColumnName = "application_version")
	})
	private VersionControl versionControl;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "hospital_cde", referencedColumnName = "hospital_cde")
	private Hospital hospital;

	public HospitalApplicationPk getHospitalApplicationPk() {
		return hospitalApplicationPk;
	}

	public void setHospitalApplicationPk(
			HospitalApplicationPk hospitalApplicationPk) {
		this.hospitalApplicationPk = hospitalApplicationPk;
	}

	public Character getApplicationEnabled() {
		return applicationEnabled;
	}

	public void setApplicationEnabled(Character applicationEnabled) {
		this.applicationEnabled = applicationEnabled;
	}
	
	public VersionControl getVersionControl() {
		return versionControl;
	}

	public void setVersionControl(VersionControl versionControl) {
		this.versionControl = versionControl;
	}
	
	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	protected void customReset() {
		hospitalApplicationPk = null;
		applicationEnabled = null;
		versionControl = null;
		hospital = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hospitalApplicationPk == null) ? 0 : hospitalApplicationPk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		HospitalApplication other = (HospitalApplication) obj;
		if (hospitalApplicationPk == null) {
			if (other.hospitalApplicationPk != null) {
				return false;
			}
		} else if (!hospitalApplicationPk.equals(other.hospitalApplicationPk)) {
			return false;
		}
		return true;
	}
}
