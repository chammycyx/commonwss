/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.upload.dao;

import javax.persistence.EntityManager;

import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;

import hk.org.ha.service.biz.image.model.fmk.exception.CidDbException;
import hk.org.ha.service.biz.image.model.persistence.entity.Hospital;
import hk.org.ha.service.biz.image.model.persistence.entity.NextNumber;
import hk.org.ha.service.biz.image.model.persistence.entity.NextNumberPk;

public class ImageUploadDao {

	private EntityManager em;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public ImageUploadDao(EntityManager em) {
		this.em = em;
	}

	public Integer generateNextNo(NextNumberPk key) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		NextNumber nextNumberEntity = null;
		Integer nextNo;

		try {
			nextNumberEntity = em.find(NextNumber.class, key);

			if (nextNumberEntity == null) {
				return null;
			}

			nextNo = nextNumberEntity.getNextNo();
			// TODO Concurrent access problem !!
			nextNumberEntity.setNextNo(nextNo + 1);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}

		return nextNo;
	}

	public Hospital findHospitalByKey(String cde) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		Hospital hospitalEntity = null;

		try {
			hospitalEntity = em.find(Hospital.class, cde);

			if (hospitalEntity == null) {
				return null;
			}

		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}

		return hospitalEntity;
	}
}
