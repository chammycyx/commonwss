/*
Change Request History:
- CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05]
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-19]
 */

package hk.org.ha.service.biz.image.model.template;

import java.util.Arrays;

import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfCategory;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfInt;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateData;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateInfo;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.WebServiceTypeConverter;
import hk.org.ha.service.biz.image.model.template.bo.ImageTemplateBoFactory;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(mappedName = "ejb/ImageTemplate")
public class ImageTemplateBean implements ImageTemplate, ImageTemplateLocal {

	@PersistenceContext
	private EntityManager em;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public ArrayOfCategory getTemplateCategory(int profileId) {
		String logContent = String.format("%s(profileId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), profileId);
		cidLogger.debug(this.getClass(), logContent);
		try {
			// Remarks: This change is to fix the java complier bug
			//return WebServiceTypeConverter.convertToWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
			//.getTemplateCategory(profileId));
			return WebServiceTypeConverter.convertToCategoryWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
					.getTemplateCategory(profileId));
			
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public ArrayOfTemplateData getTemplateData(ArrayOfInt templateId) {
		String logContent = String.format("%s(templateId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), templateId.toString());
		cidLogger.debug(this.getClass(), logContent);
		try {
			// Remarks: This change is to fix the java complier bug
			//return WebServiceTypeConverter.convertToWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
			//.getTemplateData(WebServiceTypeConverter.convertToList(templateId)));
			return WebServiceTypeConverter.convertToTemplateDataWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
					.getTemplateData(WebServiceTypeConverter.convertToList(templateId)));
			
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public ArrayOfTemplateInfo getTemplateInfo(int categoryId) {
		String logContent = String.format("%s(categoryId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), categoryId);
		cidLogger.debug(this.getClass(), logContent);
		try {
			// Remarks: This change is to fix the java complier bug
			//return WebServiceTypeConverter.convertToWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
			//.getTemplateInfo(categoryId));
			return WebServiceTypeConverter.convertToTemplateInfoWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
					.getTemplateInfo(categoryId));
			
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}
	
	public ArrayOfTemplateInfo getTemplateInfoByCategoryIds(int[] categoryIds) {
		String logContent = String.format("%s(categoryId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), Arrays.toString(categoryIds));
		cidLogger.debug(this.getClass(), logContent);
		try {
			// Remarks: This change is to fix the java complier bug
			//return WebServiceTypeConverter.convertToWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
			//.getTemplateInfo(categoryId));
			return WebServiceTypeConverter.convertToTemplateInfoWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
					.getTemplateInfoByCategoryIds(categoryIds));
			
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}
	
	public boolean uploadImageTemplate(byte[] imgBinaryArray, String filePath, String fileName){
		String logContent = String.format("%s(imgBinaryArray[%s], filePath[%s], fileName[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), "Size:" + imgBinaryArray.length, filePath, fileName);
		cidLogger.debug(this.getClass(), logContent);
		
		try {
			return ImageTemplateBoFactory.produceImageTemplateBo(em)
					.uploadImageTemplate(imgBinaryArray, filePath, fileName);
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}		
	}
	
	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param: profileId - The profile ID of the template category and template info to be retrieved
	 * Return: ArrayOfTemplateInfo - The template info objects with template categories
	 */
	public ArrayOfTemplateInfo getTemplateCategoryAndTemplateInfoByProfileId(int profileId) {
		String logContent = String.format("%s(profileId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), profileId);
		cidLogger.debug(this.getClass(), logContent);
		try {
			return WebServiceTypeConverter.convertToTemplateInfoWsArray(ImageTemplateBoFactory.produceImageTemplateBo(em)
					.getTemplateCategoryAndTemplateInfoByProfileId(profileId));
			
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}
}
