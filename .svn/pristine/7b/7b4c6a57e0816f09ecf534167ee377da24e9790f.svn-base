/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit test for <code>FileSystemManager</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-29
 * @since 1.0 2010-06-29
 */
public class FileSystemManagerTest {

	private static String baseDir;

	private static File nonExistFile;

	private static File srcFile;

	private List<File> fileToDel = new ArrayList<File>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		baseDir = ConfigurationFinder.findConfByParam("cidTestConf.properties")
				.getProperty("cid.test.unitTestEmptyBaseDir");

		nonExistFile = new File("no dir", "no file.txt");

		srcFile = new File(baseDir, "srcFile.txt");
		srcFile.createNewFile();
		OutputStream out = new FileOutputStream(srcFile);
		out.write("1234567890".getBytes());
		out.flush();
		out.close();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		srcFile.delete();
	}

	@After
	public void tearDown() throws Exception {
		Collections.reverse(fileToDel);
		for (File file : fileToDel) {
			file.delete();
		}
		fileToDel.clear();
	}

	@Test
	public void testCreateDirectory() {
		String dir = "dir0\\dir1\\dir2";
		fileToDel.add(new File(baseDir, "dir0"));
		fileToDel.add(new File(fileToDel.get(0), "dir1"));
		fileToDel.add(new File(fileToDel.get(1), "dir2"));
		File targetFolder = fileToDel.get(2);

		FileSystemManager.createDirectory(baseDir, dir);
		assertTrue(targetFolder.isDirectory());
	}

	@Test(expected = RuntimeException.class)
	public void testCreateDirectoryPathNotExist() {
		FileSystemManager.createDirectory("non existing path", "no dir");
	}

	@Test
	public void testCreateDirectoryNoAction() {
		File folder = new File(baseDir);
		File[] beforeFiles = folder.listFiles();
		FileSystemManager.createDirectory(folder.getPath(), "");
		File[] afterFiles = folder.listFiles();
		assertEquals(beforeFiles.length, afterFiles.length);
	}

	@Test
	public void testDeleteDirectory() throws IOException, Exception {
		String dir = "dir0";
		new File(baseDir + File.separator + dir + File.separator + "dir1"
				+ File.separator + "dir2").mkdirs();
		new File(baseDir + File.separator + dir + File.separator + "dir1"
				+ File.separator + "deleteMe.txt").createNewFile();

		FileSystemManager.deleteDirectory(baseDir, dir);

		String[] filenames = new File(baseDir).list();
		for (String filename : filenames) {
			assertFalse(filename.equals(dir));
		}
	}

	@Test(expected = NullPointerException.class)
	public void testDeleteDirectoryPathNotExist() throws Exception {
		FileSystemManager.deleteDirectory("non existing path", "no dir");
	}

//	@Test
//	public void testCopyFile() throws Exception {
//		File destFile = new File(baseDir, "destFile.txt");
//		fileToDel.add(destFile);
//		FileSystemManager.copyFile(srcFile, destFile);
//		assertEquals(srcFile.length(), destFile.length());
//	}
//
//	@Test
//	public void testCopyFileOverwrite() throws Exception {
//		File destFile = new File(baseDir, "destFile.txt");
//		destFile.createNewFile();
//		OutputStream out = new FileOutputStream(destFile);
//		out.write("12345".getBytes());
//		out.flush();
//		out.close();
//		fileToDel.add(destFile);
//
//		FileSystemManager.copyFile(srcFile, destFile);
//		assertEquals(srcFile.length(), destFile.length());
//	}
	
	@Test
	public void testZipFile() throws Exception {
		FileSystemManager.createZipFile("C:/Temp", "aaa", "C:/Temp", "123");
	}

//	@Test(expected = RuntimeException.class)
//	public void testCopyFileNoSource() throws Exception {
//		FileSystemManager.copyFile(nonExistFile, nonExistFile);
//	}
//
//	@Test(expected = RuntimeException.class)
//	public void testCopyFileNoDest() throws IOException, Exception {
//		FileSystemManager.copyFile(srcFile, nonExistFile);
//	}

	@Test
	public void testConvertSmbNamedPipe() throws Exception
	{
		String win32NamedPipe = "\\\\dcepr05x\\ePR\\CID\\ERS\\VH_ERS\\";
		
		System.out.println(FileSystemManager.convertSmbNamedPipe(win32NamedPipe));
	}
}
