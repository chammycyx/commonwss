/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-731: Issue and performance degrade found in TWE production drill [Boris HO 2013-06-21]
 */

package hk.org.ha.service.biz.image.endpoint.upload;

import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.upload.ImageUploadLocal;

import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

import weblogic.jws.Policies;
import weblogic.jws.Policy;
import weblogic.jws.security.RolesAllowed;
import weblogic.jws.security.SecurityRole;

@MTOM
@WebService(serviceName = "ImageUploadWebS", portName = "ImageUploadWebSSoap", targetNamespace = "http://cid.ha.org.hk/cid", endpointInterface = "hk.org.ha.service.biz.image.endpoint.upload.ImageUploadWebS")
@Policies( { @Policy(uri = "policy:usernametoken.xml", attachToWsdl = true) })
public class ImageUploadWebSImpl implements ImageUploadWebS {

	private final String UPLOAD_IMAGE_EVENT_ID = "00011";
	private final String UPLOAD_METADATA_EVENT_ID = "00012";
	
	@RolesAllowed( { @SecurityRole(role = "cid_image_getaccnows_role") })
	public String getAccessionNo(String hospCde, String deptCde, String userId,
			String workstationId, String requestSys) {
		ImageUploadLocal iu;

		iu = LocalEjbLocator.locateImageUploadEjb();

		return iu.getAccessionNo(hospCde, deptCde, userId, workstationId,
				requestSys);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_uploadimgws_role") })
	public String uploadImage(byte[] imgBinaryArray, String requestSys,
			String filename, String patientKey, String studyId,
			String seriesNo, String userId, String workstationId) {
		ImageUploadLocal iu;

		iu = LocalEjbLocator.locateImageUploadEjb();

		return iu.uploadImage(imgBinaryArray, requestSys, filename, patientKey,
				studyId, seriesNo, userId, workstationId, UPLOAD_IMAGE_EVENT_ID);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_uploadmetadatws_role") })
	public int uploadMetaData(String ha7Message, String userId,
			String workstationId, String requestSys) {
		ImageUploadLocal iu;

		iu = LocalEjbLocator.locateImageUploadEjb();

		return iu.uploadMetaData(ha7Message, userId, workstationId, requestSys, UPLOAD_METADATA_EVENT_ID);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_valaccnows_role") })
	public boolean validateAccessionNo(String hospCde, String deptCde,
			String accessionNo) {
		ImageUploadLocal iu;

		iu = LocalEjbLocator.locateImageUploadEjb();

		return iu.validateAccessionNo(hospCde, deptCde, accessionNo);
	}

}
