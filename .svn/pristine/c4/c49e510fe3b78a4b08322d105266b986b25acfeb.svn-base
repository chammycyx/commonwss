<?xml version="1.0"?>
<!DOCTYPE project>
<project name="cid-icw-web build" basedir="." default="build">
	<property name="module.name" value="cid-icw-web-6.0.0.0" />

	<!-- source -->
	<property name="main.java.dir" value="src/main/java" />
	<property name="main.javaconf.dir" value="src/main/conf" />
	<property name="test.java.dir" value="src/test/java" />
	<property name="test.javaconf.dir" value="src/test/conf" />
	<property name="main.web.dir" value="web" />

	<!-- output -->
	<property name="build.dir" value="build" />
	<property name="main.classes.dir" value="${build.dir}/main/classes" />
	<property name="main.classes.debug.dir" value="${build.dir}/main/classes-debug" />
	<property name="main.classes.instrumented.dir" value="${build.dir}/main/classes-instrumented" />
	<property name="test.classes.debug.dir" value="${build.dir}/test/classes-debug" />
	<property name="lib.java.temp.dir" value="${build.dir}/lib" />
	<property name="dist.dir" value="dist" />
	<property name="reports.dir" value="reports" />

	<!-- project -->
	<property name="repo.dir" value="../cid-icw-repository" />
	<property name="ejb.dir" value="../cid-icw-ejb" />
	<property name="ejb.dist.dir" value="${ejb.dir}/dist" />
	<path id="project.jarlib.path">
		<fileset dir="${ejb.dist.dir}">
			<include name="*.jar" />
		</fileset>
		<filelist dir="${repo.dir}/quartz">
			<file name="commons-dbcp-1.3.jar" />
			<file name="commons-pool-1.5.4.jar" />
			<file name="jta-1.1.jar" />
			<file name="quartz-all-1.8.3.jar" />
		</filelist>
		<filelist dir="${repo.dir}/jcifs">
			<file name="jcifs-1.1.11.jar" />
		</filelist>
		<filelist dir="${repo.dir}/zip">
			<file name="winzipaes.jar" />
			<file name="bcprov-ext-jdk16-145.jar" />
		</filelist>
		<filelist dir="${repo.dir}/slf4j">
			<file name="slf4j-api-1.6.1.jar" />
			<file name="slf4j-log4j12-1.6.1.jar" />
			<file name="log4j-1.2.16.jar" />
		</filelist>
	</path>
	<path id="blazeds.jarlib.path">
		<filelist dir="${repo.dir}/flex-messaging">
			<file name="backport-util-concurrent.jar" />
			<file name="cfgatewayadapter.jar" />
			<file name="commons-codec-1.3.jar" />
			<file name="commons-httpclient-3.0.1.jar" />
			<file name="commons-logging.jar" />
			<file name="concurrent.jar" />
			<file name="flex-messaging-common.jar" />
			<file name="flex-messaging-core.jar" />
			<file name="flex-messaging-opt.jar" />
			<file name="flex-messaging-proxy.jar" />
			<file name="flex-messaging-remoting.jar" />
			<file name="xalan.jar" />
		</filelist>
	</path>
	<path id="jersey.jarlib.path">
			<filelist dir="${repo.dir}/jersey">
				<file name="jackson-core-asl-1.9.2.jar" />
				<file name="jackson-jaxrs-1.9.2.jar" />
				<file name="jackson-mapper-asl-1.9.2.jar" />
				<file name="jackson-xc-1.9.2.jar" />
				<file name="jersey-bundle-1.19.1.jar" />
				<file name="jettison-1.1.jar" />
				<file name="jsr311-api-1.1.1.jar" />
				<file name="javax.annotation-api-1.2.jar" />
			</filelist>
		</path>

	<!-- environment -->
	<property name="wls.jarlib.dir" value="C:\Program Files\Oracle\Middleware_10_3_4\wlserver_10.3\server\lib" />
	<property name="wls.modules.dir" value="C:\Program Files\Oracle\Middleware_10_3_4\modules" />
	<path id="jee.jarlib.path">
		<filelist dir="${wls.jarlib.dir}">
			<file name="api.jar" />
			<file name="wls-api.jar" />
		</filelist>
		<filelist dir="${wls.modules.dir}">
			<file name="glassfish.jstl_1.2.0.1.jar" />
			<file name="javax.jsf_1.1.0.0_1-2.jar" />
			<file name="javax.ejb_3.0.1.jar" />
			<file name="javax.enterprise.deploy_1.2.jar" />
			<file name="javax.interceptor_1.0.jar" />
			<file name="javax.jms_1.1.1.jar" />
			<file name="javax.jsp_1.2.0.0_2-1.jar" />
			<file name="javax.jws_2.0.jar" />
			<file name="javax.activation_1.1.0.0_1-1.jar" />
			<file name="javax.mail_1.1.0.0_1-4-1.jar" />
			<file name="javax.xml.soap_1.3.1.0.jar" />
			<file name="javax.xml.rpc_1.2.1.jar" />
			<file name="javax.xml.ws_2.1.1.jar" />
			<file name="javax.management.j2ee_1.0.jar" />
			<file name="javax.resource_1.5.1.jar" />
			<file name="javax.servlet_1.0.0.0_2-5.jar" />
			<file name="javax.transaction_1.0.0.0_1-1.jar" />
			<file name="javax.xml.stream_1.1.1.0.jar" />
			<file name="javax.security.jacc_1.0.0.0_1-1.jar" />
			<file name="javax.xml.registry_1.0.0.0_1-0.jar" />
			<file name="com.bea.core.apache_1.2.0.1.jar" />
			<file name="com.bea.core.i18n_1.8.0.0.jar" />
			<file name="com.bea.core.logging_1.8.0.0.jar" />
			<file name="com.bea.core.utils.full_1.9.0.0.jar" />
			<file name="com.bea.core.utils.wrapper_1.4.0.0.jar" />
			<file name="com.bea.core.utils.classloaders_1.8.0.0.jar" />
			<file name="com.bea.core.common.security.providers.env_1.0.0.0_6-1-0-0.jar" />
			<file name="com.bea.core.common.security.saml2.manage_1.0.0.0_6-1-0-0.jar" />
			<file name="com.bea.core.weblogic.web.api_1.4.0.0.jar" />
			<file name="com.bea.core.weblogic.rmi.client_1.8.0.0.jar" />
			<file name="com.bea.core.transaction_2.7.0.0.jar" />
			<file name="com.bea.core.workarea_1.7.0.0.jar" />
			<file name="com.bea.core.xml.weblogic.xpath_1.4.0.0.jar" />
			<file name="com.bea.core.datasource6_1.9.0.0.jar" />
			<file name="com.bea.core.weblogic.stax_1.8.0.0.jar" />
			<file name="javax.persistence_1.0.0.0_1-0-2.jar" />
			<file name="ws.api_1.1.0.0.jar" />
		</filelist>
	</path>

	<property name="pmd.dir" value="C:\Program Files\Java\PMD 4.3" />
	<path id="pmd.jarlib.path">
		<fileset dir="${pmd.dir}">
			<include name="lib/*.jar" />
		</fileset>
	</path>

	<property name="findbugs.dir" value="C:\Program Files\Java\FindBugs 2.0.1 RC2" />
	<path id="findbugs.jarlib.path">
		<fileset dir="${findbugs.dir}">
			<include name="lib/*.jar" />
		</fileset>
	</path>

	<property name="junit.dir" value="C:\Program Files\Java\JUnit 4.10" />
	<path id="junit.jarlib.path">
		<fileset dir="${junit.dir}">
			<include name="**/*.jar" />
		</fileset>
	</path>

	<property name="easymock.dir" value="C:\Program Files\Java\EasyMock 3.1" />
	<path id="easymock.jarlib.path">
		<fileset dir="${easymock.dir}">
			<include name="easymock-3.1.jar" />
			<include name="dependency/*.jar" />
		</fileset>
	</path>

	<property name="cobertura.dir" value="C:\Program Files\Java\Cobertura 1.9.4.1" />
	<path id="cobertura.jarlib.path">
		<fileset dir="${cobertura.dir}">
			<include name="**/*.jar" />
		</fileset>
	</path>

	<!--
	<target name="build" depends="init, dist, scan, test, clean" />
	-->
	<target name="build" depends="init, dist, scan, clean" />

	<target name="init">
		<mkdir dir="${main.classes.dir}" />
		<mkdir dir="${main.classes.debug.dir}" />
		<mkdir dir="${main.classes.instrumented.dir}" />
		<mkdir dir="${test.classes.debug.dir}" />
		<mkdir dir="${lib.java.temp.dir}" />

		<delete dir="${dist.dir}" />
		<mkdir dir="${dist.dir}" />
		<delete dir="${reports.dir}" />
		<mkdir dir="${reports.dir}" />
	</target>

	<target name="dist">
		<javac destdir="${main.classes.dir}" source="1.5" target="1.5" includeantruntime="false">
			<compilerarg value="-XDignore.symbol.file=true" />
			<src path="${main.java.dir}" />
			<classpath>
				<path refid="project.jarlib.path" />
				<path refid="blazeds.jarlib.path" />
				<path refid="jersey.jarlib.path" />
				<path refid="jee.jarlib.path" />
			</classpath>
		</javac>

		<copy todir="${main.classes.dir}">
			<fileset dir="${main.javaconf.dir}" />
		</copy>

		<copy todir="${lib.java.temp.dir}" flatten="true">
			<path refid="blazeds.jarlib.path" />
			<path refid="jersey.jarlib.path" />
		</copy>

		<war destfile="${dist.dir}/${module.name}.war" manifest="${main.web.dir}/META-INF/MANIFEST.MF" webxml="${main.web.dir}/WEB-INF/web.xml">
			<lib dir="${lib.java.temp.dir}" />
			<classes dir="${main.classes.dir}" />
			<fileset dir="${main.web.dir}" />
		</war>
	</target>

	<target name="scan">
		<taskdef classpathref="pmd.jarlib.path" name="pmd" classname="net.sourceforge.pmd.ant.PMDTask" />
		<pmd shortFilenames="true" targetjdk="1.5">
			<ruleset>${pmd.dir}/HAPMDCustom-2.0.0.xml</ruleset>
			<fileset dir="${main.java.dir}" />
			<formatter type="xml" toFile="${reports.dir}/pmd.xml" />
			<auxClasspath>
				<path refid="project.jarlib.path" />
				<path refid="blazeds.jarlib.path" />
				<path refid="jersey.jarlib.path" />
				<path refid="jee.jarlib.path" />
			</auxClasspath>
		</pmd>

		<taskdef classpathref="findbugs.jarlib.path" name="findbugs" classname="edu.umd.cs.findbugs.anttask.FindBugsTask" />
		<findbugs home="${findbugs.dir}" reportLevel="low" output="xml" outputFile="${reports.dir}/findbugs.xml">
			<class location="${main.classes.dir}" />
			<sourcePath path="${main.java.dir}" />
			<auxClasspath>
				<path refid="project.jarlib.path" />
				<path refid="blazeds.jarlib.path" />
				<path refid="jersey.jarlib.path" />
				<path refid="jee.jarlib.path" />
			</auxClasspath>
		</findbugs>
	</target>

	<target name="test">
		<javac destdir="${main.classes.debug.dir}" source="1.5" target="1.5" includeantruntime="false" debug="true" debuglevel="vars,lines,source">
			<compilerarg value="-XDignore.symbol.file=true" />
			<src path="${main.java.dir}" />
			<classpath>
				<path refid="project.jarlib.path" />
				<path refid="blazeds.jarlib.path" />
				<path refid="jersey.jarlib.path" />
				<path refid="jee.jarlib.path" />
			</classpath>
		</javac>

		<javac destdir="${test.classes.debug.dir}" source="1.5" target="1.5" includeantruntime="false" debug="true" debuglevel="vars,lines,source">
			<compilerarg value="-XDignore.symbol.file=true" />
			<src path="${test.java.dir}" />
			<classpath>
				<pathelement location="${main.classes.debug.dir}" />
				<path refid="project.jarlib.path" />
				<path refid="blazeds.jarlib.path" />
				<path refid="jersey.jarlib.path" />
				<path refid="jee.jarlib.path" />
				<path refid="junit.jarlib.path" />
				<path refid="easymock.jarlib.path" />
			</classpath>
		</javac>

		<copy todir="${test.classes.debug.dir}">
			<fileset dir="${test.javaconf.dir}" />
		</copy>

		<taskdef classpathref="cobertura.jarlib.path" resource="tasks.properties" />
		<cobertura-instrument datafile="${reports.dir}/cobertura.ser" todir="${main.classes.instrumented.dir}">
			<fileset dir="${main.classes.debug.dir}">
				<include name="**/*.class" />
			</fileset>
		</cobertura-instrument>

		<junit fork="yes" forkmode="once">
			<classpath>
				<pathelement location="${test.classes.debug.dir}" />
				<pathelement location="${main.classes.instrumented.dir}" />
				<pathelement location="${main.classes.debug.dir}" />
				<path refid="project.jarlib.path" />
				<path refid="blazeds.jarlib.path" />
				<path refid="jersey.jarlib.path" />
				<path refid="jee.jarlib.path" />
				<path refid="junit.jarlib.path" />
				<path refid="easymock.jarlib.path" />
				<path refid="cobertura.jarlib.path" />
			</classpath>
			<test name="AllTests" todir="${reports.dir}" outfile="junit" />
			<formatter type="xml" />
		</junit>

		<cobertura-report format="xml" datafile="${reports.dir}/cobertura.ser" destdir="${reports.dir}">
			<fileset dir="${main.java.dir}" />
		</cobertura-report>
	</target>

	<target name="clean">
		<delete dir="${build.dir}" />
	</target>
</project>