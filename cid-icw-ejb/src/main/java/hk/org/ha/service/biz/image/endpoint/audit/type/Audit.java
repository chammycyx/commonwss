package hk.org.ha.service.biz.image.endpoint.audit.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eventId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventValue" type="{http://cid.ha.org.hk/cid}HashMapOfEventValue" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="computer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hospitalCde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestSys" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="caseNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accessionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specialtyCde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventTextData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "accessionNo", "caseNo", "computer",
		"eventId", "eventValue", "hospitalCde", "patientKey", "requestSys",
		"specialtyCde", "userId", "eventTextData" })
@XmlRootElement(name = "audit")
public class Audit {

	protected String eventId;
	protected HashMapOfEventValue eventValue;
	protected String userId;
	protected String computer;
	protected String hospitalCde;
	protected String requestSys;
	protected String patientKey;
	protected String caseNo;
	protected String accessionNo;
	protected String specialtyCde;
	protected String eventTextData;

	/**
	 * Gets the value of the eventId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEventId() {
		return eventId;
	}

	/**
	 * Sets the value of the eventId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEventId(String value) {
		this.eventId = value;
	}

	/**
	 * Gets the value of the eventValue property.
	 * 
	 * @return possible object is {@link HashMapOfEventValue }
	 * 
	 */
	public HashMapOfEventValue getEventValue() {
		return eventValue;
	}

	/**
	 * Sets the value of the eventValue property.
	 * 
	 * @param value
	 *            allowed object is {@link HashMapOfEventValue }
	 * 
	 */
	public void setEventValue(HashMapOfEventValue value) {
		this.eventValue = value;
	}

	/**
	 * Gets the value of the userId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the value of the userId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUserId(String value) {
		this.userId = value;
	}

	/**
	 * Gets the value of the computer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getComputer() {
		return computer;
	}

	/**
	 * Sets the value of the computer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setComputer(String value) {
		this.computer = value;
	}

	/**
	 * Gets the value of the hospitalCde property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHospitalCde() {
		return hospitalCde;
	}

	/**
	 * Sets the value of the hospitalCde property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setHospitalCde(String value) {
		this.hospitalCde = value;
	}

	/**
	 * Gets the value of the requestSys property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRequestSys() {
		return requestSys;
	}

	/**
	 * Sets the value of the requestSys property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRequestSys(String value) {
		this.requestSys = value;
	}

	/**
	 * Gets the value of the patientKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPatientKey() {
		return patientKey;
	}

	/**
	 * Sets the value of the patientKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPatientKey(String value) {
		this.patientKey = value;
	}

	/**
	 * Gets the value of the caseNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCaseNo() {
		return caseNo;
	}

	/**
	 * Sets the value of the caseNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCaseNo(String value) {
		this.caseNo = value;
	}

	/**
	 * Gets the value of the accessionNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccessionNo() {
		return accessionNo;
	}

	/**
	 * Sets the value of the accessionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccessionNo(String value) {
		this.accessionNo = value;
	}

	/**
	 * Gets the value of the specialtyCde property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSpecialtyCde() {
		return specialtyCde;
	}

	/**
	 * Sets the value of the specialtyCde property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSpecialtyCde(String value) {
		this.specialtyCde = value;
	}

	/**
	 * Gets the value of the eventTextData property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEventTextData() {
		return eventTextData;
	}

	/**
	 * Sets the value of the eventTextData property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEventTextData(String value) {
		this.eventTextData = value;
	}

}
