
package hk.org.ha.service.biz.image.endpoint.audit.type;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HashMapOfEventValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HashMapOfEventValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventValue" type="{http://cid.ha.org.hk/cid}EventValue" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hashMapOfEventValue", propOrder = {
    "eventValue"
})
public class HashMapOfEventValue {

    @XmlElement(name = "eventValue", nillable = true)
    protected List<EventValue> eventValue;

    /**
     * Gets the value of the eventValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventValue }
     * 
     * 
     */
    public List<EventValue> getEventValue() {
        if (eventValue == null) {
            eventValue = new ArrayList<EventValue>();
        }
        return this.eventValue;
    }

	public void setEventValue(List<EventValue> eventValue) {
		this.eventValue = eventValue;
	}
}
