
package hk.org.ha.service.biz.image.endpoint.audit.type;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the hk.org.ha.service.biz.image.endpoint.audit package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: hk.org.ha.service.biz.image.endpoint.audit
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Audit }
     * 
     */
    public Audit createAudit() {
        return new Audit();
    }

    /**
     * Create an instance of {@link AuditResponse }
     * 
     */
    public AuditResponse createAuditResponse() {
        return new AuditResponse();
    }

    /**
     * Create an instance of {@link EventValue }
     * 
     */
    public EventValue createEventValue() {
        return new EventValue();
    }

    /**
     * Create an instance of {@link HashMapOfEventValue }
     * 
     */
    public HashMapOfEventValue createHashMapOfEventValue() {
        return new HashMapOfEventValue();
    }

}
