/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.eprcid.type;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Exception", propOrder = { "message" })
public class EprFault implements Serializable {

	private static final long serialVersionUID = 6071880205554023993L;
	protected String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String value) {
		this.message = value;
	}

}
