/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.eprcid.type;

import javax.xml.ws.WebFault;

@WebFault(name = "Exception", targetNamespace = "http://epr.cid/")
public class Exception_Exception extends java.lang.Exception {

	private static final long serialVersionUID = 8393256470336607111L;

	private hk.org.ha.service.biz.image.endpoint.eprcid.type.EprFault faultInfo;

	public Exception_Exception(String message,
			hk.org.ha.service.biz.image.endpoint.eprcid.type.EprFault faultInfo) {
		super(message);
		this.faultInfo = faultInfo;
	}

	public Exception_Exception(
			String message,
			hk.org.ha.service.biz.image.endpoint.eprcid.type.EprFault faultInfo,
			Throwable cause) {
		super(message, cause);
		this.faultInfo = faultInfo;
	}
	
	public hk.org.ha.service.biz.image.endpoint.eprcid.type.EprFault getFaultInfo() {
		return faultInfo;
	}

}
