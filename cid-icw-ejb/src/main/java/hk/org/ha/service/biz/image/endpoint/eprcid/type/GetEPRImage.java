package hk.org.ha.service.biz.image.endpoint.eprcid.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for getEPRImage complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="getEPRImage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="patient_key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inst_cd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="case_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accession_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="series_seq_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="image_seq_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="version_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEPRImage", propOrder = { "patientKey", "instCd", "caseNo",
		"accessionNo", "seriesSeqNo", "imageSeqNo", "versionNo" })
public class GetEPRImage {

	@XmlElement(name = "patient_key")
	protected String patientKey;

	@XmlElement(name = "inst_cd")
	protected String instCd;

	@XmlElement(name = "case_no")
	protected String caseNo;

	@XmlElement(name = "accession_no")
	protected String accessionNo;

	@XmlElement(name = "series_seq_no")
	protected String seriesSeqNo;

	@XmlElement(name = "image_seq_no")
	protected String imageSeqNo;

	@XmlElement(name = "version_no")
	protected String versionNo;

	/**
	 * Gets the value of the patientKey property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getPatientKey() {
		return patientKey;
	}

	/**
	 * Sets the value of the patientKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setPatientKey(String value) {
		this.patientKey = value;
	}

	/**
	 * Gets the value of the instCd property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getInstCd() {
		return instCd;
	}

	/**
	 * Sets the value of the instCd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setInstCd(String value) {
		this.instCd = value;
	}

	/**
	 * Gets the value of the caseNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCaseNo() {
		return caseNo;
	}

	/**
	 * Sets the value of the caseNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setCaseNo(String value) {
		this.caseNo = value;
	}

	/**
	 * Gets the value of the accessionNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getAccessionNo() {
		return accessionNo;
	}

	/**
	 * Sets the value of the accessionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setAccessionNo(String value) {
		this.accessionNo = value;
	}

	/**
	 * Gets the value of the seriesSeqNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getSeriesSeqNo() {
		return seriesSeqNo;
	}

	/**
	 * Sets the value of the seriesSeqNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setSeriesSeqNo(String value) {
		this.seriesSeqNo = value;
	}

	/**
	 * Gets the value of the imageSeqNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getImageSeqNo() {
		return imageSeqNo;
	}

	/**
	 * Sets the value of the imageSeqNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setImageSeqNo(String value) {
		this.imageSeqNo = value;
	}

	/**
	 * Gets the value of the versionNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getVersionNo() {
		return versionNo;
	}

	/**
	 * Sets the value of the versionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setVersionNo(String value) {
		this.versionNo = value;
	}

}
