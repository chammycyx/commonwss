/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.eprcid.type;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

	private final static QName _SaveCID_QNAME = new QName("http://epr.cid/",
			"saveCID");

	private final static QName _GetEPRImageResponse_QNAME = new QName(
			"http://epr.cid/", "getEPRImageResponse");

	private final static QName _Exception_QNAME = new QName("http://epr.cid/",
			"Exception");

	private final static QName _SaveCIDResponse_QNAME = new QName(
			"http://epr.cid/", "saveCIDResponse");

	private final static QName _GetEPRImage_QNAME = new QName(
			"http://epr.cid/", "getEPRImage");

	private final static QName _GetFirstLastEPRImage_QNAME = new QName(
			"http://epr.cid/", "getFirstLastEPRImage");

	private final static QName _GetFirstLastEPRImageResponse_QNAME = new QName(
			"http://epr.cid/", "getFirstLastEPRImageResponse");

	public ObjectFactory() {
	}

	public GetFirstLastEPRImageResponse createGetFirstLastEPRImageResponse() {
		return new GetFirstLastEPRImageResponse();
	}

	public EprFault createEprException() {
		return new EprFault();
	}

	public SaveCIDResponse createSaveCIDResponse() {
		return new SaveCIDResponse();
	}

	public GetEPRImageResponse createGetEPRImageResponse() {
		return new GetEPRImageResponse();
	}

	public GetEPRImage createGetEPRImage() {
		return new GetEPRImage();
	}

	public GetFirstLastEPRImage createGetFirstLastEPRImage() {
		return new GetFirstLastEPRImage();
	}

	public SaveCID createSaveCID() {
		return new SaveCID();
	}

	@XmlElementDecl(namespace = "http://epr.cid/", name = "saveCID")
	public JAXBElement<SaveCID> createSaveCID(SaveCID value) {
		return new JAXBElement<SaveCID>(_SaveCID_QNAME, SaveCID.class, null,
				value);
	}

	@XmlElementDecl(namespace = "http://epr.cid/", name = "getEPRImageResponse")
	public JAXBElement<GetEPRImageResponse> createGetEPRImageResponse(
			GetEPRImageResponse value) {
		return new JAXBElement<GetEPRImageResponse>(_GetEPRImageResponse_QNAME,
				GetEPRImageResponse.class, null, value);
	}

	@XmlElementDecl(namespace = "http://epr.cid/", name = "Exception")
	public JAXBElement<EprFault> createEprException(EprFault value) {
		return new JAXBElement<EprFault>(_Exception_QNAME, EprFault.class,
				null, value);
	}

	@XmlElementDecl(namespace = "http://epr.cid/", name = "saveCIDResponse")
	public JAXBElement<SaveCIDResponse> createSaveCIDResponse(
			SaveCIDResponse value) {
		return new JAXBElement<SaveCIDResponse>(_SaveCIDResponse_QNAME,
				SaveCIDResponse.class, null, value);
	}

	@XmlElementDecl(namespace = "http://epr.cid/", name = "getEPRImage")
	public JAXBElement<GetEPRImage> createGetEPRImage(GetEPRImage value) {
		return new JAXBElement<GetEPRImage>(_GetEPRImage_QNAME,
				GetEPRImage.class, null, value);
	}

	@XmlElementDecl(namespace = "http://epr.cid/", name = "getFirstLastEPRImage")
	public JAXBElement<GetFirstLastEPRImage> createGetFirstLastEPRImage(
			GetFirstLastEPRImage value) {
		return new JAXBElement<GetFirstLastEPRImage>(
				_GetFirstLastEPRImage_QNAME, GetFirstLastEPRImage.class, null,
				value);
	}

	@XmlElementDecl(namespace = "http://epr.cid/", name = "getFirstLastEPRImageResponse")
	public JAXBElement<GetFirstLastEPRImageResponse> createGetFirstLastEPRImageResponse(
			GetFirstLastEPRImageResponse value) {
		return new JAXBElement<GetFirstLastEPRImageResponse>(
				_GetFirstLastEPRImageResponse_QNAME,
				GetFirstLastEPRImageResponse.class, null, value);
	}

}
