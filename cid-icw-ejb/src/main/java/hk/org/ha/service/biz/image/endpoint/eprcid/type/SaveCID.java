package hk.org.ha.service.biz.image.endpoint.eprcid.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for saveCID complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="saveCID">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HA7String" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "saveCID", propOrder = { "ha7String" })
public class SaveCID {

	@XmlElement(name = "HA7String")
	protected String ha7String;

	/**
	 * Gets the value of the ha7String property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getHA7String() {
		return ha7String;
	}

	/**
	 * Sets the value of the ha7String property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setHA7String(String value) {
		this.ha7String = value;
	}

}
