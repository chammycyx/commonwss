package hk.org.ha.service.biz.image.endpoint.retrieval.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "path", "filename", "isThumbnail", "userId",
		"workstationId", "requestSys" })
@XmlRootElement(name = "getBufferedCidImage")
public class GetBufferedCidImage {

	protected String path;

	protected String filename;

	protected boolean isThumbnail;

	protected String userId;

	protected String workstationId;

	protected String requestSys;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public boolean isThumbnail() {
		return isThumbnail;
	}

	public void setThumbnail(boolean isThumbnail) {
		this.isThumbnail = isThumbnail;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}

}
