package hk.org.ha.service.biz.image.endpoint.retrieval.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="patientKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hospCde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="caseNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accessionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seriesNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imageSeqNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="versionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "patientKey", "hospCde", "caseNo",
		"accessionNo", "seriesNo", "imageSeqNo", "versionNo", "imageId",
		"userId", "workstationId", "requestSys" })
@XmlRootElement(name = "getCidImage")
public class GetCidImage {

	protected String patientKey;

	protected String hospCde;

	protected String caseNo;

	protected String accessionNo;

	protected String seriesNo;

	protected String imageSeqNo;

	protected String versionNo;

	protected String imageId;

	protected String userId;

	protected String workstationId;

	protected String requestSys;

	/**
	 * Gets the value of the patientKey property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getPatientKey() {
		return patientKey;
	}

	/**
	 * Sets the value of the patientKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setPatientKey(String value) {
		this.patientKey = value;
	}

	/**
	 * Gets the value of the hospCde property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getHospCde() {
		return hospCde;
	}

	/**
	 * Sets the value of the hospCde property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setHospCde(String value) {
		this.hospCde = value;
	}

	/**
	 * Gets the value of the caseNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCaseNo() {
		return caseNo;
	}

	/**
	 * Sets the value of the caseNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setCaseNo(String value) {
		this.caseNo = value;
	}

	/**
	 * Gets the value of the accessionNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getAccessionNo() {
		return accessionNo;
	}

	/**
	 * Sets the value of the accessionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setAccessionNo(String value) {
		this.accessionNo = value;
	}

	/**
	 * Gets the value of the seriesNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getSeriesNo() {
		return seriesNo;
	}

	/**
	 * Sets the value of the seriesNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setSeriesNo(String value) {
		this.seriesNo = value;
	}

	/**
	 * Gets the value of the imageSeqNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getImageSeqNo() {
		return imageSeqNo;
	}

	/**
	 * Sets the value of the imageSeqNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setImageSeqNo(String value) {
		this.imageSeqNo = value;
	}

	/**
	 * Gets the value of the versionNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getVersionNo() {
		return versionNo;
	}

	/**
	 * Sets the value of the versionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setVersionNo(String value) {
		this.versionNo = value;
	}

	/**
	 * Gets the value of the imageId property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getImageId() {
		return imageId;
	}

	/**
	 * Sets the value of the imageId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setImageId(String value) {
		this.imageId = value;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
}
