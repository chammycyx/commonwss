/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.retrieval.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getCidImageResult" })
@XmlRootElement(name = "getCidImageResponse")
public class GetCidImageResponse {

	@XmlElement(name = "getCidImageResult")
	protected byte[] getCidImageResult;

	public byte[] getGetCidImageResult() {
		return (getCidImageResult != null)?getCidImageResult.clone():new byte[0];
	}

	public void setGetCidImageResult(byte[] value) {
		this.getCidImageResult = value.clone();
	}

}
