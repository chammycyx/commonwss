package hk.org.ha.service.biz.image.endpoint.retrieval.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "patientKey", "hospCde", "caseNo",
		"accessionNo", "seriesNo", "imageSeqNo", "versionNo", "isPrintedCount", "userId", "workstationId", "requestSys" })
@XmlRootElement(name = "getCidStudyImageCount")
public class GetCidStudyImageCount {

	protected String patientKey;

	protected String hospCde;

	protected String caseNo;

	protected String accessionNo;

	protected String seriesNo;

	protected String imageSeqNo;

	protected String versionNo;
	
	protected boolean isPrintedCount;
	
	protected String userId;
	
	protected String workstationId;
	
	protected String requestSys;

	public String getPatientKey() {
		return patientKey;
	}

	public void setPatientKey(String value) {
		this.patientKey = value;
	}

	public String getHospCde() {
		return hospCde;
	}

	public void setHospCde(String value) {
		this.hospCde = value;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String value) {
		this.caseNo = value;
	}

	public String getAccessionNo() {
		return accessionNo;
	}

	public void setAccessionNo(String value) {
		this.accessionNo = value;
	}

	public String getSeriesNo() {
		return seriesNo;
	}

	public void setSeriesNo(String value) {
		this.seriesNo = value;
	}

	public String getImageSeqNo() {
		return imageSeqNo;
	}

	public void setImageSeqNo(String value) {
		this.imageSeqNo = value;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String value) {
		this.versionNo = value;
	}

	public boolean isPrintedCount() {
		return isPrintedCount;
	}

	public void setPrintedCount(boolean isPrintedCount) {
		this.isPrintedCount = isPrintedCount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
}
