package hk.org.ha.service.biz.image.endpoint.retrieval.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getCidStudyImageCountResult" })
@XmlRootElement(name = "getCidStudyImageCountResponse")
public class GetCidStudyImageCountResponse {
	
	@XmlElement(name = "getCidStudyImageCountResult")
	protected int getCidStudyImageCountResult;

	public int getGetCidStudyImageCountResult() {
		return getCidStudyImageCountResult;
	}

	public void setGetCidStudyImageCountResult(int value) {
		this.getCidStudyImageCountResult = value;
	}
	
}
