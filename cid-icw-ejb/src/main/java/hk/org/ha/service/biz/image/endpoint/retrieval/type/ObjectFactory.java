package hk.org.ha.service.biz.image.endpoint.retrieval.type;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the org.tempuri package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: org.tempuri
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ExportImageResponse }
	 */
	public ExportImageResponse createExportImageResponse() {
		return new ExportImageResponse();
	}

	/**
	 * Create an instance of {@link GetCidStudyFirstLastImage }
	 */
	public GetCidStudyFirstLastImage createGetEprImage() {
		return new GetCidStudyFirstLastImage();
	}

	/**
	 * Create an instance of {@link GetCidImage }
	 */
	public GetCidImage createGetCidImage() {
		return new GetCidImage();
	}

	/**
	 * Create an instance of {@link GetCidStudyResponse }
	 */
	public GetCidStudyResponse createGetCidStudyResponse() {
		return new GetCidStudyResponse();
	}

	/**
	 * Create an instance of {@link GetCidStudyFirstLastImageResponse }
	 */
	public GetCidStudyFirstLastImageResponse createGetEprImageResponse() {
		return new GetCidStudyFirstLastImageResponse();
	}

	/**
	 * Create an instance of {@link GetCidImageResponse }
	 */
	public GetCidImageResponse createGetCidImageResponse() {
		return new GetCidImageResponse();
	}

	/**
	 * Create an instance of {@link ExportImage }
	 */
	public ExportImage createExportImage() {
		return new ExportImage();
	}

	/**
	 * Create an instance of {@link GetCidStudy }
	 */
	public GetCidStudy createGetCidStudy() {
		return new GetCidStudy();
	}

	/**
	 * Create an instance of {@link GetBufferedCidImageResponse }
	 */
	public GetBufferedCidImageResponse createGetBufferedCidImageResponse() {
		return new GetBufferedCidImageResponse();
	}
	
	/**
	 * Create an instance of {@link GetBufferedCidImage }
	 */
	public GetBufferedCidImage createGetBufferedCidImage() {
		return new GetBufferedCidImage();
	}
}
