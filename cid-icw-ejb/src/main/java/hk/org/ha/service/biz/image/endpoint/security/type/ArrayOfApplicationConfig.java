/**
 * 
 */
package hk.org.ha.service.biz.image.endpoint.security.type;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "arrayOfApplicationConfig", propOrder = { "applicationConfig" })
public class ArrayOfApplicationConfig {

	@XmlElement(name = "applicationConfig", nillable = true)
	protected List<WsApplicationConfig> applicationConfig;

	public List<WsApplicationConfig> getApplicationConfig() {
		if (applicationConfig == null) {
			applicationConfig = new ArrayList<WsApplicationConfig>();
		}
		return applicationConfig;
	}

	public void setApplicationConfig(List<WsApplicationConfig> applicationConfig) {
		this.applicationConfig = applicationConfig;
	}

	@Override
	public String toString() {
		return "ArrayOfApplicationConfig [applicationConfig="
				+ applicationConfig + "]";
	}
	
	
}
