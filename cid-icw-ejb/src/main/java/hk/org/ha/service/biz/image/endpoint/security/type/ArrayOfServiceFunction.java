package hk.org.ha.service.biz.image.endpoint.security.type;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfServiceFunction complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfServiceFunction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceFunction" type="{http://cid.ha.org.hk/cid}ServiceFunction" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "arrayOfServiceFunction", propOrder = { "serviceFunction" })
public class ArrayOfServiceFunction {

	@XmlElement(name = "serviceFunction", nillable = true)
	protected List<WsServiceFunction> serviceFunction;

	/**
	 * Gets the value of the serviceFunction property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the serviceFunction property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getServiceFunction().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link WsServiceFunction }
	 */
	public List<WsServiceFunction> getServiceFunction() {
		if (serviceFunction == null) {
			serviceFunction = new ArrayList<WsServiceFunction>();
		}
		return this.serviceFunction;
	}

	public void setServiceFunction(List<WsServiceFunction> serviceFunction) {
		this.serviceFunction = serviceFunction;
	}
}
