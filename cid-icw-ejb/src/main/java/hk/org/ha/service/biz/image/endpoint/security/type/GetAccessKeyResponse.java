package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAccessKeyResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getAccessKeyResult" })
@XmlRootElement(name = "getAccessKeyResponse")
public class GetAccessKeyResponse {

	@XmlElement(name = "getAccessKeyResult")
	protected String getAccessKeyResult;

	/**
	 * Gets the value of the getAccessKeyResult property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getGetAccessKeyResult() {
		return getAccessKeyResult;
	}

	/**
	 * Sets the value of the getAccessKeyResult property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setGetAccessKeyResult(String value) {
		this.getAccessKeyResult = value;
	}

}
