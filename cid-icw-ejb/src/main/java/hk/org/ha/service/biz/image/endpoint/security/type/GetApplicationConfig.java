/**
 * 
 */
package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "hospCode", "systemId", "applicationId", "profileCde",
		"section", "key", "userId", "workstationId", "requestSys" })
@XmlRootElement(name = "getApplicationConfig")
public class GetApplicationConfig {
	
	protected String hospCode;
	
	protected String systemId;
	
	protected String applicationId;
	
	protected String profileCde;
	
	protected String section;
	
	protected String key;
	
	protected String userId;
	
	protected String workstationId;
	
	protected String requestSys;

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getProfileCde() {
		return profileCde;
	}

	public void setProfileCde(String profileCde) {
		this.profileCde = profileCde;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
	
}
