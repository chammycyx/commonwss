/**
 * 
 */
package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getApplicationConfigResult" })
@XmlRootElement(name = "getApplicationConfigResponse")
public class GetApplicationConfigResponse {

	@XmlElement(name = "getApplicationConfigResult")
	protected ArrayOfApplicationConfig getApplicationConfigResult;

	public ArrayOfApplicationConfig getGetApplicationConfigResult() {
		return getApplicationConfigResult;
	}

	public void setGetApplicationConfigResult(
			ArrayOfApplicationConfig getApplicationConfigResult) {
		this.getApplicationConfigResult = getApplicationConfigResult;
	}

}
