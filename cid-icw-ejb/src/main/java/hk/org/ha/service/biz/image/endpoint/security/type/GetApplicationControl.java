/**
 * Change Request History:
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Alex LEE 2013-12-17]
 */
package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "applicationId", "applicationVersion", "systemId", "hospCode",
		"profileCode" })
@XmlRootElement(name = "getApplicationControl")
public class GetApplicationControl {

	protected String applicationId;
	
	protected String applicationVersion;

	protected String systemId;

	protected String hospCode;

	protected String profileCode;

	/**
	 * Gets the value of the applicationId property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * Sets the value of the applicationId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setApplicationId(String value) {
		this.applicationId = value;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	/**
	 * Gets the value of the systemId property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * Sets the value of the systemId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setSystemId(String value) {
		this.systemId = value;
	}

	/**
	 * Gets the value of the hospCode property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getHospCode() {
		return hospCode;
	}

	/**
	 * Sets the value of the hospCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setHospCode(String value) {
		this.hospCode = value;
	}

	/**
	 * Gets the value of the profileCode property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getProfileCode() {
		return profileCode;
	}

	/**
	 * Sets the value of the profileCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setProfileCode(String value) {
		this.profileCode = value;
	}

}
