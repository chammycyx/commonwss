package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFunctionsResult" type="{http://cid.ha.org.hk/cid}ArrayOfServiceFunction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getFunctionsResult" })
@XmlRootElement(name = "getFunctionsResponse")
public class GetFunctionsResponse {

	@XmlElement(name = "getFunctionsResult")
	protected ArrayOfServiceFunction getFunctionsResult;

	/**
	 * Gets the value of the getFunctionsResult property.
	 * 
	 * @return possible object is {@link ArrayOfServiceFunction }
	 */
	public ArrayOfServiceFunction getGetFunctionsResult() {
		return getFunctionsResult;
	}

	/**
	 * Sets the value of the getFunctionsResult property.
	 * 
	 * @param value
	 *            allowed object is {@link ArrayOfServiceFunction }
	 */
	public void setGetFunctionsResult(ArrayOfServiceFunction value) {
		this.getFunctionsResult = value;
	}

}
