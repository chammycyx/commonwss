package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the org.tempuri package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: org.tempuri
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link GetApplicationControl }
	 */
	public GetApplicationControl createGetApplicationControl() {
		return new GetApplicationControl();
	}

	/**
	 * Create an instance of {@link GetFunctions }
	 */
	public GetFunctions createGetFunctions() {
		return new GetFunctions();
	}

	/**
	 * Create an instance of {@link ArrayOfServiceFunction }
	 */
	public ArrayOfServiceFunction createArrayOfServiceFunction() {
		return new ArrayOfServiceFunction();
	}

	/**
	 * Create an instance of {@link GetAccessKeyResponse }
	 */
	public GetAccessKeyResponse createGetAccessKeyResponse() {
		return new GetAccessKeyResponse();
	}

	/**
	 * Create an instance of {@link GetFunctionsResponse }
	 */
	public GetFunctionsResponse createGetFunctionsResponse() {
		return new GetFunctionsResponse();
	}

	/**
	 * Create an instance of {@link GetAccessKey }
	 */
	public GetAccessKey createGetAccessKey() {
		return new GetAccessKey();
	}

	/**
	 * Create an instance of {@link WsServiceFunction }
	 */
	public WsServiceFunction createServiceFunction() {
		return new WsServiceFunction();
	}

	/**
	 * Create an instance of {@link ValidateAccessKeyByHostName }
	 */
	public ValidateAccessKeyByHostName createValidateAccessKeyByHostName() {
		return new ValidateAccessKeyByHostName();
	}

	/**
	 * Create an instance of {@link WsApplicationControl }
	 */
	public WsApplicationControl createApplicationControl() {
		return new WsApplicationControl();
	}

	/**
	 * Create an instance of {@link ValidateAccessKeyByHostNameResponse }
	 */
	public ValidateAccessKeyByHostNameResponse createValidateAccessKeyByHostNameResponse() {
		return new ValidateAccessKeyByHostNameResponse();
	}

	/**
	 * Create an instance of {@link GetApplicationControlResponse }
	 */
	public GetApplicationControlResponse createGetApplicationControlResponse() {
		return new GetApplicationControlResponse();
	}

	/**
	 * Create an instance of {@link ValidateAccessKeyResponse }
	 */
	public ValidateAccessKeyResponse createValidateAccessKeyResponse() {
		return new ValidateAccessKeyResponse();
	}

	/**
	 * Create an instance of {@link ValidateAccessKey }
	 */
	public ValidateAccessKey createValidateAccessKey() {
		return new ValidateAccessKey();
	}

	public ValidateApplicationVersion createValidateApplicationVersion(){
		return new ValidateApplicationVersion();
	}
	
	public ValidateApplicationStatus createValidateApplicationStatus(){
		return new ValidateApplicationStatus();
	}
	
	public ValidateApplicationVersionResponse createValidateApplicationVersionResponse(){
		return new ValidateApplicationVersionResponse();
	}
	
	public ValidateApplicationStatusResponse createValidateApplicationStatusResponse(){
		return new ValidateApplicationStatusResponse();
	}
	
	public ArrayOfApplicationConfig createArrayOfApplicationConfig() {
		return new ArrayOfApplicationConfig();
	}
}
