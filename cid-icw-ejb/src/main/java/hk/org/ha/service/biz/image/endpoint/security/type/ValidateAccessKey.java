package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accessKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "systemId", "ipAddress", "accessKey", "userId", "workstationId", "requestSys" })
@XmlRootElement(name = "validateAccessKey")
public class ValidateAccessKey {

	protected String systemId;

	protected String ipAddress;

	protected String accessKey;
	
	protected String userId;
	
	protected String workstationId;
	
	protected String requestSys;

	/**
	 * Gets the value of the systemId property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * Sets the value of the systemId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setSystemId(String value) {
		this.systemId = value;
	}

	/**
	 * Gets the value of the ipAddress property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Sets the value of the ipAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setIpAddress(String value) {
		this.ipAddress = value;
	}

	/**
	 * Gets the value of the accessKey property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getAccessKey() {
		return accessKey;
	}

	/**
	 * Sets the value of the accessKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setAccessKey(String value) {
		this.accessKey = value;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}

}
