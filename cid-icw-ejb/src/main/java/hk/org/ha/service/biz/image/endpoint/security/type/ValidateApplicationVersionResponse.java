package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValidateApplicationVersionResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "validateApplicationVersionResult" })
@XmlRootElement(name = "validateApplicationVersionResponse")
public class ValidateApplicationVersionResponse {

	@XmlElement(name = "validateApplicationVersionResult")
	protected boolean validateApplicationVersionResult;

	/**
	 * Gets the value of the validateApplicationVersionResult property.
	 */
	public boolean isValidateApplicationVersionResult() {
		return validateApplicationVersionResult;
	}

	/**
	 * Sets the value of the validateApplicationVersionResult property.
	 */
	public void setValidateApplicationVersionResult(boolean value) {
		this.validateApplicationVersionResult = value;
	}

}
