/**
 * 
 */
package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "section", "key", "value" })
public class WsApplicationConfig {

	@XmlElement(name = "section")
	protected String section;
	
	@XmlElement(name = "key")
	protected String key;

	@XmlElement(name = "value")
	protected String value;

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "WsApplicationConfig [section=" + section + ", key=" + key
				+ ", value=" + value + "]";
	}
	
}
