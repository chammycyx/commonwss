/**
 * Change Request History:
 * - CID-192: textbox's default font size [Alex LEE 2012-06-08]
 * - CID-222: marker's default width & height [Alex LEE 2012-06-13]
 * - CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12]
 * - CID-694: Change to provide API for ERS to set the marker & drawing status when CID-Studio is running [Boris HO 2013-07-11]
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-18]
 * - CID-786: Revise the program flow of clinical photo upload for streamlining the upload workflow [Alex LEE 2013-11-20]
 * 
 */
package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "applicationControl", propOrder = { "applicationVersion",
		"enabled", "maxPanelWidth", "maxPanelHeight", "minPanelWidth",
		"minPanelHeight", "maxUploadFileSize", "profileId",
		"maxUploadFileCount", "maxMarkerCount", "defaultTextboxFontSize",
		"defaultMarkerWidth", "defaultMarkerHeight", "enableMarkerHistory",
		"arrayOfServiceFunction", "enableUploadUi" })
public class WsApplicationControl {

	@XmlElement(name = "applicationVersion")
	protected String applicationVersion;

	@XmlElement(name = "enabled")
	protected String enabled;

	@XmlElement(name = "maxPanelWidth")
	protected int maxPanelWidth;

	@XmlElement(name = "maxPanelHeight")
	protected int maxPanelHeight;

	@XmlElement(name = "minPanelWidth")
	protected int minPanelWidth;

	@XmlElement(name = "minPanelHeight")
	protected int minPanelHeight;

	@XmlElement(name = "maxUploadFileSize")
	protected int maxUploadFileSize;

	@XmlElement(name = "profileId")
	protected int profileId;

	@XmlElement(name = "maxUploadFileCount")
	protected int maxUploadFileCount;

	@XmlElement(name = "maxMarkerCount")
	protected int maxMarkerCount;

	@XmlElement(name = "defaultTextboxFontSize")
	protected int defaultTextboxFontSize;

	@XmlElement(name = "defaultMarkerWidth")
	protected int defaultMarkerWidth;
	
	@XmlElement(name = "defaultMarkerHeight")
	protected int defaultMarkerHeight;
	
	@XmlElement(name = "enableMarkerHistory")
	protected boolean enableMarkerHistory;
	
	@XmlElement(name = "arrayOfServiceFunction")
	protected ArrayOfServiceFunction arrayOfServiceFunction;
	
	@XmlElement(name = "enableUploadUi")
	protected boolean enableUploadUi;
	
	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String value) {
		this.applicationVersion = value;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String value) {
		this.enabled = value;
	}

	public int getMaxPanelWidth() {
		return maxPanelWidth;
	}

	public void setMaxPanelWidth(int value) {
		this.maxPanelWidth = value;
	}

	public int getMaxPanelHeight() {
		return maxPanelHeight;
	}

	public void setMaxPanelHeight(int value) {
		this.maxPanelHeight = value;
	}

	public int getMinPanelWidth() {
		return minPanelWidth;
	}

	public void setMinPanelWidth(int value) {
		this.minPanelWidth = value;
	}

	public int getMinPanelHeight() {
		return minPanelHeight;
	}

	public void setMinPanelHeight(int value) {
		this.minPanelHeight = value;
	}

	public int getMaxUploadFileSize() {
		return maxUploadFileSize;
	}

	public void setMaxUploadFileSize(int value) {
		this.maxUploadFileSize = value;
	}

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int value) {
		this.profileId = value;
	}

	public int getMaxUploadFileCount() {
		return maxUploadFileCount;
	}

	public void setMaxUploadFileCount(int value) {
		this.maxUploadFileCount = value;
	}

	public int getMaxMarkerCount() {
		return maxMarkerCount;
	}

	public void setMaxMarkerCount(int value) {
		this.maxMarkerCount = value;
	}

	public int getDefaultTextboxFontSize() {
		return defaultTextboxFontSize;
	}

	public void setDefaultTextboxFontSize(int defaultTextboxFontSize) {
		this.defaultTextboxFontSize = defaultTextboxFontSize;
	}

	public int getDefaultMarkerWidth() {
		return defaultMarkerWidth;
	}

	public void setDefaultMarkerWidth(int defaultMarkerWidth) {
		this.defaultMarkerWidth = defaultMarkerWidth;
	}

	public int getDefaultMarkerHeight() {
		return defaultMarkerHeight;
	}

	public void setDefaultMarkerHeight(int defaultMarkerHeight) {
		this.defaultMarkerHeight = defaultMarkerHeight;
	}

	public boolean getEnableMarkerHistory() {
		return enableMarkerHistory;
	}

	public void setEnableMarkerHistory(boolean enableMarkerHistory) {
		this.enableMarkerHistory = enableMarkerHistory;
	}
	
	public ArrayOfServiceFunction getArrayOfServiceFunction() {
		return this.arrayOfServiceFunction;
	}

	public void setArrayOfServiceFunction(
			ArrayOfServiceFunction arrayOfServiceFunction) {
		this.arrayOfServiceFunction = arrayOfServiceFunction;
	}

	public boolean getEnableUploadUi() {
		return enableUploadUi;
	}

	public void setEnableUploadUi(boolean enableUploadUi) {
		this.enableUploadUi = enableUploadUi;
	}

}
