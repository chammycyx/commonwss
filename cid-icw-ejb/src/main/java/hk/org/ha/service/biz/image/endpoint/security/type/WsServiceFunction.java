package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ServiceFunction complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceFunction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FunctionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Enabled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "serviceFunction", propOrder = { "functionId", "enabled" })
public class WsServiceFunction {

	@XmlElement(name = "functionId")
	protected String functionId;

	@XmlElement(name = "enabled")
	protected String enabled;

	/**
	 * Gets the value of the functionId property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getFunctionId() {
		return functionId;
	}

	/**
	 * Sets the value of the functionId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setFunctionId(String value) {
		this.functionId = value;
	}

	/**
	 * Gets the value of the enabled property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getEnabled() {
		return enabled;
	}

	/**
	 * Sets the value of the enabled property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setEnabled(String value) {
		this.enabled = value;
	}

}
