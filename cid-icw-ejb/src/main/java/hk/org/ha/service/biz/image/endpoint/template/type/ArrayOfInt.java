/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.template.type;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "arrayOfInt", propOrder = { "_int" })
public class ArrayOfInt {

	@XmlElement(name = "int", type = Integer.class)
	protected List<Integer> _int;

	public List<Integer> getInt() {
		if (_int == null) {
			_int = new ArrayList<Integer>();
		}
		
		return this._int;
	}

	public List<Integer> get_int() {
		return _int;
	}

	public void set_int(List<Integer> int1) {
		_int = int1;
	}

}
