package hk.org.ha.service.biz.image.endpoint.template.type;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfTemplateData complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTemplateData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsTemplateData" type="{http://cid.ha.org.hk/cid}WsTemplateData" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "arrayOfTemplateData", propOrder = { "templateData" })
public class ArrayOfTemplateData {

	@XmlElement(name = "wsTemplateData", nillable = true)
	protected List<WsTemplateData> templateData;

	/**
	 * Gets the value of the templateData property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the templateData property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getTemplateData().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link WsTemplateData }
	 */
	public List<WsTemplateData> getTemplateData() {
		if (templateData == null) {
			templateData = new ArrayList<WsTemplateData>();
		}
		return this.templateData;
	}

	public void setTemplateData(List<WsTemplateData> templateData) {
		this.templateData = templateData;
	}

}
