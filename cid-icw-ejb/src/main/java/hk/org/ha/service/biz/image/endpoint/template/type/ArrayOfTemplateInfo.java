package hk.org.ha.service.biz.image.endpoint.template.type;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfTemplateInfo complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTemplateInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsTemplateInfo" type="{http://cid.ha.org.hk/cid}WsTemplateInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "arrayOfTemplateInfo", propOrder = { "templateInfo" })
public class ArrayOfTemplateInfo {

	@XmlElement(name = "wsTemplateInfo", nillable = true)
	protected List<WsTemplateInfo> templateInfo;

	/**
	 * Gets the value of the templateInfo property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the templateInfo property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getTemplateInfo().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link WsTemplateInfo }
	 */
	public List<WsTemplateInfo> getTemplateInfo() {
		if (templateInfo == null) {
			templateInfo = new ArrayList<WsTemplateInfo>();
		}
		return this.templateInfo;
	}

	public void setTemplateInfo(List<WsTemplateInfo> templateInfo) {
		this.templateInfo = templateInfo;
	}

}
