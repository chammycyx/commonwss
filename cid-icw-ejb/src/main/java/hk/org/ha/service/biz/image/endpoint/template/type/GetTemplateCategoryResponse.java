package hk.org.ha.service.biz.image.endpoint.template.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTemplateCategoryResult" type="{http://cid.ha.org.hk/cid}ArrayOfCategory" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getTemplateCategoryResult" })
@XmlRootElement(name = "getTemplateCategoryResponse")
public class GetTemplateCategoryResponse {

	@XmlElement(name = "getTemplateCategoryResult")
	protected ArrayOfCategory getTemplateCategoryResult;

	/**
	 * Gets the value of the getTemplateCategoryResult property.
	 * 
	 * @return possible object is {@link ArrayOfCategory }
	 */
	public ArrayOfCategory getGetTemplateCategoryResult() {
		return getTemplateCategoryResult;
	}

	/**
	 * Sets the value of the getTemplateCategoryResult property.
	 * 
	 * @param value
	 *            allowed object is {@link ArrayOfCategory }
	 */
	public void setGetTemplateCategoryResult(ArrayOfCategory value) {
		this.getTemplateCategoryResult = value;
	}

}
