package hk.org.ha.service.biz.image.endpoint.template.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="templateId" type="{http://cid.ha.org.hk/cid}ArrayOfInt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "templateId" })
@XmlRootElement(name = "getTemplateData")
public class GetTemplateData {

	protected ArrayOfInt templateId;

	/**
	 * Gets the value of the templateId property.
	 * 
	 * @return possible object is {@link ArrayOfInt }
	 */
	public ArrayOfInt getTemplateId() {
		return templateId;
	}

	/**
	 * Sets the value of the templateId property.
	 * 
	 * @param value
	 *            allowed object is {@link ArrayOfInt }
	 */
	public void setTemplateId(ArrayOfInt value) {
		this.templateId = value;
	}

}
