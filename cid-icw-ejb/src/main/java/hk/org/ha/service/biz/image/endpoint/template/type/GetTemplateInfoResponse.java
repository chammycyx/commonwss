package hk.org.ha.service.biz.image.endpoint.template.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTemplateInfoResult" type="{http://cid.ha.org.hk/cid}ArrayOfTemplateInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getTemplateInfoResult" })
@XmlRootElement(name = "getTemplateInfoResponse")
public class GetTemplateInfoResponse {

	@XmlElement(name = "getTemplateInfoResult")
	protected ArrayOfTemplateInfo getTemplateInfoResult;

	/**
	 * Gets the value of the getTemplateInfoResult property.
	 * 
	 * @return possible object is {@link ArrayOfTemplateInfo }
	 */
	public ArrayOfTemplateInfo getGetTemplateInfoResult() {
		return getTemplateInfoResult;
	}

	/**
	 * Sets the value of the getTemplateInfoResult property.
	 * 
	 * @param value
	 *            allowed object is {@link ArrayOfTemplateInfo }
	 */
	public void setGetTemplateInfoResult(ArrayOfTemplateInfo value) {
		this.getTemplateInfoResult = value;
	}

}
