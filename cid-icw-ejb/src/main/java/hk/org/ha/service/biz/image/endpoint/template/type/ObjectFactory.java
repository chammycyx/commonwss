package hk.org.ha.service.biz.image.endpoint.template.type;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the org.tempuri package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: org.tempuri
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link WsTemplateData }
	 */
	public WsTemplateData createTemplateData() {
		return new WsTemplateData();
	}

	/**
	 * Create an instance of {@link GetTemplateInfoResponse }
	 */
	public GetTemplateInfoResponse createGetTemplateInfoResponse() {
		return new GetTemplateInfoResponse();
	}

	/**
	 * Create an instance of {@link ArrayOfInt }
	 */
	public ArrayOfInt createArrayOfInt() {
		return new ArrayOfInt();
	}

	/**
	 * Create an instance of {@link GetTemplateInfo }
	 */
	public GetTemplateInfo createGetTemplateInfo() {
		return new GetTemplateInfo();
	}

	/**
	 * Create an instance of {@link GetTemplateCategory }
	 */
	public GetTemplateCategory createGetTemplateCategory() {
		return new GetTemplateCategory();
	}

	/**
	 * Create an instance of {@link GetTemplateData }
	 */
	public GetTemplateData createGetTemplateData() {
		return new GetTemplateData();
	}

	/**
	 * Create an instance of {@link WsCategory }
	 */
	public WsCategory createCategory() {
		return new WsCategory();
	}

	/**
	 * Create an instance of {@link GetTemplateCategoryResponse }
	 */
	public GetTemplateCategoryResponse createGetTemplateCategoryResponse() {
		return new GetTemplateCategoryResponse();
	}

	/**
	 * Create an instance of {@link ArrayOfTemplateData }
	 */
	public ArrayOfTemplateData createArrayOfTemplateData() {
		return new ArrayOfTemplateData();
	}

	/**
	 * Create an instance of {@link ArrayOfCategory }
	 */
	public ArrayOfCategory createArrayOfCategory() {
		return new ArrayOfCategory();
	}

	/**
	 * Create an instance of {@link WsTemplateInfo }
	 */
	public WsTemplateInfo createTemplateInfo() {
		return new WsTemplateInfo();
	}

	/**
	 * Create an instance of {@link GetTemplateDataResponse }
	 */
	public GetTemplateDataResponse createGetTemplateDataResponse() {
		return new GetTemplateDataResponse();
	}

	/**
	 * Create an instance of {@link ArrayOfTemplateInfo }
	 */
	public ArrayOfTemplateInfo createArrayOfTemplateInfo() {
		return new ArrayOfTemplateInfo();
	}
	
	public UploadImageTemplate createUploadImageTemplate() {
		return new UploadImageTemplate();
	}
	
	public UploadImageTemplateResponse createUploadImageTemplateResponse() {
		return new UploadImageTemplateResponse();
	}
}
