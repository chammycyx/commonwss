/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.template.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "imgBinaryArray", "filePath", "filename" })
@XmlRootElement(name = "uploadImageTemplate")
public class UploadImageTemplate {
	
	protected byte[] imgBinaryArray;

	protected String filePath;
	
	protected String filename;
	
	public byte[] getImgBinaryArray() {
		return (imgBinaryArray != null)?imgBinaryArray.clone():new byte[0];
	}

	public void setImgBinaryArray(byte[] value) {
		this.imgBinaryArray = value.clone();
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String value) {
		this.filename = value;
	}
}
