package hk.org.ha.service.biz.image.endpoint.template.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for WsCategory complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="WsCategory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
//modified by Boris on 2011/07/28 for CID-Studio Outstanding item 104
@XmlType(name = "wsCategory", propOrder = { "id", "subCatList", "name", "defTempId", "groupName" })
public class WsCategory {

	@XmlElement(name = "id")
	protected int id;

	@XmlElement(name = "subCatList")
	protected String subCatList;

	@XmlElement(name = "name")
	protected String name;

	//modified by Boris on 2011/07/28 for CID-Studio Outstanding item 104
	@XmlElement(name = "defaultTemplateId", required = true, type = Integer.class, nillable = true)
	protected Integer defTempId;
	
	// CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25] START
	@XmlElement(name = "groupName")
	protected String groupName;
	// CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25] END
	
	/**
	 * Gets the value of the id property.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the value of the id property.
	 */
	public void setId(int value) {
		this.id = value;
	}

	/**
	 * Gets the value of the subCatList property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getSubCatList() {
		return subCatList;
	}

	/**
	 * Sets the value of the subCatList property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setSubCatList(String value) {
		this.subCatList = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setName(String value) {
		this.name = value;
	}

	//modified by Boris on 2011/07/28 for CID-Studio Outstanding item 104
	public Integer getDefTempId() {
		return defTempId;
	}

	public void setDefTempId(Integer defTempId) {
		this.defTempId = defTempId;
	}

	/*
	 * CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25] START
	 * Param: none
	 * Return: String - the getter return value for groupName attribute
	 */
	public String getGroupName() {
		return groupName;
	}
	// CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25] END

	/*
	 * CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25] START
	 * Param: String - the setter parameter for groupName attribute
	 * Return: none
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	// CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25] END
}
