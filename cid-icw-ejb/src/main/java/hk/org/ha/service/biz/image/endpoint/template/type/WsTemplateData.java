/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.template.type;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsTemplateData", propOrder = { "id", "format", "lastModifiedDate", "data" })
public class WsTemplateData {

	@XmlElement(name = "id")
	protected int id;

	@XmlElement(name = "format")
	protected String format;
	
	@XmlElement(name = "lastModifiedDate")
	protected Date lastModifiedDate;

	@XmlElement(name = "data")
	protected byte[] data;

	public int getId() {
		return id;
	}

	public void setId(int value) {
		this.id = value;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String value) {
		this.format = value;
	}

	public Date getLastModifiedDate() {
		return (lastModifiedDate != null)?(Date)lastModifiedDate.clone():null;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = (Date)lastModifiedDate.clone();
	}

	public byte[] getData() {
		return (data != null)?data.clone():new byte[0];
	}

	public void setData(byte[] value) {
		this.data = value.clone();
	}

}
