/*
Change Request History:
- CID-730: Change to stop uploading empty diagram to ePR repository from CID-Studio [Boris HO 2013-06-20]
- CID-730: Change to stop uploading empty diagram to ePR repository from CID-Studio [Boris HO 2013-06-25]
- CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-18]
 */
package hk.org.ha.service.biz.image.endpoint.template.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsTemplateInfo", propOrder = { "id", "categoryId", "name",
		"format", "ver", "displayInImport", "displayInTemplate",
		"allowEmptyUpload", "wsCategory" })
public class WsTemplateInfo {

	@XmlElement(name = "id")
	protected int id;

	@XmlElement(name = "categoryId")
	protected int categoryId;

	@XmlElement(name = "name")
	protected String name;

	@XmlElement(name = "format")
	protected String format;

	@XmlElement(name = "ver")
	protected int ver;

	// CID-158: Change to disable the warning without defaulted template in
	// CID-Studio [Boris HO 2012-06-05] START
	@XmlElement(name = "displayInImport")
	protected boolean displayInImport;

	@XmlElement(name = "displayInTemplate")
	protected boolean displayInTemplate;
	// CID-158: Change to disable the warning without defaulted template in
	// CID-Studio [Boris HO 2012-06-05] END

	@XmlElement(name = "allowEmptyUpload")
	protected boolean allowEmptyUpload;

	@XmlElement(name = "wsCategory")
	protected WsCategory wsCategory;

	/**
	 * Gets the value of the id property.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the value of the id property.
	 */
	public void setId(int value) {
		this.id = value;
	}

	/**
	 * Gets the value of the categoryId property.
	 */
	public int getCategoryId() {
		return categoryId;
	}

	/**
	 * Sets the value of the categoryId property.
	 */
	public void setCategoryId(int value) {
		this.categoryId = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the format property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Sets the value of the format property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setFormat(String value) {
		this.format = value;
	}

	/**
	 * Gets the value of the ver property.
	 */
	public int getVer() {
		return ver;
	}

	/**
	 * Sets the value of the ver property.
	 */
	public void setVer(int value) {
		this.ver = value;
	}

	/*
	 * CID-158: Change to disable the warning without defaulted template in
	 * CID-Studio [Boris HO 2012-06-05] START Param: none Return: Integer - the
	 * getter return value for displayInImport attribute
	 */
	public boolean getDisplayInImport() {
		return displayInImport;
	}

	// CID-158: Change to disable the warning without defaulted template in
	// CID-Studio [Boris HO 2012-06-05] END

	/*
	 * CID-158: Change to disable the warning without defaulted template in
	 * CID-Studio [Boris HO 2012-06-05] START Param: Integer - the setter
	 * parameter for setting displayInImport attribute Return: none
	 */
	public void setDisplayInImport(boolean displayInImport) {
		this.displayInImport = displayInImport;
	}

	// CID-158: Change to disable the warning without defaulted template in
	// CID-Studio [Boris HO 2012-06-05] END

	/*
	 * CID-158: Change to disable the warning without defaulted template in
	 * CID-Studio [Boris HO 2012-06-05] START Param: none Return: Integer - the
	 * getter return value for displayInTemplate attribute
	 */
	public boolean getDisplayInTemplate() {
		return displayInTemplate;
	}

	// CID-158: Change to disable the warning without defaulted template in
	// CID-Studio [Boris HO 2012-06-05] END

	/*
	 * CID-158: Change to disable the warning without defaulted template in
	 * CID-Studio [Boris HO 2012-06-05] START Param: Integer - the setter
	 * parameter for setting displayInTemplate attribute Return: none
	 */
	public void setDisplayInTemplate(boolean displayInTemplate) {
		this.displayInTemplate = displayInTemplate;
	}

	// CID-158: Change to disable the warning without defaulted template in
	// CID-Studio [Boris HO 2012-06-05] END

	/*
	 * Param: none 
	 * Return: boolean - the getter return value for allowEmptyUpload attribute
	 */
	public boolean getAllowEmptyUpload() {
		return allowEmptyUpload;
	}

	/*
	 * Param: allowEmpty - the setter parameter for setting allowEmptyUpload attribute 
	 * Return: none
	 */
	public void setAllowEmptyUpload(boolean allowEmptyUpload) {
		this.allowEmptyUpload = allowEmptyUpload;
	}

	/*
	 * Param: none 
	 * Return: WsCategory - the getter return value for WsCategory attribute
	 */
	public WsCategory getWsCategory() {
		return this.wsCategory;
	}

	/*
	 * Param: WsCategory - the setter parameter for setting WsCategory attribute
	 * Return: none
	 */
	public void setWsCategory(WsCategory wsCategory) {
		this.wsCategory = wsCategory;
	}
}
