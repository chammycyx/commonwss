package hk.org.ha.service.biz.image.endpoint.upload.type;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the org.tempuri package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: org.tempuri
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ValidateAccessionNoResponse }
	 */
	public ValidateAccessionNoResponse createValidateAccessionNoResponse() {
		return new ValidateAccessionNoResponse();
	}

	/**
	 * Create an instance of {@link ValidateAccessionNo }
	 */
	public ValidateAccessionNo createValidateAccessionNo() {
		return new ValidateAccessionNo();
	}

	/**
	 * Create an instance of {@link UploadImage }
	 */
	public UploadImage createUploadImage() {
		return new UploadImage();
	}

	/**
	 * Create an instance of {@link UploadMetaData }
	 */
	public UploadMetaData createUploadMetaData() {
		return new UploadMetaData();
	}

	/**
	 * Create an instance of {@link GetAccessionNoResponse }
	 */
	public GetAccessionNoResponse createGetAccessionNoResponse() {
		return new GetAccessionNoResponse();
	}

	/**
	 * Create an instance of {@link GetAccessionNo }
	 */
	public GetAccessionNo createGetAccessionNo() {
		return new GetAccessionNo();
	}

	/**
	 * Create an instance of {@link UploadImageResponse }
	 */
	public UploadImageResponse createUploadImageResponse() {
		return new UploadImageResponse();
	}

	/**
	 * Create an instance of {@link UploadMetaDataResponse }
	 */
	public UploadMetaDataResponse createUploadMetaDataResponse() {
		return new UploadMetaDataResponse();
	}

}
