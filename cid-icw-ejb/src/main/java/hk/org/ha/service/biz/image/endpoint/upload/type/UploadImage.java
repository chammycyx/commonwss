/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.upload.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "imgBinaryArray", "requestSys", "filename",
		"patientKey", "studyId", "seriesNo", "userId", "workstationId" })
@XmlRootElement(name = "uploadImage")
public class UploadImage {

	protected byte[] imgBinaryArray;

	protected String requestSys;

	protected String filename;

	protected String patientKey;

	protected String studyId;

	protected String seriesNo;
	
	protected String userId;
	
	protected String workstationId;

	public byte[] getImgBinaryArray() {
		return (imgBinaryArray != null)?imgBinaryArray.clone():new byte[0];
	}

	public void setImgBinaryArray(byte[] value) {
		this.imgBinaryArray = value.clone();
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String value) {
		this.requestSys = value;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String value) {
		this.filename = value;
	}

	public String getPatientKey() {
		return patientKey;
	}

	public void setPatientKey(String value) {
		this.patientKey = value;
	}

	public String getStudyId() {
		return studyId;
	}

	public void setStudyId(String value) {
		this.studyId = value;
	}

	public String getSeriesNo() {
		return seriesNo;
	}

	public void setSeriesNo(String value) {
		this.seriesNo = value;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
}
