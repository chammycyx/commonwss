package hk.org.ha.service.biz.image.endpoint.upload.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "uploadImageResult" })
@XmlRootElement(name = "uploadImageResponse")
public class UploadImageResponse {

	@XmlElement(name = "uploadImageResult")
	protected String uploadImageResult;

	public String getUploadImageResult() {
		return uploadImageResult;
	}

	public void setUploadImageResult(String uploadImageResult) {
		this.uploadImageResult = uploadImageResult;
	}
}
