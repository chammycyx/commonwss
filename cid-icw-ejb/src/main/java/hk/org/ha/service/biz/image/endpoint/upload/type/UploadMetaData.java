package hk.org.ha.service.biz.image.endpoint.upload.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hl7Message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "ha7Message", "userId", "workstationId", "requestSys" })
@XmlRootElement(name = "uploadMetaData")
public class UploadMetaData {

	
	protected String ha7Message;
	
	protected String userId;
	
	protected String workstationId;
	
	protected String requestSys;

	/**
	 * Gets the value of the hl7Message property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getHa7Message() {
		return ha7Message;
	}

	/**
	 * Sets the value of the hl7Message property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setHa7Message(String value) {
		this.ha7Message = value;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}

}
