package hk.org.ha.service.biz.image.endpoint.upload.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hospCde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deptCde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accessionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "hospCde", "deptCde", "accessionNo" })
@XmlRootElement(name = "validateAccessionNo")
public class ValidateAccessionNo {

	protected String hospCde;

	protected String deptCde;

	protected String accessionNo;

	/**
	 * Gets the value of the hospCde property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getHospCde() {
		return hospCde;
	}

	/**
	 * Sets the value of the hospCde property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setHospCde(String value) {
		this.hospCde = value;
	}

	/**
	 * Gets the value of the deptCde property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getDeptCde() {
		return deptCde;
	}

	/**
	 * Sets the value of the deptCde property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setDeptCde(String value) {
		this.deptCde = value;
	}

	/**
	 * Gets the value of the accessionNo property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getAccessionNo() {
		return accessionNo;
	}

	/**
	 * Sets the value of the accessionNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setAccessionNo(String value) {
		this.accessionNo = value;
	}

}
