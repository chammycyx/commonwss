/**
 * Change Request History:
 * - CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-753: Change to support 2 versions of CID modules in EAP platform
 * 
 */

package hk.org.ha.service.biz.image.model.audit;

import hk.org.ha.service.biz.image.endpoint.audit.type.EventValue;
import hk.org.ha.service.biz.image.endpoint.audit.type.HashMapOfEventValue;
import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.persistence.entity.Event;
import hk.org.ha.service.biz.image.model.persistence.entity.EventLog;
import hk.org.ha.service.biz.image.model.persistence.entity.EventLogPk;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(mappedName = "ejb/Audit")
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class AuditBean implements Audit, AuditLocal {

	@PersistenceContext
	private EntityManager em;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void writeLog(String accessionNo, String caseNo, String computer,
			String eventId, HashMapOfEventValue eventValue, String hospitalCde,
			String patientKey, String requestSys, String specialtyCde,
			String userId, String eventData) {
		String logContent = String.format("%s(accessionNo[%s], caseNo[%s], computer[%s], eventId[%s], eventValue[%s], hospitalCde[%s], patientKey[%s], requestSys[%s], specialtyCde[%s], userId[%s], eventData[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), accessionNo, caseNo, computer, eventId, eventValue.toString(), hospitalCde, patientKey, requestSys, specialtyCde, userId, eventData);
		cidLogger.debug(this.getClass(), logContent);

		try {
			Map<String, String> eventValueMap = new WeakHashMap<String, String>();
			for(EventValue ev: eventValue.getEventValue()){
				eventValueMap.put(ev.getKey(), ev.getValue());
			}
			
			EventLogPk eventLogPk = new EventLogPk();
			eventLogPk.setEventId(eventId);
			eventLogPk.setRowGuid(UUID.randomUUID().toString());

			EventLog eventLog = new EventLog();
			eventLog.setAccessionNo(accessionNo);
			eventLog.setCaseNo(caseNo);
			eventLog.setComputer(computer);
			eventLog.setEventData(eventData);
			eventLog.setEventDesc(this.getCompletedEventDesc(eventId,
					eventValueMap));
			eventLog.setEventDtm(new Date());
			eventLog.setEventLogPK(eventLogPk);
			eventLog.setEventSource(this.geteventSourceByKey(eventId));
			eventLog.setEventStatus('A');
			eventLog.setEventType(this.getEventTypeByKey(eventId));
			eventLog.setHospitalCde(hospitalCde);
			eventLog.setPatientKey(patientKey);
			eventLog.setRequestSys(requestSys);
			eventLog.setSpecialtyCde(specialtyCde);
			eventLog.setUserId(userId);
			eventLog.setApplicationVersion(ConfigurationFinder.findProperty(
					ConfigurationFinder.findConf(), "cid.ws.version"));
			
			Event event = em.find(Event.class, eventId);
			event.addEventLog(eventLog);
			
			em.persist(eventLog);
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException(String.format("System error: %s%n",
					"Failed to write audit log"));
		} catch (CidConfigException cce) {
			cidLogger.error(this.getClass(), logContent, cce);
			throw new EJBException(String.format("System error: %s%n",
					"Failed to find application version from configuration"));
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void writeLog(String accessionNo, String caseNo, String computer,
			String eventId, Map<String, String> eventValue, String hospitalCde,
			String patientKey, String requestSys, String specialtyCde,
			String userId, String eventData) {
		String logContent = String.format("%s(accessionNo[%s], caseNo[%s], computer[%s], eventId[%s], eventValue[%s], hospitalCde[%s], patientKey[%s], requestSys[%s], specialtyCde[%s], userId[%s], eventData[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), accessionNo, caseNo, computer, eventId, eventValue.toString(), hospitalCde, patientKey, requestSys, specialtyCde, userId, eventData);
		cidLogger.debug(this.getClass(), logContent);

		try {
			cidLogger.debug(this.getClass(), eventId);
			EventLogPk eventLogPk = new EventLogPk();
			eventLogPk.setEventId(eventId);
			eventLogPk.setRowGuid(UUID.randomUUID().toString());

			EventLog eventLog = new EventLog();
			eventLog.setAccessionNo(accessionNo);
			eventLog.setCaseNo(caseNo);
			eventLog.setComputer(computer);
			eventLog.setEventData(eventData);
			eventLog.setEventDesc(this.getCompletedEventDesc(eventId,
					eventValue));
			eventLog.setEventDtm(new Date());
			eventLog.setEventLogPK(eventLogPk);
			eventLog.setEventSource(this.geteventSourceByKey(eventId));
			eventLog.setEventStatus('A');
			eventLog.setEventType(this.getEventTypeByKey(eventId));
			eventLog.setHospitalCde(hospitalCde);
			eventLog.setPatientKey(patientKey);
			eventLog.setRequestSys(requestSys);
			eventLog.setSpecialtyCde(specialtyCde);
			eventLog.setUserId(userId);
			eventLog.setApplicationVersion(ConfigurationFinder.findProperty(
							ConfigurationFinder.findConf(), "cid.ws.version"));

			Event event = em.find(Event.class, eventId);
			event.addEventLog(eventLog);
			
			em.persist(eventLog);
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException(String.format("System error: %s%n",
					"Failed to write audit log"));
		} catch (CidConfigException cce) {
			cidLogger.error(this.getClass(), logContent, cce);
			throw new EJBException(String.format("System error: %s%n",
					"Failed to find application version from configuration"));
		}
	}

	private String getCompletedEventDesc(String eventId,
			Map<String, String> eventValue) {
		// Create a pattern to <non-whitespace string>
		Pattern p = Pattern.compile("<[\\S]*>");
		// Create a matcher with an input string
		Matcher m = p.matcher(getEventDescByKey(eventId));
		StringBuffer sb = new StringBuffer();	
		
		if(eventValue == null)
			return getEventDescByKey(eventId);

		// Loop through and create a new String
		// with the replacements
		while (m.find()) {
			if (eventValue.containsKey(m.group()))
				m.appendReplacement(sb, "<" + eventValue.get(m.group()) + ">");
		}
		// Add the last segment of input to
		// the new String
		m.appendTail(sb);

		return sb.toString();
	}

	private String getEventDescByKey(String key) {
		return em.find(Event.class, key).getDesc();
	}

	private String geteventSourceByKey(String key) {
		return em.find(Event.class, key).getSource();
	}

	private String getEventTypeByKey(String key) {
		return em.find(Event.class, key).getType();
	}
	
}
