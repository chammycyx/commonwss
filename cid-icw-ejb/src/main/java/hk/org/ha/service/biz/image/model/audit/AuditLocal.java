/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.audit;

import hk.org.ha.service.biz.image.endpoint.audit.type.HashMapOfEventValue;

import java.util.Map;

import javax.ejb.Local;

@Local
public interface AuditLocal {

	public void writeLog(String accessionNo, String caseNo, String computer,
			String eventId, Map<String, String> eventValue, 
			String hospitalCde, String patientKey, String requestSys,
			String specialtyCde, String userId, String eventData);
	
	public void writeLog(String accessionNo, String caseNo, String computer,
			String eventId, HashMapOfEventValue eventValue, String hospitalCde,
			String patientKey, String requestSys, String specialtyCde,
			String userId, String eventData);
	
}
