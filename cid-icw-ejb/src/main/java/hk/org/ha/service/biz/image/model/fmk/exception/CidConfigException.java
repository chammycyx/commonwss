/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.exception;

/**
 * @author HKK904
 *
 */
public class CidConfigException extends CidException {
	
	private static final long serialVersionUID = 1L;
	public static final String READ_FAILED = "Fail to read configuration property";

	/**
	 * 
	 */
	public CidConfigException() {
		
	}
	
	/**
	 * @param arg0
	 */
//	public CidDbException(String message) {
//		super(message);
//		
//	}

	/**
	 * @param arg0
	 */
	public CidConfigException(String message, boolean isLogged) {
		super(message, isLogged);
		
	}

	/**
	 * @param arg0
	 */
//	public CidDbException(Throwable cause) {
//		super(cause);
//		
//	}
	
	public CidConfigException(Throwable cause, boolean isLogged) {
		super(cause, isLogged);
		
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
//	public CidDbException(String message, Throwable cause) {
//		super(message, cause);
//		
//	}
	
	public CidConfigException(String message, Throwable cause, boolean isLogged) {
		super(message, cause, isLogged);
		
	}
}
