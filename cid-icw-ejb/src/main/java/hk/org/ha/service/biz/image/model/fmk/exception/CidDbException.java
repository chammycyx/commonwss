/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
*/

package hk.org.ha.service.biz.image.model.fmk.exception;

public class CidDbException extends CidException {
	
	private static final long serialVersionUID = 1L;
	public static final String DB_ERROR = "System database error";

	public CidDbException() {
	}

	public CidDbException(String message, boolean isLogged) {
		super(message, isLogged);
	}

	public CidDbException(Throwable cause, boolean isLogged) {
		super(cause, isLogged);
	}

	public CidDbException(String message, Throwable cause, boolean isLogged) {
		super(message, cause, isLogged);
	}
}
