/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.fmk.exception;

public class CidException extends Exception {

	private static final long serialVersionUID = 1L;
	private boolean isLogged = false;

	public CidException() {
	}
	
	public CidException(String message, boolean isLogged) {
		super(message);
		this.isLogged = isLogged;
	}

	public CidException(Throwable cause, boolean isLogged) {
		super(cause);
		this.isLogged = isLogged;
	}

	public CidException(String message, Throwable cause, boolean isLogged) {
		super(message, cause);
		this.isLogged = isLogged;
	}

	public void setLogged(boolean isLogged) {
		this.isLogged = isLogged;
	}

	public boolean isLogged() {
		return isLogged;
	}
}
