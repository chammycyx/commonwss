/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.exception;

/**
 * @author HKK904
 *
 */
public class CidFileException extends CidException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	public CidFileException() {
		
	}

	/**
	 * @param message
	 * @param isLogged
	 */
	public CidFileException(String message, boolean isLogged) {
		super(message, isLogged);
		
	}

	/**
	 * @param cause
	 * @param isLogged
	 */
	public CidFileException(Throwable cause, boolean isLogged) {
		super(cause, isLogged);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param isLogged
	 */
	public CidFileException(String message, Throwable cause, boolean isLogged) {
		super(message, cause, isLogged);
		
	}

}
