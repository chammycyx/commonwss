/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.exception;

/**
 * @author HKK904
 *
 */
public class CidHa7Exception extends CidException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	public CidHa7Exception() {
		
	}

	/**
	 * @param message
	 * @param isLogged
	 */
	public CidHa7Exception(String message, boolean isLogged) {
		super(message, isLogged);
		
	}

	/**
	 * @param cause
	 * @param isLogged
	 */
	public CidHa7Exception(Throwable cause, boolean isLogged) {
		super(cause, isLogged);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param isLogged
	 */
	public CidHa7Exception(String message, Throwable cause, boolean isLogged) {
		super(message, cause, isLogged);
		
	}

}
