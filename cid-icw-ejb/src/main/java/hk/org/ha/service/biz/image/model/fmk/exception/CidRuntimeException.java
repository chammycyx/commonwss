package hk.org.ha.service.biz.image.model.fmk.exception;

public class CidRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private boolean isLogged = false;
	
	public CidRuntimeException() {
	}
	
	public CidRuntimeException(String message, boolean isLogged) {
		super(message);
		this.isLogged = isLogged;
	}
	
	public CidRuntimeException(Throwable cause, boolean isLogged) {
		super(cause);
		this.isLogged = isLogged;
	}

	public CidRuntimeException(String message, Throwable cause, boolean isLogged) {
		super(message, cause);
		this.isLogged = isLogged;
	}

	public void setLogged(boolean isLogged) {
		this.isLogged = isLogged;
	}

	public boolean isLogged() {
		return isLogged;
	}
}

