/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.fmk.logging;

public interface CidLogger {

	public void debug(Class<?> clazz, String msg);
	
	public void debug(Class<?> clazz, String msg, Object[] obj);
	
	public void debug(String loggerName, String msg);
	
	public void debug(String loggerName, String msg, Object[] obj);

	public void error(Class<?> clazz, Exception e);
	
	public void error(Class<?> clazz, String msg, Exception e);
	
	public void error(Class<?> clazz, String msg, Object[] obj);
	
	public void error(String loggerName, Exception e);
	
	public void error(String loggerName, String msg, Object[] obj);
	
	public void info(Class<?> clazz, String msg);
	
	public void info(Class<?> clazz, String msg, Object[] obj);
	
	public void info(String loggerName, String msg);
	
	public void info(String loggerName, String msg, Object[] obj);

	public void trace(Class<?> clazz, String msg);
	
	public void trace(Class<?> clazz, String msg, Object[] obj);
	
	public void trace(String loggerName, String msg);
	
	public void trace(String loggerName, String msg, Object[] obj);

	public void warn(Class<?> clazz, String msg);
	
	public void warn(Class<?> clazz, String msg, Object[] obj);
	
	public void warn(String loggerName, String msg);
	
	public void warn(String loggerName, String msg, Object[] obj);

}
