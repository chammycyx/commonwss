/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.logging;

/**
 * TODO Class Description
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
public class CidLoggerFactory {

	private static CidLogger cidLoggerImpl = new CidLoggerImpl();

	public static CidLogger produceLogger() {
		return cidLoggerImpl;
	}

}
