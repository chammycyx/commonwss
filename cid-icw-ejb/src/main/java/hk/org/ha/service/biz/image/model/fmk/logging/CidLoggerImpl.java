/**
 * Change Request History:
 * - CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.logging;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class CidLoggerImpl implements CidLogger {
	
	static {
		try {
			Properties conf = new Properties();
			conf.load(CidLoggerImpl.class.getClassLoader().getResourceAsStream("cid-conf-src.properties"));
			DOMConfigurator.configure(conf.getProperty("cid.conf.log4j.src"));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (RuntimeException rte) {
			rte.printStackTrace();
		}
	}

	public void debug(Class<?> clazz, String msg) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug(msg);
	}

	public void debug(Class<?> clazz, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.debug(msg, obj);
	}

	public void debug(String loggerName, String msg) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.debug(msg);
	}

	public void debug(String loggerName, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.debug(msg, obj);
	}

	public void error(Class<?> clazz, Exception e) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(e.getMessage(), e);
	}
	
	public void error(Class<?> clazz, String msg, Exception e) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(msg, e);
	}

	public void error(Class<?> clazz, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.error(msg, obj);
	}

	public void error(String loggerName, Exception e) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.error(e.getMessage(), e);
	}

	public void error(String loggerName, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.error(msg, obj);
	}

	public void info(Class<?> clazz, String msg) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.info(msg);
	}

	public void info(Class<?> clazz, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.info(msg, obj);
	}

	public void info(String loggerName, String msg) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.info(msg);
	}

	public void info(String loggerName, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.info(msg, obj);
	}

	public void trace(Class<?> clazz, String msg) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.trace(msg);
	}

	public void trace(Class<?> clazz, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.trace(msg, obj);
	}

	public void trace(String loggerName, String msg) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.trace(msg);
	}

	public void trace(String loggerName, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.trace(msg, obj);
	}

	public void warn(Class<?> clazz, String msg) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.warn(msg);
	}

	public void warn(Class<?> clazz, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(clazz);
		logger.warn(msg, obj);
	}

	public void warn(String loggerName, String msg) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.warn(msg);
	}

	public void warn(String loggerName, String msg, Object[] obj) {
		Logger logger = LoggerFactory.getLogger(loggerName);
		logger.warn(msg, obj);
	}
}
