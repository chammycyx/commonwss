/**
 * Change Request History:
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

public class ConfigurationFinder {

	private static CidLogger cidLogger = CidLoggerFactory.produceLogger();
	private static Map<String, Properties> propMap;
	static {
		Map<String, Properties> initPropMap = new HashMap<String, Properties>();
		ClassLoader cl = ConfigurationFinder.class.getClassLoader();
		FileInputStream fis = null;
		try {
			Properties conf = new Properties();
			conf.load(cl.getResourceAsStream("cid-conf-src.properties"));
			Iterator<Object> it = conf.keySet().iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				String value = conf.getProperty(key);
				if (value.matches(".*\\.properties")) {
					fis = new FileInputStream(value);
					Properties props = new Properties();
					props.load(fis);
					initPropMap.put(key, props);
				}
			}
		} catch (IOException ioe) {
			cidLogger.error(ConfigurationFinder.class, ioe);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException ioe) {
					cidLogger.error(ConfigurationFinder.class, ioe);
				}
			}
		}
		propMap = initPropMap;
	}

	public static Properties findConf() throws CidConfigException {
		String logContent = String.format("%s()%n",
				Thread.currentThread().getStackTrace()[1].getMethodName());
		cidLogger.debug(ConfigurationFinder.class, logContent);

		return findExternalConfByParam("cid.conf.src");
	}

	public static Properties findExternalConfByParam(String configKey)
			throws CidConfigException {
		String logContent = String.format("%s(configKey[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				configKey);
		cidLogger.debug(ConfigurationFinder.class, logContent);
		return propMap.get(configKey);
	}

	public static String findProperty(Properties props, String propName)
			throws CidConfigException {
		String logContent = String.format("%s(props[%s], propName[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				props, propName);
		cidLogger.debug(ConfigurationFinder.class, logContent);
		
		String propValue = null;
		propValue = props.getProperty(propName);
		if (propValue == null)
			throw new CidConfigException(String.format(
					"Property \"%s\" not found", propName), false);

		return propValue;
	}
}
