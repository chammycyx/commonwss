/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.fmk.util;

import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidFileException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import jcifs.Config;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import de.idyl.crypto.zip.AesZipFileEncrypter;

public class FileSystemManager {

	private static final String SMB_NAMED_PIPE_PREFIX = "smb://";
	private static CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public static boolean createDirectory(String directoryPath) {
		String logContent = String.format("%s(directoryPath[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				directoryPath);
		cidLogger.debug(FileSystemManager.class, logContent);

		boolean success = false;
		File newDirectory = null;

		newDirectory = new File(directoryPath);

		if (!newDirectory.exists()) {
			success = newDirectory.mkdirs();
		}

		return success;
	}

	public static boolean createDirectory(String path, String directory) {
		String logContent = String.format("%s(directoryPath[%s], directory[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				path, directory);
		cidLogger.debug(FileSystemManager.class, logContent);

		boolean success = false;
		File basePath = null;

		basePath = new File(path);

		File file = new File(basePath, directory);
		if (!file.exists()) {
			success = file.mkdirs();
		}

		return success;
	}

	public static boolean deleteDirectory(String path, String directory)
			throws CidFileException {
		String logContent = String.format("%s(path[%s], directory[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				path, directory);
		cidLogger.debug(FileSystemManager.class, logContent);

		if (directory == null || directory.trim().length() == 0) {
			return true;
		}

		File folder = new File(path, directory);
		File[] files = folder.listFiles();

		for (File file : files) {
			boolean success = false;
			if (file.isDirectory()) {
				success = deleteDirectory(folder.getPath(), file.getName());
			} else {
				cidLogger.debug(FileSystemManager.class, String.format(
						"Delete file: %s", file.getAbsolutePath()));
				success = deleteFile(file);
			}

			if (!success) {
				throw new CidFileException(String.format(
						"Failed to delete file/directory: %s%n", file
								.getAbsolutePath()), false);
			}
		}
		cidLogger.debug(FileSystemManager.class, String.format(
				"Delete folder: %s", folder.getAbsolutePath()));
		return folder.delete();
	}

	public static boolean deleteArchiveDirectory(String path, String directory,
			Date asOfDate) throws CidFileException {
		String logContent = String.format("%s(path[%s], directory[%s], asOfDate[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				path, directory, asOfDate);
		cidLogger.debug(FileSystemManager.class, logContent);

		if (directory == null || directory.trim().length() == 0) {
			return true;
		}

		File folder = new File(path, directory);
		File[] files = folder.listFiles();

		for (File file : files) {
			boolean success = false;
			Date lastModified = new Date(file.lastModified());
			if (lastModified.before(asOfDate))
				if (file.isDirectory()) {
					success = deleteDirectory(folder.getPath(), file.getName());
				} else {
					cidLogger.debug(FileSystemManager.class, String.format(
							"Delete file: %s", file.getAbsolutePath()));
					success = deleteFile(file);
				}

			if (!success) {
				throw new CidFileException(String.format(
						"Failed to delete file/directory: %s%n", file
								.getAbsolutePath()), false);
			}
		}
		cidLogger.debug(FileSystemManager.class, String.format(
				"Delete folder: %s", folder.getAbsolutePath()));
		return folder.delete();
	}

	public static void deleteFile(String path, String file)
			throws CidFileException {
		String logContent = String.format("%s(path[%s], file[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				path, file);
		cidLogger.debug(FileSystemManager.class, logContent);
		
		File f = null;
		f = new File(path, file);
		deleteFile(f);
	}

	public static boolean deleteFile(File file) throws CidFileException {
		String logContent = String.format("%s(file[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				file.getAbsolutePath());
		cidLogger.debug(FileSystemManager.class, logContent);

		boolean success = false;
		// Make sure the file or directory exists and isn't write protected
		if (!file.exists())
			throw new CidFileException(String.format(
					"File does not exist: %s%n", file.getPath()
							+ File.separator + file.getName()), false);

		if (!file.canWrite())
			throw new CidFileException(String.format(
					"Source file is unwritable: %s%n", file.getName()), false);

		// If it is a directory, make sure it is empty
		if (file.isDirectory()) {
			throw new CidFileException(String.format(
					"Source file is a directory: %s%n", file.getName()), false);
		}

		// Attempt to delete it
		success = file.delete();

		return success;
	}

	public static void createEncryptedZipFile(String srcPath,
			String srcFileName, String destPath, String destFileName,
			String password) throws CidFileException {
		String logContent = String.format("%s(srcPath[%s], srcFileName[%s], destPath[%s], destFileName[%s], password[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				srcPath, srcFileName, destPath, destFileName, password);
		cidLogger.debug(FileSystemManager.class, logContent);

		AesZipFileEncrypter aze = null;
		File encryptedDestFile = null;

		File destFile = new File(srcPath, srcFileName);
		encryptedDestFile = new File(destPath, destFileName);

		try {
			aze = new AesZipFileEncrypter(encryptedDestFile);
			aze.addEncrypted(destFile, password);
		} catch (IOException ioe) {
			throw new CidFileException("File encryption failed", ioe, false);
		}

		deleteFile(destFile);
	}

	public static void addFileToZip(String srcPath, String srcFile,
			String destZipFile) throws CidFileException {
		String logContent = String.format("%s(srcPath[%s], srcFile[%s], destZipFile[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				srcPath, srcFile, destZipFile);
		cidLogger.debug(FileSystemManager.class, logContent);

		zipFolder(srcPath, srcFile, destZipFile);
	}

	private static void copyExistingZipContent(ZipInputStream zin,
			ZipOutputStream zout) throws CidFileException {
		String logContent = String.format("%s(zin[%s], ZipOutputStream[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				"", "");
		cidLogger.debug(ConfigurationFinder.class, logContent);
		
		try {
			byte[] buf = new byte[1024];
			ZipEntry entry = zin.getNextEntry();
			while (entry != null) {
				String name = entry.getName();
				cidLogger.debug(FileSystemManager.class, "Adding entry: "
						+ name);
				// Add ZIP entry to output stream.
				zout.putNextEntry(new ZipEntry(name));
				// Transfer bytes from the ZIP file to the output file
				int len;
				while ((len = zin.read(buf)) > 0) {
					zout.write(buf, 0, len);
				}
				entry = zin.getNextEntry();
			}
		} catch (IOException ioe) {
			throw new CidFileException(
					"Existing zip entry can not be added to the newly created zip file",
					ioe, false);
		}
	}

	private static void zipFolder(String srcPath, String srcFile,
			String destZipFile) throws CidFileException {
		String logContent = String.format("%s(srcPath[%s], srcFile[%s], destZipFile[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				srcPath, srcFile, destZipFile);
		cidLogger.debug(FileSystemManager.class, logContent);

		File fSrcFile = null;
		File fTempFile = null;
		ZipInputStream zipInput = null;
		FileInputStream fileReader = null;
		ZipOutputStream zipOutput = null;
		FileOutputStream fileWriter = null;
		String uuid = UUID.randomUUID().toString();
		String tempFilename = null;
		File destFile = null;

		try {
			// source file(s) to be added to zip entry
			fSrcFile = new File(srcPath + File.separator + srcFile);

			tempFilename = uuid + ".zip";
			destFile = new File(destZipFile);
			fTempFile = new File(destFile.getParent() + File.separator
					+ tempFilename);
			if (destFile.exists()) {
				if (!destFile.renameTo(fTempFile)) {
					throw new CidFileException(
							String
									.format(
											"Destination file rename from %s to %s at location %s failed.",
											destFile.getName(), fTempFile
													.getName(), destFile
													.getAbsolutePath()), false);
				}
				fileReader = new FileInputStream(fTempFile);
				zipInput = new ZipInputStream(fileReader);
			}

			fileWriter = new FileOutputStream(destFile);
			zipOutput = new ZipOutputStream(fileWriter);

			addFolderToZip(fSrcFile, zipOutput, "");
			if (zipInput != null)
				copyExistingZipContent(zipInput, zipOutput);

			zipOutput.flush();
		} catch (FileNotFoundException fne) {
			throw new CidFileException("Zip file not found", fne, false);
		} catch (IOException ioe) {
			throw new CidFileException("Zip file creation failed", ioe, false);
		} finally {
			try {
				if (zipInput != null)
					zipInput.close();
			} catch (IOException ioe) {
				throw new CidFileException("Zip file read failed", ioe, false);
			} finally {
				try {
					if (zipOutput != null)
						zipOutput.close();
				} catch (IOException ioe) {
					throw new CidFileException("Zip file creation failed", ioe,
							false);
				} finally {
					try {
						if (fileReader != null)
							fileReader.close();
					} catch (IOException ioe) {
						throw new CidFileException(String.format(
								"Failed to close file: %s%n", fTempFile
										.getName()), ioe, false);
					} finally {
						try {
							if (fileWriter != null)
								fileWriter.close();
						} catch (IOException ioe) {
							throw new CidFileException(String.format(
									"Failed to close file: %s%n", destZipFile),
									ioe, false);
						}
					}
				}
			}
		}
	}

	public static void zipFolder(String[] srcFiles, String destZipFile)
			throws CidFileException {
		String logContent = String.format("%s(srcFiles[%s], destZipFile[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				srcFiles.length, destZipFile);
		cidLogger.debug(FileSystemManager.class, logContent);

		File fSrcFile = null;
		ZipOutputStream zipOutput = null;
		FileOutputStream fileWriter = null;
		File destFile = null;

		try {
			destFile = new File(destZipFile);
			fileWriter = new FileOutputStream(destFile);
			zipOutput = new ZipOutputStream(fileWriter);

			for (String file : srcFiles) {
				fSrcFile = new File(file);
				addFolderToZip(fSrcFile, zipOutput, "");
				fSrcFile = null;
			}

			zipOutput.flush();
			zipOutput.close();
		} catch (FileNotFoundException fne) {
			throw new CidFileException("Zip file not found", fne, false);
		} catch (IOException ioe) {
			throw new CidFileException("Zip file creation failed", ioe, false);
		} finally {
			try {
				if (zipOutput != null)
					zipOutput.close();
			} catch (IOException ioe) {
				throw new CidFileException("Zip file creation failed", ioe,
						false);
			} finally {
				if (fileWriter != null) {
					try {
						fileWriter.close();
					} catch (IOException ioe) {
						throw new CidFileException(String.format(
								"Failed to close file: %s%n", destFile
										.getName()), ioe, false);
					}
				}
			}
		}
	}

	private static void addFolderToZip(File srcFile, ZipOutputStream zip,
			String parentFolder) throws CidFileException {
		String logContent = String.format("%s(srcFile[%s], zip[%s], parentFolder[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				srcFile, "", parentFolder);
		cidLogger.debug(FileSystemManager.class, logContent);

		File[] files = srcFile.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					if (!parentFolder.equals("")) {
						try {
							zip.putNextEntry(new ZipEntry(parentFolder
									+ File.separator + file.getName()
									+ File.separator));
						} catch (IOException ioe) {
							throw new CidFileException(
									"Zip file creation failed", ioe, false);
						} finally {
							try {
								if (zip != null)
									zip.closeEntry();
							} catch (IOException ioe) {
								throw new CidFileException(String
										.format("Failed to close zip entry"),
										ioe, false);
							}
						}

						addFolderToZip(file, zip, parentFolder + File.separator
								+ file.getName());
					} else {
						addFolderToZip(file, zip, file.getName());
					}
				}
			}
		} else {
			byte[] buffer;
			FileInputStream fis = null;
			try {
				buffer = new byte[1024];
				fis = new FileInputStream(srcFile);
				String zipEntry = null;
				int length;
				if (!parentFolder.equals(""))
					zipEntry = parentFolder + File.separator
							+ srcFile.getName();
				else
					zipEntry = srcFile.getName();
				zip.putNextEntry(new ZipEntry(zipEntry));

				while ((length = fis.read(buffer)) > 0)
					zip.write(buffer, 0, length);
			} catch (FileNotFoundException fne) {
				throw new CidFileException("File to be zipped not found", fne,
						false);
			} catch (IOException ioe) {
				throw new CidFileException("Zip file creation failed", ioe,
						false);
			} finally {
				try {
					zip.closeEntry();
				} catch (IOException ioe) {
					throw new CidFileException(String
							.format("Failed to close zip entry"), ioe, false);
				} finally {
					try {
						if (fis != null)
							fis.close();
					} catch (IOException ioe) {
						throw new CidFileException(String
								.format("Failed to close file: %s%n", srcFile
										.getName()), ioe, false);
					}
				}
			}
		}
	}

	public static byte[] copyFileFromNT(String sourcePath, String sourceFileName)
			throws CidFileException, CidConfigException {
		String logContent = String.format("%s(sourcePath[%s], sourceFileName[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				sourcePath, sourceFileName);
		cidLogger.debug(FileSystemManager.class, logContent);

		return readFile(sourcePath, sourceFileName);
	}

	public static void copyFileFromNT(File destFile, String sourcePath,
			String sourceFileName) throws CidFileException, CidConfigException {
		String logContent = String.format("%s(destFile[%s], sourcePath[%s], sourceFileName[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				destFile.getAbsolutePath(), sourcePath, sourceFileName);
		cidLogger.debug(FileSystemManager.class, logContent);

		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(destFile);
			fos.write(readFile(sourcePath, sourceFileName));
			fos.flush();
		} catch (FileNotFoundException fne) {
			throw new CidFileException("File for copying not found", fne, false);
		} catch (IOException ioe) {
			throw new CidFileException("File copy failed", ioe, false);
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException ioe) {
					throw new CidFileException(String.format(
							"Failed to close file: %s%n", destFile.getName()),
							ioe, false);
				}
			}
		}
	}

	public static byte[] readFile(String filePath, String fileName)
			throws CidFileException, CidConfigException {
		String logContent = String.format("%s(filePath[%s], fileName[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				filePath, fileName);
		cidLogger.debug(FileSystemManager.class, logContent);

		Config.setProperty("jcifs.resolveOrder", "LMHOSTS,WINS,DNS,BCAST");
		Config.setProperty("jcifs.smb.client.responseTimeout", "30000");
		Config.setProperty("jcifs.smb.client.soTimeout", "35000");

		Properties conf = null;
		byte[] fileContent = null;
		SmbFile targetDirectory = null;
		SmbFile targetFile = null;
		SmbFileInputStream smbIn = null;
		Long fileLength;

		if (filePath != null && filePath.length() > 0) {
			conf = ConfigurationFinder.findConf();
			try {
				String ntDomain = ConfigurationFinder.findProperty(conf,
						"cid.ejb.ntDomain");

				String ntUser = ConfigurationFinder.findProperty(conf,
						"cid.ejb.ntUser");

				String ntPassword = ConfigurationFinder.findProperty(conf,
						"cid.ejb.ntPassword");

				// String smbFilePath = convertSmbNamedPipe(filePath);
				String smbFilePath = convertPath(filePath,
						SMB_NAMED_PIPE_PREFIX, "/");

				// Authenticate to target directory
				targetDirectory = new SmbFile(smbFilePath,
						new NtlmPasswordAuthentication(ntDomain, ntUser,
								ntPassword));

				// Create target SmbFile to be read
				targetFile = new SmbFile(targetDirectory, fileName);

				// Read file content from target
				smbIn = new SmbFileInputStream(targetFile);
				fileLength = targetFile.length();
				fileContent = new byte[fileLength.intValue()];

				if (smbIn.read(fileContent) != fileLength)
					throw new IOException("Reading incomplete SMB file bytes");
			} catch (CidConfigException ce) {
				throw new CidFileException(
						CidConfigException.READ_FAILED, ce, false);
			} catch (MalformedURLException mue) {
				throw new CidFileException("SMB file server url is invalid",
						mue, false);
			} catch (UnknownHostException uhe) {
				throw new CidFileException(
						"SMB file server can not be resolved", uhe, false);
			} catch (SmbException se) {
				throw new CidFileException(
						"Reading file from SMB file server failed", se, false);
			} catch (IOException ioe) {
				throw new CidFileException("File copy failed", ioe, false);
			} finally {
				if (smbIn != null) {
					try {
						smbIn.close();
					} catch (IOException ioe) {
						throw new CidFileException(String.format(
								"Failed to close SMB file: %s%n", targetFile
										.getName()), ioe, false);
					}
				}
			}
		}

		return fileContent; // return the content of the target file
	}

	public static Date getNTFileLastModifiedDate(String filePath,
			String fileName) throws CidFileException, CidConfigException {
		String logContent = String.format("%s(filePath[%s], fileName[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				filePath, fileName);
		cidLogger.debug(FileSystemManager.class, logContent);

		Properties conf = null;
		Date lastModifiedDate = null;
		SmbFile targetDirectory = null;
		SmbFile targetFile = null;

		if (filePath != null && filePath.length() > 0) {
			conf = ConfigurationFinder.findConf();
			try {
				String ntDomain = ConfigurationFinder.findProperty(conf,
						"cid.ejb.ntDomain");

				String ntUser = ConfigurationFinder.findProperty(conf,
						"cid.ejb.ntUser");

				String ntPassword = ConfigurationFinder.findProperty(conf,
						"cid.ejb.ntPassword");

				String responseTimeout = ConfigurationFinder.findProperty(conf,
						"cid.ejb.jcifs.responseTimeout");

				String soTimeout = ConfigurationFinder.findProperty(conf,
						"cid.ejb.jcifs.soTimeout");

				// String smbFilePath = convertSmbNamedPipe(filePath);
				String smbFilePath = convertPath(filePath,
						SMB_NAMED_PIPE_PREFIX, "/");

				Config.setProperty("jcifs.resolveOrder",
						"LMHOSTS,WINS,DNS,BCAST");
				Config.setProperty("jcifs.smb.client.responseTimeout",
						responseTimeout);
				Config.setProperty("jcifs.smb.client.soTimeout", soTimeout);

				// Authenticate to target directory
				targetDirectory = new SmbFile(smbFilePath,
						new NtlmPasswordAuthentication(ntDomain, ntUser,
								ntPassword));

				// Create target SmbFile to be read
				targetFile = new SmbFile(targetDirectory, fileName);
				lastModifiedDate = new Date(targetFile.getLastModified());
			} catch (CidConfigException ce) {
				throw new CidFileException(
						CidConfigException.READ_FAILED, ce, false);
			} catch (MalformedURLException mue) {
				throw new CidFileException("SMB file server url is invalid",
						mue, false);
			} catch (UnknownHostException uhe) {
				throw new CidFileException(
						"SMB file server can not be resolved", uhe, false);
			}
		}

		return lastModifiedDate; // return the content of the target file
	}

	public static String convertPath(String sourcePath, String destPrefix,
			String separator) {
		String logContent = String.format("%s(sourcePath[%s], destPrefix[%s], separator[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				sourcePath, destPrefix, separator);
		cidLogger.debug(FileSystemManager.class, logContent);

		StringTokenizer st = new StringTokenizer(sourcePath, "\\/");
		List<String> pathTokens = new ArrayList<String>();
		while (st.hasMoreTokens()) {
			pathTokens.add(st.nextToken());
		}

		StringBuilder sb = new StringBuilder(destPrefix);
		for (String token : pathTokens) {
			sb.append(token);
			sb.append(separator);
		}
		return sb.toString();
	}

	public static void archiveFilesBefore(Date asOfDate, String path)
			throws CidFileException {
		String logContent = String.format("%s(asOfDate[%s], path[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				asOfDate.toString(), path);
		cidLogger.debug(FileSystemManager.class, logContent);
		
		File fileToArchive = new File(path);

		if (fileToArchive.exists())
			if (fileToArchive.isDirectory()) {
				cidLogger.debug("HousekeepLogger", "Archive Path: " + path);
				File[] files = fileToArchive.listFiles();
				for (File file : files) {
					boolean success = false;
					Date lastModified = new Date(file.lastModified());
					if (lastModified.before(asOfDate)) {
						cidLogger
								.info(
										"HousekeepLogger",
										String
												.format(
														"Archiving file/directory: %s, last modified: %s",
														file.getAbsolutePath(),
														lastModified.toString()));

						success = deleteArchiveDirectory(path, file.getName(),
								asOfDate);
						if (!success) {
							cidLogger
									.error(
											"HousekeepLogger",
											new CidFileException(
													String
															.format(
																	"Failed to delete file/directory: %s%n",
																	file
																			.getAbsolutePath()),
													true));
						}
					}
				}
			}
	}

	public static void archiveFileBefore(Date asOfDate, String path)
			throws CidFileException {
		String logContent = String.format("%s(asOfDate[%s], path[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				asOfDate, path);
		cidLogger.debug(FileSystemManager.class, logContent);
		
		File fileToArchive = new File(path);

		if (fileToArchive.exists())
			if (fileToArchive.isDirectory()) {
				cidLogger.debug("HousekeepLogger", "Archive Path: " + path);
				File[] files = fileToArchive.listFiles();
				for (File file : files) {
					boolean success = false;
					Date lastModified = new Date(file.lastModified());
					if (lastModified.before(asOfDate)) {
						cidLogger.info("HousekeepLogger", String.format(
								"Archiving file: %s, last modified: %s", file
										.getAbsolutePath(), lastModified
										.toString()));

						success = deleteFile(file);
						if (!success) {
							throw new CidFileException(String.format(
									"Failed to delete file/directory: %s%n",
									file.getAbsolutePath()), false);
						}
					}
				}
			}
	}
}
