/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.fmk.util;

import hk.org.ha.service.biz.image.model.fmk.exception.CidHa7Exception;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class Ha7Parser {

	private static CidLogger cidLogger = CidLoggerFactory.produceLogger();
	// added on 2011-03-02 by Boris for fixing JAXB Context new instance bug
	private static JAXBContext ctx;
	private static JAXBException je;
	static {
		try {
			ctx = JAXBContext
					.newInstance("hk.org.ha.service.biz.image.model.fmk.util.ha7");
		} catch (JAXBException je) {
			Ha7Parser.je = je;
		}
	}

	// added on 2011-03-02 by Boris for fixing JAXB Context new instance bug

	public CIDData unmarshalHa7Message(String ha7Message)
			throws CidHa7Exception {
		String logContent = String.format("%s(ha7Message[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(), ha7Message);
		cidLogger.debug(this.getClass(), logContent);
		
		try {
			URL xsdUrl = this.getClass().getClassLoader()
					.getResource("HA7.xsd");

			// added on 2011-03-02 by Boris for fixing JAXB Context new instance
			// bug
			if (ctx == null)
				throw je;
			Unmarshaller u = ctx.createUnmarshaller();
			SchemaFactory schemaFactory = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			Schema schema = schemaFactory.newSchema(xsdUrl);
			u.setSchema(schema);

			CIDData cidData = (CIDData) u.unmarshal(new StreamSource(
					new StringReader(ha7Message)));

			return cidData;
		} catch (UnmarshalException ue) {
			throw new CidHa7Exception("HA7 message unmarshalling failed", ue,
					false);
		} catch (JAXBException je) {
			throw new CidHa7Exception("HA7 message serializing failed", je,
					false);
		} catch (SAXException se) {
			throw new CidHa7Exception("HA7 message schema matching failed", se,
					true);
		}
	}

	public String marshalHa7Message(CIDData cidData) throws CidHa7Exception {
		String logContent = String.format("%s(cidData[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(), cidData.toString());
		cidLogger.debug(this.getClass(), logContent);
		
		try {
			// added on 2011-03-02 by Boris for fixing JAXB Context new instance
			if (ctx == null)
				throw je;

			Marshaller m = ctx.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			m.marshal(cidData, sw);

			return sw.toString();
		} catch (JAXBException je) {
			throw new CidHa7Exception("HA7 message serializing failed", je,
					false);
		}
	}
}
