/**
 * Change Request History:
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import hk.org.ha.service.biz.image.model.audit.AuditLocal;
import hk.org.ha.service.biz.image.model.fmk.exception.CidRuntimeException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.retrieval.ImageRetrievalLocal;
import hk.org.ha.service.biz.image.model.security.SecurityManagerLocal;
import hk.org.ha.service.biz.image.model.template.ImageTemplateLocal;
import hk.org.ha.service.biz.image.model.upload.ImageUploadLocal;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class LocalEjbLocator {

	private static CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public static ImageUploadLocal locateImageUploadEjb() {
		String logContent = String.format("%s()%n",
				Thread.currentThread().getStackTrace()[1].getMethodName());
		cidLogger.debug(LocalEjbLocator.class, logContent);
		
		return (ImageUploadLocal) localLookup("ejb/ImageUploadLocal");
	}

	public static ImageRetrievalLocal locateImageRetrievalEjb() {
		String logContent = String.format("%s()%n",
				Thread.currentThread().getStackTrace()[1].getMethodName());
		cidLogger.debug(LocalEjbLocator.class, logContent);
		
		return (ImageRetrievalLocal) localLookup("ejb/ImageRetrievalLocal");
	}

	public static ImageTemplateLocal locateImageTemplateEjb() {
		String logContent = String.format("%s()%n",
				Thread.currentThread().getStackTrace()[1].getMethodName());
		cidLogger.debug(LocalEjbLocator.class, logContent);
		
		return (ImageTemplateLocal) localLookup("ejb/ImageTemplateLocal");
	}

	public static SecurityManagerLocal locateSecutiryManagerEjb() {
		String logContent = String.format("%s()%n",
				Thread.currentThread().getStackTrace()[1].getMethodName());
		cidLogger.debug(LocalEjbLocator.class, logContent);
		
		return (SecurityManagerLocal) localLookup("ejb/SecurityManagerLocal");
	}
	
	public static AuditLocal locateAuditEjb() {
		String logContent = String.format("%s()%n",
				Thread.currentThread().getStackTrace()[1].getMethodName());
		cidLogger.debug(LocalEjbLocator.class, logContent);
		
		return (AuditLocal) localLookup("ejb/AuditLocal");
	}

	private static Object localLookup(String name) {
		String logContent = String.format("%s(name[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(), name);
		cidLogger.debug(LocalEjbLocator.class, logContent);
		Context ctx = null;
		Object obj = null;
		
		try {
			ctx = new InitialContext();
			ctx = (Context) ctx.lookup("java:comp/env");
			obj = ctx.lookup(name);
		} catch (NamingException ne) {
			throw new CidRuntimeException("JNDI look up failed", ne, false);
		}
		
		return obj;
	}

}
