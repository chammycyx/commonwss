/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;

/**
 * Handler for SOAP header.
 * 
 * @author LTC638
 * @version 1.0 2010-06-24
 * @since 1.0 2010-06-24
 */
public class SOAPHeaderHandler implements SOAPHandler<SOAPMessageContext> {

	private String username;
	private String password;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public SOAPHeaderHandler(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public Set<QName> getHeaders() {
		return null;
	}

	public void close(MessageContext mc) {
	}

	public boolean handleFault(SOAPMessageContext smc) {
		return true;
	}

	public boolean handleMessage(SOAPMessageContext smc) {
		boolean outboundProperty = (Boolean) smc
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		
		if (outboundProperty) {
			try {
				SOAPEnvelope envelope = smc.getMessage().getSOAPPart()
						.getEnvelope();
				if (envelope.getHeader() != null) {
					envelope.getHeader().detachNode();
				}
				SOAPHeader header = envelope.addHeader();

				SOAPElement securityEle = header
						.addChildElement(
								"Security",
								"wsse",
								"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

				SOAPElement usernameTokenEle = securityEle.addChildElement(
						"UsernameToken", "wsse");
				usernameTokenEle
						.addAttribute(
								new QName("xmlns:wsu"),
								"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

				SOAPElement usernameEle = usernameTokenEle.addChildElement(
						"Username", "wsse");
				usernameEle.addTextNode(username);

				SOAPElement passwordTokenEle = usernameTokenEle
						.addChildElement("Password", "wsse");
				passwordTokenEle
						.setAttribute(
								"Type",
								"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
				passwordTokenEle.addTextNode(password);
			} catch (Exception e) {
				cidLogger.error(this.getClass(), e);
			}
		}
		
		return outboundProperty;
	}

}
