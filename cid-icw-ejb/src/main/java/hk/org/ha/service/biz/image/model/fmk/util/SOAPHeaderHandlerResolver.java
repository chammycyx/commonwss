/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * Handler resolver for SOAP header handler.
 * 
 * @author LTC638
 * @version 1.0 2010-06-24
 * @since 1.0 2010-06-24
 */
public class SOAPHeaderHandlerResolver implements HandlerResolver {

	private SOAPHandler<SOAPMessageContext> handler;

	public SOAPHeaderHandlerResolver(SOAPHandler<SOAPMessageContext> handler) {
		this.handler = handler;
	}

	@SuppressWarnings("unchecked")
	public List<Handler> getHandlerChain(PortInfo portInfo) {
		List<Handler> handlerChain = new ArrayList<Handler>();
		handlerChain.add(handler);
		return handlerChain;
	}

}
