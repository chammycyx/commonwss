/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-534: Fix Common Web Service timeout configuration is ignored by WebLogic [Boris HO 2013-01-21]
 */

package hk.org.ha.service.biz.image.model.fmk.util;

import hk.org.ha.service.biz.image.endpoint.eprcid.EprCidWSS;
import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import weblogic.wsee.jaxws.JAXWSProperties;
import weblogic.wsee.security.unt.ClientUNTCredentialProvider;
import weblogic.xml.crypto.wss.WSSecurityContext;
import weblogic.xml.crypto.wss.provider.CredentialProvider;

public class WebServiceLocator {

	private static boolean isWeblogicEnv = false;
	private static CidLogger cidLogger = CidLoggerFactory.produceLogger();
	private static volatile EprCidWSS ePRProxy = null;
	private static volatile BindingProvider bp = null;

	static {
		URL u = WebServiceLocator.class.getClassLoader().getResource(
				"weblogic/wsee/security/unt/ClientUNTCredentialProvider.class");

		if (u != null)
			isWeblogicEnv = true;
		else
			isWeblogicEnv = false;
	}

	public static EprCidWSS locateEprCidWebS() throws CidConfigException,
			MalformedURLException {
		String logContent = String.format("%s()%n",
				Thread.currentThread().getStackTrace()[1].getMethodName());
		cidLogger.debug(WebServiceLocator.class, logContent);
		
		if (ePRProxy == null) {
			synchronized (WebServiceLocator.class) {
				if (ePRProxy == null) {
					Properties conf = ConfigurationFinder.findConf();

					String wsdlUrl = ConfigurationFinder.findProperty(conf,
							"cid.ws.eprcid.wsdlUrl");

					String username = ConfigurationFinder.findProperty(conf,
							"cid.ws.eprcid.username");

					String password = ConfigurationFinder.findProperty(conf,
							"cid.ws.eprcid.password");

					String serviceNs = ConfigurationFinder.findProperty(conf,
							"cid.ws.eprcid.serviceNs");

					String serviceName = ConfigurationFinder.findProperty(conf,
							"cid.ws.eprcid.serviceName");

					String portNs = ConfigurationFinder.findProperty(conf,
							"cid.ws.eprcid.portNs");

					String portName = ConfigurationFinder.findProperty(conf,
							"cid.ws.eprcid.portName");

					String requestTimeout = ConfigurationFinder.findProperty(
							conf, "cid.ws.eprcid.requestTimeout");

					URL wsdlLocation = new URL(wsdlUrl);
					QName serviceQname = new QName(serviceNs, serviceName);
					QName portQname = new QName(portNs, portName);
					Service service = Service
							.create(wsdlLocation, serviceQname);

					if (isWeblogicEnv) {
						ePRProxy = service.getPort(EprCidWSS.class);
						bp = (BindingProvider) ePRProxy;
						List<CredentialProvider> cProviderList = new ArrayList<CredentialProvider>();
						CredentialProvider cProvider = new ClientUNTCredentialProvider(
								username.getBytes(Charset.forName("UTF-8")),
								password.getBytes(Charset.forName("UTF-8")));
						cProviderList.add(cProvider);
						bp.getRequestContext().put(
								WSSecurityContext.CREDENTIAL_PROVIDER_LIST,
								cProviderList);
					} else {
						SOAPHeaderHandler handler = new SOAPHeaderHandler(
								username, password);
						service
								.setHandlerResolver(new SOAPHeaderHandlerResolver(
										handler));
						ePRProxy = service.getPort(portQname, EprCidWSS.class);
						bp = (BindingProvider) ePRProxy;
					}
					// Apply weblogic.wsee.jaxws package constant instead of com.sun.xml.internal.ws.developer
					// Because the constant value is changed starting from JAXWS version 2.2.0
					// JAXWS version 2.1.7: com.sun.xml.internal.ws.request.timeout 
					// JAXWS version 2.2.0+: com.sun.xml.ws.request.timeout 
					bp.getRequestContext().put(JAXWSProperties.REQUEST_TIMEOUT,
							Integer.parseInt(requestTimeout));
				}
			}
		}

		return ePRProxy;
	}
}
