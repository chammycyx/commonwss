/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.fmk.util.ha7;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "CIDData")
public class CIDData {

	@XmlElement(name = "MessageDtl", required = true)
	protected CIDData.MessageDtl messageDtl;
	@XmlElement(name = "PatientDtl", required = true)
	protected CIDData.PatientDtl patientDtl;
	@XmlElement(name = "VisitDtl", required = true)
	protected CIDData.VisitDtl visitDtl;
	@XmlElement(name = "StudyDtl", required = true)
	protected CIDData.StudyDtl studyDtl;

	public CIDData.MessageDtl getMessageDtl() {
		return messageDtl;
	}

	public void setMessageDtl(CIDData.MessageDtl value) {
		this.messageDtl = value;
	}

	public CIDData.PatientDtl getPatientDtl() {
		return patientDtl;
	}

	public void setPatientDtl(CIDData.PatientDtl value) {
		this.patientDtl = value;
	}

	public CIDData.VisitDtl getVisitDtl() {
		return visitDtl;
	}

	public void setVisitDtl(CIDData.VisitDtl value) {
		this.visitDtl = value;
	}

	public CIDData.StudyDtl getStudyDtl() {
		return studyDtl;
	}

	public void setStudyDtl(CIDData.StudyDtl value) {
		this.studyDtl = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {

	})
	public static class MessageDtl {

		@XmlElement(name = "TransactionCode", required = true)
		protected String transactionCode;
		@XmlElement(name = "ServerHosp", required = true)
		protected String serverHosp;
		@XmlElement(name = "TransactionDtm", required = true)
		protected String transactionDtm;
		@XmlElement(name = "TransactionID", required = true)
		protected String transactionId;
		@XmlElement(name = "SendingApplication", required = true)
		protected String sendingApplication;

		public String getTransactionCode() {
			return transactionCode;
		}

		public void setTransactionCode(String value) {
			this.transactionCode = value;
		}

		public String getServerHosp() {
			return serverHosp;
		}

		public void setServerHosp(String value) {
			this.serverHosp = value;
		}

		public String getTransactionDtm() {
			return transactionDtm;
		}

		public void setTransactionDtm(String value) {
			this.transactionDtm = value;
		}

		public String getTransactionId() {
			return transactionId;
		}

		public void setTransactionId(String value) {
			this.transactionId = value;
		}

		public String getSendingApplication() {
			return sendingApplication;
		}

		public void setSendingApplication(String value) {
			this.sendingApplication = value;
		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {

	})
	public static class PatientDtl {

		@XmlElement(name = "HKID", required = true)
		protected String hkid;
		@XmlElement(name = "PatKey", required = true)
		protected String patKey;
		@XmlElement(name = "PatName", required = true)
		protected String patName;
		@XmlElement(name = "PatDOB", required = true)
		protected String patDob;
		@XmlElement(name = "PatSex", required = true)
		protected String patSex;
		@XmlElement(name = "DeathIndicator")
		protected String deathIndicator;

		public String getHkid() {
			return hkid;
		}

		public void setHkid(String value) {
			this.hkid = value;
		}

		public String getPatKey() {
			return patKey;
		}

		public void setPatKey(String value) {
			this.patKey = value;
		}

		public String getPatName() {
			return patName;
		}

		public void setPatName(String value) {
			this.patName = value;
		}

		public String getPatDob() {
			return patDob;
		}

		public void setPatDob(String value) {
			this.patDob = value;
		}

		public String getPatSex() {
			return patSex;
		}

		public void setPatSex(String value) {
			this.patSex = value;
		}

		public String getDeathIndicator() {
			return deathIndicator;
		}

		public void setDeathIndicator(String value) {
			this.deathIndicator = value;
		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {

	})
	public static class StudyDtl {

		@XmlElement(name = "AccessionNo", required = true)
		protected String accessionNo;
		@XmlElement(name = "StudyID", required = true)
		protected String studyId;
		@XmlElement(name = "StudyType", required = true)
		protected String studyType;
		@XmlElement(name = "StudyDtm", required = true)
		protected String studyDtm;
		@XmlElement(name = "Remark")
		protected String remark;
		@XmlElement(name = "SeriesDtls")
		protected CIDData.StudyDtl.SeriesDtls seriesDtls;

		public String getAccessionNo() {
			return accessionNo;
		}

		public void setAccessionNo(String value) {
			this.accessionNo = value;
		}

		public String getStudyId() {
			return studyId;
		}

		public void setStudyId(String value) {
			this.studyId = value;
		}

		public String getStudyType() {
			return studyType;
		}

		public void setStudyType(String value) {
			this.studyType = value;
		}

		public String getStudyDtm() {
			return studyDtm;
		}

		public void setStudyDtm(String value) {
			this.studyDtm = value;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String value) {
			this.remark = value;
		}

		public CIDData.StudyDtl.SeriesDtls getSeriesDtls() {
			return seriesDtls;
		}

		public void setSeriesDtls(CIDData.StudyDtl.SeriesDtls value) {
			this.seriesDtls = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "seriesDtl" })
		public static class SeriesDtls {

			@XmlElement(name = "SeriesDtl")
			protected List<CIDData.StudyDtl.SeriesDtls.SeriesDtl> seriesDtl;

			public List<CIDData.StudyDtl.SeriesDtls.SeriesDtl> getSeriesDtl() {
				if (seriesDtl == null) {
					seriesDtl = new ArrayList<CIDData.StudyDtl.SeriesDtls.SeriesDtl>();
				}
				return this.seriesDtl;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {

			})
			public static class SeriesDtl {

				@XmlElement(name = "SeriesNo")
				protected String seriesNo;
				@XmlElement(name = "ExamType")
				protected String examType;
				@XmlElement(name = "ExamDtm")
				protected String examDtm;
				@XmlElement(name = "EntityID")
				protected BigInteger entityId;
				@XmlElement(name = "ImageDtls")
				protected CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls imageDtls;

				public String getSeriesNo() {
					return seriesNo;
				}

				public void setSeriesNo(String value) {
					this.seriesNo = value;
				}

				public String getExamType() {
					return examType;
				}

				public void setExamType(String value) {
					this.examType = value;
				}

				public String getExamDtm() {
					return examDtm;
				}

				public void setExamDtm(String value) {
					this.examDtm = value;
				}

				public BigInteger getEntityId() {
					return entityId;
				}

				public void setEntityId(BigInteger value) {
					this.entityId = value;
				}

				public CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls getImageDtls() {
					return imageDtls;
				}

				public void setImageDtls(
						CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls value) {
					this.imageDtls = value;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "imageDtl" })
				public static class ImageDtls {

					@XmlElement(name = "ImageDtl")
					protected List<CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl> imageDtl;

					public List<CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl> getImageDtl() {
						if (imageDtl == null) {
							imageDtl = new ArrayList<CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl>();
						}
						return this.imageDtl;
					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = {

					})
					public static class ImageDtl {

						@XmlElement(name = "ImageFormat")
						protected String imageFormat;
						@XmlElement(name = "ImagePath")
						protected String imagePath;
						@XmlElement(name = "ImageFile")
						protected String imageFile;
						@XmlElement(name = "ImageID")
						protected String imageId;
						@XmlElement(name = "ImageVersion")
						protected String imageVersion;
						@XmlElement(name = "ImageSequence")
						protected BigInteger imageSequence;
						@XmlElement(name = "ImageKeyword")
						protected String imageKeyword;
						@XmlElement(name = "ImageHandling")
						protected String imageHandling;
						@XmlElement(name = "ImageStatus")
						protected BigInteger imageStatus;
						@XmlElement(name = "ImageType")
						protected String imageType;
						@XmlElement(name = "AnnotationDtls")
						protected CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls annotationDtls;

						public String getImageFormat() {
							return imageFormat;
						}

						public void setImageFormat(String value) {
							this.imageFormat = value;
						}

						public String getImagePath() {
							return imagePath;
						}

						public void setImagePath(String value) {
							this.imagePath = value;
						}

						public String getImageFile() {
							return imageFile;
						}

						public void setImageFile(String value) {
							this.imageFile = value;
						}

						public String getImageId() {
							return imageId;
						}

						public void setImageId(String value) {
							this.imageId = value;
						}

						public String getImageVersion() {
							return imageVersion;
						}

						public void setImageVersion(String value) {
							this.imageVersion = value;
						}

						public BigInteger getImageSequence() {
							return imageSequence;
						}

						public void setImageSequence(BigInteger value) {
							this.imageSequence = value;
						}

						public String getImageKeyword() {
							return imageKeyword;
						}

						public void setImageKeyword(String value) {
							this.imageKeyword = value;
						}

						public String getImageHandling() {
							return imageHandling;
						}

						public void setImageHandling(String value) {
							this.imageHandling = value;
						}

						public BigInteger getImageStatus() {
							return imageStatus;
						}

						public void setImageStatus(BigInteger value) {
							this.imageStatus = value;
						}

						public String getImageType() {
							return imageType;
						}

						public void setImageType(String value) {
							this.imageType = value;
						}

						public CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls getAnnotationDtls() {
							return annotationDtls;
						}

						public void setAnnotationDtls(
								CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls value) {
							this.annotationDtls = value;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "annotationDtl" })
						public static class AnnotationDtls {

							@XmlElement(name = "AnnotationDtl")
							protected List<CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl> annotationDtl;

							public List<CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl> getAnnotationDtl() {
								if (annotationDtl == null) {
									annotationDtl = new ArrayList<CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl>();
								}
								return this.annotationDtl;
							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "", propOrder = {

							})
							public static class AnnotationDtl {

								@XmlElement(name = "AnnotationSeq")
								protected BigInteger annotationSeq;
								@XmlElement(name = "AnnotationType")
								protected String annotationType;
								@XmlElement(name = "AnnotationText")
								protected String annotationText;
								@XmlElement(name = "AnnotationCoordinate")
								protected String annotationCoordinate;
								@XmlElement(name = "AnnotationStatus")
								protected String annotationStatus;
								@XmlElement(name = "AnnotationEditable")
								protected String annotationEditable;
								@XmlElement(name = "AnnotationUpdDtm")
								protected String annotationUpdDtm;

								public BigInteger getAnnotationSeq() {
									return annotationSeq;
								}

								public void setAnnotationSeq(BigInteger value) {
									this.annotationSeq = value;
								}

								public String getAnnotationType() {
									return annotationType;
								}

								public void setAnnotationType(String value) {
									this.annotationType = value;
								}

								public String getAnnotationText() {
									return annotationText;
								}

								public void setAnnotationText(String value) {
									this.annotationText = value;
								}

								public String getAnnotationCoordinate() {
									return annotationCoordinate;
								}

								public void setAnnotationCoordinate(String value) {
									this.annotationCoordinate = value;
								}

								public String getAnnotationStatus() {
									return annotationStatus;
								}

								public void setAnnotationStatus(String value) {
									this.annotationStatus = value;
								}

								public String getAnnotationEditable() {
									return annotationEditable;
								}

								public void setAnnotationEditable(String value) {
									this.annotationEditable = value;
								}

								public String getAnnotationUpdDtm() {
									return annotationUpdDtm;
								}

								public void setAnnotationUpdDtm(String value) {
									this.annotationUpdDtm = value;
								}

							}

						}

					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {

	})
	public static class VisitDtl {

		@XmlElement(name = "VisitHosp", required = true)
		protected String visitHosp;
		@XmlElement(name = "VisitSpec")
		protected String visitSpec;
		@XmlElement(name = "VisitWardClinic")
		protected String visitWardClinic;
		@XmlElement(name = "PayCode")
		protected String payCode;
		@XmlElement(name = "CaseNum", required = true)
		protected String caseNum;
		@XmlElement(name = "AdtDtm", required = true)
		protected String adtDtm;

		public String getVisitHosp() {
			return visitHosp;
		}

		public void setVisitHosp(String value) {
			this.visitHosp = value;
		}

		public String getVisitSpec() {
			return visitSpec;
		}

		public void setVisitSpec(String value) {
			this.visitSpec = value;
		}

		public String getVisitWardClinic() {
			return visitWardClinic;
		}

		public void setVisitWardClinic(String value) {
			this.visitWardClinic = value;
		}

		public String getPayCode() {
			return payCode;
		}

		public void setPayCode(String value) {
			this.payCode = value;
		}

		public String getCaseNum() {
			return caseNum;
		}

		public void setCaseNum(String value) {
			this.caseNum = value;
		}

		public String getAdtDtm() {
			return adtDtm;
		}

		public void setAdtDtm(String value) {
			this.adtDtm = value;
		}

	}

}
