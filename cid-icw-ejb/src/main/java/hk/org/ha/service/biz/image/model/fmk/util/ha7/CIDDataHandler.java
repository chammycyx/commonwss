/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util.ha7;

import hk.org.ha.service.biz.image.model.fmk.exception.CidHa7Exception;

import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.FileSystemManager;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData.StudyDtl.SeriesDtls.SeriesDtl;

/**
 * @author Boris Ho
 * 
 */
public class CIDDataHandler {
	private static CidLogger cidLogger = CidLoggerFactory.produceLogger();
	
	public static SeriesDtl getSeriesDtlByKey(CIDData cidData, String key) throws CidHa7Exception {
		cidLogger.debug(FileSystemManager.class, String.format("Invoking %s",
				Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		SeriesDtl foundSeriesDtl = null;
		
		if(cidData == null)
			throw new CidHa7Exception("CIDData in HA7 is empty", false);
		
		for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
				.getSeriesDtl()) {
			if (seriesDtl.getSeriesNo().equals(key))
				foundSeriesDtl = seriesDtl;
		}

		return foundSeriesDtl;
	}

	public static ImageDtl getImageDtlByKey(CIDData cidData, String key) throws CidHa7Exception {
		cidLogger.debug(FileSystemManager.class, String.format("Invoking %s",
				Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		ImageDtl foundImageDtl = null;
		
		if(cidData == null)
			throw new CidHa7Exception("CIDData in HA7 is empty", false);
		
		for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
				.getSeriesDtl()) {
			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				if (imageDtl.getImageId().equals(key))
					foundImageDtl = imageDtl;
			}
		}

		return foundImageDtl;
	}

	public static ImageDtl getImageDtlByKey(SeriesDtl seriesDtl, String key) throws CidHa7Exception {
		cidLogger.debug(FileSystemManager.class, String.format("Invoking %s",
				Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		ImageDtl foundImageDtl = null;

		if(seriesDtl == null)
			throw new CidHa7Exception("Series Details of CIDData in HA7 is empty", false);
		
		for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
			if (imageDtl.getImageId().equals(key))
				foundImageDtl = imageDtl;
		}

		return foundImageDtl;
	}
}
