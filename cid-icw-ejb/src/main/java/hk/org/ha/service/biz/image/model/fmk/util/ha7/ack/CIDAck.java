/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.fmk.util.ha7.ack;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "CIDAck")
public class CIDAck {

    @XmlElement(name = "MessageDtl", required = true)
    protected CIDAck.MessageDtl messageDtl;
    @XmlElement(name = "AckDtl", required = true)
    protected CIDAck.AckDtl ackDtl;

    public CIDAck.MessageDtl getMessageDtl() {
        return messageDtl;
    }

    public void setMessageDtl(CIDAck.MessageDtl value) {
        this.messageDtl = value;
    }

    public CIDAck.AckDtl getAckDtl() {
        return ackDtl;
    }

    public void setAckDtl(CIDAck.AckDtl value) {
        this.ackDtl = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class AckDtl {

        @XmlElement(name = "AccessionNo", required = true)
        protected String accessionNo;
        @XmlElement(name = "StudyID", required = true)
        protected String studyId;
        @XmlElement(name = "StudyDtm", required = true)
        protected String studyDtm;
        @XmlElement(name = "ActionDtm", required = true)
        protected String actionDtm;
        @XmlElement(name = "ActionStatus", required = true)
        protected String actionStatus;
        @XmlElement(name = "ActionRemarks")
        protected String actionRemarks;

        public String getAccessionNo() {
            return accessionNo;
        }

        public void setAccessionNo(String value) {
            this.accessionNo = value;
        }

        public String getStudyId() {
            return studyId;
        }

        public void setStudyId(String value) {
            this.studyId = value;
        }

        public String getStudyDtm() {
            return studyDtm;
        }

        public void setStudyDtm(String value) {
            this.studyDtm = value;
        }

        public String getActionDtm() {
            return actionDtm;
        }

        public void setActionDtm(String value) {
            this.actionDtm = value;
        }

        public String getActionStatus() {
            return actionStatus;
        }

        public void setActionStatus(String value) {
            this.actionStatus = value;
        }

        public String getActionRemarks() {
            return actionRemarks;
        }

        public void setActionRemarks(String value) {
            this.actionRemarks = value;
        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class MessageDtl {

        @XmlElement(name = "TransactionCode", required = true)
        protected String transactionCode;
        @XmlElement(name = "ServerHosp", required = true)
        protected String serverHosp;
        @XmlElement(name = "TransactionDtm", required = true)
        protected String transactionDtm;
        @XmlElement(name = "TransactionID", required = true)
        protected String transactionId;
        @XmlElement(name = "SendingApplication", required = true)
        protected String sendingApplication;

        public String getTransactionCode() {
            return transactionCode;
        }

        public void setTransactionCode(String value) {
            this.transactionCode = value;
        }

        public String getServerHosp() {
            return serverHosp;
        }

        public void setServerHosp(String value) {
            this.serverHosp = value;
        }

        public String getTransactionDtm() {
            return transactionDtm;
        }

        public void setTransactionDtm(String value) {
            this.transactionDtm = value;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String value) {
            this.transactionId = value;
        }

        public String getSendingApplication() {
            return sendingApplication;
        }

        public void setSendingApplication(String value) {
            this.sendingApplication = value;
        }

    }

}
