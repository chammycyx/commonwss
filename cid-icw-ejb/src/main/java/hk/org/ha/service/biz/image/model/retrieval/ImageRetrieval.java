/**
 * 
 */
package hk.org.ha.service.biz.image.model.retrieval;

import javax.ejb.Remote;

/**
 * EJB remote interface exposed for <code>ImageRetrievalBean</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Remote
public interface ImageRetrieval {

}
