/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-731: Issue and performance degrade found in TWE production drill [Boris HO 2013-06-21]
 */

package hk.org.ha.service.biz.image.model.retrieval;

import hk.org.ha.service.biz.image.model.audit.AuditLocal;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.retrieval.bo.ImageRetrievalBoFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(mappedName = "ejb/ImageRetrieval")
public class ImageRetrievalBean implements ImageRetrieval, ImageRetrievalLocal {

	@PersistenceContext
	private EntityManager em;
	@EJB
	private AuditLocal al;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public byte[] exportImage(String ha7Msg, String password, String userId,
			String workstationId, String requestSys, String eventId) {
		String logContent = String
				.format(
						"%s(ha7Msg[%s], password[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), ha7Msg, password, userId,
						workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			byte[] result;
			Map<String, String> ev = new WeakHashMap<String, String>();
			al.writeLog("", "", workstationId, eventId, ev, "", "", requestSys,
					"", userId, ha7Msg);

			result = ImageRetrievalBoFactory.produceImageRetrievalBo(em)
					.exportImage(ha7Msg, password);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public byte[] getCidImage(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String imageId, String userId,
			String workstationId, String requestSys, String eventId) {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], imageId[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo, imageId,
						userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			byte[] result;
			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<AccessionNo>", accessionNo);
			ev.put("<SeriesNo>", seriesNo);
			ev.put("<ImageSeqNo>", imageSeqNo);
			ev.put("<VersionNo>", versionNo);
			ev.put("<ImageId>", imageId);
			al.writeLog(accessionNo, caseNo, workstationId, eventId, ev,
					hospCde, patientKey, requestSys, "", userId, "");

			result = ImageRetrievalBoFactory.produceImageRetrievalBo(em)
					.getCidImage(patientKey, hospCde, caseNo, accessionNo,
							seriesNo, imageSeqNo, versionNo, imageId);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public byte[] getCidImageThumbnail(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String imageId, String userId,
			String workstationId, String requestSys) {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], imageId[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo, imageId,
						userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			byte[] result;
			result = ImageRetrievalBoFactory.produceImageRetrievalBo(em)
					.getCidImageThumbnail(patientKey, hospCde, caseNo,
							accessionNo, seriesNo, imageSeqNo, versionNo,
							imageId);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public byte[] getBufferedCidImage(String path, String filename,
			boolean isThumbnail, String userId, String workstationId,
			String requestSys) {
		String logContent = String
				.format(
						"%s(path[%s], filename[%s], isThumbnail[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), path, filename, isThumbnail,
						userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			byte[] result;
			result = ImageRetrievalBoFactory.produceImageRetrievalBo(em)
					.getBufferedCidImage(path, filename, isThumbnail);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public String getCidStudy(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String userId, String workstationId,
			String requestSys, String eventId) {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo, userId,
						workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			String result;
			
			result = ImageRetrievalBoFactory.produceImageRetrievalBo(em)
					.getCidStudy(patientKey, hospCde, caseNo, accessionNo,
							seriesNo, imageSeqNo, versionNo);
			
			// Audit log content included result, so process result before writing audit log
			Map<String, String> ev = new HashMap<String, String>();
			ev.put("<AccessionNo>", accessionNo);
			ev.put("<SeriesNo>", seriesNo);
			ev.put("<ImageSeqNo>", imageSeqNo);
			ev.put("<VersionNo>", versionNo);
			al.writeLog(accessionNo, caseNo, workstationId, eventId, ev,
					hospCde, patientKey, requestSys, "", userId, result);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public String getCidStudyFirstLastImage(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String userId,
			String workstationId, String requestSys, String eventId) {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo, userId,
						workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);
		try {
			String result;
			
			result = ImageRetrievalBoFactory.produceImageRetrievalBo(em)
					.getCidStudyFirstLastImage(patientKey, hospCde, caseNo,
							accessionNo, seriesNo, imageSeqNo, versionNo);
			
			// Audit log content included result, so process result before writing audit log
			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<AccessionNo>", accessionNo);
			ev.put("<SeriesNo>", seriesNo);
			ev.put("<ImageSeqNo>", imageSeqNo);
			ev.put("<VersionNo>", versionNo);
			al.writeLog(accessionNo, caseNo, workstationId, eventId, ev,
					hospCde, patientKey, requestSys, "", userId, result);
			
			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	/*
	 * Param: patientKey - Patient key of study hospCde - Hospital code of study
	 * caseNo - Case no of study accessionNo - Accession no of study seriesNo -
	 * Series no of study imageSeqNo - Image ID of study versionNo - Version no
	 * of image of study isPrintedCount - Flag to indicate the count is
	 * representing printed image count userId - User id of user invoked the web
	 * service for audit logging purpose workstationId - Workstation id of host
	 * machine invoked the web service for audit logging purpose requestSys -
	 * Request system id of application invoked the web service for audit
	 * logging purpose Return: imageCount - The counting of number of image in
	 * the requested study int - a positive integer to indicate the number of
	 * images in the requested study
	 */
	public int getCidStudyImageCount(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, boolean isPrintedCount,
			String userId, String workstationId, String requestSys) {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], isPrintedCount[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo,
						isPrintedCount, userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			int result;
			result = ImageRetrievalBoFactory.produceImageRetrievalBo(em)
					.getCidStudyImageCount(patientKey, hospCde, caseNo,
							accessionNo, seriesNo, imageSeqNo, versionNo,
							isPrintedCount);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}
}
