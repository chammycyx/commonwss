/*
Change Request History:
- CID-731: Issue and performance degrade found in TWE production drill [Boris HO 2013-06-21]
 */
package hk.org.ha.service.biz.image.model.retrieval;

import javax.ejb.Local;

@Local
public interface ImageRetrievalLocal {
	public byte[] exportImage(String ha7Msg, String password, String userId,
			String workstationId, String requestSys, String eventId);

	public byte[] getCidImage(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String imageId, String userId,
			String workstationId, String requestSys, String eventId);

	public byte[] getCidImageThumbnail(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String imageId, String userId,
			String workstationId, String requestSys);

	public byte[] getBufferedCidImage(String path, String filename,
			boolean isThumbnail, String userId, String workstationId,
			String requestSys);

	public String getCidStudy(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String userId, String workstationId,
			String requestSys, String eventId);

	public String getCidStudyFirstLastImage(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String userId,
			String workstationId, String requestSys, String eventId);
	
	/*
	 * Param:	patientKey - Patient key of study
	 * 			hospCde - Hospital code of study
	 * 			caseNo - Case no of study
	 * 			accessionNo - Accession no of study
	 * 			seriesNo - Series no of study
	 * 			imageSeqNo - Image ID of study
	 * 			versionNo - Version no of image of study
	 * 			isPrintedCount - Flag to indicate the count is representing printed image count
	 * 			userId - User id of user invoked the web service for audit logging purpose
	 * 			workstationId - Workstation id of host machine invoked the web service for audit logging purpose
	 * 			requestSys - Request system id of application invoked the web service for audit logging purpose
	 * Return:	imageCount - The counting of number of image in the requested study
	 * 			int - a positive integer to indicate the number of images in the requested study
	 */
	public int getCidStudyImageCount(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, boolean isPrintedCount, String userId,
			String workstationId, String requestSys);
}
