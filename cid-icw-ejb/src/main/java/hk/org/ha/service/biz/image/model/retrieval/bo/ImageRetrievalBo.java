/**
 * 
 */
package hk.org.ha.service.biz.image.model.retrieval.bo;

import hk.org.ha.service.biz.image.model.fmk.exception.CidException;

/**
 * TODO Class Description
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
public interface ImageRetrievalBo {

	public byte[] exportImage(String ha7Msg, String password) throws CidException;

//	public CIDImage getEprHa7(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo, String imageSeqNo, String versionNo);
	
	public byte[] getCidImage(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String imageId) throws CidException;

	public byte[] getCidImageThumbnail(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String imageId)
			throws CidException;
			
	public byte[] getBufferedCidImage(String path, String filename,
			boolean isThumbnail) throws CidException;
	
	public String getCidStudy(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo) throws CidException;

	public String getCidStudyFirstLastImage(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo) throws CidException;
	
	/*
	 * Param:	patientKey - Patient key of study
	 * 			hospCde - Hospital code of study
	 * 			caseNo - Case no of study
	 * 			accessionNo - Accession no of study
	 * 			seriesNo - Series no of study
	 * 			imageSeqNo - Image ID of study
	 * 			versionNo - Version no of image of study
	 * 			isPrintedCount - Flag to indicate the count is representing printed image count
	 * Return:	imageCount - The counting of number of image in the requested study
	 * 			int - a positive integer to indicate the number of images in the requested study
	 */
	public int getCidStudyImageCount(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, boolean isPrintedCount) throws CidException;
}
