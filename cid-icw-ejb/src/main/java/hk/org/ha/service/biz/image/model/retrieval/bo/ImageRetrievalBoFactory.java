/**
 * 
 */
package hk.org.ha.service.biz.image.model.retrieval.bo;

import javax.persistence.EntityManager;

import hk.org.ha.service.biz.image.model.retrieval.bo.ImageRetrievalBoImpl;
import hk.org.ha.service.biz.image.model.retrieval.dao.ImageRetrievalDao;

/**
 * TODO Class Description
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
public class ImageRetrievalBoFactory {

	public static ImageRetrievalBo produceImageRetrievalBo(EntityManager em) {
		ImageRetrievalBoImpl impl = new ImageRetrievalBoImpl();
		impl.setDao(new ImageRetrievalDao(em));
		return impl;
	}

}
