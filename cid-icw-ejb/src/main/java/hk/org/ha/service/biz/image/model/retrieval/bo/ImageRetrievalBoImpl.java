/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-652: Fix ICW return the wrong number of printed image to consumer [Ben YAU 2013-04-22]
 */

package hk.org.ha.service.biz.image.model.retrieval.bo;

import hk.org.ha.service.biz.image.endpoint.eprcid.EprCidWSS;
import hk.org.ha.service.biz.image.endpoint.eprcid.type.Exception_Exception;
import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidFileException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.fmk.util.FileSystemManager;
import hk.org.ha.service.biz.image.model.fmk.util.Ha7Parser;
import hk.org.ha.service.biz.image.model.fmk.util.WebServiceLocator;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDDataHandler;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData.StudyDtl.SeriesDtls.SeriesDtl;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;
import hk.org.ha.service.biz.image.model.retrieval.dao.ImageRetrievalDao;

import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.WeakHashMap;

import javax.imageio.ImageIO;
import javax.xml.ws.WebServiceException;

class ImageRetrievalBoImpl implements ImageRetrievalBo {

	// private ImageRetrievalDao dao;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public void setDao(ImageRetrievalDao dao) {
		// this.dao = dao;
	}

	public byte[] exportImage(String ha7Msg, String password)
			throws CidException {
		String logContent = String.format("%s(ha7Msg[%s], password[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				ha7Msg, password);
		cidLogger.debug(this.getClass(), logContent);

		Ha7Parser ha7Parser = null;
		CIDData cidData = null;
		Properties conf = null;
		String imageSourcePath = null;
		String imageZipDir = null;
		String zipDestPath = null;
		String encryptedZipDestPath = null;
		String uuid = null;
		DateFormat dateFormat = null;
		Date now = null;
		String zipFileName = null;
		FileInputStream fileinputstream = null;
		FileChannel fc = null;
		ByteBuffer buffer = null;

		try {
			conf = ConfigurationFinder.findConf();
			imageSourcePath = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageStoragePath");
			imageZipDir = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageZipDir");
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}

		uuid = UUID.randomUUID().toString();
		now = new Date();
		dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		zipFileName = dateFormat.format(now) + ".zip";

		ha7Parser = new Ha7Parser();
		cidData = ha7Parser.unmarshalHa7Message(ha7Msg);

		// Create Zip folder
		FileSystemManager.createDirectory(generateFolderPath(imageSourcePath,
				imageZipDir), uuid);

		ArrayList<String> fileList = new ArrayList<String>();

		for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
				.getSeriesDtl()) {
			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				File imageFile = new File(generateFolderPath(imageDtl
						.getImagePath(), imageDtl.getImageFile()));

				zipDestPath = generateFolderPath(imageSourcePath, imageZipDir);
				zipDestPath = generateFolderPath(zipDestPath, uuid);
				zipDestPath = generateFolderPath(zipDestPath, "t" + zipFileName);

				if (imageFile.exists()) {
					fileList.add(imageDtl.getImagePath() + File.separator
							+ imageDtl.getImageFile());
					// comment on 2011/01/17 for enhancing batch files zip file
					// FileSystemManager.addFileToZip(imageDtl.getImagePath(),
					// imageDtl.getImageFile(), zipDestPath);
				} else {
					throw new CidException(String.format(
							"Image not found: %s%n", imageFile.getName()),
							false);
				}
			}
		}

		FileSystemManager.zipFolder((String[]) fileList
				.toArray(new String[fileList.size()]), zipDestPath);

		encryptedZipDestPath = generateFolderPath(imageSourcePath, imageZipDir);
		encryptedZipDestPath = generateFolderPath(encryptedZipDestPath, uuid);

		FileSystemManager.createEncryptedZipFile(encryptedZipDestPath, "t"
				+ zipFileName, encryptedZipDestPath, zipFileName, password);

		try {
			fileinputstream = new FileInputStream(generateFolderPath(
					encryptedZipDestPath, zipFileName));

			if (fileinputstream != null) {
				fc = fileinputstream.getChannel();
				buffer = ByteBuffer.allocate(fileinputstream.available());
				int result = fc.read(buffer);
			}
		} catch (FileNotFoundException fne) {
			throw new CidFileException("Export Zip file not found", fne, false);
		} catch (IOException ioe) {
			throw new CidFileException("Failed to read the export zip file",
					ioe, false);
		} finally {
			if (fc != null) {
				try {
					fc.close();
				} catch (IOException ioe) {
					throw new CidException(String.format(
							"Failed to read the zip file: %s%n",
							generateFolderPath(encryptedZipDestPath,
									zipFileName)), ioe, false);
				}
			}
		}

		return buffer.array();
	}

	public CIDData getEprHa7(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo) throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo);
		cidLogger.debug(this.getClass(), logContent);

		Ha7Parser ha7Parser = null;
		CIDData cidData = null;

		try {
			EprCidWSS eprCidWebS = WebServiceLocator.locateEprCidWebS();
			String response = eprCidWebS.getEPRImage(patientKey, hospCde,
					caseNo, accessionNo, seriesNo, imageSeqNo, versionNo);

			if (response == null || response.trim().length() == 0) {
				throw new CidException(
						"Return message from ePR service is empty", false);
			}
			cidLogger.info(this.getClass(), response);
			ha7Parser = new Ha7Parser();
			cidData = ha7Parser.unmarshalHa7Message(response);
		} catch (Exception_Exception ee) {
			throw new CidException(ee.getMessage(), false);
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		} catch (MalformedURLException mue) {
			throw new CidException("ePR web service URL is invalid", mue, false);
		} catch (WebServiceException wse) {
			throw new CidException("ePR web service URL is unavailable", wse,
					false);
		}

		return cidData;
	}

	public CIDData getFirstLastEprHa7(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo) throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo);
		cidLogger.debug(this.getClass(), logContent);

		Ha7Parser ha7Parser = null;
		CIDData cidData = null;

		try {
			EprCidWSS eprCidWebS = WebServiceLocator.locateEprCidWebS();
			String response = eprCidWebS.getFirstLastEPRImage(patientKey,
					hospCde, caseNo, accessionNo, seriesNo, imageSeqNo,
					versionNo);

			if (response == null || response.trim().length() == 0) {
				throw new CidException(
						"Return message from ePR service is empty", false);
			}
			cidLogger.info(this.getClass(), response);
			ha7Parser = new Ha7Parser();
			cidData = ha7Parser.unmarshalHa7Message(response);
		} catch (Exception_Exception ee) {
			throw new CidException(ee.getMessage(), false);
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		} catch (MalformedURLException mue) {
			throw new CidException("ePR web service URL is invalid", mue, false);
		} catch (WebServiceException wse) {
			throw new CidException("ePR web service URL is unavailable", wse,
					false);
		}
		return cidData;
	}

	public byte[] getCidImage(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String imageId) throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], imageId[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo, imageId);
		cidLogger.debug(this.getClass(), logContent);

		CIDData cidData = null;
		ImageDtl imageDtl = null;
		byte[] readFile;

		cidData = this.getEprHa7(patientKey, hospCde, caseNo, accessionNo,
				seriesNo, imageSeqNo, versionNo);

		if (cidData.getStudyDtl().getStudyId().trim().equals(""))
			throw new CidException("ePR Record not found", false);

		imageDtl = CIDDataHandler.getImageDtlByKey(cidData, imageId);

		if (imageDtl != null) {
			readFile = FileSystemManager.copyFileFromNT(
					imageDtl.getImagePath(), imageDtl.getImageFile());
		} else {
			throw new CidException("Image detail not found", false);
		}

		return readFile;
	}

	public byte[] getCidImageThumbnail(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String imageId)
			throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], imageId[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo, imageId);
		cidLogger.debug(this.getClass(), logContent);

		CIDData cidData = null;
		ImageDtl imageDtl = null;
		byte[] readFile;
		byte[] readThumbnailFile = null;
		int thumbnailWidth = 0, thumbnailHeight = 0;
		Properties conf = null;

		cidData = this.getEprHa7(patientKey, hospCde, caseNo, accessionNo,
				seriesNo, imageSeqNo, versionNo);

		if (cidData.getStudyDtl().getStudyId().trim().equals(""))
			throw new CidException("ePR Record not found", false);

		imageDtl = CIDDataHandler.getImageDtlByKey(cidData, imageId);

		if (imageDtl != null) {
			readFile = FileSystemManager.copyFileFromNT(
					imageDtl.getImagePath(), imageDtl.getImageFile());
		} else {
			throw new CidException("Image detail not found", false);
		}

		try {
			conf = ConfigurationFinder.findConf();
			thumbnailWidth = Integer.parseInt(ConfigurationFinder.findProperty(
					conf, "cid.ejb.imageThumbnailSize"));
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}

		if (conf != null) {
			Image image = Toolkit.getDefaultToolkit().createImage(readFile);
			MediaTracker mediaTracker = new MediaTracker(new Container());
			mediaTracker.addImage(image, 0);
			ByteArrayOutputStream baos = null;
			try {
				mediaTracker.waitForID(0);
			} catch (InterruptedException ie) {
				throw new CidException("Fail to create Thumbnail image", ie,
						false);
			}

			thumbnailHeight = image.getHeight(null) * thumbnailWidth
					/ image.getWidth(null);

			BufferedImage thumbnailImage = new BufferedImage(thumbnailWidth,
					thumbnailHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D graphics2D = thumbnailImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			graphics2D.drawImage(image, 0, 0, thumbnailWidth, thumbnailHeight,
					null);

			try {
				baos = new ByteArrayOutputStream();
				ImageIO.write(thumbnailImage, "jpg", baos);
				baos.flush();
				readThumbnailFile = baos.toByteArray();
				baos.close();
			} catch (FileNotFoundException fne) {
				throw new CidFileException(
						"Image file not found for creating thumbnail", fne,
						false);
			} catch (IOException ioe) {
				throw new CidFileException(
						"Fail to read Image file for creating thumbnail", ioe,
						false);
			} finally {
				try {
					if (baos != null)
						baos.close();
				} catch (IOException ioe) {
					throw new CidFileException("Failed to close thumbnail image file",
							ioe, false);
				}
			}
		}

		return readThumbnailFile;
	}

	public byte[] getBufferedCidImage(String path, String filename,
			boolean isThumbnail) throws CidException {
		String logContent = String.format(
				"%s(path[%s], filename[%s], isThumbnail[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				path, filename, isThumbnail);
		cidLogger.debug(this.getClass(), logContent);

		File targetFile = null;
		FileInputStream fileIn = null;
		Long fileLength;
		byte[] readFile;
		Properties conf = null;
		String imageThumbnailDir = null;
		String formattedPath = null;

		try {
			conf = ConfigurationFinder.findConf();
			imageThumbnailDir = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageThumbnailDir");
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}

		try {
			if (isThumbnail) {
				formattedPath = generateFolderPath(path, imageThumbnailDir);
			}
			else{
				formattedPath = path;
			}

			targetFile = new File(formattedPath, filename);
			fileIn = new FileInputStream(targetFile);
			fileLength = targetFile.length();
			readFile = new byte[fileLength.intValue()];

			if(fileIn.read(readFile) != fileLength)
				throw new IOException("Reading incomplete image file bytes");
		} catch (FileNotFoundException fne) {
			throw new CidFileException("Image file not found", fne, false);
		} catch (IOException ioe) {
			throw new CidFileException("Fail to read Image file", ioe, false);
		} finally{
			if (fileIn != null) {
				try {
					fileIn.close();
				} catch (IOException ioe) {
					throw new CidFileException(String.format(
							"Failed to close file: %s%n", targetFile.getName()),
							ioe, false);
				}
			}
		}
		
		return readFile;
	}

	public String getCidFullStudy(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo) throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo);
		cidLogger.debug(this.getClass(), logContent);

		Ha7Parser ha7Parser = null;
		String ha7Result = null;
		String uuid = null;
		Properties conf = null;
		String imageStoragePath = null;
		CIDData cidData = null;

		uuid = UUID.randomUUID().toString();
		ha7Parser = new Ha7Parser();

		try {
			conf = ConfigurationFinder.findConf();
			imageStoragePath = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageStoragePath");
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}
		imageStoragePath = generateFolderPath(imageStoragePath, uuid);

		cidData = this.getEprHa7(patientKey, hospCde, caseNo, accessionNo,
				seriesNo, imageSeqNo, versionNo);

		if (cidData.getStudyDtl().getStudyId().trim().equals(""))
			throw new CidException("ePR Record not found", false);

		for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
				.getSeriesDtl()) {
			String imageDestPath = generateFolderPath(imageStoragePath,
					seriesDtl.getSeriesNo());

			FileSystemManager.createDirectory(imageStoragePath, seriesDtl
					.getSeriesNo());

			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				this.imageCopyFromEpr(imageDestPath, imageDtl.getImagePath(),
						imageDtl.getImageFile());
				imageDtl.setImagePath(imageDestPath);
			}
		}

		ha7Result = ha7Parser.marshalHa7Message(cidData);
		cidLogger.debug(this.getClass(), ha7Result);

		return ha7Result;
	}

	public String getCidStudy(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo) throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo);
		cidLogger.debug(this.getClass(), logContent);

		Ha7Parser ha7Parser = null;
		String uuid = null;
		CIDData cidData = null;
		Properties conf = null;
		int maxVersion = 0, minVersion = 0, imageVersion = 0;
		String examType = null;
		String imageId = null;
		String ha7Result = null;
		String imageStoragePath = null;
		String imageThumbnailDir = null;

		ha7Parser = new Ha7Parser();
		uuid = UUID.randomUUID().toString();

		List<String> rtcExamType = null;
		
		try {
			conf = ConfigurationFinder.findConf();
			imageStoragePath = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageStoragePath");
			imageThumbnailDir = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageThumbnailDir");
			rtcExamType = Arrays.asList(ConfigurationFinder.findProperty(conf,"cid.rtc.examType").split(","));
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}

		cidData = this.getEprHa7(patientKey, hospCde, caseNo,
				accessionNo, seriesNo, imageSeqNo, versionNo);

		if (cidData.getStudyDtl().getStudyId().trim().equals(""))
			throw new CidException("ePR Record not found", false);

		for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
				.getSeriesDtl()) {
			// create series directory
			String imageDestPath = generateFolderPath(imageStoragePath, uuid);
			imageDestPath = generateFolderPath(imageDestPath, seriesDtl
					.getSeriesNo());

			FileSystemManager.createDirectory(imageDestPath);

			examType = seriesDtl.getExamType();
					
			String thumbnailDestPath = generateFolderPath(imageStoragePath,
					uuid);
			thumbnailDestPath = generateFolderPath(thumbnailDestPath, seriesDtl
					.getSeriesNo());
			thumbnailDestPath = generateFolderPath(thumbnailDestPath,
					imageThumbnailDir);

			FileSystemManager.createDirectory(thumbnailDestPath);

			Map<String, String> minVersionMap = new WeakHashMap<String, String>();
			Map<String, String> maxVersionMap = new WeakHashMap<String, String>();

			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				if (Integer.parseInt(imageDtl.getImageVersion()) == 1) {
					minVersionMap.put(imageDtl.getImageId(), imageDtl
							.getImageVersion());
					maxVersionMap.put(imageDtl.getImageId(), imageDtl
							.getImageVersion());
				}
			}

			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				if (maxVersionMap.containsKey(imageDtl.getImageId()))
					if (Integer.parseInt(imageDtl.getImageVersion()) > Integer
							.parseInt(maxVersionMap.get(imageDtl.getImageId())))
						maxVersionMap.put(imageDtl.getImageId(), imageDtl
								.getImageVersion());
			}

			Iterator<ImageDtl> it = seriesDtl.getImageDtls().getImageDtl()
					.iterator();
			
			while (it.hasNext()) {
				ImageDtl imageDtl = it.next();
				
				imageId = imageDtl.getImageId();
				minVersion = Integer.parseInt(minVersionMap.get(imageId));
				maxVersion = Integer.parseInt(maxVersionMap.get(imageId));
				imageVersion = Integer.parseInt(imageDtl.getImageVersion());
								
				if(rtcExamType.contains(examType)){
					if (imageVersion != minVersion && imageVersion != maxVersion) {
						it.remove();
					}
				}
				else{					
					if (imageVersion != minVersion && imageVersion != maxVersion && imageVersion != (maxVersion -1)) {
						it.remove();
					}
				}
			}

			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				this.imageCopyFromEpr(imageDestPath, imageDtl.getImagePath(),
						imageDtl.getImageFile());
				imageDtl.setImagePath(imageDestPath);
				this.createImageThumbnail(thumbnailDestPath, imageDestPath,
						imageDtl.getImageFile());
			}
		}

		ha7Result = ha7Parser.marshalHa7Message(cidData);
		cidLogger.debug(this.getClass(), ha7Result);

		return ha7Result;
	}
	
	public String getCidStudyFirstLastImage(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo) throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo);
		cidLogger.debug(this.getClass(), logContent);

		Ha7Parser ha7Parser = null;
		String uuid = null;
		CIDData cidData = null;
		Properties conf = null;
		int maxVersion = 0;
		String ha7Result = null;
		String imageStoragePath = null;
		String imageThumbnailDir = null;

		ha7Parser = new Ha7Parser();
		uuid = UUID.randomUUID().toString();

		try {
			conf = ConfigurationFinder.findConf();
			imageStoragePath = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageStoragePath");
			imageThumbnailDir = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageThumbnailDir");
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}

		cidData = this.getFirstLastEprHa7(patientKey, hospCde, caseNo,
				accessionNo, seriesNo, imageSeqNo, versionNo);

		if (cidData.getStudyDtl().getStudyId().trim().equals(""))
			throw new CidException("ePR Record not found", false);

		for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
				.getSeriesDtl()) {
			// create series directory
			String imageDestPath = generateFolderPath(imageStoragePath, uuid);
			imageDestPath = generateFolderPath(imageDestPath, seriesDtl
					.getSeriesNo());

			FileSystemManager.createDirectory(imageDestPath);

			// create thumbnail directory
			// comment on 2010/12/30 by Boris for performance tuning
			String thumbnailDestPath = generateFolderPath(imageStoragePath,
					uuid);
			thumbnailDestPath = generateFolderPath(thumbnailDestPath, seriesDtl
					.getSeriesNo());
			thumbnailDestPath = generateFolderPath(thumbnailDestPath,
					imageThumbnailDir);

			FileSystemManager.createDirectory(thumbnailDestPath);

			// 2011/01/12 bug fix
			Map<String, String> minVersionMap = new WeakHashMap<String, String>();
			Map<String, String> maxVersionMap = new WeakHashMap<String, String>();

			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				if (Integer.parseInt(imageDtl.getImageVersion()) == 1) {
					minVersionMap.put(imageDtl.getImageId(), imageDtl
							.getImageVersion());
					maxVersionMap.put(imageDtl.getImageId(), imageDtl
							.getImageVersion());
				}
			}

			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				if (maxVersionMap.containsKey(imageDtl.getImageId()))
					if (Integer.parseInt(imageDtl.getImageVersion()) > Integer
							.parseInt(maxVersionMap.get(imageDtl.getImageId())))
						maxVersionMap.put(imageDtl.getImageId(), imageDtl
								.getImageVersion());
			}

			Iterator<ImageDtl> it = seriesDtl.getImageDtls().getImageDtl()
					.iterator();
			while (it.hasNext()) {
				ImageDtl imageDtl = it.next();
				if (Integer.parseInt(imageDtl.getImageVersion()) != Integer
						.parseInt(maxVersionMap.get(imageDtl.getImageId()))
						&& Integer.parseInt(imageDtl.getImageVersion()) != Integer
								.parseInt(minVersionMap.get(imageDtl
										.getImageId()))) {
					// match version
					it.remove();
				}
			}

			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				this.imageCopyFromEpr(imageDestPath, imageDtl.getImagePath(),
						imageDtl.getImageFile());
				imageDtl.setImagePath(imageDestPath);
				// comment on 2010/12/30 by Boris for performance tuning
				this.createImageThumbnail(thumbnailDestPath, imageDestPath,
						imageDtl.getImageFile());
			}
		}

		ha7Result = ha7Parser.marshalHa7Message(cidData);
		cidLogger.debug(this.getClass(), ha7Result);

		return ha7Result;
	}

	/*
	 * Param: patientKey - Patient key of study hospCde - Hospital code of study
	 * caseNo - Case no of study accessionNo - Accession no of study seriesNo -
	 * Series no of study imageSeqNo - Image ID of study versionNo - Version no
	 * of image of study isPrintedCount - Flag to indicate the count is
	 * representing printed image count Return: imageCount - The counting of
	 * number of image in the requested study int - a positive integer to
	 * indicate the number of images in the requested study
	 */
	public int getCidStudyImageCount(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, boolean isPrintedCount)
			throws CidException {
		String logContent = String
				.format(
						"%s(patientKey[%s], hospCde[%s], caseNo[%s], accessionNo[%s], seriesNo[%s], imageSeqno[%s], versionNo[%s], isPrintedCount[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), patientKey, hospCde, caseNo,
						accessionNo, seriesNo, imageSeqNo, versionNo,
						isPrintedCount);
		cidLogger.debug(this.getClass(), logContent);

		int imageCount = 0;
		CIDData cidData = null;

		cidData = this.getFirstLastEprHa7(patientKey, hospCde, caseNo, accessionNo,
				seriesNo, imageSeqNo, versionNo);

		if (cidData.getStudyDtl().getStudyId().trim().equals(""))
			throw new CidException("ePR Record not found", false);

		for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
				.getSeriesDtl()) {
			for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
				if (isPrintedCount) {
					// Count image entry with "Print on report" indicator is set
					// to Y
					if (imageDtl.getImageHandling().trim().equals("Y"))
						imageCount++;
				} else {
					// Count version 1 image entry of each image pairs only to
					// eliminate duplicate image copies count
					if (Integer.parseInt(imageDtl.getImageVersion().trim()) == 1)
						imageCount++;
				}
			}
		}

		return imageCount;
	}

	private void imageCopyFromEpr(String imageStoragePath, String sourcePath,
			String sourceFileName) throws CidException {
		String logContent = String
				.format(
						"%s(imageStoragePath[%s], sourcePath[%s], sourceFileName[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), imageStoragePath, sourcePath,
						sourceFileName);
		cidLogger.debug(this.getClass(), logContent);

		String formattedSourcePath = null;
		
		File destFile = new File(generateFolderPath(imageStoragePath,
				sourceFileName));

		if (sourcePath.charAt(sourcePath.length() - 1) != File.separator
				.charAt(0)){
			formattedSourcePath = sourcePath + File.separator;
		}else{
			formattedSourcePath = sourcePath;
		}

		FileSystemManager.copyFileFromNT(destFile, formattedSourcePath, sourceFileName);
	}

	private void createImageThumbnail(String imageStoragePath,
			String sourcePath, String sourceFileName) throws CidException {
		String logContent = String
				.format(
						"%s(imageStoragePath[%s], sourcePath[%s], sourceFileName[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), imageStoragePath, sourcePath,
						sourceFileName);
		cidLogger.debug(this.getClass(), logContent);

		int thumbnailWidth = 0, thumbnailHeight = 0;
		String thumbnailTargetPath = null;
		Properties conf = null;

		try {
			conf = ConfigurationFinder.findConf();
			thumbnailWidth = Integer.parseInt(ConfigurationFinder.findProperty(
					conf, "cid.ejb.imageThumbnailSize"));
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}

		if (conf != null) {
			thumbnailTargetPath = imageStoragePath;
			Image image = Toolkit.getDefaultToolkit().getImage(
					sourcePath + File.separator + sourceFileName);
			MediaTracker mediaTracker = new MediaTracker(new Container());
			mediaTracker.addImage(image, 0);
			try {
				mediaTracker.waitForID(0);
			} catch (InterruptedException ie) {
				throw new CidException("Fail to create Thumbnail image", ie,
						false);
			}

			thumbnailHeight = image.getHeight(null) * thumbnailWidth
					/ image.getWidth(null);

			BufferedImage thumbnailImage = new BufferedImage(thumbnailWidth,
					thumbnailHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D graphics2D = thumbnailImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			graphics2D.drawImage(image, 0, 0, thumbnailWidth, thumbnailHeight,
					null);

			try {
				ImageIO.write(thumbnailImage, "jpg", new File(
						thumbnailTargetPath + File.separator + sourceFileName));
			} catch (FileNotFoundException fne) {
				throw new CidFileException(
						"Image file not found for creating thumbnail", fne,
						false);
			} catch (IOException ioe) {
				throw new CidFileException(
						"Fail to read Image file for creating thumbnail", ioe,
						false);
			}
		}
	}

	private String generateFolderPath(String basePath, String childFolder) {
		String logContent = String.format(
				"%s(basePath[%s], childFolder[%s])%n", Thread.currentThread()
						.getStackTrace()[1].getMethodName(), basePath,
				childFolder);
		cidLogger.debug(this.getClass(), logContent);

		StringBuilder folderPath = new StringBuilder();
		folderPath.append(basePath);
		folderPath.append(File.separator);
		folderPath.append(childFolder);

		return folderPath.toString();
	}
}
