/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.retrieval.dao;

import javax.persistence.EntityManager;

public class ImageRetrievalDao {

	private EntityManager em;
	
	public ImageRetrievalDao(EntityManager em) {
		this.em = em;
	}
	
	public void setEm(EntityManager em) {
		this.em = em;
	}

	public EntityManager getEm() {
		return em;
	}
}
