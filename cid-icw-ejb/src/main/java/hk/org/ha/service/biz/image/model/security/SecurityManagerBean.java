/**
 * Change Request History:
 * - CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Alex LEE 2013-12-17]
 */
package hk.org.ha.service.biz.image.model.security;

import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfServiceFunction;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;
import hk.org.ha.service.biz.image.model.audit.AuditLocal;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.WebServiceTypeConverter;
import hk.org.ha.service.biz.image.model.security.bo.SecurityManagerBoFactory;

import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(mappedName = "ejb/SecurityManager")
public class SecurityManagerBean implements SecurityManager,
		SecurityManagerLocal {

	@PersistenceContext
	private EntityManager em;
	@EJB
	private AuditLocal al;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public String getAccessKey(String systemId, String hostName, String appKey,
			String userId, String workstationId, String requestSys) {
		String logContent = String
				.format("%s(systemId[%s], hostName[%s], appKey[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), systemId, hostName, appKey,
						userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);
		try {
			String result;

			result = SecurityManagerBoFactory.produceSecurityManagerBo(em)
					.getAccessKey(systemId, hostName, appKey);

			// Audit log content included result, so process result before
			// writing audit log
			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<HostName>", hostName);
			ev.put("<AppKey>", appKey);
			al.writeLog("", "", workstationId, "00017", ev, "", "", requestSys,
					"", userId, String.format("Session Key: %s", result));

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public WsApplicationControl getApplicationControl(String applicationId,
			String applicationVersion, String systemId, String hospCode,
			String profileCode) {
		String logContent = String
				.format("%s(applicationId[%s], applicationVersion[%s], systemId[%s], hospCode[%s], profileCode[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), applicationId,
						applicationVersion, systemId, hospCode, profileCode);
		cidLogger.debug(this.getClass(), logContent);
		try {
			WsApplicationControl result = null;

			boolean validApplicationVersion = false;
			validApplicationVersion = SecurityManagerBoFactory
					.produceSecurityManagerBo(em).validateApplicationStatus(
							applicationId, hospCode, applicationVersion);

			if (validApplicationVersion) {
				result = SecurityManagerBoFactory.produceSecurityManagerBo(em)
						.getApplicationControl(applicationId, systemId,
								hospCode, profileCode);
			}

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public ArrayOfServiceFunction getFunctions(int profileId) {
		String logContent = String.format("%s(profileId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), profileId);
		cidLogger.debug(this.getClass(), logContent);
		try {
			ArrayOfServiceFunction result = null;
			result = WebServiceTypeConverter
					.convertToServiceFunctionWsArray(SecurityManagerBoFactory
							.produceSecurityManagerBo(em).getFunctions(
									profileId));

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public boolean validateAccessKey(String systemId, String ipAddress,
			String accessKey, String userId, String workstationId,
			String requestSys) {
		String logContent = String
				.format("%s(systemId[%s], ipAddress[%s], accessKey[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), systemId, ipAddress,
						accessKey, userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);
		try {
			boolean result = false;
			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<IpAddress>", ipAddress);
			ev.put("<AccessKey>", accessKey);
			al.writeLog("", "", workstationId, "00018", ev, "", "", requestSys,
					"", userId, "");

			result = SecurityManagerBoFactory.produceSecurityManagerBo(em)
					.validateAccessKey(systemId, ipAddress, accessKey);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public boolean validateAccessKeyByHostName(String systemId,
			String hostName, String accessKey, String userId,
			String workstationId, String requestSys) {
		String logContent = String
				.format("%s(systemId[%s], hostName[%s], accessKey[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), systemId, hostName,
						accessKey, userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			boolean result = false;
			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<HostName>", hostName);
			ev.put("<AccessKey>", accessKey);
			al.writeLog("", "", workstationId, "00019", ev, "", "", requestSys,
					"", userId, "");

			result = SecurityManagerBoFactory.produceSecurityManagerBo(em)
					.validateAccessKeyByHostName(systemId, hostName, accessKey);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public boolean validateApplicationVersion(String applicationId,
			String hospCode, String applicationVersion, String userId,
			String workstationId, String requestSys) {
		String logContent = String
				.format("%s(applicationId[%s], hospCode[%s], applicationVersion[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), applicationId, hospCode,
						applicationVersion, userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			boolean result = false;
			result = SecurityManagerBoFactory.produceSecurityManagerBo(em)
					.validateApplicationVersion(applicationId, hospCode,
							applicationVersion);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public boolean validateApplicationStatus(String applicationId,
			String hospCode, String applicationVersion, String userId,
			String workstationId, String requestSys) {
		String logContent = String
				.format("%s(applicationId[%s], hospCode[%s], applicationVersion[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), applicationId, hospCode,
						applicationVersion, userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			boolean result = false;
			result = SecurityManagerBoFactory.produceSecurityManagerBo(em)
					.validateApplicationStatus(applicationId, hospCode,
							applicationVersion);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public ArrayOfApplicationConfig getApplicationConfig(
			String hospitalCde, String systemId, String applicationId,
			String profileCde, String section, String key, String userId,
			String workstationId, String requestSys, String eventId) {
		String logContent = String.format(
				"%s(hospitalCde[%s], systemId[%s], applicationId[%s], "
						+ "profileCde[%s], section[%s], key[%s], userId[%s], "
						+ "workstationId[%s], requestSys[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				hospitalCde, systemId, applicationId, profileCde, section, key,
				userId, workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);

		try {
			List<WsApplicationConfig> result = null;
			result = SecurityManagerBoFactory.produceSecurityManagerBo(em)
					.getApplicationConfig(hospitalCde, systemId, applicationId,
							profileCde, section, key);
			
			ArrayOfApplicationConfig config = WebServiceTypeConverter
					.convertToApplicationConfigWsArray(result);
			
			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<HospCde>", hospitalCde);
			ev.put("<SystemId>", systemId);
			ev.put("<ApplicationId>", applicationId);
			ev.put("<ProfileCde>", profileCde);
			ev.put("<Section>", section);
			ev.put("<Key>", key);
			al.writeLog("", "", workstationId, eventId, ev, "", "", requestSys,
					"", userId, config.toString());

			return config;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n",
					ce.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}
}
