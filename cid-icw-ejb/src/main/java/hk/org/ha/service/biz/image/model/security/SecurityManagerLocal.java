/**
 * Change Request History:
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Alex LEE 2013-12-17]
 */
package hk.org.ha.service.biz.image.model.security;

import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfServiceFunction;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;

import javax.ejb.Local;

/**
 * EJB local interface exposed for <code>SecurityManagerBean</code>. This
 * interface is just a separation of concern on EJB related issue from interface
 * <code>SecurityManagerBo</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Local
public interface SecurityManagerLocal {
	public String getAccessKey(String systemId, String hostName, String appKey,
			String userId, String workstationId, String requestSys);

	public boolean validateAccessKey(String systemId, String ipAddress,
			String accessKey, String userId, String workstationId,
			String requestSys);

	public boolean validateAccessKeyByHostName(String systemId,
			String hostName, String accessKey, String userId,
			String workstationId, String requestSys);

	public WsApplicationControl getApplicationControl(String applicationId,
			String applicationVersion, String systemId, String hospCode,
			String profileCode);

	// public WsServiceFunction[] getFunctions(int profileId);
	public ArrayOfServiceFunction getFunctions(int profileId);

	public boolean validateApplicationVersion(String applicationId,
			String hospCode, String applicationVersion, String userId,
			String workstationId, String requestSys);

	public boolean validateApplicationStatus(String applicationId,
			String hospCode, String applicationVersion, String userId,
			String workstationId, String requestSys);

	public ArrayOfApplicationConfig getApplicationConfig(
			String hospitalCde, String systemId, String applicationId,
			String profileCde, String section, String key, String userId,
			String workstationId, String requestSys, String eventId);
}
