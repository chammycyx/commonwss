/**
 * 
 */
package hk.org.ha.service.biz.image.model.security.bo;

import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;
import hk.org.ha.service.biz.image.endpoint.security.type.WsServiceFunction;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;

import java.util.List;

/**
 * TODO Class Description
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
public interface SecurityManagerBo {

	public String getAccessKey(String systemId, String hostName, String appKey)
			throws CidException;

	public boolean validateAccessKey(String systemId, String ipAddress,
			String accessKey) throws CidException;

	public boolean validateAccessKeyByHostName(String systemId,
			String hostName, String accessKey) throws CidException;

	public WsApplicationControl getApplicationControl(String applicationId,
			String systemId, String hospCode, String profileCode)
			throws CidException;

	// public WsServiceFunction[] getFunctions(int profileId);
	public List<WsServiceFunction> getFunctions(int profileId)
			throws CidException;

	public boolean validateApplicationVersion(String applicationId,
			String hospCode, String applicationVersion) throws CidException;

	public boolean validateApplicationStatus(String applicationId,
			String hospCode, String applicationVersion) throws CidException;

	public List<WsApplicationConfig> getApplicationConfig(String hospitalCde, String systemId,
			String applicationId, String profileCde, String section, String key) throws CidException;
}
