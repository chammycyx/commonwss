/**
 * Change Request History:
 * - CID-192: textbox's default font size [Alex LEE 2012-06-08]
 * - CID-222: marker's default width & height [Alex LEE 2012-06-13]
 * - CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12]
 * - CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-694: Change to provide API for ERS to set the marker & drawing status when CID-Studio is running [Boris HO 2013-07-11]
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-18]
 * - CID-786: Revise the program flow of clinical photo upload for streamlining the upload workflow [Alex LEE 2013-11-20]
 * 
 */
package hk.org.ha.service.biz.image.model.security.bo;

import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;
import hk.org.ha.service.biz.image.endpoint.security.type.WsServiceFunction;
import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidDbException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.fmk.util.WebServiceTypeConverter;
import hk.org.ha.service.biz.image.model.persistence.entity.ApplicationConfig;
import hk.org.ha.service.biz.image.model.persistence.entity.ApplicationControl;
import hk.org.ha.service.biz.image.model.persistence.entity.HospitalApplication;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfile;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileDetail;
import hk.org.ha.service.biz.image.model.persistence.entity.TrustedServer;
import hk.org.ha.service.biz.image.model.security.dao.SecurityManagerDao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

class SecurityManagerBoImpl implements SecurityManagerBo {

	private SecurityManagerDao dao;

	// private CidLogger cidLogger = CidLoggerFactory.produceLogger();
	private CidLogger cidLogger;

	public void setDao(SecurityManagerDao dao) {
		this.dao = dao;
	}

	public void setCidLogger(CidLogger cidLogger) {
		this.cidLogger = cidLogger;
	}

	public String getAccessKey(String systemId, String hostName, String appKey)
			throws CidException {
		String logContent = String.format(
				"%s(systemId[%s], hostName[%s], appKey[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				systemId, hostName, appKey);
		cidLogger.debug(this.getClass(), logContent);

		String accessKey = null;

		try {
			accessKey = this.dao.findAccessKeyByAppKey(systemId, hostName,
					appKey);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (accessKey == null)
			throw new CidException("Access key not found", false);

		return accessKey;
	}

	public WsApplicationControl getApplicationControl(String applicationId,
			String systemId, String hospCode, String profileCode)
			throws CidException {
		String logContent = String
				.format("%s(applicationId[%s], systemId[%s], hospCode[%s], profileCode[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), applicationId, systemId,
						hospCode, profileCode);
		cidLogger.debug(this.getClass(), logContent);

		WsApplicationControl wsac = null;
		ApplicationControl ac = null;
		ServiceProfile sp = null;

		wsac = new WsApplicationControl();

		try {
			sp = this.dao.findServiceProfile(applicationId, systemId, hospCode,
					profileCode);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (sp == null) {
			try {
				sp = this.dao.findServiceProfile(applicationId, systemId,
						"ALL", profileCode);
			} catch (CidDbException e) {
				throw new CidException(CidDbException.DB_ERROR, e, false);
			}
		}

		if (sp == null)
			throw new CidException("Application control not found", false);

		ac = sp.getApplicationControl();

		wsac.setApplicationVersion(ac.getVersion());
		wsac.setEnabled(ac.getfEnabled().toString());
		wsac.setMaxPanelWidth(ac.getMaxPanelWidth());
		wsac.setMaxPanelHeight(ac.getMaxPanelHeight());
		wsac.setMinPanelWidth(ac.getMinPanelWidth());
		wsac.setMinPanelHeight(ac.getMinPanelHeight());
		wsac.setMaxUploadFileSize(ac.getMaxUploadFileSize());
		wsac.setProfileId(sp.getId());
		wsac.setMaxUploadFileCount(sp.getMaxUploadFileCount());
		wsac.setMaxMarkerCount(sp.getMaxMarkerCount());
		wsac.setDefaultTextboxFontSize(sp.getDefaultTextboxFontSize());
		wsac.setDefaultMarkerWidth(sp.getDefaultMarkerWidth());
		wsac.setDefaultMarkerHeight(sp.getDefaultMarkerHeight());
		wsac.setEnableMarkerHistory(sp.getEnableMarkerHistory());
		wsac.setArrayOfServiceFunction(WebServiceTypeConverter
				.convertToServiceFunctionWsArray(getFunctions(wsac
						.getProfileId())));
		wsac.setEnableUploadUi(sp.getEnableUploadUi());

		return wsac;
	}

	public List<WsServiceFunction> getFunctions(int profileId)
			throws CidException {
		String logContent = String.format("%s(profileId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), profileId);
		cidLogger.debug(this.getClass(), logContent);

		List<WsServiceFunction> wssfList = null;
		List<ServiceProfileDetail> spdList = null;
		boolean isEnabled = false;

		wssfList = new ArrayList<WsServiceFunction>();

		try {
			spdList = this.dao.findServiceProfileDetail(profileId);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (spdList == null)
			throw new CidException("Service function not found", false);

		Iterator<ServiceProfileDetail> it = spdList.iterator();
		while (it.hasNext()) {
			isEnabled = false;
			ServiceProfileDetail spd = it.next();
			if (spd.getfEnabled().toString().equals("Y")
					&& spd.getServiceFunction().getfEnabled().toString()
							.equals("Y"))
				isEnabled = true;

			WsServiceFunction wssf = new WsServiceFunction();
			wssf.setFunctionId(spd.getServiceProfileDetailPk().getFunctionId());
			wssf.setEnabled(String.valueOf(isEnabled));
			wssfList.add(wssf);
		}

		return wssfList;
	}

	public boolean validateAccessKey(String systemId, String ipAddress,
			String accessKey) throws CidException {
		String logContent = String.format(
				"%s(systemId[%s], ipAddress[%s], accessKey[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				systemId, ipAddress, accessKey);
		cidLogger.debug(this.getClass(), logContent);

		TrustedServer trustedServer = null;
		boolean isValid = false;

		try {
			trustedServer = this.dao.findValidAccessKey(systemId, ipAddress,
					accessKey);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (trustedServer != null)
			isValid = true;

		return isValid;
	}

	public boolean validateAccessKeyByHostName(String systemId,
			String hostName, String accessKey) throws CidException {
		String logContent = String.format(
				"%s(systemId[%s], hostName[%s], accessKey[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				systemId, hostName, accessKey);
		cidLogger.debug(this.getClass(), logContent);

		TrustedServer trustedServer = null;
		boolean isValid = false;

		try {
			trustedServer = this.dao.findValidAccessKeyByHostName(systemId,
					hostName, accessKey);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (trustedServer != null)
			isValid = true;

		return isValid;
	}

	public boolean validateApplicationVersion(String applicationId,
			String hospCode, String applicationVersion) throws CidException {
		String logContent = String
				.format("%s(applicationId[%s], hospCode[%s], applicationVersion[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), applicationId, hospCode,
						applicationVersion);
		cidLogger.debug(this.getClass(), logContent);

		HospitalApplication ha = null;
		boolean isValid = false;
		Properties conf = null;
		String cidWssVersion = null;

		try {
			conf = ConfigurationFinder.findConf();
			cidWssVersion = ConfigurationFinder.findProperty(conf,
					"cid.ws.version");
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce, false);
		}

		try {
			ha = this.dao.findValidVersionByHospitalCde(applicationId,
					hospCode, applicationVersion);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (ha == null) {
			try {
				ha = this.dao.findValidVersionByHospitalCde(applicationId,
						"ALL", applicationVersion);
			} catch (CidDbException e) {
				throw new CidException(CidDbException.DB_ERROR, e, false);
			}
		}

		if (ha == null)
			throw new CidException("Application version not found", false);

		isValid = ha.getVersionControl().getCidWssVersion()
				.equals(cidWssVersion);

		return isValid;
	}

	public boolean validateApplicationStatus(String applicationId,
			String hospCode, String applicationVersion) throws CidException {
		String logContent = String
				.format("%s(applicationId[%s], hospCode[%s], applicationVersion[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), applicationId, hospCode,
						applicationVersion);
		cidLogger.debug(this.getClass(), logContent);

		HospitalApplication ha = null;
		boolean isVersionValid = false;
		boolean isVersionEnabled = false;
		boolean isApplicationEnabled = false;
		Properties conf = null;
		String cidWssVersion = null;

		try {
			conf = ConfigurationFinder.findConf();
			cidWssVersion = ConfigurationFinder.findProperty(conf,
					"cid.ws.version");
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce, false);
		}
		try {
			ha = this.dao.findValidVersionByHospitalCde(applicationId,
					hospCode, applicationVersion);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (ha == null) {
			try {
				ha = this.dao.findValidVersionByHospitalCde(applicationId,
						"ALL", applicationVersion);
			} catch (CidDbException e) {
				throw new CidException(CidDbException.DB_ERROR, e, false);
			}
		}

		if (ha == null)
			throw new CidException("Application version not found", false);

		isVersionValid = ha.getVersionControl().getCidWssVersion()
				.equals(cidWssVersion);

		isVersionEnabled = ha.getVersionControl().getVersionEnabled()
				.toString().equalsIgnoreCase("Y");

		if (isVersionValid & isVersionEnabled)
			isApplicationEnabled = ha.getApplicationEnabled().toString()
					.equalsIgnoreCase("Y");

		return isApplicationEnabled;
	}

	public List<WsApplicationConfig> getApplicationConfig(String hospitalCde, String systemId,
			String applicationId, String profileCde, String section, String key) throws CidException {
		String logContent = String.format(
				"%s(hospitalCde[%s], systemId[%s], applicationId[%s], profileCde[%s], "
						+ "section[%s], key[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				hospitalCde, systemId, applicationId, profileCde, section, key);
		cidLogger.debug(this.getClass(), logContent);

		List<ApplicationConfig> configs = null;
		try {
			if(key != null && !key.isEmpty()) {
				configs = this.dao.getApplicationConfig(hospitalCde, systemId, applicationId, profileCde, section, key);
			} else {
				configs = this.dao.getApplicationConfigBySection(hospitalCde, systemId, applicationId, profileCde, section);
			}
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}
		
		if(configs.size() == 0) {
			try {
				if(key != null && !key.isEmpty()) {
					configs = this.dao.getApplicationConfig("ALL", systemId, applicationId, profileCde, section, key);
				} else {
					configs = this.dao.getApplicationConfigBySection("ALL", systemId, applicationId, profileCde, section);
				}
			} catch (CidDbException e) {
				throw new CidException(CidDbException.DB_ERROR, e, false);
			}
		}
		
		List<WsApplicationConfig> result = new ArrayList<WsApplicationConfig>();
		
		for(ApplicationConfig config : configs) {
			WsApplicationConfig wsApplicationConfig = new WsApplicationConfig();
			wsApplicationConfig.setSection(config.getApplicationConfigPk().getSection());
			wsApplicationConfig.setKey(config.getApplicationConfigPk().getSectionKey());
			wsApplicationConfig.setValue(config.getValue());
			result.add(wsApplicationConfig);
		}

		return result;
	}

}
