/**
 * 
 */
package hk.org.ha.service.biz.image.model.security.dao;

import hk.org.ha.service.biz.image.model.fmk.exception.CidDbException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.persistence.entity.ApplicationConfig;
import hk.org.ha.service.biz.image.model.persistence.entity.ApplicationConfigPk;
import hk.org.ha.service.biz.image.model.persistence.entity.HospitalApplication;
import hk.org.ha.service.biz.image.model.persistence.entity.HospitalApplicationPk;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfile;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileDetail;
import hk.org.ha.service.biz.image.model.persistence.entity.TrustedServer;
import hk.org.ha.service.biz.image.model.persistence.entity.TrustedServerPk;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

/**
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
public class SecurityManagerDao {

	private EntityManager em;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public SecurityManagerDao(EntityManager em) {
		this.em = em;
	}

	public String findAccessKeyByKey(TrustedServerPk key) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		TrustedServer trustedServerEntity = null;

		try {
			trustedServerEntity = em.find(TrustedServer.class, key);
			if (trustedServerEntity == null) {
				return null;
			}
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
		return trustedServerEntity.getSessionKey();
	}

	public String findAccessKeyByAppKey(String systemId, String hostName,
			String appKey) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		TrustedServerPk tspk = null;
		Query q = null;

		tspk = new TrustedServerPk();
		tspk.setHostName(hostName);
		tspk.setSystemId(systemId);

		try {
			q = em.createQuery("SELECT ts.sessionKey FROM TrustedServer ts "
					+ "WHERE ts.trustedServerPk = :pk AND ts.appKey = :appkey "
					+ "AND ts.status = 'A'");
			q.setParameter("pk", tspk);
			q.setParameter("appkey", appKey);

			return (String) q.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		} catch (NonUniqueResultException nue) {
			throw new CidDbException(String.format("%s%n", nue.getMessage()),
					false);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	public ServiceProfile findServiceProfile(String applicationId,
			String systemId, String hospCode, String profileCode)
			throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		Query q = null;

		try {
			q = em.createQuery("SELECT sp FROM ServiceProfile sp "
					+ "JOIN sp.applicationControl ac "
					+ "WHERE ac.id = :applicationid "
					+ "AND sp.systemId = :systemid "
					+ "AND sp.hospitalCde = :hospitalcde "
					+ "AND sp.cde = :cde AND sp.status = 'A' "
					+ "AND ac.status = 'A'");
			q.setParameter("applicationid", applicationId);
			q.setParameter("systemid", systemId);
			q.setParameter("hospitalcde", hospCode);
			q.setParameter("cde", profileCode);

			return (ServiceProfile) q.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		} catch (NonUniqueResultException nue) {
			throw new CidDbException(String.format("%s%n", nue.getMessage()),
					false);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ServiceProfileDetail> findServiceProfileDetail(int profileId)
			throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		Query q = null;

		try {
			q = em.createQuery("SELECT spd FROM ServiceProfileDetail spd "
					+ "JOIN spd.serviceFunction sf JOIN spd.serviceProfile sp "
					+ "WHERE sp.id = :profileId " + "AND sf.status = 'A'");
			q.setParameter("profileId", profileId);

			return (List<ServiceProfileDetail>) q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	public TrustedServer findValidAccessKey(String systemId, String ipAddress,
			String accessKey) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		Query q = null;

		try {
			q = em.createQuery("SELECT ts FROM TrustedServer ts "
					+ "WHERE ts.trustedServerPk.systemId = :systemid "
					+ "AND ts.ipAddress = :ipaddress "
					+ "AND ts.sessionKey = :accesskey " + "AND ts.status = 'A'");
			q.setParameter("systemid", systemId);
			q.setParameter("ipaddress", ipAddress);
			q.setParameter("accesskey", accessKey);

			return (TrustedServer) q.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		} catch (NonUniqueResultException nue) {
			throw new CidDbException(String.format("%s%n", nue.getMessage()),
					false);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	public TrustedServer findValidAccessKeyByHostName(String systemId,
			String hostName, String accessKey) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		TrustedServerPk tspk = null;
		Query q = null;

		tspk = new TrustedServerPk();
		tspk.setHostName(hostName);
		tspk.setSystemId(systemId);

		try {
			q = em.createQuery("SELECT ts FROM TrustedServer ts "
					+ "WHERE ts.trustedServerPk = :pk "
					+ "AND ts.sessionKey = :accesskey " + "AND ts.status = 'A'");
			q.setParameter("pk", tspk);
			q.setParameter("accesskey", accessKey);

			return (TrustedServer) q.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		} catch (NonUniqueResultException nue) {
			throw new CidDbException(String.format("%s%n", nue.getMessage()),
					false);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	// public String findValidCidWssVersionByKey(VersionControlPk key)
	// throws CidDbException {
	// cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
	// .currentThread().getStackTrace()[1].getMethodName()));
	//
	// VersionControl versionControlEntity = null;
	//
	// try {
	// versionControlEntity = em.find(VersionControl.class, key);
	// if (versionControlEntity == null) {
	// return null;
	// }
	// } catch (Exception e) {
	// throw new CidDbException(String.format("%s%n", e.getMessage()),
	// false);
	// }
	//
	// return versionControlEntity.getCidWssVersion();
	// }

	public HospitalApplication findValidVersionByHospitalCde(
			String applicationId, String hospCode, String applicationVersion)
			throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		HospitalApplicationPk hapk = null;
		Query q = null;

		hapk = new HospitalApplicationPk();
		hapk.setApplicationId(applicationId);
		hapk.setApplicationVersion(applicationVersion);
		hapk.setCde(hospCode);

		try {
			q = em.createQuery("SELECT ha "
					+ "FROM HospitalApplication ha INNER JOIN ha.versionControl vc "
					+ "WHERE ha.hospitalApplicationPk = :hapk");
			q.setParameter("hapk", hapk);

			return (HospitalApplication) q.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		} catch (NonUniqueResultException nue) {
			throw new CidDbException(String.format("%s%n", nue.getMessage()),
					false);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ApplicationConfig> getApplicationConfig(String hospitalCde, String systemId,
			String applicationId, String profileCde, String section,
			String key) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		ApplicationConfigPk acPk = null;
		Query q = null;

		acPk = new ApplicationConfigPk();
		acPk.setHospitalCde(hospitalCde);
		acPk.setSystemId(systemId);
		acPk.setApplicationId(applicationId);
		acPk.setProfileCde(profileCde);
		acPk.setSection(section);
		acPk.setSectionKey(key);

		try {
			q = em.createQuery("SELECT ac FROM ApplicationConfig ac "
					+ "WHERE ac.applicationConfigPk = :acPk AND ac.status = 'A' ");
			q.setParameter("acPk", acPk);

			return q.getResultList();
		} catch (IllegalStateException ise) {
			throw new CidDbException(String.format("%s%n", ise.getMessage()),
					false);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ApplicationConfig> getApplicationConfigBySection(String hospitalCde, String systemId,
			String applicationId, String profileCde, String section) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		Query q = null;

		try {
			String str = "SELECT ac FROM ApplicationConfig ac " +
						"WHERE ac.applicationConfigPk.hospitalCde = :hospitalCde " +
						" AND ac.applicationConfigPk.systemId = :systemId " +
						" AND ac.applicationConfigPk.applicationId = :applicationId " +
						" AND ac.applicationConfigPk.profileCde = :profileCde " +
						" AND ac.applicationConfigPk.section = :section " +
						"AND ac.status = 'A' ";
			
			q = em.createQuery(str);
			q.setParameter("hospitalCde", hospitalCde);
			q.setParameter("systemId", systemId);
			q.setParameter("applicationId", applicationId);
			q.setParameter("profileCde", profileCde);
			q.setParameter("section", section);

			return q.getResultList();
		} catch (IllegalStateException ise) {
			throw new CidDbException(String.format("%s%n", ise.getMessage()),
					false);
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}
}
