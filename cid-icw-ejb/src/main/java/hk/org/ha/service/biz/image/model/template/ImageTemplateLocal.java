/*
Change Request History:
- CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-18]
 */
package hk.org.ha.service.biz.image.model.template;

import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfCategory;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfInt;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateData;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateInfo;

import javax.ejb.Local;

@Local
public interface ImageTemplateLocal {
	public ArrayOfCategory getTemplateCategory(int profileId);

	public ArrayOfTemplateInfo getTemplateInfo(int categoryId);

	public ArrayOfTemplateInfo getTemplateInfoByCategoryIds(int[] categoryIds);

	public ArrayOfTemplateData getTemplateData(ArrayOfInt templateId);

	public boolean uploadImageTemplate(byte[] imgBinaryArray, String filePath,
			String fileName);

	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param: profileId - The profile ID of the template category and template info to be retrieved
	 * Return: ArrayOfTemplateInfo - The template info objects with template categories
	 */
	public ArrayOfTemplateInfo getTemplateCategoryAndTemplateInfoByProfileId(
			int profileId);
}
