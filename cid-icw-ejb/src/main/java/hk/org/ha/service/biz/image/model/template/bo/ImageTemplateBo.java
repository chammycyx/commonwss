/*
Change Request History:
- CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-18]
 */
package hk.org.ha.service.biz.image.model.template.bo;

import hk.org.ha.service.biz.image.endpoint.template.type.WsCategory;
import hk.org.ha.service.biz.image.endpoint.template.type.WsTemplateData;
import hk.org.ha.service.biz.image.endpoint.template.type.WsTemplateInfo;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;

import java.util.List;

public interface ImageTemplateBo {

	public List<WsCategory> getTemplateCategory(int profileId)
			throws CidException;

	public List<WsTemplateInfo> getTemplateInfo(int categoryId)
			throws CidException;

	public List<WsTemplateInfo> getTemplateInfoByCategoryIds(int[] categoryIds)
			throws CidException;

	public List<WsTemplateData> getTemplateData(List<Integer> templateId)
			throws CidException;

	public boolean uploadImageTemplate(byte[] imgBinaryArray, String filePath,
			String fileName) throws CidException;

	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param: profileId - The profile ID of the template category and template info to be retrieved
	 * Return: List<WsTemplateInfo> - The template info objects with template categories in Web Service object type
	 */
	public List<WsTemplateInfo> getTemplateCategoryAndTemplateInfoByProfileId(
			int profileId) throws CidException;
}
