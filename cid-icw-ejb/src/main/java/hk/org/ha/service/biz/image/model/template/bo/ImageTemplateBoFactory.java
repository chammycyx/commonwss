/**
 * 
 */
package hk.org.ha.service.biz.image.model.template.bo;

import javax.persistence.EntityManager;

import hk.org.ha.service.biz.image.model.template.dao.ImageTemplateDao;

/**
 * TODO Class Description
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
public class ImageTemplateBoFactory {

	public static ImageTemplateBo produceImageTemplateBo(EntityManager em) {
		ImageTemplateBoImpl impl = new ImageTemplateBoImpl();
		impl.setDao(new ImageTemplateDao(em));
		return impl;
	}
}
