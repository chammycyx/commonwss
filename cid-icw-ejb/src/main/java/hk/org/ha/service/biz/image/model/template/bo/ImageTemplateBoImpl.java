/**
 * Change Request History:
 * - CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12]
 * - CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25]
 * - CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-730: Change to stop uploading empty diagram to ePR repository from CID-Studio [Boris HO 2013-06-20]
 * - CID-730: Change to stop uploading empty diagram to ePR repository from CID-Studio [Boris HO 2013-06-25]
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * - CID-868: (Bug)Fix the issue of empty templates retrieved if one of the templates failed to load [Boris HO 2013-10-10]
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-19]
 * - CID-917: (Bug)Multiple version template category is not working [Boris HO 2014-01-08]
 * - CID-956: Failed to set default template of VH [Alex LEE 2014-01-27]
 */

package hk.org.ha.service.biz.image.model.template.bo;

import hk.org.ha.service.biz.image.endpoint.template.type.WsCategory;
import hk.org.ha.service.biz.image.endpoint.template.type.WsTemplateData;
import hk.org.ha.service.biz.image.endpoint.template.type.WsTemplateInfo;
import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidDbException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidFileException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.fmk.util.FileSystemManager;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileVersionedParameter;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileVersionedParameterPk;
import hk.org.ha.service.biz.image.model.persistence.entity.Template;
import hk.org.ha.service.biz.image.model.persistence.entity.TemplateCategory;
import hk.org.ha.service.biz.image.model.template.dao.ImageTemplateDao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

class ImageTemplateBoImpl implements ImageTemplateBo {

	private static final String LINUX_PATH_TYPE = "LNX";
	private static final String WINDOWS_PATH_TYPE = "UNC";
	private static final int INVALID_TEMPLATE_CATEGORY_ID = -1;
	private ImageTemplateDao dao;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public void setDao(ImageTemplateDao dao) {
		this.dao = dao;
	}

	public List<WsCategory> getTemplateCategory(int profileId)
			throws CidException {
		String logContent = String.format("%s(profileId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), profileId);
		cidLogger.debug(this.getClass(), logContent);

		List<WsCategory> wscList = new ArrayList<WsCategory>();
		List<TemplateCategory> tcList = null;
		boolean readTemplateWithVersion = false;

		Properties conf = ConfigurationFinder.findConf();
		String applicationVersion = ConfigurationFinder.findProperty(conf,
				"cid.ws.version");

		ServiceProfileVersionedParameterPk serviceProfileVersionedParameterPk = new ServiceProfileVersionedParameterPk();
		serviceProfileVersionedParameterPk.setProfileId(profileId);
		serviceProfileVersionedParameterPk
				.setApplication_version(applicationVersion);
		serviceProfileVersionedParameterPk.setKey("readTemplateWithVersion");
		ServiceProfileVersionedParameter serviceProfileVersionedParameter = dao
				.findServiceProfileVersionedParameterByKey(serviceProfileVersionedParameterPk);
		if (serviceProfileVersionedParameter != null) {
			readTemplateWithVersion = Boolean
					.parseBoolean(serviceProfileVersionedParameter.getValue());
		}

		try {
			if (readTemplateWithVersion) {
				tcList = dao.findTemplateCategoryByProfileIdApplicationVersion(
						profileId, applicationVersion);
			} else {
				tcList = dao.findTemplateCategoryByProfileId(profileId);
			}
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (tcList == null)
			throw new CidException("Template category not found", false);

		List<TemplateCategory> tcVerifyList = new ArrayList<TemplateCategory>(
				tcList);
		Iterator<TemplateCategory> it = tcList.iterator();
		while (it.hasNext()) {
			TemplateCategory templateCategory = it.next();
			Integer defaultTemplateId = null;
			boolean isValidCategoryPath = false;
			String categoryPath = null;
			String verifyCategoryPath = null;
			Integer categoryId = null;

			categoryPath = (templateCategory.getCategoryPath() != null) ? templateCategory
					.getCategoryPath() : "";
			Iterator<TemplateCategory> itVerify = tcVerifyList.iterator();

			while (itVerify.hasNext()) {
				TemplateCategory verifyTemplateCategory = itVerify.next();
				verifyCategoryPath = (verifyTemplateCategory.getCategoryPath() != null) ? verifyTemplateCategory
						.getCategoryPath() : "";

				if (categoryPath.lastIndexOf('/') >= 0) {
					if (verifyCategoryPath.equals(categoryPath.substring(0,
							categoryPath.lastIndexOf('/')))) {
						isValidCategoryPath = true;
					}
				} else {
					isValidCategoryPath = true;
					break;
				}
			}

			Iterator<Template> iTemplateList = templateCategory.getTemplates()
					.iterator();
			while (iTemplateList.hasNext()) {
				Template template = iTemplateList.next();
				if (template.getIsDefaultTemplate()) {
					defaultTemplateId = template.getId();
					break;
				}
			}

			if (isValidCategoryPath)
				categoryId = templateCategory.getId();
			else
				categoryId = INVALID_TEMPLATE_CATEGORY_ID;

			WsCategory wsCategory = new WsCategory();
			wsCategory.setId(categoryId);
			wsCategory.setSubCatList(categoryPath);
			wsCategory.setName(templateCategory.getName());
			wsCategory.setDefTempId(defaultTemplateId);
			wsCategory.setGroupName(templateCategory.getGroupName());
			wscList.add(wsCategory);
		}

		return wscList;
	}

	public List<WsTemplateData> getTemplateData(List<Integer> templateId)
			throws CidException {
		String logContent = String.format("%s(templateId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), "Size:"
				+ templateId.size());
		cidLogger.debug(this.getClass(), logContent);

		int[] templateIdArray = new int[templateId.size()];
		List<WsTemplateData> wstdList = null;
		List<Template> tdList = null;
		int i = 0;
		List<Integer> temp = templateId;
		boolean isTemplateDataValid = true;
		StringBuffer sbInvalidTemplateDataList = new StringBuffer(
				String.format("%nTemplate Data Error: %n"));

		// for (Integer integer : templateId){
		// templateIdArray[i++] = integer.intValue();
		// added on 2011/08/15 by Boris for Double value return by BlazeDS
		// workaround
		for (i = 0; i < temp.size(); i++)
			// method in temp.get(i) cannot be called to cater the case
			// that it actually stores a double (e.g. 9.99999999E8)
			// created from BlazeDS
			templateIdArray[i] = new Double(temp.get(i) + "").intValue();

		wstdList = new ArrayList<WsTemplateData>();
		try {
			tdList = this.dao.findTemplateByTemplateId(templateIdArray);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (tdList == null)
			throw new CidException("Template data not found", false);

		Iterator<Template> it = tdList.iterator();
		while (it.hasNext()) {
			Template t = it.next();
			WsTemplateData wsTd = new WsTemplateData();
			wsTd.setId(t.getId());
			wsTd.setFormat(t.getFormat());
			cidLogger.debug(
					this.getClass(),
					String.format("PATH: %s%nFILE: %s%nTYPE: %s",
							t.getFilePath(), t.getFileName(), t.getType()));
			byte[] fileContent = null;
			File tempFile = null;
			FileInputStream tempFileFis = null;
			Long fileLength;

			try {
				if (t.getType().equals(WINDOWS_PATH_TYPE)) {
					fileContent = FileSystemManager.copyFileFromNT(
							t.getFilePath(), t.getFileName());
					wsTd.setLastModifiedDate(FileSystemManager
							.getNTFileLastModifiedDate(t.getFilePath(),
									t.getFileName()));
				} else if (t.getType().equals(LINUX_PATH_TYPE)) {
					tempFile = new File(t.getFilePath(), t.getFileName());

					wsTd.setLastModifiedDate(new Date(tempFile.lastModified()));
					tempFileFis = new FileInputStream(tempFile);
					fileLength = tempFile.length();
					fileContent = new byte[fileLength.intValue()];
					if (tempFileFis.read(fileContent) != fileLength.intValue())
						throw new IOException(
								"Reading incomplete template file bytes");
				} else
					throw new FileNotFoundException(String.format(
							"Template file path type invalid: %s", t.getType()));
			} catch (FileNotFoundException fne) {
				isTemplateDataValid = false;
				sbInvalidTemplateDataList.append(String.format(
						"%nTemplate ID: %s - Cause: %s%n", t.getId(),
						fne.getMessage()));
			} catch (IOException ioe) {
				isTemplateDataValid = false;
				sbInvalidTemplateDataList.append(String.format(
						"%nTemplate ID: %s - Cause: %s%n", t.getId(),
						ioe.getMessage()));
			} finally {
				try {
					if (tempFileFis != null)
						tempFileFis.close();
				} catch (IOException ioe) {
					isTemplateDataValid = false;
					sbInvalidTemplateDataList.append(String.format(
							"%nTemplate ID: %s - Cause: %s%n", t.getId(),
							ioe.getMessage()));
				}
			}
			if (fileContent != null) {
				wsTd.setData(fileContent);
				wstdList.add(wsTd);
			}
		}

		if (!isTemplateDataValid) {
			throw new CidException(sbInvalidTemplateDataList.toString(), false);
		}

		return wstdList;
	}

	public List<WsTemplateInfo> getTemplateInfo(int categoryId)
			throws CidException {
		String logContent = String
				.format("%s(categoryId[%s])%n", Thread.currentThread()
						.getStackTrace()[1].getMethodName(), categoryId);
		cidLogger.debug(this.getClass(), logContent);

		List<WsTemplateInfo> wstiList = null;
		List<Template> tiList = null;

		wstiList = new ArrayList<WsTemplateInfo>();

		try {
			tiList = this.dao.findTemplateByCategoryId(categoryId);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (tiList == null)
			throw new CidException("Template information not found", false);

		Iterator<Template> it = tiList.iterator();
		while (it.hasNext()) {
			Template t = it.next();

			WsTemplateInfo wsTi = new WsTemplateInfo();
			wsTi.setId(t.getId());
			wsTi.setName(t.getName());
			wsTi.setVer(t.getVer());
			wsTi.setFormat(t.getFormat());
			wsTi.setDisplayInImport(t.getDisplayInImport());
			wsTi.setDisplayInTemplate(t.getDisplayInTemplate());
			wsTi.setAllowEmptyUpload(t.getAllowEmptyUpload());

			wstiList.add(wsTi);
		}

		return wstiList;
	}

	public List<WsTemplateInfo> getTemplateInfoByCategoryIds(int[] categoryIds)
			throws CidException {
		String logContent = String.format("%s(categoryId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), Arrays
				.toString(categoryIds));
		cidLogger.debug(this.getClass(), logContent);

		List<WsTemplateInfo> wstiList = null;
		List<Template> tiList = null;

		wstiList = new ArrayList<WsTemplateInfo>();

		try {
			tiList = this.dao.findTemplateByCategoryIds(categoryIds);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (tiList == null)
			throw new CidException("Template information not found", false);

		Iterator<Template> it = tiList.iterator();
		while (it.hasNext()) {
			Template t = it.next();

			WsTemplateInfo wsTi = new WsTemplateInfo();
			wsTi.setId(t.getId());
			wsTi.setCategoryId(t.getTemplateCategory().getId());
			wsTi.setName(t.getName());
			wsTi.setVer(t.getVer());
			wsTi.setFormat(t.getFormat());
			wsTi.setDisplayInImport(t.getDisplayInImport());
			wsTi.setDisplayInTemplate(t.getDisplayInTemplate());
			wsTi.setAllowEmptyUpload(t.getAllowEmptyUpload());

			wstiList.add(wsTi);
		}

		return wstiList;
	}

	public boolean uploadImageTemplate(byte[] imgBinaryArray, String filePath,
			String fileName) throws CidException {
		String logContent = String.format(
				"%s(imgBinaryArray[%s], filePath[%s], fileName[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				"Size:" + imgBinaryArray.length, filePath, fileName);
		cidLogger.debug(this.getClass(), logContent);

		FileChannel fc = null;
		File imageTemplateFolder = null;
		String imageTemplatePath = null;
		Properties conf = null;

		try {
			conf = ConfigurationFinder.findConf();
			imageTemplatePath = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageTemplatePath");
		} catch (Exception e) {
			throw new CidException(CidConfigException.READ_FAILED, e, false);
		}

		try {
			FileSystemManager.createDirectory(imageTemplatePath, filePath);
			imageTemplateFolder = new File(imageTemplatePath, filePath);

			File image = new File(imageTemplateFolder.getAbsolutePath(),
					fileName);
			fc = new FileOutputStream(image).getChannel();
			ByteBuffer buf = ByteBuffer.allocate(imgBinaryArray.length);
			buf.put(imgBinaryArray);
			buf.flip();
			fc.write(buf);
			fc.force(true);
		} catch (FileNotFoundException fne) {
			throw new CidFileException("Upload image template file not found",
					fne, false);
		} catch (Exception e) {
			throw new CidException(e.getMessage(), false);
		} finally {
			if (fc != null) {
				try {
					fc.close();
				} catch (Exception e) {
					throw new CidException(e.getMessage(), false);
				}
			}
		}
		return true;
	}

	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param: profileId - The profile ID of the template category and template info to be retrieved
	 * Return: List<WsTemplateInfo> - The template info objects with template categories in Web Service object type
	 */
	public List<WsTemplateInfo> getTemplateCategoryAndTemplateInfoByProfileId(
			int profileId) throws CidException {
		String logContent = String.format("%s(profileId[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(),
				(profileId));
		cidLogger.debug(this.getClass(), logContent);

		List<WsTemplateInfo> wstiList = null;
		List<Template> tiList = null;
		wstiList = new ArrayList<WsTemplateInfo>();
		boolean readTemplateWithVersion = false;

		Properties conf = ConfigurationFinder.findConf();
		String applicationVersion = ConfigurationFinder.findProperty(conf,
				"cid.ws.version");

		ServiceProfileVersionedParameterPk serviceProfileVersionedParameterPk = new ServiceProfileVersionedParameterPk();
		serviceProfileVersionedParameterPk.setProfileId(profileId);
		serviceProfileVersionedParameterPk
				.setApplication_version(applicationVersion);
		serviceProfileVersionedParameterPk.setKey("readTemplateWithVersion");
		ServiceProfileVersionedParameter serviceProfileVersionedParameter = dao
				.findServiceProfileVersionedParameterByKey(serviceProfileVersionedParameterPk);
		if (serviceProfileVersionedParameter != null) {
			readTemplateWithVersion = Boolean
					.parseBoolean(serviceProfileVersionedParameter.getValue());
		}

		try {
			if (readTemplateWithVersion) {
				tiList = dao.findTemplateCategoryAndTemplateInfoByProfileIdApplicationVersion(
						profileId, applicationVersion);
			} else {
				tiList = dao.findTemplateCategoryAndTemplateInfoByProfileId(profileId);
			}
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (tiList == null)
			throw new CidException("Template information not found", false);

		Iterator<Template> templateItr = tiList.iterator();
		Hashtable<String, String> tableOfParents = new Hashtable<String, String>();

		while (templateItr.hasNext()) {
			Template t = templateItr.next();

			WsTemplateInfo wsTi = new WsTemplateInfo();
			wsTi.setId(t.getId());
			wsTi.setCategoryId(t.getTemplateCategory().getId());
			wsTi.setName(t.getName());
			wsTi.setVer(t.getVer());
			wsTi.setFormat(t.getFormat());
			wsTi.setDisplayInImport(t.getDisplayInImport());
			wsTi.setDisplayInTemplate(t.getDisplayInTemplate());
			wsTi.setAllowEmptyUpload(t.getAllowEmptyUpload());

			wsTi.setWsCategory(convertTemplateCategoryToWsCategory(t
					.getTemplateCategory()));

			String path = t.getTemplateCategory().getCategoryPath();
			path = (path == null) ? "" : path; // A null value in category path means "root"

			// Check if this is root or top level
			if (path.indexOf("/") < 0) {
				tableOfParents.put(path, path);
			} else {
				// There is a path from (possibly) viable parents to a child
				int nextSlashPos = -1;
				while ((nextSlashPos = path.indexOf("/", nextSlashPos + 1)) > 0) {
					String parent = (path.substring(0, nextSlashPos));
					if (tableOfParents.containsKey(parent))
					{
						if (path.indexOf("/", nextSlashPos + 1) < 0)
							tableOfParents.put(path, path); // A valid child is a potential parent, add the child to the list
					} else {
						// Mark this node as dirty parent
						wsTi.getWsCategory()
								.setId(INVALID_TEMPLATE_CATEGORY_ID);
						break;
					}
				}
			}
			wstiList.add(wsTi);
		}

		return wstiList;
	}

	/* This function is created for converting TemplateCategory object type to Web Service Template Category object type
	 * Param: templateCategory - TemplateCategory object representing a specific template category
	 * Return: WsCategory - A TemplateCategory object in Web Service object type
	 */
	protected WsCategory convertTemplateCategoryToWsCategory(
			TemplateCategory templateCategory) {
		Integer defaultTemplateId = null;
		String categoryPath = null;
		Integer categoryId = null;

		categoryPath = (templateCategory.getCategoryPath() != null) ? templateCategory
				.getCategoryPath() : "";

		Iterator<Template> iTemplateList = templateCategory.getTemplates()
				.iterator();
		while (iTemplateList.hasNext()) {
			Template template = iTemplateList.next();
			if (template.getIsDefaultTemplate()) {
				defaultTemplateId = template.getId();
				break;
			}
		}

		categoryId = templateCategory.getId();

		WsCategory wsCategory = new WsCategory();
		wsCategory.setId(categoryId);
		wsCategory.setSubCatList(categoryPath);
		wsCategory.setName(templateCategory.getName());
		wsCategory.setDefTempId(defaultTemplateId);
		wsCategory.setGroupName(templateCategory.getGroupName());
		return wsCategory;
	}
}
