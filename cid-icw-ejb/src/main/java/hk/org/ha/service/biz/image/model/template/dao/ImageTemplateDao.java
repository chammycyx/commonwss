/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-19]
 */
package hk.org.ha.service.biz.image.model.template.dao;

import hk.org.ha.service.biz.image.model.fmk.exception.CidDbException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileVersionedParameter;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileVersionedParameterPk;
import hk.org.ha.service.biz.image.model.persistence.entity.Template;
import hk.org.ha.service.biz.image.model.persistence.entity.TemplateCategory;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.eclipse.persistence.config.QueryHints;

public class ImageTemplateDao {
	private EntityManager em;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public ImageTemplateDao(EntityManager em) {
		this.em = em;
	}

	@SuppressWarnings("unchecked")
	public List<TemplateCategory> findTemplateCategoryByProfileId(int profileId)
			throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		Query q = null;
		try {
			q = em.createQuery("SELECT tc "
					+ "FROM ServiceProfile sp INNER JOIN sp.templateCategories tc "
					+ "WHERE sp.id = :profileid AND tc.status = 'A' AND tc.applicationVersion IS NULL");
			q.setParameter("profileid", profileId);

			return (List<TemplateCategory>) q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TemplateCategory> findTemplateCategoryByProfileIdApplicationVersion(
			int profileId, String applicationVersion) throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		
		Query q = null;
		try {
			q = em.createQuery("SELECT tc "
					+ "FROM ServiceProfile sp INNER JOIN sp.templateCategories tc "
					+ "WHERE sp.id = :profileid AND tc.status = 'A' AND tc.applicationVersion = :applicationVersion");
			q.setParameter("profileid", profileId);
			q.setParameter("applicationVersion", applicationVersion);

			return (List<TemplateCategory>) q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<Template> findTemplateByCategoryId(int categoryId)
			throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		Query q = null;

		try {
			q = em.createQuery("SELECT t FROM Template t "
					+ "WHERE t.templateCategory.id = :categoryid "
					+ "AND t.status = 'A'");
			q.setParameter("categoryid", categoryId);

			return (List<Template>) q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Template> findTemplateByCategoryIds(int[] categoryIds)
			throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		Query q = null;
		
		String categoryIdParam = genQueryParamList("categoryId", categoryIds.length);
		
		try {
			q = em.createQuery("SELECT t FROM Template t "
					+ "WHERE t.templateCategory.id in ( "
					+ categoryIdParam + " ) AND t.status = 'A'");
			
			for (int i = 0; i < categoryIds.length; i++)
				q.setParameter("categoryId" + i, categoryIds[i]);

			return (List<Template>) q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Template> findTemplateByTemplateId(int[] templateId)
			throws CidDbException {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		String templateIdParam = null;
		Query q = null;

		try {
			templateIdParam = genQueryParamList("templateid", templateId.length);

			q = em.createQuery("SELECT t FROM Template t WHERE t.id in ("
					+ templateIdParam + ") AND t.status = 'A'");

			for (int i = 0; i < templateId.length; i++)
				q.setParameter("templateid" + i, templateId[i]);

			return (List<Template>) q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
	}
	
	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param: profileId - The profile ID of the template category and template info to be retrieved
	 * Return: List<Template> - The template info objects with template categories in JPA object type
	 */
	@SuppressWarnings("unchecked")
	public List<Template> findTemplateCategoryAndTemplateInfoByProfileId(int profileId)
		throws CidDbException
	{
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		
		Query q = null;
		
		try {			
			q = em.createQuery("SELECT t  from Template t" +
					" WHERE t.templateCategory.serviceProfile.id=:profileId AND t.status = 'A' " +
					" AND t.templateCategory.applicationVersion IS NULL" + 
					" ORDER BY t.templateCategory.categoryPath asc, t.name asc");
					
			q.setParameter("profileId", profileId);
			q.setHint(QueryHints.FETCH, "t.templateCategory.serviceProfile");
			
			return (List<Template>)q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
		
	}
	
	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param:	profileId - The profile ID of the template category and template info to be retrieved
	 * 			applicationVersion - The application version of the related template category and template info to be retrieved
	 * Return:	List<Template> - The template info objects with template categories in JPA object type
	 */
	@SuppressWarnings("unchecked")
	public List<Template> findTemplateCategoryAndTemplateInfoByProfileIdApplicationVersion(int profileId, String applicationVersion)
		throws CidDbException
	{
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));
		
		Query q = null;
		
		try {			
			q = em.createQuery("SELECT t  from Template t" +
					" WHERE t.templateCategory.serviceProfile.id=:profileId AND t.status = 'A' " +
					" AND t.templateCategory.applicationVersion = :applicationVersion" + 
					" ORDER BY t.templateCategory.categoryPath asc, t.name asc");
					
			q.setParameter("profileId", profileId);
			q.setParameter("applicationVersion", applicationVersion);
			q.setHint(QueryHints.FETCH, "t.templateCategory.serviceProfile");
			
			return (List<Template>)q.getResultList();
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			throw new CidDbException(String.format("%s%n", e.getMessage()),
					false);
		}
		
	}
	
	public ServiceProfileVersionedParameter findServiceProfileVersionedParameterByKey(ServiceProfileVersionedParameterPk serviceProfileVersionedParameterPk) {
		return em.find(ServiceProfileVersionedParameter.class, serviceProfileVersionedParameterPk);
	}

	private String genQueryParamList(String fieldName, int fieldCount) {
		cidLogger.debug(this.getClass(), String.format("Invoking %s", Thread
				.currentThread().getStackTrace()[1].getMethodName()));

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < fieldCount; i++) {
			sb.append(":");
			sb.append(fieldName);
			sb.append(i);
			if ((fieldCount - 1) > i)
				sb.append(",");
		}

		return sb.toString();
	}
	
}
