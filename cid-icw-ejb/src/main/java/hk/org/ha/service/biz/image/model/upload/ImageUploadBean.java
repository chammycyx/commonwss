/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-731: Issue and performance degrade found in TWE production drill [Boris HO 2013-06-21]
 */

package hk.org.ha.service.biz.image.model.upload;

import hk.org.ha.service.biz.image.model.audit.AuditLocal;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.upload.bo.ImageUploadBoFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(mappedName = "ejb/ImageUpload")
public class ImageUploadBean implements ImageUpload, ImageUploadLocal {

	@PersistenceContext
	private EntityManager em;
	@EJB
	private AuditLocal al;
	@EJB
	private NextNoLocal nn;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public String getAccessionNo(String hospCde, String deptCde, String userId,
			String workstationId, String requestSys) {
		String logContent = String
				.format(
						"%s(hospCde[%s], deptCde[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), hospCde, deptCde, userId,
						workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);
		try {
			String result = null;

			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<HospCde>", hospCde);
			ev.put("<DeptCde>", deptCde);
			al.writeLog("", "", workstationId, "00010", ev, "", "", requestSys,
					"", userId, "");

			result = ImageUploadBoFactory.produceImageUploadBo(em)
					.getAccessionNo(hospCde, deptCde,
							nn.generateNextNo(hospCde, deptCde));

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public String uploadImage(byte[] imgBinaryArray, String requestSys,
			String filename, String patientKey, String studyId,
			String seriesNo, String userId, String workstationId, String eventId) {
		String logContent = String
				.format(
						"%s(imgBinaryArray[%s], requestSys[%s], filename[%s], patientKey[%s], studyId[%s], seriesNo[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), "Size:"
								+ imgBinaryArray.length, requestSys, filename,
						patientKey, studyId, seriesNo, userId, workstationId,
						requestSys);
		cidLogger.debug(this.getClass(), logContent);
		try {
			String filePath = null;
			Map<String, String> ev = new WeakHashMap<String, String>();
			ev.put("<FileName>", filename);
			ev.put("<StudyId>", studyId);
			ev.put("<SeriesNo>", seriesNo);
			al.writeLog("", "", workstationId, eventId, ev, "", patientKey,
					requestSys, "", userId, "");

			filePath = ImageUploadBoFactory.produceImageUploadBo(em)
					.uploadImage(imgBinaryArray, requestSys, filename,
							patientKey, studyId, seriesNo);

			return filePath;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public int uploadMetaData(String ha7Message, String userId,
			String workstationId, String requestSys, String eventId) {
		String logContent = String
				.format(
						"%s(ha7Message[%s], userId[%s], workstationId[%s], requestSys[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), ha7Message, userId,
						workstationId, requestSys);
		cidLogger.debug(this.getClass(), logContent);
		try {
			int result = 0;

			Map<String, String> ev = new WeakHashMap<String, String>();
			al.writeLog("", "", workstationId, eventId, ev, "", "", requestSys,
					"", userId, ha7Message);

			result = ImageUploadBoFactory.produceImageUploadBo(em)
					.uploadMetaData(ha7Message);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

	public boolean validateAccessionNo(String hospCde, String deptCde,
			String accessionNo) {
		String logContent = String.format(
				"%s(hospCde[%s], deptCde[%s], accessionNo[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				hospCde, deptCde, accessionNo);
		cidLogger.debug(this.getClass(), logContent);
		try {
			boolean result = false;

			Map<String, String> ev = new HashMap<String, String>();
			ev.put("<DeptCode>", deptCde);
			al.writeLog("", "", "", "00004", ev, "", "", "", "", "", "");

			result = ImageUploadBoFactory.produceImageUploadBo(em)
					.validateAccessionNo(hospCde, deptCde, accessionNo);

			return result;
		} catch (CidException ce) {
			if (!ce.isLogged())
				cidLogger.error(this.getClass(), logContent, ce);
			throw new EJBException(String.format("System error: %s%n", ce
					.getMessage()));
		} catch (EJBException ee) {
			throw new EJBException(ee.getMessage());
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Unknown system error");
		}
	}

}
