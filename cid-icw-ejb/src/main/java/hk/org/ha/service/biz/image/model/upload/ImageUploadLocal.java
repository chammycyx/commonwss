/*
Change Request History:
- CID-731: Issue and performance degrade found in TWE production drill [Boris HO 2013-06-21]
 */
package hk.org.ha.service.biz.image.model.upload;

import javax.ejb.Local;

@Local
public interface ImageUploadLocal {

	public String getAccessionNo(String hospCde, String deptCde, String userId, String workstationId, String requestSys);

	public boolean validateAccessionNo(String hospCde, String deptCde,
			String accessionNo);

	public String uploadImage(byte[] imgBinaryArray, String requestSys,
			String filename, String patientKey, String studyId, String seriesNo, String userId, String workstationId, String eventId);

	public int uploadMetaData(String ha7Message, String userId, String workstationId, String requestSys, String eventId);

}
