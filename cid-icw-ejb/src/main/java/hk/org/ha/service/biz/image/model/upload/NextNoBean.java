/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.upload;

import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.persistence.entity.NextNumber;
import hk.org.ha.service.biz.image.model.persistence.entity.NextNumberPk;

import java.util.Calendar;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class NextNoBean implements NextNoLocal {

	@PersistenceContext
	private EntityManager em;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public NextNoBean() {
	}

	public Integer generateNextNo(String hospCde, String deptCde)
			throws CidException {
		String logContent = String.format("%s(hospCde[%s], deptCde[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				hospCde, deptCde);
		cidLogger.debug(this.getClass(), logContent);

		NextNumber nextNumberEntity = null;
		Integer nextNo;

		try {

			NextNumberPk nextNumberPk = new NextNumberPk();
			nextNumberPk.setFieldName("access_no");
			nextNumberPk.setCategory1(hospCde);
			nextNumberPk.setCategory2(deptCde);
			nextNumberPk.setCalYear(Calendar.getInstance().get(Calendar.YEAR));

			Query query = em
					.createNativeQuery("SELECT * "
							+ "FROM next_to_use_numbers WITH (UPDLOCK, ROWLOCK)"
							+ "WHERE cal_year = ? and field_name = ? and category_1 = ? and category_2 = ? ");
			query.setParameter(1, nextNumberPk.getCalYear());
			query.setParameter(2, nextNumberPk.getFieldName());
			query.setParameter(3, nextNumberPk.getCategory1());
			query.setParameter(4, nextNumberPk.getCategory2());

			query.getResultList();

			nextNumberEntity = em.find(NextNumber.class, nextNumberPk);

			if (nextNumberEntity == null) {
				throw new CidException(
						"Record for generating accession no. not found", false);
			}

			nextNo = nextNumberEntity.getNextNo();

			nextNumberEntity.setNextNo(nextNo + 1);
		} catch (RuntimeException e) {
			cidLogger.error(this.getClass(), logContent, e);
			throw new EJBException("Failed to generate next accesion no. sequence number");
		}

		return nextNo;
	}
}
