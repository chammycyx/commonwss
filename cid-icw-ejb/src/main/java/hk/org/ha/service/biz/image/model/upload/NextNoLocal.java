package hk.org.ha.service.biz.image.model.upload;
import javax.ejb.Local;

import hk.org.ha.service.biz.image.model.fmk.exception.CidException;

@Local
public interface NextNoLocal {
	public Integer generateNextNo(String hospCde, String deptCde) throws CidException;
}
