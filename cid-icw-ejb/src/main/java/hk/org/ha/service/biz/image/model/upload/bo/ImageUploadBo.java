/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
*/

package hk.org.ha.service.biz.image.model.upload.bo;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;

public interface ImageUploadBo {

	/**
	 * Generate accession number using hospital code, department code, a number
	 * from a sequence and a checksum.
	 * 
	 * @param hospCde
	 *            the hospital code
	 * @param deptCde
	 *            the department code
	 * @return the accession number
	 * @throws CidException 
	 */
	public String getAccessionNo(String hospCde, String deptCde, Integer nextNo) throws CidException ;

	/**
	 * Validate an accession number for a department in a hospital.
	 * 
	 * @param hospCde
	 *            the hospital code
	 * @param deptCde
	 *            the department code
	 * @param accessionNo
	 *            the accession number to validate
	 * @return whether the accession number is valid for the department
	 */
	public boolean validateAccessionNo(String hospCde, String deptCde,
			String accessionNo) throws CidException ;

	/**
	 * Upload image to CID for temporary storage.
	 * 
	 * @param imgBinaryArray
	 *            the binary source of an image
	 * @param requestSys
	 *            obsolete parameter
	 * @param filename
	 *            the image filename
	 * @param patientKey
	 *            the key of patient record
	 * @param studyId
	 *            the ID of study
	 * @param seriesNo
	 *            the series number in a study
	 */
	public String uploadImage(byte[] imgBinaryArray, String requestSys,
			String filename, String patientKey, String studyId, String seriesNo) throws CidException;

	/**
	 * Upload meta-data for uploaded images specified in HA7 XML to ePR.
	 * 
	 * @param hl7Message
	 *            the HA7 XML
	 */
	public int uploadMetaData(String ha7Message) throws CidException;

}
