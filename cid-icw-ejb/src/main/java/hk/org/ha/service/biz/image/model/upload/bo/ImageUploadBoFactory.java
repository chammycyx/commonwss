/**
 * 
 */
package hk.org.ha.service.biz.image.model.upload.bo;

import javax.persistence.EntityManager;

import hk.org.ha.service.biz.image.model.upload.dao.ImageUploadDao;

/**
 * Factory class for <code>ImageUploadBo</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
public class ImageUploadBoFactory {

	/**
	 * Create <code>ImageUploadBo</code> with an implementation.
	 * 
	 * @param em
	 *            the entity manager used by DAO
	 * @return the imageUploadBo
	 */
	public static ImageUploadBo produceImageUploadBo(EntityManager em) {
		ImageUploadBoImpl impl = new ImageUploadBoImpl();
		impl.setDao(new ImageUploadDao(em));
		return impl;
	}

}
