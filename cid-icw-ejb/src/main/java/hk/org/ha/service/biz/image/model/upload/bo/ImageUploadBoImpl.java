/*
Change Request History:
- CID-438: Change to alert the failed cases of calling CID Web Service [Boris HO 2012-11-23]
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.model.upload.bo;

import hk.org.ha.service.biz.image.endpoint.eprcid.EprCidWSS;
import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidDbException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidException;
import hk.org.ha.service.biz.image.model.fmk.exception.CidFileException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.fmk.util.Ha7AckParser;
import hk.org.ha.service.biz.image.model.fmk.util.Ha7Parser;
import hk.org.ha.service.biz.image.model.fmk.util.WebServiceLocator;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData.StudyDtl.SeriesDtls.SeriesDtl;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.ack.CIDAck;
import hk.org.ha.service.biz.image.model.persistence.entity.Hospital;
import hk.org.ha.service.biz.image.model.upload.dao.ImageUploadDao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;

import javax.xml.ws.WebServiceException;

class ImageUploadBoImpl implements ImageUploadBo {

	private static final String FOLDER_DATE_FORMAT = "yyyyMMdd";

	private static final int UPLOAD_METADATA_SUCCESS_STATUS = 0;
	private static final int UPLOAD_METADATA_PENDING_STATUS = 1;

	private ImageUploadDao dao;
	private CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public void setDao(ImageUploadDao dao) {
		this.dao = dao;
	}

	public String getAccessionNo(String hospCde, String deptCde, Integer nextNo)
			throws CidException {
		String logContent = String.format(
				"%s(hospCde[%s], deptCde[%s], nextNo[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				hospCde, deptCde, nextNo);
		cidLogger.debug(this.getClass(), logContent);

		String nextNoStr = null;
		String yearStr = null;
		String runSeq = null;
		char checkDigit = '\0';
		String newAccessionNo = null;
		Hospital hospital = null;

		try {
			hospital = dao.findHospitalByKey(hospCde);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (hospital == null)
			throw new CidException("Accession number not found", false);

		nextNoStr = nextNo.toString();
		yearStr = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));

		runSeq = generateRunSeq(yearStr, nextNoStr);
		checkDigit = generateCheckDigit(hospital.getId(), runSeq);
		newAccessionNo = generateAccessionNo(hospCde, deptCde, runSeq,
				checkDigit);

		return newAccessionNo;
	}

	public boolean validateAccessionNo(String hospCde, String deptCde,
			String accessionNo) throws CidException {
		String logContent = String.format(
				"%s(hospCde[%s], deptCde[%s], accessionNo[%s])%n", Thread
						.currentThread().getStackTrace()[1].getMethodName(),
				hospCde, deptCde, accessionNo);
		cidLogger.debug(this.getClass(), logContent);

		String runSeq = null;
		char checkDigit = '\0';
		String targetAccessionNo = null;
		Hospital hospital = null;

		if (hospCde.length() < 2 || hospCde.length() > 3) {
			throw new CidException("Invalid hospital code", false);
		}

		if (deptCde.length() != 2) {
			throw new CidException("Invalid department code", false);
		}

		if (accessionNo.length() != 16) {
			throw new CidException("Invalid accession no", false);
		}

		try {
			hospital = dao.findHospitalByKey(hospCde);
		} catch (CidDbException e) {
			throw new CidException(CidDbException.DB_ERROR, e, false);
		}

		if (hospital == null) {
			throw new CidException("Hospital not found", false);
		}

		runSeq = accessionNo.substring(5, 15);
		checkDigit = generateCheckDigit(hospital.getId(), runSeq);
		targetAccessionNo = generateAccessionNo(hospCde, deptCde, runSeq,
				checkDigit);

		return accessionNo.equals(targetAccessionNo);
	}

	public String uploadImage(byte[] imgBinaryArray, String requestSys,
			String filename, String patientKey, String studyId, String seriesNo)
			throws CidException {
		String logContent = String
				.format(
						"%s(imgBinaryArray[%s], requestSys[%s], filename[%s], patientKey[%s], studyId[%s], seriesNo[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), "Size:"
								+ imgBinaryArray.length, requestSys, filename,
						patientKey, studyId, seriesNo);
		cidLogger.debug(this.getClass(), logContent);

		FileChannel fc = null;
		File uploadFolder = null;
		ByteBuffer buf = null;
		String uuid = null;
		String filePath = null;

		try {
			uuid = UUID.randomUUID().toString();
			uploadFolder = generateUploadFolder(patientKey, studyId, seriesNo,
					uuid);

			if (!uploadFolder.exists())
				if (!uploadFolder.mkdirs()) {
					throw new CidFileException(String.format(
							"Create directory %s at location %s failed.",
							uploadFolder.getName(), uploadFolder
									.getAbsolutePath()), false);
				}

			File image = new File(uploadFolder, filename);

			fc = new FileOutputStream(image).getChannel();
			buf = ByteBuffer.allocate(imgBinaryArray.length);
			buf.put(imgBinaryArray);
			buf.flip();
			fc.write(buf);
			fc.force(true);
			filePath = image.getParent() + File.separator;
		} catch (FileNotFoundException fne) {
			throw new CidFileException("Upload image not found", fne, false);
		} catch (IOException ioe) {
			throw new CidFileException(String.format(
					"Failed to read upload image: %s%n", filename), ioe, false);
		} finally {
			if (buf != null) {
				buf.clear();
				buf = null;
			}
			if (fc != null) {
				try {
					fc.close();
				} catch (IOException ioe) {
					throw new CidException(String.format(
							"Failed to read upload image: %s%n", filename),
							ioe, false);
				}
			}
		}

		return filePath;
	}

	public int uploadMetaData(String ha7Message) throws CidException {
		String logContent = String
				.format("%s(ha7Message[%s])%n", Thread.currentThread()
						.getStackTrace()[1].getMethodName(), ha7Message);
		cidLogger.debug(this.getClass(), logContent);

		Ha7Parser ha7Parser = null;
		CIDData cidData = null;
		String uploadFailedStatus = null;
		String uploadSuccessStatus = null;
		String uploadPendingStatus = null;
		int uploadMetaDataStatus = 0;

		if (ha7Message == null || ha7Message.trim().length() == 0)
			throw new CidException("HA7 message is empty", false);

		try {
			ha7Parser = new Ha7Parser();

			cidData = ha7Parser.unmarshalHa7Message(ha7Message);

			for (SeriesDtl seriesDtl : cidData.getStudyDtl().getSeriesDtls()
					.getSeriesDtl()) {

				for (ImageDtl imageDtl : seriesDtl.getImageDtls().getImageDtl()) {
					File imageFile = new File(imageDtl.getImagePath(), imageDtl
							.getImageFile());
					// added on 2011/07/13 by Boris for Image deletion
					// (ISG-RFC-42)
					if (!imageFile.exists()
							&& imageDtl.getImageStatus().intValue() != 0) {
						// if (!imageFile.exists()) {
						throw new CidException(String.format(
								"Image not found: %s%n", imageFile
										.getAbsolutePath()), false);
					}
				}
			}

			// web service input
			String newHa7Message = ha7Parser.marshalHa7Message(cidData);

			cidLogger.info(this.getClass(), newHa7Message);

			// get proxy for ePR web service
			EprCidWSS eprCidWebS = WebServiceLocator.locateEprCidWebS();

			// call web service and evaluate the result
			String response = eprCidWebS.saveCID(newHa7Message);

			cidLogger.info(this.getClass(), response);

			if (response == null || response.trim().length() == 0) {
				throw new CidException("ePR response is empty", false);
			}

			Ha7AckParser ha7AckParser = new Ha7AckParser();
			CIDAck cidAck = null;

			Properties conf = ConfigurationFinder.findConf();
			cidAck = ha7AckParser.unmarshalHa7AckMessage(response);
			uploadFailedStatus = ConfigurationFinder.findProperty(conf,
					"cid.ws.eprcid.imageUploadFail");
			uploadSuccessStatus = ConfigurationFinder.findProperty(conf,
					"cid.ws.eprcid.imageUploadSuccess");
			uploadPendingStatus = ConfigurationFinder.findProperty(conf,
					"cid.ws.eprcid.imageUploadPending");

			if (cidAck.getAckDtl().getActionStatus().equalsIgnoreCase(
					uploadFailedStatus)) {
				if (cidAck.getAckDtl().getActionRemarks() != null)
					throw new CidException(String.format(
							"Image upload failed. Reason: %s%n", cidAck
									.getAckDtl().getActionRemarks()), false);
				else
					throw new CidException(
							"Image upload failed. Reason: no action remark",
							false);

			} else if (cidAck.getAckDtl().getActionStatus().equalsIgnoreCase(
					uploadSuccessStatus))
				uploadMetaDataStatus = UPLOAD_METADATA_SUCCESS_STATUS;
			else if (cidAck.getAckDtl().getActionStatus().equalsIgnoreCase(
					uploadPendingStatus))
				uploadMetaDataStatus = UPLOAD_METADATA_PENDING_STATUS;

		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		} catch (MalformedURLException mue) {
			throw new CidException("ePR web service URL is invalid", mue, false);
		} catch (WebServiceException wse) {
			throw new CidException("ePR web service URL is unavailable", wse,
					false);
		}

		return uploadMetaDataStatus;
	}

	private String generateRunSeq(String year, String nextNo) {
		String logContent = String.format("%s(year[%s], nextNo[%s])%n", Thread
				.currentThread().getStackTrace()[1].getMethodName(), year,
				nextNo);
		cidLogger.debug(this.getClass(), logContent);

		StringBuilder runSeq = new StringBuilder();
		runSeq.append(year.substring(year.length() - 2, year.length()));
		for (int i = 0; i < 8 - nextNo.length(); i++) {
			runSeq.append('0');
		}
		runSeq.append(nextNo);

		return runSeq.toString();
	}

	private char generateCheckDigit(int hospitalId, String runSeq) {
		String logContent = String.format("%s(hospitalId[%s], runSeq[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				hospitalId, runSeq);
		cidLogger.debug(this.getClass(), logContent);

		int chkSum = 0;
		final String CHECK_DIGIT_CHAR_SET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		for (int digitCount = 10; digitCount > 0; digitCount--) {
			String runSeqDigit = runSeq.substring(digitCount - 1, digitCount);
			chkSum += Integer.parseInt(runSeqDigit) * (13 - digitCount);
		}
		chkSum = 11 - (chkSum % 11);
		chkSum += hospitalId;
		chkSum %= CHECK_DIGIT_CHAR_SET.length();

		return CHECK_DIGIT_CHAR_SET.charAt(chkSum);
	}

	private String generateAccessionNo(String hospCde, String deptCde,
			String runSeq, char checkDigit) {
		String logContent = String.format(
				"%s(hospCde[%s], deptCde[%s], runSeq[%s], checkDigit[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				hospCde, deptCde, runSeq, checkDigit);
		cidLogger.debug(this.getClass(), logContent);

		StringBuilder accessionNo = new StringBuilder();
		accessionNo.append(hospCde);
		String rtrimHospCde = ("0" + hospCde).trim().substring(1);
		for (int i = 0; i < 3 - rtrimHospCde.length(); i++) {
			accessionNo.append(' ');
		}
		accessionNo.append(deptCde);
		accessionNo.append(runSeq);
		accessionNo.append(checkDigit);

		return accessionNo.toString();
	}

	private File generateUploadFolder(String patientKey, String studyId,
			String seriesNo, String uuid) throws CidException {
		String logContent = String.format(
				"%s(patientKey[%s], studyId[%s], seriesNo[%s], uuid[%s])%n",
				Thread.currentThread().getStackTrace()[1].getMethodName(),
				patientKey, studyId, seriesNo, uuid);
		cidLogger.debug(this.getClass(), logContent);

		Properties conf = null;
		String imageUploadPath = null;

		try {
			conf = ConfigurationFinder.findConf();
			imageUploadPath = ConfigurationFinder.findProperty(conf,
					"cid.ejb.imageUploadPath");
		} catch (CidConfigException ce) {
			throw new CidException(CidConfigException.READ_FAILED, ce,
					false);
		}

		return new File(generateFolderPath(imageUploadPath, patientKey,
				studyId, seriesNo, uuid));
	}

	private String generateFolderPath(String basePath, String patientKey,
			String studyId, String seriesNo, String uuid) {
		String logContent = String
				.format(
						"%s(basePath[%s], patientKey[%s], studyId[%s], seriesNo[%s], uuid[%s])%n",
						Thread.currentThread().getStackTrace()[1]
								.getMethodName(), basePath, patientKey,
						studyId, seriesNo, uuid);
		cidLogger.debug(this.getClass(), logContent);

		StringBuilder folderPath = new StringBuilder();
		folderPath.append(basePath);
		folderPath.append(File.separator);
		folderPath.append(new SimpleDateFormat(FOLDER_DATE_FORMAT)
				.format(new Date()));
		folderPath.append(File.separator);
		folderPath.append(patientKey);
		folderPath.append(File.separator);
		folderPath.append(studyId);
		folderPath.append(File.separator);
		folderPath.append(seriesNo);
		folderPath.append(File.separator);
		folderPath.append(uuid);

		return folderPath.toString();
	}

}
