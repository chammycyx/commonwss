/**
 * 
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite for all unit tests.
 * 
 * @author LTC638
 * @version 1.0 2010-06-23
 * @since 1.0 2010-06-23
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( {
		hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinderTest.class,
		hk.org.ha.service.biz.image.model.fmk.util.Ha7ParserTest.class,
		hk.org.ha.service.biz.image.model.fmk.util.WebServiceLocatorTest.class,
		hk.org.ha.service.biz.image.model.fmk.util.FileSystemManagerTest.class,
		hk.org.ha.service.biz.image.model.upload.bo.ImageUploadBoImplTest.class})
public class AllUnitTests {

}
