/**
 * 
 */
import static org.junit.Assert.*;

import java.net.URL;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.junit.Before;
import org.junit.Test;

import hk.org.ha.service.biz.image.endpoint.retrieval.ImageRetrievalWebS;
import hk.org.ha.service.biz.image.endpoint.security.SecurityManagerWebS;
import hk.org.ha.service.biz.image.endpoint.template.ImageTemplateWebS;
import hk.org.ha.service.biz.image.endpoint.upload.ImageUploadWebS;
import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;

/**
 * A standalone test to connect to the web service of our deployment.
 * 
 * @author LTC638
 * @version 1.0 2010-06-29
 * @since 1.0 2010-06-29
 */
public class DeployedWebServiceSelfTest {

	private ImageUploadWebS iuWs;

	private ImageRetrievalWebS irWs;

	private ImageTemplateWebS itWs;

	private SecurityManagerWebS smWs;

	@Before
	public void setUp() throws Exception {
		String serviceNs = "http://tempuri.org/";
		String portNs = "http://tempuri.org/";
		QName serviceQname = null;
		Service service = null;
		QName portQname = null;
		URL wsdlLocation = null;
		Properties conf = ConfigurationFinder
				.findConfByParam("cidTestConf.properties");

		wsdlLocation = new URL(conf.getProperty("cid.test.iuws.wsdlUrl"));
		serviceQname = new QName(serviceNs, "ImageUploadWebS");
		service = Service.create(wsdlLocation, serviceQname);
		portQname = new QName(portNs, "ImageUploadWebSSoap");
		iuWs = service.getPort(portQname, ImageUploadWebS.class);

//		// Image Retrieval Web Service proxy
//		wsdlLocation = new URL(conf.getProperty("cid.test.irws.wsdlUrl"));
//		serviceQname = new QName(serviceNs, "ImageRetrievalWebS");
//		service = Service.create(wsdlLocation, serviceQname);
//		portQname = new QName(portNs, "ImageRetrievalWebSSoap");
//		irWs = service.getPort(portQname, ImageRetrievalWebS.class);
//
//		// Image Template Web Service proxy
//		wsdlLocation = new URL(conf.getProperty("cid.test.itws.wsdlUrl"));
//		serviceQname = new QName(serviceNs, "ImageTemplateWebS");
//		service = Service.create(wsdlLocation, serviceQname);
//		portQname = new QName(portNs, "ImageTemplateWebSSoap");
//		itWs = service.getPort(portQname, ImageTemplateWebS.class);
//
//		// Security Manager Web Service proxy
//		wsdlLocation = new URL(conf.getProperty("cid.test.smws.wsdlUrl"));
//		serviceQname = new QName(serviceNs, "SecurityManagerWebS");
//		service = Service.create(wsdlLocation, serviceQname);
//		portQname = new QName(portNs, "SecurityManagerWebSSoap");
//		smWs = service.getPort(portQname, SecurityManagerWebS.class);
	}

	@Test
	public void testImageUploadWebService() {
		// get accession number
		String accessionNo = iuWs.getAccessionNo("QMH", "MR");
		System.out.println(accessionNo);
		assertNotNull(accessionNo);
//		
//		// upload image
//		String patientKey = "1";
//		String studyId = "2";
//		String seriesNo = "3";
//		String filename = "testUploadImage.txt";
//		byte[] imgBinaryArray = "1234567890".getBytes();
//		iuWs.uploadImage(imgBinaryArray, "", filename, patientKey, studyId,
//				seriesNo);
//		assertTrue(true);
	}

}
