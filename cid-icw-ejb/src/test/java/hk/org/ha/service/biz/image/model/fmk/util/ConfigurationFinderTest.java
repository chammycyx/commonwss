package hk.org.ha.service.biz.image.model.fmk.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Properties;

import org.junit.Test;

/**
 * Unit test for <code>ConfigurationFinder</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-23
 * @since 1.0 2010-06-23
 */
public class ConfigurationFinderTest {

	@Test
	public void testFindConf() throws Exception {
		Properties conf = ConfigurationFinder.findConf();
		assertNotNull(conf);
		assertNotNull(conf.getProperty("cid.ejb.imageUploadPath"));
	}

	@Test
	public void testFindConfByParamNoFile() throws Exception {
		Properties conf = ConfigurationFinder.findConfByParam("no file");
		assertNull(conf);
	}

}
