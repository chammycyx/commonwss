/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import org.junit.BeforeClass;
import org.junit.Test;

import hk.org.ha.service.biz.image.model.fmk.util.ha7.ack.CIDAckImage;

/**
 * @author hkk904
 *
 */
public class Ha7AckParserTest {
	private static String sampleXML = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream is = Ha7ParserTest.class.getClassLoader()
				.getResourceAsStream("HA7ACK.xml");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = null;
		StringBuilder fullContent = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			fullContent.append(line.trim());
		}
		sampleXML = fullContent.toString();
	}

	@Test
	public void testUnmarshalAndMarshalHa7Message() throws Exception {
		Ha7AckParser parser = new Ha7AckParser();
		CIDAckImage cidAckImage = parser.unmarshalHa7AckMessage(sampleXML);
		assertEquals(cidAckImage.getMessageDtl().getTransactionCode(), "CIDIMAGE");
		assertEquals(cidAckImage.getMessageDtl().getServerHosp(), "VH");
		assertEquals(cidAckImage.getMessageDtl().getTransactionDtm(), "20100131123456.999");
		assertEquals(cidAckImage.getMessageDtl().getTransactionID(), "TransactionID");
		assertEquals(cidAckImage.getMessageDtl().getSendingApplication(), "CID");
		
		assertEquals(cidAckImage.getImageAckDtl().getAccessionNo(), "VH ER09000260906");
		assertEquals(cidAckImage.getImageAckDtl().getStudyID(), "study_id");
		assertEquals(cidAckImage.getImageAckDtl().getStudyDtm(), "20100131123456");
		assertEquals(cidAckImage.getImageAckDtl().getUploadDtm(), "20100716161217");
		assertEquals(cidAckImage.getImageAckDtl().getUploadStatus(), "Failed");
		assertEquals(cidAckImage.getImageAckDtl().getUploadRemarks(), "VH ER09000260906 CopyFile from smb://DCEPR05X/ePR/CID/ERS/VH_ERSTestImage.JPEG to \\\\DCEPR05X/ePR/CID\\ERS\\VH_ERS\\20100131\\TestImage_v1.JPEG: smb:/DCEPR05X/ePR/CID/ERS/VH_ERSTestImage.JPEG (No such file or directory)");		

		String xml = parser.marshalHa7AckMessage(cidAckImage);
		System.out.println(xml);
		BufferedReader reader = new BufferedReader(new StringReader(xml));
		String line = null;
		StringBuilder fullContent = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			fullContent.append(line.trim());
		}
		assertEquals(fullContent.toString(), sampleXML);
	}
}
