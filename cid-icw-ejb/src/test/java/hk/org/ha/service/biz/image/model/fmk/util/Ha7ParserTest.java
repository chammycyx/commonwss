/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import org.junit.BeforeClass;
import org.junit.Test;

import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDImage;

/**
 * Unit test for <code>Ha7Parser</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-23
 * @since 1.0 2010-06-23
 */
public class Ha7ParserTest {

	private static String sampleXML = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream is = Ha7ParserTest.class.getClassLoader()
				.getResourceAsStream("saveCID_copyFromWindowsFile.xml");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = null;
		StringBuilder fullContent = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			fullContent.append(line.trim());
		}
		sampleXML = fullContent.toString();
	}

	@Test
	public void testUnmarshalAndMarshalHa7Message() throws Exception {
		Ha7Parser parser = new Ha7Parser();
		CIDImage cidImage = parser.unmarshalHa7Message(sampleXML);
		assertEquals(cidImage.getMessageDtl().getTransactionCode(), "A");
		assertEquals(cidImage.getStudyDtl().getSeriesDtls().getSeriesDtl().get(
				0).getSeriesNo(), "R");

		String xml = parser.marshalHa7Message(cidImage);
		BufferedReader reader = new BufferedReader(new StringReader(xml));
		String line = null;
		StringBuilder fullContent = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			fullContent.append(line.trim());
		}
		assertEquals(fullContent.toString(), sampleXML);
	}

}
