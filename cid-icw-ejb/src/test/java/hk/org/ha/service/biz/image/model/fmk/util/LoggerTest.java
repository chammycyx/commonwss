/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDImage;

/**
 * @author HKK904
 *
 */
public class LoggerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@Test
	public void testLogging() throws Exception {
		Logger logger = LoggerFactory.getLogger(LoggerTest.class);
		
		logger.info("hello");
		logger.debug("ADFASDFASDF");
	}
}
