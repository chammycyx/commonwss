/**
 * 
 */
package hk.org.ha.service.biz.image.model.fmk.util;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import hk.org.ha.service.biz.image.endpoint.eprcid.EprCidWSS;

/**
 * Unit test for <code>WebServiceLocator</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-23
 * @since 1.0 2010-06-23
 */
public class WebServiceLocatorTest {

	@Test
	public void testLocateEprWebS() throws Exception {
		EprCidWSS eprCidWebS = WebServiceLocator.locateEprCidWebS();
		assertNotNull(eprCidWebS);
		String response = eprCidWebS.saveCID("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><CIDImage>    <MessageDtl>        <TransactionCode>CIDIMAGE</TransactionCode>        <ServerHosp>VH</ServerHosp>        <TransactionDtm>20100131123456.999</TransactionDtm>        <TransactionID>TransactionID</TransactionID>        <SendingApplication>CID</SendingApplication>    </MessageDtl>    <PatientDtl>        <HKID>K5454549</HKID>        <PatKey>00008591</PatKey>        <PatName>CHAN, TAI MAN</PatName>        <PatDOB>19490101</PatDOB>        <PatSex>M</PatSex>        <PatClass>0</PatClass>        <DeathIndicator></DeathIndicator>    </PatientDtl>    <VisitDtl>        <VisitHosp></VisitHosp>        <VisitSpec></VisitSpec>        <VisitWardClinic></VisitWardClinic>        <PayCode></PayCode>        <CaseNum>HN05000207X</CaseNum>        <AdtDtm>20100131235959</AdtDtm>    </VisitDtl>    <StudyDtl>        <AccessionNo>VH ER09000260906</AccessionNo>        <StudyID>study_id</StudyID>        <StudyType>ERS</StudyType>        <StudyDtm>20100131123456</StudyDtm>        <SeriesDtls>            <SeriesDtl>                <SeriesNo>3889bda3-473a-4188-9c79-c65cae88917e</SeriesNo>                <ExamType>ES</ExamType>                <ExamDtm>20100131234559</ExamDtm>                <EntityID>123456</EntityID>                <ImageDtls>                    <ImageDtl>                        <ImageFormat>JPEG</ImageFormat>                        <ImagePath>/appl/cid/backup</ImagePath>                        <ImageFile>TestCopyFile_10k.dat</ImageFile>                        <ImageID>8d187942-9ce4-44de-9b70-f464b3d1a832</ImageID>                        <ImageVersion>1</ImageVersion>                        <ImageSequence>1</ImageSequence>                        <ImageKeyword>ImageKeyword</ImageKeyword>                        <ImageHandling>Y</ImageHandling>                        <ImageType>JPEG</ImageType>                        <AnnotationDtls>                            <AnnotationDtl>                                <AnnotationSeq>1</AnnotationSeq>                                <AnnotationType>AN</AnnotationType>                                <AnnotationText>100^2*3</AnnotationText>                                <AnnotationXAxis>123</AnnotationXAxis>                                <AnnotationYAxis>234</AnnotationYAxis>                                <AnnotationStatus>A</AnnotationStatus>                                <AnnotationEditable>Y</AnnotationEditable>                                <AnnotationUpdDtm>20100531123456</AnnotationUpdDtm>                            </AnnotationDtl>                        </AnnotationDtls>                    </ImageDtl>                </ImageDtls>            </SeriesDtl>        </SeriesDtls>    </StudyDtl></CIDImage>");
		assertNotNull(response);
		System.out.println(response);
	}

}
