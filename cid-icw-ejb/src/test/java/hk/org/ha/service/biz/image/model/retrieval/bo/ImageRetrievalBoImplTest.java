/**
 * 
 */
package hk.org.ha.service.biz.image.model.retrieval.bo;

import static org.junit.Assert.*;

import java.io.*;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author hkk904
 * 
 */
public class ImageRetrievalBoImplTest {
	private static String sampleXML = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream is = ImageRetrievalBoImplTest.class.getClassLoader()
				.getResourceAsStream("saveCID_copyFromWindowsFile.xml");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line = null;
		StringBuilder fullContent = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			fullContent.append(line.trim());
		}
		sampleXML = fullContent.toString();
	}

	@After
	public void tearDown() throws Exception {

	}

//	@Test
//	public void testgetCidStudy() {
//		try {
//			ImageRetrievalBoImpl imp = new ImageRetrievalBoImpl();
//			String temp = "";
//			String patientKey = "00008591";
//			String hospCde = "VH";
//			String caseNo = "HN05000207X";
//			String accessionNo = "VH ER09000260906";
//			String seriesNo = "3889bda3-473a-4188-9c79-c65cae88917e";
//			String imageSeqNo = "1";
//			String versionNo = "1";
//			temp = imp.getCidStudy(patientKey, hospCde, caseNo, accessionNo, seriesNo, imageSeqNo, versionNo);
//			assertEquals(temp, sampleXML);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	@Test
	public void testGetCidStudyFirstLastImage() {
		try {
			ImageRetrievalBoImpl imp = new ImageRetrievalBoImpl();
			String temp = "";
			String patientKey = "00008591";
			String hospCde = "VH";
			String caseNo = "HN05000207X";
			String accessionNo = "VH ER09000260906";
			String seriesNo = "3889bda3-473a-4188-9c79-c65cae88917e";
			String imageSeqNo = "1";
			String versionNo = "1";
			temp = imp.getCidStudyFirstLastImage(patientKey, hospCde, caseNo,
					accessionNo, seriesNo, imageSeqNo, versionNo);
			//assertEquals(temp, sampleXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
