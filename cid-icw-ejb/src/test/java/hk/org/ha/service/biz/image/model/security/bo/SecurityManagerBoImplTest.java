/**
 * 
 */
package hk.org.ha.service.biz.image.model.security.bo;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import hk.org.ha.service.biz.image.model.persistence.entity.ServiceFunction;

import hk.org.ha.service.biz.image.endpoint.security.type.WsServiceFunction;

import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileDetailPk;

import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfileDetail;

import hk.org.ha.service.biz.image.model.persistence.entity.TrustedServer;

import hk.org.ha.service.biz.image.model.persistence.entity.TrustedServerPk;

import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfile;
import hk.org.ha.service.biz.image.model.persistence.entity.Hospital;
import hk.org.ha.service.biz.image.model.security.dao.SecurityManagerDao;
import hk.org.ha.service.biz.image.endpoint.security.type.*;


/**
 * @author HKK904
 *
 */
public class SecurityManagerBoImplTest {
	private static SecurityManagerDao mockDao;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		mockDao = createMock(SecurityManagerDao.class);
		TrustedServerPk trustedServerPk = new TrustedServerPk();
		trustedServerPk.setHostName("riscid02");
		trustedServerPk.setSystemId("ERS");
		TrustedServer trustedServer = new TrustedServer();
		trustedServer.setAppKey("5C434A42-642B-4573-AC2A-6FFE57B787CB");
		trustedServer.setIpAddress("160.1.39.188");
		trustedServer.setSessionKey("1FD2B404-662E-48C2-8351-BDF2C40E80CB");
		trustedServer.setTrustedServerPK(trustedServerPk);
		expect(mockDao.findAccessKeyByAppKey("ERS", "riscid02", "5C434A42-642B-4573-AC2A-6FFE57B787CB")).andReturn(
				trustedServer.getSessionKey()).anyTimes();
		ServiceProfile sp = new ServiceProfile();
		sp.setCde("TEST");
		sp.setDefaultTemplateId(3);
		sp.setHospitalCde("ALL");
		sp.setId(3);
		sp.setSystemId("RIS");
		expect(mockDao.findServiceProfile("DRAWSRV", "RIS", "", "TEST")).andReturn(
				null).anyTimes();
		expect(mockDao.findServiceProfile("DRAWSRV", "RIS", "ALL", "TEST")).andReturn(
				null).anyTimes();
		List<ServiceProfileDetail> spdList = new ArrayList<ServiceProfileDetail>();
//		WsServiceFunction wsf = new WsServiceFunction();
//		wsf.setFunctionId("I0001");
//		wsf.setEnabled("Y");
//		wssfList.add(wsf);
//		
//		WsServiceFunction wsf2 = new WsServiceFunction();
//		wsf2.setFunctionId("I0002");
//		wsf2.setEnabled("Y");
//		wssfList.add(wsf2);
		
		ServiceFunction sf = new ServiceFunction();
		sf.setId("I0001");
		sf.setName("HELLO");
		sf.setfEnabled('Y');
		
		ServiceFunction sf2 = new ServiceFunction();
		sf2.setId("I0002");
		sf2.setName("KITTY");
		sf2.setfEnabled('Y');
		
		ServiceProfileDetailPk spk = new ServiceProfileDetailPk();
		spk.setFunctionId("I0001");
		spk.setProfileId(3);
		ServiceProfileDetail spd = new ServiceProfileDetail();
		spd.setfEnabled('Y');
		spd.setServiceProfileDetailPk(spk);
		//spd.setServiceFunction(sf);
		spdList.add(spd);
				
		ServiceProfileDetailPk spk2 = new ServiceProfileDetailPk();
		spk2.setFunctionId("I0002");
		spk2.setProfileId(3);
		ServiceProfileDetail spd2 = new ServiceProfileDetail();
		spd2.setfEnabled('Y');
		spd2.setServiceProfileDetailPk(spk2);
		//spd2.setServiceFunction(sf2);
		spdList.add(spd2);
		
		expect(mockDao.findServiceProfileDetail(3)).andReturn(
				spdList).anyTimes();
		replay(mockDao);
	}

	@After
	public void tearDown() throws Exception {

	}
	
	@Test
	public void testGetAccessKey(){
		SecurityManagerBoImpl impl = new SecurityManagerBoImpl();
		String systemId = "ERS";
		String hostName = "riscid02";
		String appKey = "5C434A42-642B-4573-AC2A-6FFE57B787CB";
		String response = "";
		
		impl.setDao(mockDao);
		try{
			response = impl.getAccessKey(systemId, hostName, appKey);
		}catch(Exception e){
			System.out.println(e.toString());
		}
		System.out.println(response);
		//assertEquals("1FD2B404-662E-48C2-8351-BDF2C40E80CB", response);
	}
	
	@Test
	public void testGetApplicationControl(){
		SecurityManagerBoImpl impl = new SecurityManagerBoImpl();
		String applicationId = "DRAWSRV";
		String systemId = "RIS";
		String hospitalCode = "";
		String profileCode = "TEST";
		
		impl.setDao(mockDao);
		try{
			WsApplicationControl wsAppCtrl = impl.getApplicationControl(applicationId, systemId, hospitalCode, profileCode);
			System.out.println(wsAppCtrl);
		}catch(Exception e){
			System.out.println(e.toString());
		}
	}
	
	@Test
	public void testGetFunctions(){
		SecurityManagerBoImpl impl = new SecurityManagerBoImpl();
		impl.setDao(mockDao);
		try{
			ArrayOfServiceFunction aosf = impl.getFunctions(3);
			List<WsServiceFunction> wssfList = aosf.getServiceFunction();
			System.out.println(wssfList.get(0).getFunctionId()+","+wssfList.get(0).getEnabled());
			System.out.println(wssfList.get(1).getFunctionId()+","+wssfList.get(1).getEnabled());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
