/**
 * 
 */
package hk.org.ha.service.biz.image.model.template.bo;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import hk.org.ha.service.biz.image.endpoint.template.type.WsCategory;

import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfCategory;

import hk.org.ha.service.biz.image.model.persistence.entity.TemplateCategory;
import hk.org.ha.service.biz.image.model.template.dao.ImageTemplateDao;
import hk.org.ha.service.biz.image.model.persistence.entity.ServiceProfile;


/**
 * @author HKK904
 *
 */
public class ImageTemplateBoImplTest {
	private static ImageTemplateDao mockDao;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		mockDao = createMock(ImageTemplateDao.class);
		
		TemplateCategory tc1 = new TemplateCategory();
		tc1.setId(1);
		tc1.setCde("01");
		tc1.setName("A&E");
		tc1.setSubCategory1(null);
		tc1.setStatus('A');
		
		TemplateCategory tc2 = new TemplateCategory();
		tc2.setId(2);
		tc2.setCde("02");
		tc2.setName("Allied Heath/Occ");
		tc2.setSubCategory1("OCC");
		tc2.setStatus('A');
		
		TemplateCategory tc3 = new TemplateCategory();
		tc3.setId(3);
		tc3.setCde("02");
		tc3.setName("Allied Heath");
		tc3.setSubCategory1("PHYSIO");
		tc3.setStatus('A');
		
		TemplateCategory tc4 = new TemplateCategory();
		tc4.setId(4);
		tc4.setCde("03");
		tc4.setName("Dental");
		tc4.setSubCategory1(null);
		tc4.setStatus('A');
		
		ServiceProfile sp = new ServiceProfile();
		sp.setCde("TEST");
		sp.setDefaultTemplateId(3);
		sp.setHospitalCde("ALL");
		sp.setId(3);
		sp.setSystemId("RIS");
		sp.addTemplateCategory(tc1);
		sp.addTemplateCategory(tc2);
		sp.addTemplateCategory(tc3);
		sp.addTemplateCategory(tc4);
		
		List list = new ArrayList();
		list = sp.getTemplateCategories();
		
		expect(mockDao.findTemplateCategoryByProfileId(3)).andReturn(
				list).anyTimes();
		
		replay(mockDao);
		
		System.out.print(mockDao.findTemplateCategoryByProfileId(3).size());
	}

	@After
	public void tearDown() throws Exception {

	}
	
	@Test
	public void testgetTemplateCategory() throws Exception {
		System.out.println(this.getClass().getClassLoader().getResource("weblogic/wsee/security/unt/ClientUNTCredentialProvider.class").toString());
//		System.out.print("call size:"+mockDao.findTemplateCategoryByProfileId(3).size());
//		ImageTemplateBoImpl impl = new ImageTemplateBoImpl();
//		impl.setDao(mockDao);
//		ArrayOfCategory aoc = impl.getTemplateCategory(3);
//		System.out.println(aoc.getCategory().size());
//		List<WsCategory> wtcList = aoc.getCategory();
//		
//		for(WsCategory wc:wtcList){
//			System.out.println(wc.getId());
//			System.out.println(wc.getCode());
//			System.out.println(wc.getName());
//		}		
	}
}
