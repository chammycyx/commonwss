/**
 * 
 */
package hk.org.ha.service.biz.image.model.upload.bo;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import hk.org.ha.service.biz.image.model.fmk.util.Ha7Parser;
import hk.org.ha.service.biz.image.model.fmk.util.ha7.CIDImage;

import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.persistence.entity.Hospital;
import hk.org.ha.service.biz.image.model.persistence.entity.NextNumberPk;
import hk.org.ha.service.biz.image.model.upload.dao.ImageUploadDao;

/**
 * Unit test for <code>ImageUploadBoImpl</code>.
 * 
 * @author LTC638
 * @version 1.0 2010-06-28
 * @since 1.0 2010-06-28
 */
public class ImageUploadBoImplTest {

	private static ImageUploadDao mockDao;

	private List<File> fileToDel = new ArrayList<File>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		mockDao = createMock(ImageUploadDao.class);
		expect(mockDao.generateNextNo(anyObject(NextNumberPk.class)))
				.andReturn(10).anyTimes();
		Hospital hospital = new Hospital();
		hospital.setId(99);
		expect(mockDao.findHospitalByKey(anyObject(String.class))).andReturn(
				hospital).anyTimes();
		replay(mockDao);
	}

	@After
	public void tearDown() throws Exception {
		Collections.reverse(fileToDel);
		for (File file : fileToDel) {
			file.delete();
		}
		fileToDel.clear();
	}

//	@Test
//	public void testGetAccessionNo() throws Exception {
//		String hospCde = "AAA";
//		String deptCde = "BB";
//		String expectedAccessionNo = "AAABB1000000010X";
//
//		ImageUploadBoImpl impl = new ImageUploadBoImpl();
//		impl.setDao(mockDao);
//		String accessionNo = impl.getAccessionNo(hospCde, deptCde);
//		assertEquals(accessionNo, expectedAccessionNo);
//	}
//
//	@Test
//	public void testValidateAccessionNo() throws Exception {
//		String hospCde = "AAA";
//		String deptCde = "BB";
//		String accessionNo = "AAABB1000000010X";
//
//		ImageUploadBoImpl impl = new ImageUploadBoImpl();
//		impl.setDao(mockDao);
//		assertTrue(impl.validateAccessionNo(hospCde, deptCde, accessionNo));
//	}
//
//	@Test
//	public void testUploadImage() throws Exception {
//		String rootPath = ConfigurationFinder.findConf().getProperty(
//				"cid.ejb.imageUploadPath");
//		String patientKey = "1";
//		String studyId = "2";
//		String seriesNo = "3";
//		String filename = "testUploadImage.txt";
//		byte[] strBytes = "1234567890".getBytes();
//		String datePart = new SimpleDateFormat("yyyyMMdd").format(new Date());
//
//		fileToDel.add(new File(rootPath));
//		fileToDel.add(new File(fileToDel.get(0), patientKey));
//		fileToDel.add(new File(fileToDel.get(1), datePart));
//		fileToDel.add(new File(fileToDel.get(2), studyId));
//		fileToDel.add(new File(fileToDel.get(3), seriesNo));
//		fileToDel.add(new File(fileToDel.get(4), filename));
//		File targetFile = fileToDel.get(5);
//
//		ImageUploadBoImpl impl = new ImageUploadBoImpl();
//		impl.setDao(mockDao);
//		try{
//		impl.uploadImage(strBytes, "", filename, patientKey, studyId, seriesNo);
//		}catch(Exception e){
//			System.err.println(e.getMessage());
//		}
//
//		assertEquals(strBytes.length, targetFile.length());
//	}
//
//	@Test
//	public void testUploadMetaData() {
//		// TODO implement this test after ImageUploadBoImpl.testUploadMetaData()
//		// is completed
//		
//		Ha7Parser ha7Parser = null;
//		CIDImage cidImage = null;
//		ImageUploadBoImpl impl = new ImageUploadBoImpl();
//		
//		try {
//			InputStream is = ImageUploadBoImplTest.class.
//			getClassLoader()
//			.getResourceAsStream("saveCID_copyFromUnixFile.xml");
//			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
//			String line = null;
//			StringBuilder fullContent = new StringBuilder();
//			while ((line = reader.readLine()) != null) {
//				fullContent.append(line.trim());
//			}
//
//			//ha7Parser = new Ha7Parser();
//			//cidImage = ha7Parser.unmarshalHa7Message(fullContent.toString());
//			impl.uploadMetaData(fullContent.toString());
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//		
//	}
	
	@Test
	public void testReg(){		
        // Create a pattern to match cat
        Pattern p = Pattern.compile("<[\\S]*>");
        // Create a matcher with an input string
        Matcher m = p.matcher("GetAccessKey - HostName: <HostName>, AppKey: <AppKey>");
        StringBuffer sb = new StringBuffer();
        boolean result = m.find();
        // Loop through and create a new String 
        // with the replacements
        while(result) {
        	System.out.println(m.group());
            m.appendReplacement(sb, "dog");
            result = m.find();
        }
        // Add the last segment of input to 
        // the new String
        m.appendTail(sb);
        System.out.println(sb.toString());


	}
}
