package hk.org.ha.service.biz.image.model.persistence.entity;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 * Base entity class for all concrete entities.
 * 
 * @author LTC638
 * @version 1.0 2010-06-15
 * @since 1.0 2010-06-15
 */
@MappedSuperclass
public abstract class AbstractBaseEntity {

	@Column(name = "last_upd_by", nullable = false)
	private String lastUpdBy;

	@Column(name = "last_upd_dtm", nullable = false)
	@Temporal(TIMESTAMP)
	private Date lastUpdDtm;
	
	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
	@Transient
	protected final String LAST_UPDATE_BY = "cid_image";
	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END

	/**
	 * Template method for child to provide entity values' resetting logic.
	 */
	protected abstract void customReset();

	/**
	 * @return the lastUpdBy
	 */
	public String getLastUpdBy() {
		return lastUpdBy;
	}
	
	/*
	 * CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
	 * Param: String - the setter parameter for lastUpdBy attribute
	 * Return: none
	 */
	protected void setLastUpdBy(String lastUpdBy) {
		this.lastUpdBy = lastUpdBy;
	}
	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END

	/**
	 * @return the lastUpdDtm
	 */
	public Date getLastUpdDtm() {
		return (Date)lastUpdDtm.clone();
	}
	
	/*
	 * CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
	 * Param: Date - the setter parameter for lastUpdDtm attribute
	 * Return: none
	 */
	protected void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}
	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END

	/*
	 * CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
	 * Param: none
	 * Return: none
	 */
	@PrePersist
	@PreUpdate
	protected void updateLastUpdtInfo() {
		lastUpdBy = LAST_UPDATE_BY;
		lastUpdDtm = new Date();
	}
	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END

	/**
	 * Reset values in entity.
	 */
	public void reset() {
		customReset();
		lastUpdBy = null;
		lastUpdDtm = null;
	}

	/**
	 * Nullify all references.
	 */
//	@Override
//	protected void finalize() throws Throwable {
//		reset();
//		super.finalize();
//	}

}
