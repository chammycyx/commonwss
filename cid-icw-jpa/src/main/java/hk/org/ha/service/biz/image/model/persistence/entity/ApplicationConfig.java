package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Application Config
 * 
 * @author YKF491
 * @version 1.0 2015-10-23
 * @since 1.0 2015-10-23
 */
@Entity
@Table(name = "cid_application_config")
public class ApplicationConfig extends AbstractBaseEntity {
	
	@EmbeddedId
	private ApplicationConfigPk applicationConfigPk;
	
	@Column(name = "value", nullable = false)
	protected String value;
	
	@Column(name = "status", nullable = false)
	protected String status;
	
	@Column(name = "remark", nullable = true)
	protected String remark;

	@Override
	protected void customReset() {
		value = null;
		status = null;
		remark = null;
	}

	public ApplicationConfigPk getApplicationConfigPk() {
		return applicationConfigPk;
	}

	public void setApplicationConfigPk(ApplicationConfigPk applicationConfigPk) {
		this.applicationConfigPk = applicationConfigPk;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((applicationConfigPk == null) ? 0 : applicationConfigPk
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationConfig other = (ApplicationConfig) obj;
		if (applicationConfigPk == null) {
			if (other.applicationConfigPk != null)
				return false;
		} else if (!applicationConfigPk.equals(other.applicationConfigPk))
			return false;
		return true;
	}

}
