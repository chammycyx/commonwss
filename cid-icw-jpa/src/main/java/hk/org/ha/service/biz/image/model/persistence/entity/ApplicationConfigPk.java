/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Application Config Primary Key
 * 
 * @author YKF491
 * @version 1.0 2015-10-23
 * @since 1.0 2015-10-23
 */
@Embeddable
public class ApplicationConfigPk {

	@Column(name = "hospital_cde", nullable = false)
	protected String hospitalCde;
	
	@Column(name = "system_id", nullable = false)
	protected String systemId;
	
	@Column(name = "application_id", nullable = false)
	protected String applicationId;
	
	@Column(name = "profile_cde", nullable = false)
	protected String profileCde;
	
	@Column(name = "section", nullable = false)
	protected String section;
	
	@Column(name = "section_key", nullable = false)
	protected String sectionKey;

	public String getHospitalCde() {
		return hospitalCde;
	}

	public void setHospitalCde(String hospitalCde) {
		this.hospitalCde = hospitalCde;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getProfileCde() {
		return profileCde;
	}

	public void setProfileCde(String profileCde) {
		this.profileCde = profileCde;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSectionKey() {
		return sectionKey;
	}

	public void setSectionKey(String sectionKey) {
		this.sectionKey = sectionKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((applicationId == null) ? 0 : applicationId.hashCode());
		result = prime * result
				+ ((section == null) ? 0 : section.hashCode());
		result = prime * result
				+ ((sectionKey == null) ? 0 : sectionKey.hashCode());
		result = prime * result
				+ ((hospitalCde == null) ? 0 : hospitalCde.hashCode());
		result = prime * result
				+ ((profileCde == null) ? 0 : profileCde.hashCode());
		result = prime * result
				+ ((systemId == null) ? 0 : systemId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationConfigPk other = (ApplicationConfigPk) obj;
		if (applicationId == null) {
			if (other.applicationId != null)
				return false;
		} else if (!applicationId.equals(other.applicationId))
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		if (sectionKey == null) {
			if (other.sectionKey != null)
				return false;
		} else if (!sectionKey.equals(other.sectionKey))
			return false;
		if (hospitalCde == null) {
			if (other.hospitalCde != null)
				return false;
		} else if (!hospitalCde.equals(other.hospitalCde))
			return false;
		if (profileCde == null) {
			if (other.profileCde != null)
				return false;
		} else if (!profileCde.equals(other.profileCde))
			return false;
		if (systemId == null) {
			if (other.systemId != null)
				return false;
		} else if (!systemId.equals(other.systemId))
			return false;
		return true;
	}
	
}
