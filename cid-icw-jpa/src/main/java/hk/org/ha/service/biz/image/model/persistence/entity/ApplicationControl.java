package hk.org.ha.service.biz.image.model.persistence.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cid_application_control")
public class ApplicationControl extends AbstractBaseEntity {

	@Id
	@Column(name = "application_id", nullable = true)
	private String id;

	@Column(name = "application_name")
	private String name;

	@Column(name = "application_version", nullable = false)
	private String version;

	@Column(name = "f_enabled", nullable = false)
	private Character fEnabled;

	@Column(name = "max_panel_width", nullable = false)
	private Integer maxPanelWidth;

	@Column(name = "max_panel_height", nullable = false)
	private Integer maxPanelHeight;

	@Column(name = "min_panel_width", nullable = false)
	private Integer minPanelWidth;

	@Column(name = "min_panel_height", nullable = false)
	private Integer minPanelHeight;

	@Column(name = "max_upload_file_size", nullable = false)
	private Integer maxUploadFileSize;

	@Column(name = "status", nullable = false)
	private Character status;

	@OneToMany(mappedBy = "applicationControl")
	private List<ServiceProfile> serviceProfiles = new ArrayList<ServiceProfile>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Character getfEnabled() {
		return fEnabled;
	}

	public void setfEnabled(Character fEnabled) {
		this.fEnabled = fEnabled;
	}

	public Integer getMaxPanelWidth() {
		return maxPanelWidth;
	}

	public void setMaxPanelWidth(Integer maxPanelWidth) {
		this.maxPanelWidth = maxPanelWidth;
	}

	public Integer getMaxPanelHeight() {
		return maxPanelHeight;
	}

	public void setMaxPanelHeight(Integer maxPanelHeight) {
		this.maxPanelHeight = maxPanelHeight;
	}

	public Integer getMinPanelWidth() {
		return minPanelWidth;
	}

	public void setMinPanelWidth(Integer minPanelWidth) {
		this.minPanelWidth = minPanelWidth;
	}

	public Integer getMinPanelHeight() {
		return minPanelHeight;
	}

	public void setMinPanelHeight(Integer minPanelHeight) {
		this.minPanelHeight = minPanelHeight;
	}

	public Integer getMaxUploadFileSize() {
		return maxUploadFileSize;
	}

	public void setMaxUploadFileSize(Integer maxUploadFileSize) {
		this.maxUploadFileSize = maxUploadFileSize;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public List<ServiceProfile> getServiceProfiles() {
		return Collections.unmodifiableList(serviceProfiles);
	}

	public void addServiceProfile(ServiceProfile serviceProfile) {
		serviceProfile.setApplicationControl(this);
		serviceProfiles.add(serviceProfile);
	}

	public void removeServiceProfile(ServiceProfile serviceProfile) {
		serviceProfiles.remove(serviceProfile);
	}

	@Override
	protected void customReset() {
		id = null;
		name = null;
		version = null;
		fEnabled = null;
		maxPanelWidth = null;
		maxPanelHeight = null;
		minPanelWidth = null;
		minPanelHeight = null;
		maxUploadFileSize = null;
		status = null;
		serviceProfiles.clear();
	}

}
