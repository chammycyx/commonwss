/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Event entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Entity
@Table(name = "cid_event")
public class Event extends AbstractBaseEntity {

	@Id
	@Column(name = "event_id", nullable = false)
	private String id;

	@Column(name = "event_type", nullable = false)
	private String type;

	@Column(name = "event_source", nullable = false)
	private String source;

	@Column(name = "event_desc", nullable = false)
	private String desc;

	@Column(name = "rowguid", nullable = false)
	private String rowGuid;

	@Column(name = "status_cde", nullable = true)
	private String statusCde;

	@OneToMany(mappedBy = "event")
	private List<EventLog> eventLogs = new ArrayList<EventLog>();

	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
	@Transient
	private boolean propertyChanged = false;
	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
		this.propertyChanged = true;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
		this.propertyChanged = true;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source
	 *            the source to set
	 */
	public void setSource(String source) {
		this.source = source;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
		this.propertyChanged = true;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
		this.propertyChanged = true;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
	}

	/**
	 * @return the rowGuid
	 */
	public String getRowGuid() {
		return rowGuid;
	}

	/**
	 * @param rowGuid
	 *            the rowGuid to set
	 */
	public void setRowGuid(String rowGuid) {
		this.rowGuid = rowGuid;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
		this.propertyChanged = true;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
	}

	/**
	 * @return the statusCde
	 */
	public String getStatusCde() {
		return statusCde;
	}

	/**
	 * @param statusCde
	 *            the statusCde to set
	 */
	public void setStatusCde(String statusCde) {
		this.statusCde = statusCde;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
		this.propertyChanged = true;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
	}

	/**
	 * @return the unmodifiable eventLogs
	 */
	public List<EventLog> getEventLogs() {
		return Collections.unmodifiableList(eventLogs);
	}

	/**
	 * @param eventLog
	 *            the event log to add to <code>eventLogs</code>
	 */
	public void addEventLog(EventLog eventLog) {
		eventLog.setEvent(this);
		eventLogs.add(eventLog);
	}

	/**
	 * @param eventLog
	 *            the event log to remove from <code>eventLogs</code>
	 */
	public void removeEventLog(EventLog eventLog) {
		eventLogs.remove(eventLog);
	}

	@Override
	protected void customReset() {
		id = null;
		type = null;
		source = null;
		desc = null;
		rowGuid = null;
		statusCde = null;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
		this.propertyChanged = false;
		// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
		eventLogs.clear();
	}

	/*
	 * CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] START
	 * Param: String - the setter parameter for groupName attribute
	 * Return: none
	 */
	@PrePersist
	@PreUpdate
	@Override 
	protected void updateLastUpdtInfo() {
		if (propertyChanged) {
			this.setLastUpdBy(super.LAST_UPDATE_BY);
			this.setLastUpdDtm(new Date());
		}
		propertyChanged = false;
	}
	// CID-251: Change to fix cid_event table is unexpectedly updated during the insert of event log [Boris HO 2012-06-25] END
}
