/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "cid_event_log")
public class EventLog extends AbstractBaseEntity {

	@EmbeddedId
	private EventLogPk eventLogPk = null;

	@Column(name = "event_type")
	private String eventType;

	@Column(name = "event_source", nullable = false)
	private String eventSource;

	@Column(name = "event_dtm", nullable = false)
	@Temporal(TIMESTAMP)
	private Date eventDtm;

	@Column(name = "event_desc", nullable = false)
	private String eventDesc;

	@Column(name = "event_data")
	private String eventData;

	@Column(name = "event_status")
	private Character eventStatus;

	@Column(name = "request_sys")
	private String requestSys;

	@Column(name = "hospital_cde")
	private String hospitalCde;

	@Column(name = "computer")
	private String computer;

	@Column(name = "user_id", nullable = false)
	private String userId;

	@Column(name = "specialty_cde")
	private String specialtyCde;

	@Column(name = "patient_key")
	private String patientKey;

	@Column(name = "case_no")
	private String caseNo;

	@Column(name = "accession_no")
	private String accessionNo;
	
	@Column(name = "application_version", nullable = false)
	private String applicationVersion;

	@ManyToOne(optional = false)
	@JoinColumn(name = "event_id", referencedColumnName = "event_id")
	private Event event;

	public EventLogPk getEventLogPK() {
		return eventLogPk;
	}

	public void setEventLogPK(EventLogPk eventLogPk) {
		this.eventLogPk = eventLogPk;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventSource() {
		return eventSource;
	}

	public void setEventSource(String eventSource) {
		this.eventSource = eventSource;
	}

	public Date getEventDtm() {
		return (Date)eventDtm.clone();
	}

	public void setEventDtm(Date eventDtm) {
		this.eventDtm = (Date)eventDtm.clone();
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

	public Character getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(Character eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}

	public String getHospitalCde() {
		return hospitalCde;
	}

	public void setHospitalCde(String hospitalCde) {
		this.hospitalCde = hospitalCde;
	}

	public String getComputer() {
		return computer;
	}

	public void setComputer(String computer) {
		this.computer = computer;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSpecialtyCde() {
		return specialtyCde;
	}

	public void setSpecialtyCde(String specialtyCde) {
		this.specialtyCde = specialtyCde;
	}

	public String getPatientKey() {
		return patientKey;
	}

	public void setPatientKey(String patientKey) {
		this.patientKey = patientKey;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getAccessionNo() {
		return accessionNo;
	}

	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public Event getEvent() {
		return event;
	}

	void setEvent(Event event) {
		this.event = event;
	}

	@Override
	protected void customReset() {
		eventLogPk = null;
		eventType = null;
		eventSource = null;
		eventDtm = null;
		eventDesc = null;
		eventData = null;
		eventStatus = null;
		requestSys = null;
		hospitalCde = null;
		computer = null;
		userId = null;
		specialtyCde = null;
		patientKey = null;
		caseNo = null;
		accessionNo = null;
		applicationVersion = null;
		event = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventLogPk == null) ? 0 : eventLogPk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EventLog other = (EventLog) obj;
		if (eventLogPk == null) {
			if (other.eventLogPk != null) {
				return false;
			}
		} else if (!eventLogPk.equals(other.eventLogPk)) {
			return false;
		}
		return true;
	}

}
