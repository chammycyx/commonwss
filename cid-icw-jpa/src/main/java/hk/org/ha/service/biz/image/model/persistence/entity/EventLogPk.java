package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Primary key of <code>EventLog</code> entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Embeddable
public class EventLogPk {

	@Column(name = "rowguid", nullable = false)
	private String rowGuid;

	@Column(name = "event_id", nullable = false, insertable = false, updatable = false)
	private String eventId;

	/**
	 * @return the rowGuid
	 */
	public String getRowGuid() {
		return rowGuid;
	}

	/**
	 * @param rowGuid
	 *            the rowGuid to set
	 */
	public void setRowGuid(String rowGuid) {
		this.rowGuid = rowGuid;
	}

	/**
	 * @return the eventId
	 */
	public String getEventId() {
		return eventId;
	}

	/**
	 * @param eventId
	 *            the eventId to set
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
		result = prime * result + ((rowGuid == null) ? 0 : rowGuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventLogPk other = (EventLogPk) obj;
		if (eventId == null) {
			if (other.eventId != null)
				return false;
		} else if (!eventId.equals(other.eventId))
			return false;
		if (rowGuid == null) {
			if (other.rowGuid != null)
				return false;
		} else if (!rowGuid.equals(other.rowGuid))
			return false;
		return true;
	}

}
