/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 * Hospital entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Entity
@Table(name = "cid_hospitals")
public class Hospital extends AbstractBaseEntity {

	@Column(name = "hospital_id", nullable = false)
	private Integer id;

	@Id
	@Column(name = "hospital_cde", nullable = false)
	private String cde;

	@Column(name = "hospital_name", nullable = false)
	private String name;

	@Column(name = "hospital_type", nullable = false)
	private Character type;

	@Column(name = "status_cde", nullable = false)
	private Character statusCde;

	@OneToMany(mappedBy = "hospital")
	private List<HospitalApplication> hospitalApplications = new ArrayList<HospitalApplication>();
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the cde
	 */
	public String getCde() {
		return cde;
	}

	/**
	 * @param cde
	 *            the cde to set
	 */
	public void setCde(String cde) {
		this.cde = cde;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public Character getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(Character type) {
		this.type = type;
	}

	/**
	 * @return the statusCde
	 */
	public Character getStatusCde() {
		return statusCde;
	}

	/**
	 * @param statusCde
	 *            the statusCde to set
	 */
	public void setStatusCde(Character statusCde) {
		this.statusCde = statusCde;
	}
	
	/**
	 * @return the unmodifiable eventLogs
	 */
	public List<HospitalApplication> getHospitalApplication() {
		return Collections.unmodifiableList(hospitalApplications);
	}

	/**
	 * @param eventLog
	 *            the event log to add to <code>eventLogs</code>
	 */
	public void addHospitalApplication(HospitalApplication hospitalApplication) {
		hospitalApplication.setHospital(this);
		hospitalApplications.add(hospitalApplication);
	}

	/**
	 * @param eventLog
	 *            the event log to remove from <code>eventLogs</code>
	 */
	public void removeHospitalApplication(HospitalApplication hospitalApplication) {
		hospitalApplications.remove(hospitalApplication);
	}

	@Override
	protected void customReset() {
		id = null;
		cde = null;
		name = null;
		type = null;
		statusCde = null;
	}

}
