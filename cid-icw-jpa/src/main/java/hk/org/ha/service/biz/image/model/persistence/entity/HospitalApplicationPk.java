package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class HospitalApplicationPk {

	@Column(name = "hospital_cde", nullable = false, updatable = false, insertable = false)
	private String cde;

	@Column(name = "application_id", nullable = false, updatable = false, insertable = false)
	private String applicationId;

	@Column(name = "application_version", nullable = false, updatable = false, insertable = false)
	private String applicationVersion;

	public String getCde() {
		return cde;
	}

	public void setCde(String cde) {
		this.cde = cde;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((applicationId == null) ? 0 : applicationId.hashCode());
		result = prime
				* result
				+ ((applicationVersion == null) ? 0 : applicationVersion
						.hashCode());
		result = prime * result + ((cde == null) ? 0 : cde.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HospitalApplicationPk other = (HospitalApplicationPk) obj;
		if (applicationId == null) {
			if (other.applicationId != null)
				return false;
		} else if (!applicationId.equals(other.applicationId))
			return false;
		if (applicationVersion == null) {
			if (other.applicationVersion != null)
				return false;
		} else if (!applicationVersion.equals(other.applicationVersion))
			return false;
		if (cde == null) {
			if (other.cde != null)
				return false;
		} else if (!cde.equals(other.cde))
			return false;
		return true;
	}

	
}
