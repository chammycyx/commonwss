/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 * Next Number entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Entity
@Table(name = "next_to_use_numbers")
public class NextNumber extends AbstractBaseEntity {

	@EmbeddedId
	private NextNumberPk nextNumberPk;

	@Column(name = "next_no", nullable = false)
	private Integer nextNo;
	
	/**
	 * @return the nextNumberPk
	 */
	public NextNumberPk getNextNumberPK() {
		return nextNumberPk;
	}

	/**
	 * @param nextNumberPk
	 *            the nextNumberPk to set
	 */
	public void setNextNumberPK(NextNumberPk nextNumberPk) {
		this.nextNumberPk = nextNumberPk;
	}

	/**
	 * @return the nextNo
	 */
	public Integer getNextNo() {
		return nextNo;
	}

	/**
	 * @param nextNo
	 *            the nextNo to set
	 */
	public void setNextNo(Integer nextNo) {
		this.nextNo = nextNo;
	}

	@Override
	protected void customReset() {
		nextNumberPk = null;
		nextNo = null;
	}

}
