package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Primary key of <code>NextNumber</code> entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Embeddable
public class NextNumberPk {

	@Column(name = "cal_year", nullable = false)
	private Integer calYear;

	@Column(name = "field_name", nullable = false)
	private String fieldName;

	@Column(name = "category_1", nullable = false)
	private String category1;

	@Column(name = "category_2", nullable = false)
	private String category2;

	/**
	 * @return the calYear
	 */
	public Integer getCalYear() {
		return calYear;
	}

	/**
	 * @param calYear
	 *            the calYear to set
	 */
	public void setCalYear(Integer calYear) {
		this.calYear = calYear;
	}

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName
	 *            the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return the category1
	 */
	public String getCategory1() {
		return category1;
	}

	/**
	 * @param category1
	 *            the category1 to set
	 */
	public void setCategory1(String category1) {
		this.category1 = category1;
	}

	/**
	 * @return the category2
	 */
	public String getCategory2() {
		return category2;
	}

	/**
	 * @param category2
	 *            the category2 to set
	 */
	public void setCategory2(String category2) {
		this.category2 = category2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((calYear == null) ? 0 : calYear.hashCode());
		result = prime * result
				+ ((category1 == null) ? 0 : category1.hashCode());
		result = prime * result
				+ ((category2 == null) ? 0 : category2.hashCode());
		result = prime * result
				+ ((fieldName == null) ? 0 : fieldName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NextNumberPk other = (NextNumberPk) obj;
		if (calYear == null) {
			if (other.calYear != null)
				return false;
		} else if (!calYear.equals(other.calYear))
			return false;
		if (category1 == null) {
			if (other.category1 != null)
				return false;
		} else if (!category1.equals(other.category1))
			return false;
		if (category2 == null) {
			if (other.category2 != null)
				return false;
		} else if (!category2.equals(other.category2))
			return false;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		return true;
	}

}
