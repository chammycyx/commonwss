/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 * Service Function entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-15
 * @since 1.0 2010-06-15
 */
@Entity
@Table(name = "cid_service_function")
public class ServiceFunction extends AbstractBaseEntity {

	@Id
	@Column(name = "function_id", nullable = false)
	private String id;

	@Column(name = "function_name")
	private String name;

	@Column(name = "f_enabled", nullable = false)
	private Character fEnabled;

	@Column(name = "status", nullable = false)
	private Character status;

	@OneToMany(mappedBy = "serviceFunction")
	private List<ServiceProfileDetail> serviceProfileDetails = new ArrayList<ServiceProfileDetail>();

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fEnabled
	 */
	public Character getfEnabled() {
		return fEnabled;
	}

	/**
	 * @param fEnabled
	 *            the fEnabled to set
	 */
	public void setfEnabled(Character fEnabled) {
		this.fEnabled = fEnabled;
	}

	/**
	 * @return the status
	 */
	public Character getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Character status) {
		this.status = status;
	}

	/**
	 * @return the unmodifiable serviceProfileDetails
	 */
	public List<ServiceProfileDetail> getServiceProfileDetails() {
		return Collections.unmodifiableList(serviceProfileDetails);
	}

	/**
	 * @param serviceProfileDetail
	 *            the service profile detail to add to
	 *            <code>serviceProfileDetails</code>
	 */
	public void addServiceProfileDetail(
			ServiceProfileDetail serviceProfileDetail) {
		serviceProfileDetail.setServiceFunction(this);
		serviceProfileDetails.add(serviceProfileDetail);
	}

	/**
	 * @param serviceProfileDetail
	 *            the service profile detail to remove from
	 *            <code>serviceProfileDetails</code>
	 */
	public void removeServiceProfileDetail(
			ServiceProfileDetail serviceProfileDetail) {
		serviceProfileDetails.remove(serviceProfileDetail);
	}

	@Override
	protected void customReset() {
		id = null;
		name = null;
		fEnabled = null;
		status = null;
		serviceProfileDetails.clear();
	}

}
