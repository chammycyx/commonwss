/**
 * Change Request History:
 * - CID-192: textbox's default font size [Alex LEE 2012-06-08]
 * - CID-222: marker's default width & height [Alex LEE 2012-06-13]
 * - CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12]
 * - CID-694: Change to provide API for ERS to set the marker & drawing status when CID-Studio is running [Boris HO 2013-07-11]
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * - CID-786: Revise the program flow of clinical photo upload for streamlining the upload workflow [Alex LEE 2013-11-20]
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cid_service_profile")
public class ServiceProfile extends AbstractBaseEntity {

	@Id
	@Column(name = "profile_id", nullable = false)
	private Integer id;

	@Column(name = "system_id", nullable = false)
	private String systemId;

	@Column(name = "profile_cde", nullable = false)
	private String cde;

	@Column(name = "hospital_cde", nullable = false)
	private String hospitalCde;

	@Column(name = "max_upload_file_count", nullable = false)
	private Integer maxUploadFileCount;

	@Column(name = "max_marker_count", nullable = false)
	private Integer maxMarkerCount;

	@Column(name = "default_textbox_font_size", nullable = false)
	private Integer defaultTextboxFontSize;

	@Column(name = "default_marker_width", nullable = false)
	private Integer defaultMarkerWidth;
	
	@Column(name = "default_marker_height", nullable = false)
	private Integer defaultMarkerHeight;
	
	@Column(name = "f_enable_marker_history", nullable = false)
	private boolean enableMarkerHistory;
	
	@Column(name = "f_enable_upload_ui", nullable = false)
	private boolean enableUploadUi;

	@Column(name = "status", nullable = false)
	private Character status;

	@ManyToOne
	@JoinColumn(name = "application_id", referencedColumnName = "application_id")
	private ApplicationControl applicationControl;

	@OneToMany(mappedBy = "serviceProfile")
	private List<ServiceProfileDetail> serviceProfileDetails = new ArrayList<ServiceProfileDetail>();

	@OneToMany(mappedBy = "serviceProfile")
	private List<TemplateCategory> templateCategories = new ArrayList<TemplateCategory>();
	
	@OneToMany(mappedBy = "serviceProfile")
	private List<ServiceProfileVersionedParameter> serviceProfileVersionedParameters = new ArrayList<ServiceProfileVersionedParameter>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getCde() {
		return cde;
	}

	public void setCde(String cde) {
		this.cde = cde;
	}

	public String getHospitalCde() {
		return hospitalCde;
	}

	public void setHospitalCde(String hospitalCde) {
		this.hospitalCde = hospitalCde;
	}

	public Integer getMaxUploadFileCount() {
		return maxUploadFileCount;
	}

	public void setMaxUploadFileCount(Integer maxUploadFileCount) {
		this.maxUploadFileCount = maxUploadFileCount;
	}

	public Integer getMaxMarkerCount() {
		return maxMarkerCount;
	}

	public void setMaxMarkerCount(Integer maxMarkerCount) {
		this.maxMarkerCount = maxMarkerCount;
	}

	public Integer getDefaultTextboxFontSize() {
		return defaultTextboxFontSize;
	}

	public void setDefaultTextboxFontSize(Integer defaultTextboxFontSize) {
		this.defaultTextboxFontSize = defaultTextboxFontSize;
	}

	public Integer getDefaultMarkerWidth() {
		return defaultMarkerWidth;
	}

	public void setDefaultMarkerWidth(Integer defaultMarkerWidth) {
		this.defaultMarkerWidth = defaultMarkerWidth;
	}

	public Integer getDefaultMarkerHeight() {
		return defaultMarkerHeight;
	}

	public void setDefaultMarkerHeight(Integer defaultMarkerHeight) {
		this.defaultMarkerHeight = defaultMarkerHeight;
	}

	public boolean getEnableMarkerHistory() {
		return enableMarkerHistory;
	}

	public void setEnableMarkerHistory(boolean enableMarkerHistory) {
		this.enableMarkerHistory = enableMarkerHistory;
	}

	public boolean getEnableUploadUi() {
		return enableUploadUi;
	}

	public void setEnableUploadUi(boolean enableUploadUi) {
		this.enableUploadUi = enableUploadUi;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public ApplicationControl getApplicationControl() {
		return applicationControl;
	}

	void setApplicationControl(ApplicationControl applicationControl) {
		this.applicationControl = applicationControl;
	}

	public List<ServiceProfileDetail> getServiceProfileDetails() {
		return Collections.unmodifiableList(serviceProfileDetails);
	}

	public void addServiceProfileDetail(ServiceProfileDetail serviceProfileDetail) {
		serviceProfileDetail.setServiceProfile(this);
		serviceProfileDetails.add(serviceProfileDetail);
	}

	public void removeServiceProfileDetail(ServiceProfileDetail serviceProfileDetail) {
		serviceProfileDetails.remove(serviceProfileDetail);
	}

	public List<TemplateCategory> getTemplateCategories() {
		return Collections.unmodifiableList(templateCategories);
	}

	public void removeTemplateCategory(TemplateCategory templateCategory) {
		templateCategories.remove(templateCategory);
	}

	public void addTemplateCategory(TemplateCategory templateCategory) {
		templateCategory.setServiceProfile(this);
		templateCategories.add(templateCategory);
	}

	public List<ServiceProfileVersionedParameter> getServiceProfileVersionedParameters() {
		return Collections.unmodifiableList(serviceProfileVersionedParameters);
	}
	
	public void removeServiceProfileVersionedParameter(ServiceProfileVersionedParameter serviceProfileVersionedParameter) {
		serviceProfileVersionedParameters.remove(serviceProfileVersionedParameter);
	}
	
	public void addServiceProfileVersionedParameter(ServiceProfileVersionedParameter serviceProfileVersionedParameter) {
		serviceProfileVersionedParameter.setServiceProfile(this);
		serviceProfileVersionedParameters.add(serviceProfileVersionedParameter);
	}
	
	@Override
	protected void customReset() {
		id = null;
		systemId = null;
		cde = null;
		hospitalCde = null;
		maxUploadFileCount = null;
		maxMarkerCount = null;
		defaultTextboxFontSize = null;
		status = null;
		applicationControl = null;
		serviceProfileDetails.clear();
		templateCategories.clear();
		serviceProfileVersionedParameters.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ServiceProfile other = (ServiceProfile) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
