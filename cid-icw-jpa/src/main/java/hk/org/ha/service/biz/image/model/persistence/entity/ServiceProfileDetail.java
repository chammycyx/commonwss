/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 * Service Profile Detail entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Entity
@Table(name = "cid_service_profile_detail")
public class ServiceProfileDetail extends AbstractBaseEntity {

	@EmbeddedId
	private ServiceProfileDetailPk serviceProfileDetailPk;

	@Column(name = "f_enabled")
	private Character fEnabled;

	@ManyToOne(optional = false)
	@JoinColumn(name = "function_id", referencedColumnName = "function_id")
	private ServiceFunction serviceFunction;

	@ManyToOne(optional = false)
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
	private ServiceProfile serviceProfile;

	/**
	 * @return the serviceProfileDetailPk
	 */
	public ServiceProfileDetailPk getServiceProfileDetailPk() {
		return serviceProfileDetailPk;
	}

	/**
	 * @param serviceProfileDetailPk
	 *            the serviceProfileDetailPk to set
	 */
	public void setServiceProfileDetailPk(
			ServiceProfileDetailPk serviceProfileDetailPk) {
		this.serviceProfileDetailPk = serviceProfileDetailPk;
	}

	/**
	 * @return the fEnabled
	 */
	public Character getfEnabled() {
		return fEnabled;
	}

	/**
	 * @param fEnabled
	 *            the fEnable to set
	 */
	public void setfEnabled(Character fEnabled) {
		this.fEnabled = fEnabled;
	}

	/**
	 * @return the serviceFunction
	 */
	public ServiceFunction getServiceFunction() {
		return serviceFunction;
	}

	/**
	 * @param serviceFunction
	 *            the serviceFunction to set
	 */
	void setServiceFunction(ServiceFunction serviceFunction) {
		this.serviceFunction = serviceFunction;
	}

	/**
	 * @return the serviceProfile
	 */
	public ServiceProfile getServiceProfile() {
		return serviceProfile;
	}

	/**
	 * @param serviceProfile
	 *            the serviceProfile to set
	 */
	void setServiceProfile(ServiceProfile serviceProfile) {
		this.serviceProfile = serviceProfile;
	}

	@Override
	protected void customReset() {
		fEnabled = null;
		serviceFunction = null;
		serviceProfile = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((serviceProfileDetailPk == null) ? 0
						: serviceProfileDetailPk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ServiceProfileDetail other = (ServiceProfileDetail) obj;
		if (serviceProfileDetailPk == null) {
			if (other.serviceProfileDetailPk != null) {
				return false;
			}
		} else if (!serviceProfileDetailPk.equals(other.serviceProfileDetailPk)) {
			return false;
		}
		return true;
	}

}
