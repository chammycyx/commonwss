/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Primary key of <code>ServiceProfileDetail</code> entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Embeddable
public class ServiceProfileDetailPk {

	@Column(name = "profile_id", nullable = false, insertable = false, updatable = false)
	private Integer profileId;

	@Column(name = "function_id", nullable = false, insertable = false, updatable = false)
	private String functionId;

	/**
	 * @return the profileId
	 */
	public Integer getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId
	 *            the profileId to set
	 */
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the functionId
	 */
	public String getFunctionId() {
		return functionId;
	}

	/**
	 * @param functionId
	 *            the functionId to set
	 */
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((functionId == null) ? 0 : functionId.hashCode());
		result = prime * result
				+ ((profileId == null) ? 0 : profileId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ServiceProfileDetailPk other = (ServiceProfileDetailPk) obj;
		if (functionId == null) {
			if (other.functionId != null) {
				return false;
			}
		} else if (!functionId.equals(other.functionId)) {
			return false;
		}
		if (profileId == null) {
			if (other.profileId != null) {
				return false;
			}
		} else if (!profileId.equals(other.profileId)) {
			return false;
		}
		return true;
	}

}
