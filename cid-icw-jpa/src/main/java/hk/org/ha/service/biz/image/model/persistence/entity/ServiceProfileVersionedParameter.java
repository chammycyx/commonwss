/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cid_service_profile_versioned_parameter")
public class ServiceProfileVersionedParameter extends AbstractBaseEntity {
	
	@EmbeddedId
	private ServiceProfileVersionedParameterPk serviceProfileVersionedParameterPk;

	@Column(name = "parameter_value")
	private String value;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
	private ServiceProfile serviceProfile;
	
	
	public ServiceProfileVersionedParameterPk getServiceProfileVersionedParameterPk() {
		return serviceProfileVersionedParameterPk;
	}

	public void setServiceProfileVersionedParameterPk(
			ServiceProfileVersionedParameterPk serviceProfileVersionedParameterPk) {
		this.serviceProfileVersionedParameterPk = serviceProfileVersionedParameterPk;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ServiceProfile getServiceProfile() {
		return serviceProfile;
	}

	public void setServiceProfile(ServiceProfile serviceProfile) {
		this.serviceProfile = serviceProfile;
	}

	@Override
	protected void customReset() {
		value = null;
		serviceProfile = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((serviceProfileVersionedParameterPk == null) ? 0
						: serviceProfileVersionedParameterPk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceProfileVersionedParameter other = (ServiceProfileVersionedParameter) obj;
		if (serviceProfileVersionedParameterPk == null) {
			if (other.serviceProfileVersionedParameterPk != null)
				return false;
		} else if (!serviceProfileVersionedParameterPk.equals(other.serviceProfileVersionedParameterPk))
			return false;
		return true;
	}

}
