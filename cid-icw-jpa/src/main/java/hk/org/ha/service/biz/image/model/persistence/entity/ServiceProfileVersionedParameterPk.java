/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ServiceProfileVersionedParameterPk {

	@Column(name = "profile_id", nullable = false, insertable = false, updatable = false)
	private Integer profileId;
	
	@Column(name = "application_version", nullable = false)
	private String application_version;
	
	@Column(name = "parameter_key", nullable = false)
	private String key;

	public Integer getProfileId() {
		return profileId;
	}

	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}

	public String getApplication_version() {
		return application_version;
	}

	public void setApplication_version(String application_version) {
		this.application_version = application_version;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((application_version == null) ? 0 : application_version.hashCode());
		result = prime * result
				+ ((key == null) ? 0 : key.hashCode());
		result = prime * result
				+ ((profileId == null) ? 0 : profileId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceProfileVersionedParameterPk other = (ServiceProfileVersionedParameterPk) obj;
		if (application_version == null) {
			if (other.application_version != null)
				return false;
		} else if (!application_version.equals(other.application_version))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (profileId == null) {
			if (other.profileId != null)
				return false;
		} else if (!profileId.equals(other.profileId))
			return false;
		return true;
	}

}
