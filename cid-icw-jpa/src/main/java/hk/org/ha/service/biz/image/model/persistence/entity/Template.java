/*
Change Request History:
- CID-730: Change to stop uploading empty diagram to ePR repository from CID-Studio [Boris HO 2013-06-20]
- CID-730: Change to stop uploading empty diagram to ePR repository from CID-Studio [Boris HO 2013-06-25]
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Template entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-15
 * @since 1.0 2010-06-15
 */
@Entity
@Table(name = "cid_template")
public class Template extends AbstractBaseEntity {

	@Id
	@Column(name = "template_id", nullable = false)
	private Integer id;

	@Column(name = "template_name")
	private String name;

	@Column(name = "format", nullable = false)
	private String format;

	@Column(name = "file_path")
	private String filePath;
	
	@Column(name = "file_name")
	private String fileName;
	
	@Column(name = "type")
	private String type;

	@Column(name = "ver", nullable = false)
	private Integer ver;
	
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] START
	@Column(name = "f_display_in_import", nullable = false)
	private boolean displayInImport;

	@Column(name = "f_display_in_template", nullable = false)
	private boolean displayInTemplate;
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] END
	
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12] START
	@Column(name = "f_is_default_template", nullable = false)
	private boolean isDefaultTemplate;
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12] END
	
	@Column(name = "f_allow_empty_upload", nullable = false)
	private boolean allowEmptyUpload;
	
	@Column(name = "status", nullable = false)
	private Character status;

	@ManyToOne
	@JoinColumn(name = "category_id", referencedColumnName = "category_id")
	private TemplateCategory templateCategory;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the data
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the ver
	 */
	public Integer getVer() {
		return ver;
	}

	/**
	 * @param ver
	 *            the ver to set
	 */
	public void setVer(Integer ver) {
		this.ver = ver;
	}

	/**
	 * @return the status
	 */
	public Character getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Character status) {
		this.status = status;
	}

	/**
	 * @return the templateCategory
	 */
	public TemplateCategory getTemplateCategory() {
		return templateCategory;
	}

	/**
	 * @param templateCategory
	 *            the templateCategory to set
	 */
	void setTemplateCategory(TemplateCategory templateCategory) {
		this.templateCategory = templateCategory;
	}

	@Override
	protected void customReset() {
		id = null;
		name = null;
		format = null;
		filePath = null;
		fileName = null;
		type = null;
		ver = null;
		status = null;
		templateCategory = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Template other = (Template) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	/*
	 * CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] START
	 * Param: none
	 * Return: Integer - the getter return value for displayInImport attribute
	 */
	public boolean getDisplayInImport() {
		return displayInImport;
	}
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] END

	/*
	 * CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] START
	 * Param: Integer - the setter parameter for setting displayInImport attribute
	 * Return: none
	 */
	public void setDisplayInImport(boolean displayInImport) {
		this.displayInImport = displayInImport;
	}
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] END

	/*
	 * CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] START
	 * Param: none
	 * Return: Integer - the getter return value for displayInTemplate attribute
	 */
	public boolean getDisplayInTemplate() {
		return displayInTemplate;
	}
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] END

	/*
	 * CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] START
	 * Param: Integer - the setter parameter for setting displayInTemplate attribute
	 * Return: none
	 */
	public void setDisplayInTemplate(boolean displayInTemplate) {
		this.displayInTemplate = displayInTemplate;
	}
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-06-05] END
	
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12] START
	public boolean getIsDefaultTemplate() {
		return isDefaultTemplate;
	}

	public void setIsDefaultTemplate(boolean isDefaultTemplate) {
		this.isDefaultTemplate = isDefaultTemplate;
	}	
	// CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12] END

	/*
	 * Param: none
	 * Return: boolean - the getter return value for allowEmpty attribute
	 */
	public boolean getAllowEmptyUpload() {
		return allowEmptyUpload;
	}

	/*
	 * Param: allowEmpty - the setter parameter for setting allowEmpty attribute
	 * Return: none
	 */
	public void setAllowEmptyUpload(boolean allowEmptyUpload) {
		this.allowEmptyUpload = allowEmptyUpload;
	}	
}
