/**
 * Change Request History:
 * - CID-204: Change to support dynamically change of the label in the template in CID-Studio [Boris HO 2012-06-25]
 * - CID-158: Change to disable the warning without defaulted template in CID-Studio [Boris HO 2012-07-12]
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cid_template_category")
public class TemplateCategory extends AbstractBaseEntity {

	@Id
	@Column(name = "category_id")
	private Integer id;

	@Column(name = "category_name")
	private String name;

	@Column(name = "category_path")
	private String categoryPath;

	@Column(name = "status", nullable = false)
	private Character status;
	
	@Column (name = "group_name")
	private String groupName;
	
	@Column(name = "application_version")
	private String applicationVersion;

	@ManyToOne
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
	private ServiceProfile serviceProfile;
	
	@OneToMany(mappedBy = "templateCategory")
	private List<Template> templates = new ArrayList<Template>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}	

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public List<Template> getTemplates() {
		return Collections.unmodifiableList(templates);
	}

	public void addTemplate(Template template) {
		template.setTemplateCategory(this);
		templates.add(template);
	}

	public void removeTemplate(Template template) {
		templates.remove(template);
	}

	public ServiceProfile getServiceProfile() {
		return serviceProfile;
	}

	public void setServiceProfile(ServiceProfile serviceProfile) {
		this.serviceProfile = serviceProfile;
	}
	
	public String getCategoryPath() {
		return categoryPath;
	}

	public void setCategoryPath(String categoryPath) {
		this.categoryPath = categoryPath;
	}

	@Override
	protected void customReset() {
		id = null;
		name = null;
		categoryPath = null;
		status = null;
		groupName = null;
		applicationVersion = null;
		templates.clear();
		serviceProfile = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TemplateCategory other = (TemplateCategory) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
