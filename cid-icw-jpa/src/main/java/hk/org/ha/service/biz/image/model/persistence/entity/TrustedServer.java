/**
 * 
 */
package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 * Trusted Server entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Entity
@Table(name = "cid_trusted_server")
public class TrustedServer extends AbstractBaseEntity {

	@EmbeddedId
	private TrustedServerPk trustedServerPk;

	@Column(name = "ip_address", nullable = false)
	private String ipAddress;

	@Column(name = "app_key", nullable = false)
	private String appKey;

	@Column(name = "access_key", nullable = false)
	private String sessionKey;

	@Column(name = "status", nullable = false)
	private Character status;

	@Column(name = "remarks")
	private String remarks;

	/**
	 * @return the trustedServerPk
	 */
	public TrustedServerPk getTrustedServerPK() {
		return trustedServerPk;
	}

	/**
	 * @param trustedServerPk
	 *            the trustedServerPk to set
	 */
	public void setTrustedServerPK(TrustedServerPk trustedServerPk) {
		this.trustedServerPk = trustedServerPk;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the appKey
	 */
	public String getAppKey() {
		return appKey;
	}

	/**
	 * @param appKey
	 *            the appKey to set
	 */
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	/**
	 * @return the sessionKey
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey
	 *            the sessionKey to set
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	/**
	 * @return the status
	 */
	public Character getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Character status) {
		this.status = status;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	protected void customReset() {
		trustedServerPk = null;
		ipAddress = null;
		appKey = null;
		sessionKey = null;
		status = null;
		remarks = null;
	}

}
