package hk.org.ha.service.biz.image.model.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Primary key of <code>TrustedServer</code> entity.
 * 
 * @author LTC638
 * @version 1.0 2010-06-17
 * @since 1.0 2010-06-17
 */
@Embeddable
public class TrustedServerPk {

	@Column(name = "system_id", nullable = false)
	private String systemId;

	@Column(name = "hostname", nullable = false)
	private String hostName;

	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param systemId
	 *            the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * @param hostName
	 *            the hostName to set
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hostName == null) ? 0 : hostName.hashCode());
		result = prime * result
				+ ((systemId == null) ? 0 : systemId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrustedServerPk other = (TrustedServerPk) obj;
		if (hostName == null) {
			if (other.hostName != null)
				return false;
		} else if (!hostName.equals(other.hostName))
			return false;
		if (systemId == null) {
			if (other.systemId != null)
				return false;
		} else if (!systemId.equals(other.systemId))
			return false;
		return true;
	}

}
