package hk.org.ha.service.biz.image.model.persistence.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

@Entity
@Table(name = "cid_version_control")
public class VersionControl extends AbstractBaseEntity {
	@EmbeddedId
	private VersionControlPk versionControlPk;

	@Column(name = "cid_wss_version", nullable = false)
	private String cidWssVersion;

	@Column(name = "version_enabled", nullable = false)
	private Character versionEnabled;

	@Column(name = "status", nullable = false)
	private Character status;
	
	@OneToMany(mappedBy = "versionControl")
	private List<HospitalApplication> hospitalApplications = new ArrayList<HospitalApplication>();

	public VersionControlPk getVersionControlPk() {
		return versionControlPk;
	}

	public void setVersionControlPk(VersionControlPk versionControlPk) {
		this.versionControlPk = versionControlPk;
	}

	public String getCidWssVersion() {
		return cidWssVersion;
	}

	public void setCidWssVersion(String cidWssVersion) {
		this.cidWssVersion = cidWssVersion;
	}

	public Character getVersionEnabled() {
		return versionEnabled;
	}

	public void setVersionEnabled(Character versionEnabled) {
		this.versionEnabled = versionEnabled;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}
	
	/**
	 * @return the unmodifiable HospitalApplication
	 */
	public List<HospitalApplication> getHospitalApplication() {
		return Collections.unmodifiableList(hospitalApplications);
	}

	/**
	 * @param hospitalApplication
	 *            the hospital application to add to <code>HospitalApplication</code>
	 */
	public void addHospitalApplication(HospitalApplication hospitalApplication) {
		hospitalApplication.setVersionControl(this);
		hospitalApplications.add(hospitalApplication);
	}

	/**
	 * @param hospitalApplication
	 *            the hospital application to remove from <code>HospitalApplication</code>
	 */
	public void removeHospitalApplication(HospitalApplication hospitalApplication) {
		hospitalApplications.remove(hospitalApplication);
	}

	@Override
	protected void customReset() {
		versionControlPk = null;
		cidWssVersion = null;
		versionEnabled = null;
		status = null;
		hospitalApplications.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((versionControlPk == null) ? 0 : versionControlPk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VersionControl other = (VersionControl) obj;
		if (versionControlPk == null) {
			if (other.versionControlPk != null) {
				return false;
			}
		} else if (!versionControlPk.equals(other.versionControlPk)) {
			return false;
		}
		return true;
	}
}
