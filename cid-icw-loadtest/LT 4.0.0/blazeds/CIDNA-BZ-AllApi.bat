@echo off

rem Get the directory of this bat file
set ROOT=%~dp0

rem SoapUI properties
set testSuite=%1
set testCase=%2
set loadTest=%3

rem Soak test properties
set maxLimit=%4
set threadCount=%5

rem Endpoint properties
set username=%6
set password=%7

rem Get the datetime to create a working directory
for /f %%a in ('WMIC OS GET LocalDateTime ^| FIND "."') do set DTS=%%a
set datetime=%DTS:~0,4%-%DTS:~4,2%-%DTS:~6,2%_%DTS:~8,2%-%DTS:~10,2%-%DTS:~12,2%

rem Fail to run it in Windows Task Scheduler
rem for /F "tokens=*" %%a in ("%ROOT%getTimestamp.bat") do set datetime=%%a

rem Create the directory on the fly in need
if not exist %CIDNA%\Result\%testSuite%\%testCase%\%datetime% mkdir %CIDNA%\Result\%testSuite%\%testCase%\%datetime%

rem Run the loadtestrunner.bat
"%SOAPUI_BIN%\loadtestrunner.bat" -s%testSuite% -c%testCase% -l%loadTest% -m%maxLimit% -n%threadCount% -r -f%CIDNA%\Result\%testSuite%\%testCase%\%datetime% "%ROOT%\..\soapui-project\CIDNA-BZ-AllApi-SoakTest-SoapUI-Project.xml" -wText -u%username% -p%password%
