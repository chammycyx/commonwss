Before the load test using the project, replace the environment specific parameter first.
1. Open the Web Service project (CIDNA-WS-AllApi-SoakTest-SoapUI-Project.xml)
	A. config image path
	   replace "[workspace_location]" to the workspace of the soupui project location, e.g. "C:/Users/ysm729/Simon/ICW/loadtest/load_test_workspace/"
	 
	B. config the endpoint host, 
	   replace "hostname1" with the hostname, e.g. dc2cidesst02
	 
	C. config the web service api user and password 
	   replcae "[user]" with the username and "[pwd]" with the password

   	
2. Open the BlazeDS project (CIDNA-BlazeDS-AllApi-SoakTest-soapui-project.xml)
	A. config the username and password of blazeDs 
	   replace "[user:pwd]" with the user:password, e.g. bzProxy1:bz@Proxy2
 
	B. config the endpoint host, 
	   replace "hostname1" with the hostname, e.g. dc2cidesst02
 
3. CIDNA-ESB-SoakTest-SoapUI-Project.xml
    A. config the endpoint host, 
	   replace "hostname1" with the hostname, e.g. dc2cidesst04
	   
	B.    
	   
4. For the load strategy global replacement
   1. Limit Type: COUNT_PER_THREAD(Runs per Thread)/TIME(Seconds)/COUNT(Total Runs)
      replace "<con:limitType>.+</con:limitType>" by regular expression is checked to "<con:limitType>[TIME/COUNT_PER_THREAD/COUNT]</con:limitType>"
	  
   2. Test Limit: replace "<con:testLimit>.+</con:testLimit>" by regular expression is checked to "<con:testLimit>[number]</con:testLimit>"
   
   3. Thread: "<con:threadCount>5</con:threadCount>" to "<con:threadCount>[number]</con:threadCount>"
   
   4. Strategy, Test Delay, Random 
   replace the required postion of the full string: "<con:loadStrategy><con:type>Simple</con:type><con:config><testDelay>10000</testDelay><randomFactor>0.5</randomFactor></con:config></con:loadStrategy>" 
   or simplified string: "<con:loadStrategy>.+</con:loadStrategy>"
   by using "<con:loadStrategy><con:type>.+</con:type><con:config><testDelay>.+</testDelay><randomFactor>.+</randomFactor></con:config></con:loadStrategy>"
