
::set wsendpoint_upload=http://dc1cidsd01:8080/axcid-proxy/ImageUploadWebS
::set wsendpoint_download=http://dc1cidsd01:8080/axcid-proxy/ImageRetrievalWebS
::set emfendpoint=http://dc1cidsd01:8080/axcid-proxy/messagebroker/amf
set soupuiDir=C:\Users\ysm729\Simon\software\soapui-4.5.0\bin
set projectDir=C:\Users\ysm729\Simon\ICW\loadtest\cid-icw-loadtest

:: set limit=1800 :: in minutes
set limit=900

echo off
CALL stress_test_ws_retrieve_workflow_thread 50  stress_test_ws_retrieve_workflow_thread_50
CALL stress_test_ws_retrieve_workflow_thread 30  stress_test_ws_retrieve_workflow_thread_30
CALL stress_test_ws_retrieve_workflow_thread 20  stress_test_ws_retrieve_workflow_thread_20
CALL stress_test_ws_retrieve_workflow_thread 10  stress_test_ws_retrieve_workflow_thread_10
CALL stress_test_ws_retrieve_workflow_thread 5  stress_test_ws_retrieve_workflow_thread_05
CALL stress_test_ws_retrieve_workflow_thread 1  stress_test_ws_retrieve_workflow_thread_01

CALL stress_test_blazeDs_retrieve_workflow_thread 50  stress_test_blazeDs_retrieve_workflow_thread_50
CALL stress_test_blazeDs_retrieve_workflow_thread 30  stress_test_blazeDs_retrieve_workflow_thread_30
CALL stress_test_blazeDs_retrieve_workflow_thread 20  stress_test_blazeDs_retrieve_workflow_thread_20
CALL stress_test_blazeDs_retrieve_workflow_thread 10  stress_test_blazeDs_retrieve_workflow_thread_10
CALL stress_test_blazeDs_retrieve_workflow_thread 5  stress_test_blazeDs_retrieve_workflow_thread_05
CALL stress_test_blazeDs_retrieve_workflow_thread 1  stress_test_blazeDs_retrieve_workflow_thread_01







