set threadCount=%1
set logDirName=%2

:: *****************************************
set reportDir=%projectDir%\LT\stress_test\log\%logDirName%
mkdir %reportDir%


"%soupuiDir%\loadtestrunner.bat" -m"%limit%" -n"%threadCount%" -r -f"%reportDir%" "%projectDir%\CID-Proxy-Load-Test-soapui-project-BlazeDS-Load-Retrieve-workflow.xml"