package hk.org.ha.service.biz.image.fmk.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReadConfServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String configFileParam = request.getServletPath();
		String configFilePath = this.getServletConfig().getInitParameter(configFileParam);
		String mimeType = getServletContext().getMimeType(configFilePath);
		File file = new File(configFilePath);

		response.setContentLength((int) file.length());
		response.setContentType(mimeType);

		FileInputStream in = new FileInputStream(file);
		OutputStream out = response.getOutputStream();

		try {
			byte[] buffer = new byte[1024];
			int count = 0;
			while ((count = in.read(buffer)) != -1) {
				out.write(buffer, 0, count);
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

}
