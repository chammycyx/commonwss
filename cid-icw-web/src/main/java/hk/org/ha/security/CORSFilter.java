package hk.org.ha.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CORSFilter implements Filter {
    
	/**
     * Default constructor.
     */
    public CORSFilter() {
        
    }
 
    /**
     * @see Filter#destroy()
     */
    public void destroy() {
        // TODO Auto-generated method stub
    }
	
    public void doFilter(
    		ServletRequest servletRequest, 
    		ServletResponse servletResponse, 
    		FilterChain chain
    	) throws IOException, ServletException {
    
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String origin = request.getHeader("Origin");
        String allowOriginPattern = "cid-";

        if(origin != null && origin.contains(allowOriginPattern))
            ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Origin", origin);
        else 
        	((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Origin", "*");

        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Credentials", "true");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Methods","POST, OPTIONS");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Headers","Authorization, Content-Type, Cache-Control, Origin, X-Requested-With, Accept, SOAPAction");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin");
        
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        
        if (request.getMethod().equals("OPTIONS")) {
            resp.setStatus(HttpServletResponse.SC_OK);
            return;
        }
        
        chain.doFilter(request, servletResponse);
    }
    
    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }

}