package hk.org.ha.service.biz.image.endpoint.audit;

import hk.org.ha.service.biz.image.endpoint.audit.type.HashMapOfEventValue;
import hk.org.ha.service.biz.image.model.audit.AuditLocal;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;

public class AuditDs {

	public void writeLog(String accessionNo, String caseNo, String computer,
			String eventId, HashMapOfEventValue eventValue, String hospitalCde,
			String patientKey, String requestSys, String specialtyCde,
			String userId, String eventTextData) {
		
		AuditLocal al = LocalEjbLocator.locateAuditEjb();
		al.writeLog(accessionNo, caseNo, computer, eventId, eventValue, hospitalCde, patientKey, requestSys, specialtyCde, userId, eventTextData);
	}
}
