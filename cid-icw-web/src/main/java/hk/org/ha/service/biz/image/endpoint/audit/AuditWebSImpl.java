/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.endpoint.audit;

import hk.org.ha.service.biz.image.endpoint.audit.type.HashMapOfEventValue;
import hk.org.ha.service.biz.image.model.audit.AuditLocal;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;

import javax.jws.WebService;

import weblogic.jws.Policies;
import weblogic.jws.Policy;
import weblogic.jws.security.RolesAllowed;
import weblogic.jws.security.SecurityRole;

@WebService(serviceName = "AuditWebS", portName = "AuditWebSSoap", targetNamespace = "http://cid.ha.org.hk/cid", endpointInterface = "hk.org.ha.service.biz.image.endpoint.audit.AuditWebS")
@Policies( { @Policy(uri = "policy:usernametoken.xml", attachToWsdl = true) })
public class AuditWebSImpl implements AuditWebS {

	@RolesAllowed({ @SecurityRole(role = "cid_image_uploadimgws_role"), 
					@SecurityRole(role = "cid_image_uploadmetadatws_role"),
					@SecurityRole(role = "cid_image_getcidstdflimgws_role") })
	public void writeLog(String accessionNo, String caseNo, String computer,
			String eventId, HashMapOfEventValue eventValue, String hospitalCde,
			String patientKey, String requestSys, String specialtyCde,
			String userId, String eventTextData) {
		AuditLocal al = LocalEjbLocator.locateAuditEjb();
		al.writeLog(accessionNo, caseNo, computer, eventId, eventValue, hospitalCde, patientKey, requestSys, specialtyCde, userId, eventTextData);
	}

}
