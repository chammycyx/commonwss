package hk.org.ha.service.biz.image.endpoint.bds;

import java.security.Principal;


public class EmptyPrincipal implements Principal {
	
	private String name;
	
	public EmptyPrincipal(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
