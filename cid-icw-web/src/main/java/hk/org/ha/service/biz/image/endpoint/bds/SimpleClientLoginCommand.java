/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.endpoint.bds;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletConfig;

import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;

import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;

import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;

import flex.messaging.io.MessageIOConstants;
import flex.messaging.security.LoginCommand;
import flex.messaging.security.LoginCommandExt;

public class SimpleClientLoginCommand implements LoginCommand, LoginCommandExt {
	
	private final static CidLogger cidLogger = CidLoggerFactory.produceLogger();

	private final static int HASH_INITIAL_VALUE = 0;
	
	private final static int usernameHashCode;
	
	private final static int passwordHashCode;
	
	static {
		String username = null;
		String password = null;
		try {
			Properties conf = ConfigurationFinder.findConf();
			username = ConfigurationFinder.findProperty(conf, "cid.bds.auth.username"); 
			password = ConfigurationFinder.findProperty(conf, "cid.bds.auth.password");
			cidLogger.debug(SimpleClientLoginCommand.class, "init username/password:" + username + "/" + password);
		} catch (CidConfigException cce) {
			cidLogger.error(SimpleClientLoginCommand.class, cce);
			cce.printStackTrace();
		}
		
		usernameHashCode = (username != null)?username.hashCode():HASH_INITIAL_VALUE;
		passwordHashCode = (password != null)?password.hashCode():HASH_INITIAL_VALUE;
	}

	public Principal doAuthentication(String username, Object credentials) {
		String password = extractPassword(credentials);
		cidLogger.debug(SimpleClientLoginCommand.class, "doAuthentication username/password:" + username + "/" + password);
		if (usernameHashCode == username.hashCode() && passwordHashCode == password.hashCode()) {
			return new EmptyPrincipal(username);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean doAuthorization(Principal principal, List roles) {
		if (principal == null) {
			return false;
		}
		return true;
	}

	public boolean logout(Principal principal) {
		return true;
	}

	public void start(ServletConfig servletConfig) {
	}

	public void stop() {
	}

	public String getPrincipalNameFromCredentials(String username, Object credentials) {
		if (doAuthentication(username, credentials) != null) {
			return username;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private String extractPassword(Object credentials) {
		String password = null;
		if (credentials instanceof String) {
			password = (String) credentials;
		} else if (credentials instanceof Map) {
			password = (String) ((Map) credentials).get(MessageIOConstants.SECURITY_CREDENTIALS);
		}
		return password;
	}

}
