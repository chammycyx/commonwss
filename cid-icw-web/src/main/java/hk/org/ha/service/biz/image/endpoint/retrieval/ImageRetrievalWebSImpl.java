/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-731: Issue and performance degrade found in TWE production drill [Boris HO 2013-06-21]
 */

package hk.org.ha.service.biz.image.endpoint.retrieval;

import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.retrieval.ImageRetrievalLocal;

import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

import weblogic.jws.Policies;
import weblogic.jws.Policy;
import weblogic.jws.security.RolesAllowed;
import weblogic.jws.security.SecurityRole;

@MTOM
@WebService(serviceName = "ImageRetrievalWebS", portName = "ImageRetrievalWebSSoap", targetNamespace = "http://cid.ha.org.hk/cid", endpointInterface = "hk.org.ha.service.biz.image.endpoint.retrieval.ImageRetrievalWebS")
@Policies( { @Policy(uri = "policy:usernametoken.xml", attachToWsdl = true) })
public class ImageRetrievalWebSImpl implements ImageRetrievalWebS {

	private final String EXPORT_IMAGE_EVENT_ID = "00013";
	private final String GET_CID_IMAGE_EVENT_ID = "00014";
	private final String GET_CID_STUDY_EVENT_ID = "00015";
	private final String GET_CID_STUDY_FIRST_LAST_IMAGE_EVENT_ID = "00016";
	
	@RolesAllowed( { @SecurityRole(role = "cid_image_exportimgws_role") })
	public byte[] exportImage(String ha7Msg, String password, String userId,
			String workstationId, String requestSys) {
		ImageRetrievalLocal ir;

		ir = LocalEjbLocator.locateImageRetrievalEjb();

		return ir.exportImage(ha7Msg, password, userId, workstationId,
				requestSys, EXPORT_IMAGE_EVENT_ID);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_getcidimgws_role") })
	public byte[] getCidImage(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String imageId, String userId,
			String workstationId, String requestSys) {
		ImageRetrievalLocal ir;

		ir = LocalEjbLocator.locateImageRetrievalEjb();

		return ir.getCidImage(patientKey, hospCde, caseNo, accessionNo,
				seriesNo, imageSeqNo, versionNo, imageId, userId,
				workstationId, requestSys, GET_CID_IMAGE_EVENT_ID);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_getcidimgtmbws_role") })
	public byte[] getCidImageThumbnail(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String imageId, String userId,
			String workstationId, String requestSys) {
		ImageRetrievalLocal ir;

		ir = LocalEjbLocator.locateImageRetrievalEjb();

		return ir.getCidImageThumbnail(patientKey, hospCde, caseNo,
				accessionNo, seriesNo, imageSeqNo, versionNo, imageId, userId,
				workstationId, requestSys);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_getbufcidimgws_role") })
	public byte[] getBufferedCidImage(String path, String filename,
			boolean isThumbnail, String userId, String workstationId,
			String requestSys) {
		ImageRetrievalLocal ir;

		ir = LocalEjbLocator.locateImageRetrievalEjb();

		return ir.getBufferedCidImage(path, filename, isThumbnail, userId,
				workstationId, requestSys);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_getcidstdws_role") })
	public String getCidStudy(String patientKey, String hospCde, String caseNo,
			String accessionNo, String seriesNo, String imageSeqNo,
			String versionNo, String userId, String workstationId,
			String requestSys) {
		ImageRetrievalLocal ir;

		ir = LocalEjbLocator.locateImageRetrievalEjb();

		return ir.getCidStudy(patientKey, hospCde, caseNo, accessionNo,
				seriesNo, imageSeqNo, versionNo, userId, workstationId,
				requestSys, GET_CID_STUDY_EVENT_ID);
	}

	@RolesAllowed( { @SecurityRole(role = "cid_image_getcidstdflimgws_role") })
	public String getCidStudyFirstLastImage(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String userId,
			String workstationId, String requestSys) {
		ImageRetrievalLocal ir;

		ir = LocalEjbLocator.locateImageRetrievalEjb();

		return ir.getCidStudyFirstLastImage(patientKey, hospCde, caseNo,
				accessionNo, seriesNo, imageSeqNo, versionNo, userId,
				workstationId, requestSys, GET_CID_STUDY_FIRST_LAST_IMAGE_EVENT_ID);
	}

	/*
	 * Param: patientKey - Patient key of study hospCde - Hospital code of study
	 * caseNo - Case no of study accessionNo - Accession no of study seriesNo -
	 * Series no of study imageSeqNo - Image ID of study versionNo - Version no
	 * of image of study isPrintedCount - Flag to indicate the count is
	 * representing printed image count userId - User id of user invoked the web
	 * service for audit logging purpose workstationId - Workstation id of host
	 * machine invoked the web service for audit logging purpose requestSys -
	 * Request system id of application invoked the web service for audit
	 * logging purpose Return: imageCount - The counting of number of image in
	 * the requested study int - a positive integer to indicate the number of
	 * images in the requested study
	 */
	@RolesAllowed( { @SecurityRole(role = "cid_image_getcidstdimgcntws_role") })
	public int getCidStudyImageCount(String patientKey, String hospCde,
			String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, boolean isPrintedCount,
			String userId, String workstationId, String requestSys) {
		ImageRetrievalLocal ir;

		ir = LocalEjbLocator.locateImageRetrievalEjb();

		return ir.getCidStudyImageCount(patientKey, hospCde, caseNo,
				accessionNo, seriesNo, imageSeqNo, versionNo, isPrintedCount,
				userId, workstationId, requestSys);
	}
}
