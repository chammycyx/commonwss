/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.endpoint.rs.audit;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public interface AuditRs {
	
	/**
	 * 
	 * @param jsonObj
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/writeLog")
	@RolesAllowed({"cid_image_uploadimgws_role","cid_image_uploadmetadatws_role","cid_image_getcidstdflimgws_role"})
	public void writeLog(AuditParam jsonObj);
}
