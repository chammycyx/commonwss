/**
 * Change Request History:
 * - CID-753: Change to support 2 versions of CID modules in EAP platform [Alex LEE 2013-10-07]
 * 
 */
package hk.org.ha.service.biz.image.endpoint.rs.audit;

import java.util.List;

import hk.org.ha.service.biz.image.endpoint.audit.type.EventValue;
import hk.org.ha.service.biz.image.endpoint.audit.type.HashMapOfEventValue;
import hk.org.ha.service.biz.image.model.audit.AuditLocal;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;

import javax.ws.rs.Path;

@Path("/audit/audit-manager")
public class AuditRsImpl implements AuditRs{

	public void writeLog(AuditParam jsonObj){
		String accessionNo = jsonObj.getAccessionNo();
		String caseNo = jsonObj.getCaseNo();
		String computer = jsonObj.getComputer();
		String eventId = jsonObj.getEventId();
		List<EventValue> eventValueList = jsonObj.getEventValue();
		String hospitalCde  = jsonObj.getHospitalCde();
		String patientKey = jsonObj.getPatientKey();
		String requestSys = jsonObj.getRequestSys();
		String specialtyCde = jsonObj.getSpecialtyCde();
		String userId = jsonObj.getUserId();
		String eventTextData = jsonObj.getEventTextData();
		
		HashMapOfEventValue eventValueMap = new HashMapOfEventValue();
	    eventValueMap.setEventValue(eventValueList);
	    
		AuditLocal al = LocalEjbLocator.locateAuditEjb();
		al.writeLog(accessionNo, caseNo, computer, eventId, eventValueMap, 
				hospitalCde, patientKey, requestSys, specialtyCde, userId, eventTextData);
		
	}
}
