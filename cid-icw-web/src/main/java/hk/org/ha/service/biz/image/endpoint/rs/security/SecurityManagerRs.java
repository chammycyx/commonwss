package hk.org.ha.service.biz.image.endpoint.rs.security;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.annotation.security.RolesAllowed;
import org.codehaus.jackson.node.ObjectNode;

public interface SecurityManagerRs {
	
	/**
	 * 
	 * @param jsonObj
	 * @return String
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/getAccessKey")
	@RolesAllowed("cid_image_getacckeyws_role")
	public String getAccessKey(
			SecurityManagerParam jsonObj);

	/**
	 * 
	 * @param jsonObj
	 * @return String
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/validateAccessKey")
	@RolesAllowed("cid_image_valacckeyws_role")
	public String validateAccessKey(
			SecurityManagerParam jsonObj);

	/**
	 * 
	 * @param jsonObj
	 * @return String
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/validateAccessKeyByHostName")
	@RolesAllowed("cid_image_valacckeybyhostws_role")
	public String validateAccessKeyByHostName(
			SecurityManagerParam jsonObj);
	
	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getApplicationControl")
	@RolesAllowed("cid_image_getappctrlws_role")
	public ObjectNode getApplicationControl(
			SecurityManagerParam jsonObj);
	
	
	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getFunctions")
	@RolesAllowed("cid_image_getfuncws_role")
	public ObjectNode getFunctions(
			SecurityManagerParam jsonObj);
	
	/**
	 * 
	 * @param jsonObj
	 * @return String
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/validateApplicationVersion")
	@RolesAllowed("cid_image_valappverws_role")
	public String validateApplicationVersion(
			SecurityManagerParam jsonObj);
	
	/**
	 * 
	 * @param jsonObj
	 * @return String
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/validateApplicationStatus")
	@RolesAllowed("cid_image_valappstatws_role")
	public String validateApplicationStatus(
			SecurityManagerParam jsonObj);

	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getApplicationConfig")
	@RolesAllowed("cid_image_getappcfgws_role")
	public ObjectNode getApplicationConfig(
			SecurityManagerParam jsonObj);
	
}