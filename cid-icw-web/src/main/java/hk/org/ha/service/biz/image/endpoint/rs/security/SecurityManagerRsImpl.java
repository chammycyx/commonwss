package hk.org.ha.service.biz.image.endpoint.rs.security;


import javax.ws.rs.Path;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import hk.org.ha.service.biz.image.endpoint.rs.security.SecurityManagerRs;
import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;
import hk.org.ha.service.biz.image.fmk.util.ApplicationConfigUtil;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.security.SecurityManagerLocal;

@Path("/security/security-manager")
public class SecurityManagerRsImpl implements SecurityManagerRs{

	private final String GET_APPLICATION_CONFIG_EVENT_ID = "00028";
	
	public String getAccessKey(
			SecurityManagerParam jsonObj) {
		
		String systemId = jsonObj.getSystemId();
		String hostName = jsonObj.getHostName();
		String appKey = jsonObj.getAppKey();
		String userId = jsonObj.getUserId();
		String workstationId = jsonObj.getWorkstationId();
		String requestSys = jsonObj.getRequestSys();
		
		SecurityManagerLocal sm;

		sm = LocalEjbLocator.locateSecutiryManagerEjb();

		return sm.getAccessKey(systemId, hostName, appKey, userId,
				workstationId, requestSys);
	}
	
	public String validateAccessKey(
			SecurityManagerParam jsonObj){
		
		String systemId = jsonObj.getSystemId();
		String ipAddress = jsonObj.getIpAddress();
		String accessKey = jsonObj.getAccessKey();
		String userId = jsonObj.getUserId();
		String workstationId = jsonObj.getWorkstationId();
		String requestSys = jsonObj.getRequestSys();
		SecurityManagerLocal sm;
		sm = LocalEjbLocator.locateSecutiryManagerEjb();
	    return String.valueOf(sm.validateAccessKey(systemId, ipAddress, accessKey, userId,
				workstationId, requestSys));
	}
	
	public String validateAccessKeyByHostName(
			SecurityManagerParam jsonObj){
		String systemId = jsonObj.getSystemId();
		String hostName = jsonObj.getHostName();
		String accessKey = jsonObj.getAccessKey();
		String userId = jsonObj.getUserId();
		String workstationId = jsonObj.getWorkstationId();
		String requestSys = jsonObj.getRequestSys();
		SecurityManagerLocal sm;

		sm = LocalEjbLocator.locateSecutiryManagerEjb();
		
		return String.valueOf(sm.validateAccessKeyByHostName(systemId, hostName, accessKey,
				userId, workstationId, requestSys));
	}
	
	public ObjectNode getApplicationControl(
			SecurityManagerParam jsonObj) {
		String applicationId = jsonObj.getApplicationId();
		String applicationVersion = jsonObj.getApplicationVersion();
		String systemId = jsonObj.getSystemId();
		String hospCode = jsonObj.getHospCode();
		String profileCode = jsonObj.getProfileCde();

		SecurityManagerLocal sm;

		sm = LocalEjbLocator.locateSecutiryManagerEjb();

		WsApplicationControl wsApplicationControl = sm.getApplicationControl(applicationId, applicationVersion,
				systemId, hospCode, profileCode);
		
		ObjectMapper mapper = new ObjectMapper(); 
		ObjectNode node = mapper.convertValue(wsApplicationControl, ObjectNode.class);
		
		return node;
	}
	
	public ObjectNode getFunctions(
			SecurityManagerParam jsonObj) {
		int profileId = jsonObj.getProfileId();
		
		SecurityManagerLocal sm;

		sm = LocalEjbLocator.locateSecutiryManagerEjb();
		
		ObjectMapper mapper = new ObjectMapper(); 
		ObjectNode node = mapper.convertValue(sm.getFunctions(profileId), ObjectNode.class);
		
		return node;
	}
	
	
	
	public String validateApplicationStatus(
			SecurityManagerParam jsonObj){
		String applicationId = jsonObj.getApplicationId();
		String hospCode = jsonObj.getHospCode();
		String applicationVersion = jsonObj.getApplicationVersion();
		String userId = jsonObj.getUserId();
		String workstationId = jsonObj.getWorkstationId();
		String requestSys = jsonObj.getRequestSys();
		SecurityManagerLocal sm;

		sm = LocalEjbLocator.locateSecutiryManagerEjb();

		return String.valueOf(sm.validateApplicationStatus(applicationId, hospCode,
				applicationVersion, userId, workstationId, requestSys));
	
	}
	
	
	public String validateApplicationVersion(
			SecurityManagerParam jsonObj) {
		String applicationId = jsonObj.getApplicationId();
		String hospCode = jsonObj.getHospCode();
		String applicationVersion = jsonObj.getApplicationVersion();
		String userId = jsonObj.getUserId();
		String workstationId = jsonObj.getWorkstationId();
		String requestSys = jsonObj.getRequestSys();
		
		SecurityManagerLocal sm;

		sm = LocalEjbLocator.locateSecutiryManagerEjb();

		return String.valueOf(sm.validateApplicationVersion(applicationId, hospCode,
				applicationVersion, userId, workstationId, requestSys));
	}
	
	
	public ObjectNode getApplicationConfig(
			SecurityManagerParam jsonObj){
		String hospCode = jsonObj.getHospCode();
		String systemId = jsonObj.getSystemId();
		String applicationId = jsonObj.getApplicationId();
		String profileCde = jsonObj.getProfileCde();
		String section = jsonObj.getSection();
		String key = jsonObj.getKey();
		String userId = jsonObj.getUserId();
		String workstationId = jsonObj.getWorkstationId();
		String requestSys = jsonObj.getRequestSys();
		SecurityManagerLocal sm;
		
		sm = LocalEjbLocator.locateSecutiryManagerEjb();

		ArrayOfApplicationConfig arrOfAppConfig = sm.getApplicationConfig(hospCode, systemId,
				applicationId, profileCde, section, key, userId,
				workstationId, requestSys, GET_APPLICATION_CONFIG_EVENT_ID);
		
		return ApplicationConfigUtil.regroupArrayOfApplicationConfigToJson(arrOfAppConfig);
	
	}
}