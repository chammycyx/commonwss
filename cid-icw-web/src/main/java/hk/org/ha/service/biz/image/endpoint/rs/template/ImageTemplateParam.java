package hk.org.ha.service.biz.image.endpoint.rs.template;

import java.util.List;

public class ImageTemplateParam {
	private int profileId;
	private int categoryId;	
	private List<Integer> templateId;
	private String imageBase64;
	private String filePath;
	private String filename;
	
	public String getImageBase64() {
		return imageBase64;
	}
	public void setImageBase64(String imageBase64) {
		this.imageBase64 = imageBase64;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getProfileId() {
		return profileId;
	}
	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}
	public List<Integer> getTemplateId() {
		return templateId;
	}
	public void setTemplateId(List<Integer> templateId) {
		this.templateId = templateId;
	}
}
