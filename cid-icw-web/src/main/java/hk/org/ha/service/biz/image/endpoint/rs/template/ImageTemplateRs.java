package hk.org.ha.service.biz.image.endpoint.rs.template;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.annotation.security.RolesAllowed;
import org.codehaus.jackson.node.ObjectNode;

public interface ImageTemplateRs {

	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTemplateCategory")
	@RolesAllowed("cid_image_gettmpcatws_role")
	public ObjectNode getTemplateCategory(
			ImageTemplateParam jsonObj);

	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTemplateInfo")
	@RolesAllowed("cid_image_gettmpinfows_role")
	public ObjectNode getTemplateInfo(
			ImageTemplateParam jsonObj);
	
	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTemplateData")
	@RolesAllowed("cid_image_gettmpdatws_role")
	public ObjectNode getTemplateData(
			ImageTemplateParam jsonObj);
	
	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/uploadImageTemplate")
	@RolesAllowed("cid_image_uploadimgtmpws_role")
	public String uploadImageTemplate(
			ImageTemplateParam jsonObj);

	/**
	 * 
	 * @param jsonObj
	 * @return ObjectNode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTemplateCategoryAndTemplateInfoByProfileId")
	@RolesAllowed("cid_image_gettmpcatinfows_role")
	public ObjectNode getTemplateCategoryAndTemplateInfoByProfileId(
			ImageTemplateParam jsonObj);
	
}