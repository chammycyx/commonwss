package hk.org.ha.service.biz.image.endpoint.rs.template;

import java.util.List;

import javax.ws.rs.Path;


import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import com.sun.jersey.core.util.Base64;


import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfInt;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.template.ImageTemplateLocal;

@Path("/template/image-template")
public class ImageTemplateRsImpl implements ImageTemplateRs{

	public ObjectNode getTemplateCategory(
			ImageTemplateParam jsonObj) {
		int profileId = jsonObj.getProfileId();
		
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();
		ObjectMapper mapper = new ObjectMapper(); 
		ObjectNode node = mapper.convertValue(it.getTemplateCategory(profileId), ObjectNode.class);

		return node;
	}
	
	public ObjectNode getTemplateInfo(ImageTemplateParam jsonObj) {
		int categoryId = jsonObj.getCategoryId();
				
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();

		ObjectMapper mapper = new ObjectMapper(); 
		ObjectNode node = mapper.convertValue(it.getTemplateInfo(categoryId), ObjectNode.class);

		return node;
	}
	
	public ObjectNode getTemplateData(
			ImageTemplateParam jsonObj) {
		
		List<Integer> int1 = jsonObj.getTemplateId();
		ArrayOfInt templateId = new ArrayOfInt();
		templateId.set_int(int1);
		
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();

		ObjectMapper mapper = new ObjectMapper(); 
		ObjectNode node = mapper.convertValue(it.getTemplateData(templateId), ObjectNode.class);

		return node;
	}
	
	public String uploadImageTemplate(
			ImageTemplateParam jsonObj) {
		
		byte[] imageBase64 = Base64.decode(jsonObj.getImageBase64());
		String filePath = jsonObj.getFilePath();
		String filename = jsonObj.getFilename();
		
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();
		return String.valueOf(it.uploadImageTemplate(imageBase64, filePath, filename));
	}
	
	public ObjectNode getTemplateCategoryAndTemplateInfoByProfileId(
			ImageTemplateParam jsonObj) {
		
		int profileId = jsonObj.getProfileId();
		
		ImageTemplateLocal it;
		
		it = LocalEjbLocator.locateImageTemplateEjb();
		
		ObjectMapper mapper = new ObjectMapper(); 
		ObjectNode node = mapper.convertValue(it.getTemplateCategoryAndTemplateInfoByProfileId(profileId), ObjectNode.class);

		return node;
	}
	
}