package hk.org.ha.service.biz.image.endpoint.rs.upload;

public class ImageUploadParam {
	private String hospCde;
	private String deptCde;
	private String userId;
	private String workstationId;
	private String requestSys;
	private String accessionNo;
	
	public String getAccessionNo() {
		return accessionNo;
	}
	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}
	public String getHospCde() {
		return hospCde;
	}
	public void setHospCde(String hospCde) {
		this.hospCde = hospCde;
	}
	public String getDeptCde() {
		return deptCde;
	}
	public void setDeptCde(String deptCde) {
		this.deptCde = deptCde;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWorkstationId() {
		return workstationId;
	}
	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
	public String getRequestSys() {
		return requestSys;
	}
	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
	
	
}
