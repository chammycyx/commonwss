package hk.org.ha.service.biz.image.endpoint.rs.upload;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.annotation.security.RolesAllowed;

public interface ImageUploadRs {

	/**
	 * 
	 * @param jsonObj
	 * @return String
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/getAccessionNo")
	@RolesAllowed("cid_image_getaccnows_role")
	public String getAccessionNo(
			ImageUploadParam jsonObj);
	
	/**
	 * 
	 * @param jsonObj
	 * @return String
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/validateAccessionNo")
	@RolesAllowed("cid_image_valaccnows_role")
	public String validateAccessionNo(
			ImageUploadParam jsonObj);
	
}