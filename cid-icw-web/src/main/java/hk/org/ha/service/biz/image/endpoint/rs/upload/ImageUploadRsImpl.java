package hk.org.ha.service.biz.image.endpoint.rs.upload;

import javax.ws.rs.Path;

import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.upload.ImageUploadLocal;

@Path("/upload/image-upload")
public class ImageUploadRsImpl implements ImageUploadRs{

	public String getAccessionNo(ImageUploadParam jsonObj) {
		String hospCde = jsonObj.getHospCde();
		String deptCde = jsonObj.getDeptCde();
		String userId = jsonObj.getUserId();
		String workstationId = jsonObj.getWorkstationId();
		String requestSys = jsonObj.getRequestSys();
		
		ImageUploadLocal iu;

		iu = LocalEjbLocator.locateImageUploadEjb();

		return iu.getAccessionNo(hospCde, deptCde, userId, workstationId,
				requestSys);
	}
	
	public String validateAccessionNo(ImageUploadParam jsonObj) {
		String hospCde = jsonObj.getHospCde();
		String deptCde = jsonObj.getDeptCde();
		String accessionNo = jsonObj.getAccessionNo();
		
		ImageUploadLocal iu;

		iu = LocalEjbLocator.locateImageUploadEjb();

		return String.valueOf(iu.validateAccessionNo(hospCde, deptCde, accessionNo));
	}
	
}