/**
 * Change Request History:
 * - CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 * - CID-875: Apply performance tuning to CID-Studio's initialization [Alex LEE 2013-12-17]
 */
package hk.org.ha.service.biz.image.endpoint.security;

import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfServiceFunction;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.security.SecurityManagerLocal;

public class SecurityManagerDs {
	
	private final String GET_APPLICATION_CONFIG_EVENT_ID = "00029";

	public String getAccessKey(String systemId, String hostName, String appKey,
			String userId, String workstationId, String requestSys) {
		SecurityManagerLocal sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.getAccessKey(systemId, hostName, appKey, userId,
				workstationId, requestSys);
	}

	public boolean validateAccessKey(String systemId, String ipAddress,
			String accessKey, String userId, String workstationId,
			String requestSys) {
		SecurityManagerLocal sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.validateAccessKey(systemId, ipAddress, accessKey, userId,
				workstationId, requestSys);
	}

	public boolean validateAccessKeyByHostName(String systemId,
			String hostName, String accessKey, String userId,
			String workstationId, String requestSys) {
		SecurityManagerLocal sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.validateAccessKeyByHostName(systemId, hostName, accessKey,
				userId, workstationId, requestSys);
	}

	public WsApplicationControl getApplicationControl(String applicationId,
			String applicationVersion, String systemId, String hospCode,
			String profileCode) {
		SecurityManagerLocal sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.getApplicationControl(applicationId, applicationVersion,
				systemId, hospCode, profileCode);
	}

	public ArrayOfServiceFunction getFunctions(int profileId) {
		SecurityManagerLocal sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.getFunctions(profileId);
	}

	public boolean validateApplicationVersion(String applicationId,
			String hospCode, String applicationVersion, String userId,
			String workstationId, String requestSys) {
		SecurityManagerLocal sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.validateApplicationVersion(applicationId, hospCode,
				applicationVersion, userId, workstationId, requestSys);
	}

	public boolean validateApplicationStatus(String applicationId,
			String hospCode, String applicationVersion, String userId,
			String workstationId, String requestSys) {
		SecurityManagerLocal sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.validateApplicationStatus(applicationId, hospCode,
				applicationVersion, userId, workstationId, requestSys);
	}

	public ArrayOfApplicationConfig getApplicationConfig(String hospitalCde, String systemId,
			String applicationId, String profileCde, String section, String key,
			String userId, String workstationId,
			String requestSys) {
		SecurityManagerLocal sm;
		sm = LocalEjbLocator.locateSecutiryManagerEjb();
		return sm.getApplicationConfig(hospitalCde, systemId, applicationId,
				profileCde, section, key, userId, workstationId,
				requestSys, GET_APPLICATION_CONFIG_EVENT_ID);
	}
}
