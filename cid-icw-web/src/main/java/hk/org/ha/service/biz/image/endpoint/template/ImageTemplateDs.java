/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-19]
*/

package hk.org.ha.service.biz.image.endpoint.template;

import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfCategory;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfInt;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateData;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateInfo;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.template.ImageTemplateLocal;

public class ImageTemplateDs {

	public ArrayOfCategory getTemplateCategory(int profileId) {
		ImageTemplateLocal it = LocalEjbLocator.locateImageTemplateEjb();
		return it.getTemplateCategory(profileId);
	}

	public ArrayOfTemplateInfo getTemplateInfo(int categoryId) {
		ImageTemplateLocal it = LocalEjbLocator.locateImageTemplateEjb();
		return it.getTemplateInfo(categoryId);
	}

	public ArrayOfTemplateInfo getTemplateInfoByCategoryIds(int[] categoryIds) {
		ImageTemplateLocal it = LocalEjbLocator.locateImageTemplateEjb();
		return it.getTemplateInfoByCategoryIds(categoryIds);
	}

	public ArrayOfTemplateData getTemplateData(ArrayOfInt templateId) {
		ImageTemplateLocal it = LocalEjbLocator.locateImageTemplateEjb();
		return it.getTemplateData(templateId);
	}

	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param: profileId - The profile ID of the template category and template info to be retrieved
	 * Return: ArrayOfTemplateInfo - The template info objects with template categories
	 */
	public ArrayOfTemplateInfo getTemplateCategoryAndTemplateInfoByProfileId(
			int profileId) {
		ImageTemplateLocal it = LocalEjbLocator.locateImageTemplateEjb();
		return it.getTemplateCategoryAndTemplateInfoByProfileId(profileId);
	}
}
