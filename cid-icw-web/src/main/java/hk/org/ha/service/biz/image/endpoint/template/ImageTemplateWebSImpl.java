/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-875: Apply performance tuning to CID-Studio's initialization [Boris HO 2013-11-19]
 */

package hk.org.ha.service.biz.image.endpoint.template;

import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfCategory;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfInt;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateData;
import hk.org.ha.service.biz.image.endpoint.template.type.ArrayOfTemplateInfo;
import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.template.ImageTemplateLocal;

import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

import weblogic.jws.Policies;
import weblogic.jws.Policy;
import weblogic.jws.security.RolesAllowed;
import weblogic.jws.security.SecurityRole;

@MTOM
@WebService(serviceName = "ImageTemplateWebS", portName = "ImageTemplateWebSSoap", targetNamespace = "http://cid.ha.org.hk/cid", endpointInterface = "hk.org.ha.service.biz.image.endpoint.template.ImageTemplateWebS")
@Policies({ @Policy(uri = "policy:usernametoken.xml", attachToWsdl = true) })
public class ImageTemplateWebSImpl implements ImageTemplateWebS {

	@RolesAllowed({ @SecurityRole(role = "cid_image_gettmpcatws_role") })
	public ArrayOfCategory getTemplateCategory(int profileId) {
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();

		return it.getTemplateCategory(profileId);
	}

	@RolesAllowed({ @SecurityRole(role = "cid_image_gettmpdatws_role") })
	public ArrayOfTemplateData getTemplateData(ArrayOfInt templateId) {
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();

		return it.getTemplateData(templateId);
	}

	@RolesAllowed({ @SecurityRole(role = "cid_image_gettmpinfows_role") })
	public ArrayOfTemplateInfo getTemplateInfo(int categoryId) {
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();

		return it.getTemplateInfo(categoryId);
	}

	@RolesAllowed({ @SecurityRole(role = "cid_image_uploadimgtmpws_role") })
	public boolean uploadImageTemplate(byte[] imgBinaryArray, String filePath,
			String filename) {
		ImageTemplateLocal it;

		it = LocalEjbLocator.locateImageTemplateEjb();

		return it.uploadImageTemplate(imgBinaryArray, filePath, filename);
	}

	/* This function is for consumer to retrieve Template Info and Template Category in single call 
	 * Param: profileId - The profile ID of the template category and template info to be retrieved
	 * Return: ArrayOfTemplateInfo - The template info objects with template categories
	 */
	@RolesAllowed({ @SecurityRole(role = "cid_image_gettmpcatinfows_role") })
	public ArrayOfTemplateInfo getTemplateCategoryAndTemplateInfoByProfileId(
			int profileId) {
		ImageTemplateLocal it;
		
		it = LocalEjbLocator.locateImageTemplateEjb();
		
		return it.getTemplateCategoryAndTemplateInfoByProfileId(profileId);
	}
}
