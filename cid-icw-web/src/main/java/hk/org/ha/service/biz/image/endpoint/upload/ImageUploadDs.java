/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
- CID-731: Issue and performance degrade found in TWE production drill [Boris HO 2013-06-21]
 */

package hk.org.ha.service.biz.image.endpoint.upload;

import hk.org.ha.service.biz.image.model.fmk.util.LocalEjbLocator;
import hk.org.ha.service.biz.image.model.upload.ImageUploadLocal;

public class ImageUploadDs {

	private final String UPLOAD_IMAGE_EVENT_ID = "00021";
	private final String UPLOAD_METADATA_EVENT_ID = "00022";
	
	public String getAccessionNo(String hospCde, String deptCde, String userId,
			String workstationId, String requestSys) {
		ImageUploadLocal iu = LocalEjbLocator.locateImageUploadEjb();
		return iu.getAccessionNo(hospCde, deptCde, userId, workstationId,
				requestSys);
	}

	public boolean validateAccessionNo(String hospCde, String deptCde,
			String accessionNo) throws Exception {
		ImageUploadLocal iu = LocalEjbLocator.locateImageUploadEjb();
		return iu.validateAccessionNo(hospCde, deptCde, accessionNo);
	}

	public String uploadImage(byte[] imgBinaryArray, String requestSys,
			String filename, String patientKey, String studyId,
			String seriesNo, String userId, String workstationId) {
		ImageUploadLocal iu = LocalEjbLocator.locateImageUploadEjb();
		return iu.uploadImage(imgBinaryArray, requestSys, filename, patientKey,
				studyId, seriesNo, userId, workstationId, UPLOAD_IMAGE_EVENT_ID);
	}

	public int uploadMetaData(String ha7Message, String userId,
			String workstationId, String requestSys) {
		ImageUploadLocal iu = LocalEjbLocator.locateImageUploadEjb();
		return iu.uploadMetaData(ha7Message, userId, workstationId, requestSys, UPLOAD_METADATA_EVENT_ID);
	}

}
