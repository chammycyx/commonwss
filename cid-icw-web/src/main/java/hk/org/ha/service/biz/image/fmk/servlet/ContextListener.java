/*
Change Request History:
- CID-320: Fix FindBugs and PMD issues in CID ICW [Boris HO 2012-12-05]
 */

package hk.org.ha.service.biz.image.fmk.servlet;

import hk.org.ha.service.biz.image.fmk.util.LogScheduleJob;
import hk.org.ha.service.biz.image.fmk.util.ScheduleJob;
import hk.org.ha.service.biz.image.model.fmk.exception.CidConfigException;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;
import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;

import java.text.ParseException;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.ObjectAlreadyExistsException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class ContextListener implements ServletContextListener {
	CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public void contextInitialized(ServletContextEvent arg0) {
		cidLogger.info("HousekeepLogger",
				"CID Image Common Wss Archive Job initializing");

		final String jobDescription = "%nJob Group: %s%nJob Name: %s%n";
		// ClassLoader cl = this.getClass().getClassLoader();
		Properties housekeepProp = null;
		Properties housekeepLogProp = null;
		String jobName = null;
		String jobGroup = null;
		String cronTrigger = null;
		String cronTriggerExpr = null;
		String logJobGroup = null;
		String logJobName = null;
		String logCronTrigger = null;
		String logCronTriggerExpr = null;
		boolean isUnscheduled = false;
		boolean isDeleted = false;

		try {
			housekeepProp = ConfigurationFinder
					.findExternalConfByParam("cid.conf.housekeep.src");
			jobName = housekeepProp.getProperty("cid.web.housekeep.jobName");
			jobGroup = housekeepProp.getProperty("cid.web.housekeep.jobGroup");
			cronTrigger = housekeepProp
					.getProperty("cid.web.housekeep.cronTriggerName");
			cronTriggerExpr = housekeepProp
					.getProperty("cid.web.housekeep.cronTriggerExpression");

			SchedulerFactory sf = new StdSchedulerFactory(housekeepProp);
			Scheduler scheduler = null;

			scheduler = sf.getScheduler();
			scheduler.addGlobalTriggerListener(new LoggingTriggerListener());
			
			// Check the job to be created is exists in the quartz scheduler
			boolean isJobExists = false;
			String[] currentJobGroups = scheduler.getJobGroupNames();
			String[] jobsInGroup;
			for (String tempJobGroup : currentJobGroups) {
				jobsInGroup = scheduler.getJobNames(tempJobGroup);
				for (String tempJobName : jobsInGroup){
					if (tempJobGroup.equals(jobGroup)
							&& tempJobName.equals(jobName))
						isJobExists = true;
				}
			}

			// Unschedule and delete existing job in the quartz scheduler
			if (isJobExists) {
				cidLogger
						.info(
								"HousekeepLogger",
								"The following Schedule Job is being unscheduled and deleted from quartz scheduler");
				isUnscheduled = scheduler.unscheduleJob(cronTrigger, jobGroup);

				cidLogger.info("HousekeepLogger", String.format(jobDescription
						+ "Unscheduled: %s%n", jobGroup, jobName, String
						.valueOf(isUnscheduled)));
				
				isDeleted = scheduler.deleteJob(jobName, jobGroup);
				cidLogger.info("HousekeepLogger", String.format(jobDescription
						+ "Deleted: %s%n", jobGroup, jobName, String
						.valueOf(isDeleted)));
			}

			// Added housekeep job to the quartz scheduler
			JobDetail jobDetail = new JobDetail(jobName, jobGroup,
					ScheduleJob.class);

			Trigger trigger = new CronTrigger(cronTrigger, jobGroup,
					cronTriggerExpr);
			trigger.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_FIRE_ONCE_NOW);
			scheduler.scheduleJob(jobDetail, trigger);
			cidLogger.info("HousekeepLogger",
					"The following Schedule Job is added to quartz scheduler");
			cidLogger.info("HousekeepLogger", String.format(jobDescription,
					jobGroup, jobName));

			scheduler.start();
		} catch (CidConfigException cce){
			cidLogger.error("HousekeepLogger", cce);
		} catch (ObjectAlreadyExistsException oae) {
			// handle the case of server time not synchronized, and display
			// message for the latter server during schedule job addition
			cidLogger
					.info("HousekeepLogger",
							"The following Schedule Job is already exists in quartz scheduler");
			cidLogger.info("HousekeepLogger", String.format(jobDescription,
					jobGroup, jobName));
		} catch (SchedulerException se){
			cidLogger.error("HousekeepLogger", se);
		} catch (ParseException pe) {
			cidLogger.error("HousekeepLogger", pe);
		} 
		
		try {
			housekeepLogProp = ConfigurationFinder
			.findExternalConfByParam("cid.conf.housekeep.log.src");
			logJobGroup = housekeepLogProp.getProperty("cid.web.housekeep.log.jobGroup");
			logJobName = housekeepLogProp.getProperty("cid.web.housekeep.log.jobName");
			logCronTrigger = housekeepLogProp.getProperty("cid.web.housekeep.log.cronTriggerName");
			logCronTriggerExpr = housekeepLogProp
					.getProperty("cid.web.housekeep.log.cronTriggerExpression");

			SchedulerFactory logSf = new StdSchedulerFactory(housekeepLogProp);
			Scheduler logScheduler = null;

			logScheduler = logSf.getScheduler();
			logScheduler.addGlobalTriggerListener(new LoggingTriggerListener());
			
			// Added housekeep log job to the quartz scheduler
			JobDetail logJobDetail = new JobDetail(logJobName, logJobGroup,
					LogScheduleJob.class);

			Trigger logTrigger = new CronTrigger(logCronTrigger, logJobGroup,
					logCronTriggerExpr);
			logTrigger.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_FIRE_ONCE_NOW);
			logScheduler.scheduleJob(logJobDetail, logTrigger);
			cidLogger.info("HousekeepLogger",
					"The following Schedule Job is added to quartz scheduler");
			cidLogger.info("HousekeepLogger", String.format(jobDescription,
					logJobGroup, logJobName));

			logScheduler.start();
		} catch (CidConfigException cce){
			cidLogger.error("HousekeepLogger", cce);
		} catch (ObjectAlreadyExistsException oae) {
			// handle the case of server time not synchronized, and display
			// message for the latter server during schedule job addition
			cidLogger
					.info("HousekeepLogger",
							"The following Schedule Job is already exists in quartz scheduler");
			cidLogger.info("HousekeepLogger", String.format(jobDescription,
					logJobGroup, logJobName));
		} catch (SchedulerException se){
			cidLogger.error("HousekeepLogger", se);
		} catch (ParseException pe) {
			cidLogger.error("HousekeepLogger", pe);
		} catch (Exception e) {
			cidLogger.error("HousekeepLogger", e);
		}

		cidLogger.info("HousekeepLogger",
				"CID Image Common Wss Archive Job initializied");
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		cidLogger.info("HousekeepLogger",
				"CID Image Common Wss Archive Job stopping");
		// modified on 2011-03-16 for Cluster Job and Non Cluster Job isolation
		try {
			// comment on 2011-02-23 by Boris for application support
			// configuration change
			// ClassLoader cl = this.getClass().getClassLoader();
			// Properties conf = new Properties();
			// conf.load(cl.getResourceAsStream("quartz.properties"));
			// SchedulerFactory sf = new StdSchedulerFactory(conf);

			// added on 2011-02-23 by Boris for application support
			// configuration change
			Properties housekeepConf = ConfigurationFinder
					.findExternalConfByParam("cid.conf.housekeep.src");
			SchedulerFactory sf = new StdSchedulerFactory(housekeepConf);
			Scheduler scheduler = sf.getScheduler();
			scheduler.shutdown();
		} catch (Exception e) {
			cidLogger.error("HousekeepLogger", e);
		}
		
		// modified on 2011-03-16 for Cluster Job and Non Cluster Job isolation		
		try {
			Properties housekeepLogConf = ConfigurationFinder
					.findExternalConfByParam("cid.conf.housekeep.log.src");
			
			SchedulerFactory logSf = new StdSchedulerFactory(housekeepLogConf);
			Scheduler logScheduler = logSf.getScheduler();
			logScheduler.shutdown();
		} catch (CidConfigException cce){
			cidLogger.error("HousekeepLogger", cce);
		} catch (SchedulerException se){
			cidLogger.error("HousekeepLogger", se);
		} catch (Exception e) {
			cidLogger.error("HousekeepLogger", e);
		}
		cidLogger.info("HousekeepLogger",
				"CID Image Common Wss Archive Job stopped");
	}

}
