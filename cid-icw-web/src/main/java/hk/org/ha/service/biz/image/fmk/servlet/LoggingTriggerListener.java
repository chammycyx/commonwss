package hk.org.ha.service.biz.image.fmk.servlet;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;

import hk.org.ha.service.biz.image.model.fmk.exception.CidException;

import hk.org.ha.service.biz.image.model.fmk.logging.CidLogger;
import hk.org.ha.service.biz.image.model.fmk.logging.CidLoggerFactory;

public class LoggingTriggerListener implements TriggerListener {

	CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public String getName() {
		return "LoggingTriggerListener";
	}

	public void triggerComplete(Trigger arg0, JobExecutionContext arg1, int arg2) {
		//cidLogger.info("HousekeepLogger", "Detected Trigger COMPLETE!");
		// Do nothing
	}

	public void triggerFired(Trigger arg0, JobExecutionContext arg1) {
		cidLogger.info("HousekeepLogger", String.format("Job \"%s\" is FIRED.", arg0.getJobName()));
	}

	public void triggerMisfired(Trigger arg0) {
		cidLogger.error("HousekeepLogger", new CidException(String.format("Job \"%s\" is MISFIRED.", arg0.getJobName()), true));
	}

	public boolean vetoJobExecution(Trigger arg0, JobExecutionContext arg1) {
		// do nothing
		return false;
	}

}
