package hk.org.ha.service.biz.image.fmk.util;

import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationConfig;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class ApplicationConfigUtil {
	public static ObjectNode regroupArrayOfApplicationConfigToJson(ArrayOfApplicationConfig appConfigArr) {
		List<WsApplicationConfig> appConfigArrList = appConfigArr.getApplicationConfig();
		ObjectMapper mapper = new ObjectMapper();
		
		ObjectNode jsonObj = mapper.createObjectNode();
		
		for(int i=0; i<appConfigArrList.size(); i++) {
			String section = appConfigArrList.get(i).getSection();
			if(!jsonObj.has(section)) {
				jsonObj.put(section, mapper.createObjectNode());
			}
			
			((ObjectNode) jsonObj.get(section)).put(appConfigArrList.get(i).getKey(), appConfigArrList.get(i).getValue());
		}
		
		return jsonObj;
	}
}
