package hk.org.ha.service.biz.image.fmk.util;

import java.util.Calendar;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.fmk.util.FileSystemManager;
import hk.org.ha.service.biz.image.model.fmk.logging.*;

public class LogScheduleJob implements Job {
	CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		cidLogger
				.info("HousekeepLogger", String.format(
						"Quartz Job \"%s\" Started", context.getTrigger()
								.getJobName()));

		try {
			Calendar calLog = Calendar.getInstance();
			int logArchiveOffset = 0;
			String logArchiveUnit = null;
			String logPath = null;

			Properties ejbConf = ConfigurationFinder.findConf();
			logPath = ConfigurationFinder.findProperty(ejbConf,
					"cid.log.logPath");

			Properties scheduleJobProp = ConfigurationFinder
					.findExternalConfByParam("cid.conf.housekeep.log.src");

			logArchiveOffset = Integer.parseInt(ConfigurationFinder
					.findProperty(scheduleJobProp,
							"cid.web.housekeep.log.archiveOffset"));
			logArchiveUnit = ConfigurationFinder.findProperty(scheduleJobProp,
					"cid.web.housekeep.log.archiveUnit");

			calLog.add(getUnitValue(logArchiveUnit), logArchiveOffset);
			FileSystemManager.archiveFileBefore(calLog.getTime(), logPath);
		} catch (Exception e) {
			cidLogger.error("HousekeepLogger", e);
		}
		cidLogger.info("HousekeepLogger", String.format(
				"Quartz Job \"%s\" Ended", context.getTrigger().getJobName()));
	}

	public int getUnitValue(String unit) throws Exception {
		if (unit.equals("Y"))
			return Calendar.YEAR;
		else if (unit.equals("M"))
			return Calendar.MONTH;
		else if (unit.equals("D"))
			return Calendar.DATE;
		else if (unit.equals("H"))
			return Calendar.HOUR;
		else if (unit.equals("m"))
			return Calendar.MINUTE;
		else
			throw new Exception(String.format("The archive unit %s is not valid", unit)); 
	}
}