package hk.org.ha.service.biz.image.fmk.util;

import java.util.Calendar;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import hk.org.ha.service.biz.image.model.fmk.util.ConfigurationFinder;
import hk.org.ha.service.biz.image.model.fmk.util.FileSystemManager;
import hk.org.ha.service.biz.image.model.fmk.logging.*;

public class ScheduleJob implements Job {
	CidLogger cidLogger = CidLoggerFactory.produceLogger();

	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		cidLogger
				.info("HousekeepLogger", String.format(
						"Quartz Job \"%s\" Started", context.getTrigger()
								.getJobName()));

		try {
			Calendar cal = Calendar.getInstance();
//			Calendar calLog = Calendar.getInstance();
			int archiveOffset = 0;
//			int logArchiveOffset = 0;
			String archiveUnit = null;
//			String logArchiveUnit = null;
			String imageUploadPath = null;
			String imageStoragePath = null;
//			String logPath = null;

			Properties ejbConf = ConfigurationFinder.findConf();
			imageUploadPath = ConfigurationFinder.findProperty(ejbConf,
					"cid.ejb.imageUploadPath");
			imageStoragePath = ConfigurationFinder.findProperty(ejbConf,
					"cid.ejb.imageStoragePath");
//			logPath = ConfigurationFinder.findProperty(ejbConf,
//					"cid.log.logPath");

			// comment on 2011-02-23 by Boris for application support
			// configuration change

			// ClassLoader cl = this.getClass().getClassLoader();
			// Properties scheduleJobProp = new Properties();
			// scheduleJobProp
			// .load(cl.getResourceAsStream("housekeep.properties"));

			// added on 2011-02-23 by Boris for application support
			// configuration change
			Properties scheduleJobProp = ConfigurationFinder
					.findExternalConfByParam("cid.conf.housekeep.src");

			archiveOffset = Integer.parseInt(ConfigurationFinder.findProperty(
					scheduleJobProp, "cid.web.housekeep.archiveOffset"));
//			logArchiveOffset = Integer.parseInt(ConfigurationFinder
//					.findProperty(scheduleJobProp,
//							"cid.web.housekeep.log.archiveOffset"));

			archiveUnit = ConfigurationFinder.findProperty(scheduleJobProp,
					"cid.web.housekeep.archiveUnit");
//			logArchiveUnit = ConfigurationFinder.findProperty(scheduleJobProp,
//					"cid.web.housekeep.log.archiveUnit");

			cal.add(getUnitValue(archiveUnit), archiveOffset);
			FileSystemManager
					.archiveFilesBefore(cal.getTime(), imageUploadPath);
			FileSystemManager.archiveFilesBefore(cal.getTime(),
					imageStoragePath);
//			calLog.add(getUnitValue(logArchiveUnit), logArchiveOffset);
//			FileSystemManager.archiveFileBefore(calLog.getTime(), logPath);
		} catch (Exception e) {
			cidLogger.error("HousekeepLogger", e);
		}
		cidLogger.info("HousekeepLogger", String.format(
				"Quartz Job \"%s\" Ended", context.getTrigger().getJobName()));
	}

	public int getUnitValue(String unit) throws Exception {
		if (unit.equals("Y"))
			return Calendar.YEAR;
		else if (unit.equals("M"))
			return Calendar.MONTH;
		else if (unit.equals("D"))
			return Calendar.DATE;
		else if (unit.equals("H"))
			return Calendar.HOUR;
		else if (unit.equals("m"))
			return Calendar.MINUTE;
		else
			throw new Exception(String.format("The archive unit %s is not valid", unit)); 
	}
}