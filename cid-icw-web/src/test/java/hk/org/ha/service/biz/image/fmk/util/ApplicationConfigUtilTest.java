package hk.org.ha.service.biz.image.fmk.util;

import hk.org.ha.service.biz.image.endpoint.security.type.ArrayOfApplicationConfig;
import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationConfig;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ApplicationConfigUtilTest {
	
	@Test
	public void testregroupArrayOfApplicationConfigToJson1() {
		List<WsApplicationConfig> appConfigArrList = new ArrayList<WsApplicationConfig>();
		
		WsApplicationConfig appConfig1 = new WsApplicationConfig();
		appConfig1.setSection("ImageServiceRS");
		appConfig1.setKey("RS_Password");
		appConfig1.setValue("QI7lxc/dCbggsyE1uOHjgg==");
		
		WsApplicationConfig appConfig2 = new WsApplicationConfig();
		appConfig2.setSection("ImageServiceRS");
		appConfig2.setKey("RS_URL");
		appConfig2.setValue("http://dc1cidsd01:8080/axcid-proxy");
		
		WsApplicationConfig appConfig3 = new WsApplicationConfig();
		appConfig3.setSection("ImageServiceRS");
		appConfig3.setKey("RS_Username");
		appConfig3.setValue("QI7lxc/dCbggsyE1uOHjgg==");
		
		appConfigArrList.add(appConfig1);
		appConfigArrList.add(appConfig2);
		appConfigArrList.add(appConfig3);
		
		ArrayOfApplicationConfig appConfigArr = new ArrayOfApplicationConfig();
		appConfigArr.setApplicationConfig(appConfigArrList);
		
		String expectedRslt = "{\"ImageServiceRS\":{\"RS_Password\":\"QI7lxc/dCbggsyE1uOHjgg==\",\"RS_URL\":\"http://dc1cidsd01:8080/axcid-proxy\",\"RS_Username\":\"QI7lxc/dCbggsyE1uOHjgg==\"}}";
		String actualRslt = ApplicationConfigUtil.regroupArrayOfApplicationConfigToJson(appConfigArr).toString();
		
		Assert.assertEquals(expectedRslt, actualRslt);
	}
	
	@Test
	public void testregroupArrayOfApplicationConfigToJson2() {
		List<WsApplicationConfig> appConfigArrList = new ArrayList<WsApplicationConfig>();
		
		WsApplicationConfig appConfig1 = new WsApplicationConfig();
		appConfig1.setSection("ImageServiceRS");
		appConfig1.setKey("RS_Password");
		appConfig1.setValue("QI7lxc/dCbggsyE1uOHjgg==");
		
		WsApplicationConfig appConfig2 = new WsApplicationConfig();
		appConfig2.setSection("ImageServiceRS");
		appConfig2.setKey("RS_URL");
		appConfig2.setValue("http://dc1cidsd01:8080/axcid-proxy");
		
		WsApplicationConfig appConfig3 = new WsApplicationConfig();
		appConfig3.setSection("ImageServiceRS");
		appConfig3.setKey("RS_Username");
		appConfig3.setValue("QI7lxc/dCbggsyE1uOHjgg==");
		
		WsApplicationConfig appConfig4 = new WsApplicationConfig();
		appConfig4.setSection("TestServiceRS");
		appConfig4.setKey("TestKey4");
		appConfig4.setValue("TestValue4");
		
		appConfigArrList.add(appConfig1);
		appConfigArrList.add(appConfig2);
		appConfigArrList.add(appConfig3);
		appConfigArrList.add(appConfig4);
		
		ArrayOfApplicationConfig appConfigArr = new ArrayOfApplicationConfig();
		appConfigArr.setApplicationConfig(appConfigArrList);
		
		String expectedRslt = "{\"ImageServiceRS\":{\"RS_Password\":\"QI7lxc/dCbggsyE1uOHjgg==\",\"RS_URL\":\"http://dc1cidsd01:8080/axcid-proxy\",\"RS_Username\":\"QI7lxc/dCbggsyE1uOHjgg==\"},\"TestServiceRS\":{\"TestKey4\":\"TestValue4\"}}";
		String actualRslt = ApplicationConfigUtil.regroupArrayOfApplicationConfigToJson(appConfigArr).toString();
		
		Assert.assertEquals(expectedRslt, actualRslt);
	}
	
	@Test
	public void testregroupArrayOfApplicationConfigToJson3() {
		List<WsApplicationConfig> appConfigArrList = new ArrayList<WsApplicationConfig>();
		
		WsApplicationConfig appConfig1 = new WsApplicationConfig();
		appConfig1.setSection("ImageServiceRS");
		appConfig1.setKey("RS_Password");
		appConfig1.setValue("QI7lxc/dCbggsyE1uOHjgg==");
		
		WsApplicationConfig appConfig4 = new WsApplicationConfig();
		appConfig4.setSection("TestServiceRS");
		appConfig4.setKey("TestKey4");
		appConfig4.setValue("TestValue4");
		
		WsApplicationConfig appConfig2 = new WsApplicationConfig();
		appConfig2.setSection("ImageServiceRS");
		appConfig2.setKey("RS_URL");
		appConfig2.setValue("http://dc1cidsd01:8080/axcid-proxy");
		
		WsApplicationConfig appConfig3 = new WsApplicationConfig();
		appConfig3.setSection("ImageServiceRS");
		appConfig3.setKey("RS_Username");
		appConfig3.setValue("QI7lxc/dCbggsyE1uOHjgg==");
		
		WsApplicationConfig appConfig5 = new WsApplicationConfig();
		appConfig5.setSection("TestServiceRS");
		appConfig5.setKey("TestKey5");
		appConfig5.setValue("TestValue5");
		
		WsApplicationConfig appConfig6 = new WsApplicationConfig();
		appConfig6.setSection("TestServiceRS");
		appConfig6.setKey("TestKey6");
		appConfig6.setValue("TestValue6");
		
		appConfigArrList.add(appConfig1);
		appConfigArrList.add(appConfig2);
		appConfigArrList.add(appConfig3);
		appConfigArrList.add(appConfig4);
		appConfigArrList.add(appConfig5);
		appConfigArrList.add(appConfig6);
		
		ArrayOfApplicationConfig appConfigArr = new ArrayOfApplicationConfig();
		appConfigArr.setApplicationConfig(appConfigArrList);
		
		String expectedRslt = "{\"ImageServiceRS\":{\"RS_Password\":\"QI7lxc/dCbggsyE1uOHjgg==\",\"RS_URL\":\"http://dc1cidsd01:8080/axcid-proxy\",\"RS_Username\":\"QI7lxc/dCbggsyE1uOHjgg==\"},\"TestServiceRS\":{\"TestKey4\":\"TestValue4\",\"TestKey5\":\"TestValue5\",\"TestKey6\":\"TestValue6\"}}";
		String actualRslt = ApplicationConfigUtil.regroupArrayOfApplicationConfigToJson(appConfigArr).toString();
		
		Assert.assertEquals(expectedRslt, actualRslt);
	}
	
	@Test
	public void testregroupArrayOfApplicationConfigToJson4() {
		List<WsApplicationConfig> appConfigArrList = new ArrayList<WsApplicationConfig>();
		
		ArrayOfApplicationConfig appConfigArr = new ArrayOfApplicationConfig();
		appConfigArr.setApplicationConfig(appConfigArrList);
		
		String expectedRslt = "{}";
		String actualRslt = ApplicationConfigUtil.regroupArrayOfApplicationConfigToJson(appConfigArr).toString();
		
		Assert.assertEquals(expectedRslt, actualRslt);
	}
}
