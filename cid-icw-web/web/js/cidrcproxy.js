//global  constant
CIDRCWEBPLUGIN_PAGE_NAME    = "cidrcwebplugin.js";
CIDRCWEBPLUGIN_CTRL_NAME    = "cidRcWebPlugin";
CIDRCWEBPLUGIN_CTRL_TYPE    = "JSAPI-Auto Javascript Object";
CIDRCWEBPLUGIN_MIME_TYPE    = "application/x-cidrcwebplugin";
CIDRCWEBPLUGIN_ROOT         = "C:\\cms\\"; 

var cidRcWebPluginObj;

//CIDRCProxy constructor  
function CIDRCProxy() {
    try{
		cidRcWebPluginObj = document.createElement("object"); 
        cidRcWebPluginObj.setAttribute("id",CIDRCWEBPLUGIN_CTRL_NAME);
        cidRcWebPluginObj.setAttribute("type",CIDRCWEBPLUGIN_MIME_TYPE);	
        cidRcWebPluginObj.setAttribute("height", 1);
        cidRcWebPluginObj.setAttribute("width", 1);

		document.body.appendChild(cidRcWebPluginObj);

        if(cidRcWebPluginObj==null){
            alert("Failed to load CIDRCProxy Object!");
            return;
        }
        
        var returnMsg = cidRcWebPluginObj.setRootPath(CIDRCWEBPLUGIN_ROOT);
        if (returnMsg != "")
        {
            cidRcWebPluginObj = null;
            alert("Failed to set root path!");
        }
    }
   	catch(e)
	{
		alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"constructor: "+e.description);
	}
}

function getInternetExplorerVersion(){
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).

  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer'){
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}

//public methods and properties
CIDRCProxy.prototype = {
	getSessionKey: function(systemID, accessKey, userID, workstationID) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.getSessionKey(systemID, accessKey, userID, workstationID);
		     
		} catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"getSessionKey: "+e.description);
		}
	},
	
	initialize: function(systemID, sessionKey, posX, posY, height, width) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return -1;
		    }
		    return cidRcWebPluginObj.initialize(systemID, sessionKey, posX, posY, height, width);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"initialize: "+e.description);
		}
	},

	startSession: function(systemID, sessionKey, xmlInput) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return -1;
		    }
		    return cidRcWebPluginObj.startSession(systemID, sessionKey, xmlInput);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"startSession: "+e.description);
		}
	},

	getStatus: function() {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.getStatus();
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"getStatus: "+e.description);
		}
	},

	save: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.save(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"save: "+e.description);
		}
	},
		
	closeSession: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.closeSession(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"closeSession: "+e.description);
		}
	},
	
	unload: function(systemID, accessKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.unload(systemID, accessKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"unload: "+e.description);
		}
	},	
	
	reset: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.reset(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"reset: "+e.description);
		}
	},		
	
	getSelectedImage: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.getSelectedImage(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"getSelectedImage: "+e.description);
		}
	},	
	
	getSelectedImageEx: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.getSelectedImageEx(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"getSelectedImageEx: "+e.description);
		}
	},	
	
	getPrintImage: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.getPrintImage(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"getPrintImage: "+e.description);
		}
	},		
	
	browseImage: function(systemID, sessionKey, xmlInput, printOnXml, posX, posY, height, width) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.browseImage(systemID, sessionKey, xmlInput, printOnXml, posX, posY, height, width);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"browseImage: "+e.description);
		}
	},	

	registerWindow: function(systemID, sessionKey, windowName) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    var ver = getInternetExplorerVersion();

		    if ( ver >= 7.0 ) 
		        return cidRcWebPluginObj.registerWindow(systemID, sessionKey, windowName + this.getBrowserName().replace("Microsoft", "Windows"));
		    else
		        return cidRcWebPluginObj.registerWindow(systemID, sessionKey, windowName + this.getBrowserName());
		    		alert("Registered");    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"registerWindow: "+e.description);
		}
	},	
	
	showWindow: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.showWindow(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"showWindow: "+e.description);
		}
	},
	
	getBrowserName: function(){
        var nAgt = navigator.userAgent;
        var browserName  = navigator.appName;

        if (nAgt.indexOf("MSIE")!=-1) {
             browserName = " - Microsoft Internet Explorer";
        }
        else if (nAgt.indexOf("Opera")!=-1) {
            browserName = " - Opera";
        }
        else if (nAgt.indexOf("Chrome")!=-1) {
            browserName = " - Google Chrome";
        }
        else if (nAgt.indexOf("Safari")!=-1) {
             browserName = " - Safari";
        }
        else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
            browserName = " - Mozilla Firefox";
        }
        return browserName;
	},
	
	getPrintedImageCount: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.getPrintedImageCount(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"getPrintedImageCount: "+e.description);
		}
	},
	
	getUploadedImageCount: function(systemID, sessionKey) {
		try {
		    if (cidRcWebPluginObj== null){
		        alert("Failed to load CIDRCProxy Object!");
		        return "";
		    }
		    return cidRcWebPluginObj.getUploadedImageCount(systemID, sessionKey);
		    		    
        } catch (e){
			alert(CIDRCWEBPLUGIN_PAGE_NAME+"."+"getUploadedImageCount: "+e.description);
		}
	}
}

