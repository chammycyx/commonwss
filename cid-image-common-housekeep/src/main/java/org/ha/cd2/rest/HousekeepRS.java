package org.ha.cd2.rest;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.ha.cd2.tool.FileCleaner;

@Path("/")
public class HousekeepRS {

	@GET
	@Path("{path : (.+)?}")
	public Response run(@PathParam("path") String path) {
		
		try {
			File directory = new File(path);
			if(!directory.exists() || !path.startsWith("/appl/cid/cfs/") || path.contains("cid_template")) {
				return Response.status(400).entity("Invalid input").build();
			}
			FileCleaner cleaner = new FileCleaner();
			cleaner.clean(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(400).entity("Failed to process").build();
		}
		
		return Response.status(200).entity("Success").build();
	}
}
