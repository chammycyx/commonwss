package org.ha.cd2.tool;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.DirectoryWalker;

@SuppressWarnings("rawtypes")
public class FileCleaner extends DirectoryWalker {
	
	public FileCleaner() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List clean(File startDirectory) throws IOException {
		List results = new ArrayList();
		walk(startDirectory, results);
		
		return results;
	}

	protected boolean handleDirectory(File directory, int depth,
			Collection results) {
		
		if(directory.getAbsolutePath().contains("cid_template")) {
			return false;
		}
		
		return true;
	}

	@SuppressWarnings("unchecked")
	protected void handleFile(File file, int depth, Collection results) {
		// delete file and add to list of deleted
		file.delete();
		results.add(file);
	}
}
